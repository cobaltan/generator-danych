import pandas as pd


def from_excel(file):
    excel = pd.read_excel(file, sheet_name="Arkusz 2", index_col=None)
    animals = []
    for i in range(0, len(excel["Gatunek"])):
        animals.append({
            "name": excel["Gatunek"][i],
            "male": excel["Samce"][i],
            "female": excel["Samice"][i],
            "young": excel["Młode"][i],
            "ill": excel["Chore"][i],
            "exposition_number": excel["Ekspozycja"][i],
            "category": excel["Kategoria"][i],
        })
    return animals


def to_excel(animals, writer):
    name = []
    male = []
    female = []
    young = []
    ill = []
    exposition_number = []
    category = []

    for animal in animals:
        name.append(animal["name"])
        male.append(animal["male"])
        female.append(animal["female"])
        young.append(animal["young"])
        ill.append(animal["ill"])
        exposition_number.append(animal["exposition_number"])
        category.append(animal["category"])

    animals_dataframe = pd.DataFrame(data={
        "Gatunek": name,
        "Samce": male,
        "Samice": female,
        "Młode": young,
        "Chore": ill,
        "Ekspozycja": exposition_number,
        "Kategoria": category,
    })
    animals_dataframe.to_excel(writer, "Arkusz 2", index=False)
