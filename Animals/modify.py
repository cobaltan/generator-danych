import random


def modify(data, chance_to_death=0.1, chance_to_born=0.4, chance_to_ill=0.3, chance_to_female=0.5, chance_to_grow=0.6,
           chance_to_heal=0.3, max_death=2, max_born=4, max_grow=4, max_ill=3, max_heal=3):

    for animal in data:
        if random.random() < chance_to_death:
            number_of_deads = random.randint(1, max_death)
            if random.random() < chance_to_female:
                animal['female'] -= number_of_deads
                if animal['female'] < 0:
                    animal['female'] = 0
            else:
                animal['male'] -= number_of_deads
                if animal['male'] < 0:
                    animal['male'] = 0

        if random.random() < chance_to_grow:
            number_of_grow = random.randint(0, min(max_grow, animal['young']))
            animal['young'] -= number_of_grow
            if random.random() < chance_to_female:
                animal['female'] += number_of_grow
            else:
                animal['male'] += number_of_grow

        if random.random() < chance_to_born:
            number_of_born = random.randint(1, max_born)
            animal['young'] += number_of_born

        if random.random() < chance_to_heal:
            number_of_healed = random.randint(1, max_heal)
            animal['ill'] -= number_of_healed
            if animal['ill'] < 0:
                animal['ill'] = 0

        if random.random() < chance_to_ill:
            number_of_ill = random.randint(1, max_ill)
            animal['ill'] += number_of_ill
