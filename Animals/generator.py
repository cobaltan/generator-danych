import random


def generate(male_max=5, female_max=5, young_max=5, ill_max=3, last_exposition_number=-1):
    file_animals = open("Animals/animals.txt", encoding="utf8")
    file_categories = open("Animals/categories.txt", encoding="utf8")

    animal_name = []
    categories = []

    for line in file_animals:
        if line.count('\n') > 0:
            animal_name.append(line[:-1])
        else:
            animal_name.append(line)

    for line in file_categories:
        if line.count('\n') > 0:
            categories.append(line[:-1])
        else:
            categories.append(line)

    file_animals.close()
    file_categories.close()

    animals = []
    for i in range(0, len(animal_name)):
        name = animal_name[i]
        category = categories[i]
        last_exposition_number += 1
        male = random.randint(0, male_max)
        female = random.randint(0, female_max)
        young = random.randint(0, young_max)
        ill = random.randint(0, ill_max)
        exposition_number = last_exposition_number
        animals.append({
            "name": name,
            "male": male,
            "female": female,
            "young": young,
            "ill": ill,
            "exposition_number": exposition_number,
            "category": category
        })

    return animals
