import decimal
import json
import random
import shutil

from faker import Faker

from Database.utils import generate_tables_file, add_insert, generate_account_number, generate_phone_number, \
    generate_date, is_expired


def generate(filename, start_year, end_year, last_worker_id, number_of_companies=20, number_of_products=20,
             max_orders_per_month=7, max_items_per_order=10, chance_order_exeption = 0.1, chance_is_wrong_quality=0.5, chance_is_wrong_quality_and_broken=0.1,
             max_quality_error=10, max_broken_error=2, chance_is_ordered=0.8, chance_is_delivered=0.9, chance_is_finished=0.9, chance_is_discounted=0.3,
             modify=None):
    fake = Faker('pl_PL')
    file_tables_json = open("Database/data.json", 'r', encoding="utf8")
    json_data = json.loads(file_tables_json.read())
    file_tables_json.close()


    if modify is None:
        generate_tables_file(json_data['tables'], filename)
        file_inserts_sql = open(filename + '-t1-inserts.sql', 'w')
    else:
        shutil.copy(filename + '-t1-inserts.sql', filename + '-t2-inserts.sql')
        file_inserts_sql = open(filename + '-t2-inserts.sql', 'r+')
        file_inserts_sql.read()

    if modify is None:
        for i in range(min(len(json_data['companies']), number_of_companies)):
            add_insert(file_inserts_sql, "Company", [
                {"name": "ID", "value": i},
                {"name": "PhoneNumber", "value": generate_phone_number()},
                {"name": "Address:", "value": fake.address().replace('\n', ', ')},
                {"name": "Name", "value": json_data['companies'][i]['name']},
                {"name": "AccountNumber", "value": generate_account_number()}])

        for i in range(min(len(json_data['products']), number_of_products)):
            if json_data['products'][i]['forAnimal']:
                for_animal = 1
                animal_category = json_data['products'][i]['animalCategory']
            else:
                for_animal = 0
                animal_category = None

            add_insert(file_inserts_sql, "Product", [
                {"name": "ID", "value": i},
                {"name": "UnitPrice", "value": decimal.Decimal(random.randint(json_data['products'][i]['unitPrice'][0],
                                                                              json_data['products'][i]['unitPrice'][
                                                                                  1])) / 100},
                {"name": "CompanyID", "value": random.randint(0, min(len(json_data['companies']), number_of_companies))},
                {"name": "ForAnimal", "value": for_animal},
                {"name": "AnimalCategory", "value": animal_category},
                {"name": "Name", "value": json_data['products'][i]['name']}
            ])

    months = (end_year - start_year) * 12
    last_order_id = -1
    finished_orders = []
    finished_orders_date = []
    for i in range(months):
        orders = random.randint(1, max_orders_per_month)
        for _ in range(orders):

            is_ordered = 0
            if random.random() < chance_is_ordered:
                is_ordered = 1

            if is_ordered == 1:
                is_delivered = 0
                if random.random() < chance_is_delivered:
                    is_delivered = 1
            else:
                is_delivered = 0

            if is_delivered == 1:
                is_finished = 0
                if random.random() < chance_is_finished:
                    is_finished = 1
            else:
                is_finished = 0
            last_order_id += 1

            date = generate_date(start_year, i)

            if is_finished == 1:
                finished_orders.append(last_order_id)
                finished_orders_date.append(date)
            add_insert(file_inserts_sql, "Order", [
                {"name": "ID", "value": last_order_id},
                {"name": "Date", "value": date},
                {"name": "IsOrdered", "value": is_ordered},
                {"name": "IsDelivered", "value": is_delivered},
                {"name": "IsFinished", "value": is_finished},
                {"name": "WorkerID", "value": random.randint(0, last_worker_id)}
            ])
    finished_products_orderid = []
    finished_products_date = []
    finished_products_quantity = []
    finished_products_expirable =[]
    products_quantity = []
    last_orderitem_id = -1
    for i in range(last_order_id):
        items_per_order = random.randint(1, max_items_per_order)
        products = []
        for _ in range(min(items_per_order, len(json_data['products']))):
            product = random.randint(0, len(json_data['products'])-1)
            while product in products:
                product = random.randint(0, len(json_data['products'])-1)
            products.append(product)

            quantity = decimal.Decimal(random.randint(json_data['products'][product]['quantityRange'][0],
                                                      json_data['products'][product]['quantityRange'][1]))
            if not json_data['products'][product]['countable']:
                quantity /= 10000

            is_discounted = 0
            if random.random() < chance_is_discounted:
                is_discounted = 1

            if is_discounted == 0:
                discounted_price = None
            else:
                discount = decimal.Decimal(100 - random.randint(10, 40))/100
                discounted_price = decimal.Decimal(json_data['products'][product]['unitPrice'][0])/100*discount

            last_orderitem_id += 1
            products_quantity.append(quantity)
            if i in finished_orders:
                finished_products_orderid.append(last_orderitem_id)
                finished_products_date.append(finished_orders_date[finished_orders.index(i)])
                finished_products_quantity.append(quantity)
                finished_products_expirable.append(json_data['products'][product]['expiriable'])
            add_insert(file_inserts_sql, "OrderItem", [
                {"name": "ID", "value": last_orderitem_id},
                {"name": "Quantity", "value": quantity},
                {"name": "OrderID", "value": i},
                {"name": "ProductID", "value": product},
                {"name": "IsDiscounted", "value": is_discounted},
                {"name": "DiscountedUnitPrice", "value": discounted_price}
            ])

    last_order_exeption_id = -1
    for i in range(last_orderitem_id):
        if random.random() < chance_order_exeption:
            last_order_exeption_id += 1
            if random.random() < chance_is_wrong_quality:
                is_wrong_quality = 1
                is_broken = 0
            elif random.random() < chance_is_wrong_quality_and_broken:
                is_wrong_quality = 1
                is_broken = 1
            else:
                is_wrong_quality = 0
                is_broken = 1

            product_quantity = products_quantity[i]

            quantity_error = None
            if is_wrong_quality == 1:
                quantity_error = random.randint(1, min(int(product_quantity), max_quality_error))
                if i in finished_products_orderid:
                    finished_products_quantity[finished_products_orderid.index(i)] -= quantity_error

            quantity_of_broken = None
            if is_broken == 1:
                quantity_of_broken = random.randint(1, min(int(product_quantity), max_broken_error))
                if i in finished_products_orderid:
                    finished_products_quantity[finished_products_orderid.index(i)] -= quantity_of_broken

            add_insert(file_inserts_sql, "OrderItemException", [
                {"name": "ID", "value": last_order_exeption_id},
                {"name": "IsWrongQuantity", "value": is_wrong_quality},
                {"name": "QuantityError", "value": quantity_error},
                {"name": "IsProductBroken", "value": is_broken},
                {"name": "QuantityOfBrokenProducts", "value": quantity_of_broken},
                {"name": "OrderItemID", "value": i}
            ])
            
    for i in range(len(finished_products_orderid)):
        is_product_expired = 0
        expired_quantity = None
        expire_date = None
        if finished_products_expirable[i]:
            expire_date = list(finished_products_date[i])
            expire_date[3] = str(int(expire_date[3])+1)
            expire_date = "".join(expire_date)
            is_product_expired = is_expired(expire_date, end_year)

        has_experience = 0
        if finished_products_expirable[i]:
            has_experience = 1

        add_insert(file_inserts_sql, "ProductInStorage", [
            {"name": "ID", "value": i},
            {"name": "Quantity", "value": finished_products_quantity[i]},
            {"name": "HasExpireDate", "value": has_experience},
            {"name": "ExpireDate", "value": expire_date},
            {"name": "InStorageDate", "value": finished_products_date[i]},
            {"name": "IsExpired", "value": is_product_expired},
            {"name": "ExpiredQuantity", "value": expired_quantity},
            {"name": "OrderItemID", "value": finished_products_orderid[i]}
        ])

    file_inserts_sql.close()
