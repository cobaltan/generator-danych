import random


def generate_tables_file(tables, filename):
    file_tables_sql = open(filename + '-tables.sql', 'w')
    for table in tables:
        file_tables_sql.write("CREATE TABLE " + table['name'] + " {\n")
        for field in table['fields']:
            file_tables_sql.write('"' + field['name'] + '" ' + field['type'] + ",\n")
        file_tables_sql.write("}\n\n")
    file_tables_sql.close()


def generate_phone_number():
    return random.randint(100000000, 999999999)


def generate_account_number():
    return random.randint(10000000000000000000000000, 99999999999999999999999999)


def add_insert(file, column_name, data):
    file.write("INSERT INTO " + column_name + " (")
    for i in range(len(data)):
        if i != 0:
            file.write(", ")
        file.write('"' + data[i]['name'] + '"')
    file.write(")\nVALUES (")
    for i in range(len(data)):
        if i != 0:
            file.write(", ")
        if data[i]['value'] == None:
            file.write("NULL")
        elif isinstance(data[i]['value'], str):
            file.write('"' + data[i]['value'] + '"')
        else:
            file.write(str(data[i]['value']))
    file.write(")\n\n")


def generate_date(start_year, month_count):
    year = start_year + int(month_count/12)
    month = month_count % 12
    if month == 2:
        day = random.randint(1, 29)
    elif month % 2 == 0:
        day = random.randint(1, 30)
    else:
        day = random.randint(1, 31)

    if month < 10:
        month_str = "0" + str(month)
    else:
        month_str = str(month)
    if day < 10:
        day_str = "0" + str(day)
    else:
        day_str = str(day)

    return str(year) + "-" + month_str + "-" + day_str


def is_expired(expire_date, now_year):
    expire_year = int(expire_date[0:4])
    if expire_year > now_year:
        return 0
    else:
        return 1
