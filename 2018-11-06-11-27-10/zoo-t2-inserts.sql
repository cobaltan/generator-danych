INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (0, 237978342, "ulica Per�owa 574, 64-728 Bia�a Podlaska", "Krajewski-Olszewski", 82728544089500469009053366)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (1, 353099149, "pl. Weso�a 542, 76-837 Radomsko", "Nowak", 37455667951999105283417671)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (2, 777103347, "al. Lawendowa 56/92, 23-296 Ostrowiec �wi�tokrzyski", "Kaczmarek", 97982057455040547826589799)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (3, 675524338, "al. Orla 08, 70-580 Ko�obrzeg", "Wr�bel", 42845112877761143592535665)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (4, 914078524, "ul. Na�kowskiej 61, 77-268 Wielu�", "Szyma�ski", 20881537037769895916959938)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (5, 236801077, "ul. Rumiankowa 909, 95-970 Marki", "Wilk sp. k.", 67435892314796044279850794)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (6, 598281442, "pl. Wi�niowa 82/26, 40-849 Katowice", "Sikorska sp. p.", 61553920984384984456422158)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (7, 876666132, "pl. Ko�cielna 32, 51-788 Czechowice-Dziedzice", "Nowakowska-Baranowska", 63235538089017005499047802)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (8, 845073947, "ulica Lazurowa 97/61, 43-032 K�dzierzyn-Ko�le", "Sikora", 77150471782651710296359252)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (9, 151105619, "al. Boczna 28/35, 11-602 Mi�sk Mazowiecki", "G�owacki sp. z o.o.", 81875083339339247950558520)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (10, 949649461, "pl. B��kitna 57, 42-610 Lubliniec", "Grupa Michalska", 42733257164048126900014096)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (11, 583745720, "ulica Popie�uszki 183, 13-922 Grodzisk Mazowiecki", "Olszewska s. c.", 36666692161047919940120896)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (12, 558544079, "aleja Dobra 01, 68-976 Lubin", "Fundacja G�rska", 98213177929975993025031205)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (13, 243043687, "ulica Nowa 93/85, 74-186 Lubin", "Wilk-Brzezi�ski", 99676370251986032387671266)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (14, 497762307, "ul. Wsp�lna 71/99, 50-726 Siemianowice �l�skie", "Duda sp. j.", 22597747678328884671514868)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (15, 521423882, "pl. Bursztynowa 682, 46-293 W�growiec", "Sp�dzielnia W�odarczyk", 25384140662991466452093430)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (16, 408001184, "pl. Maczka 59, 69-718 Skierniewice", "Jaworski-Mr�z", 39454470489675238421576756)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (17, 355029095, "aleja G�owackiego 38, 00-199 Starogard Gda�ski", "Laskowska-Wojciechowska", 12773490841170171137090182)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (18, 762256457, "ulica Lipca 11, 28-469 Malbork", "Wysocka sp. z o.o.", 69917142144660960741588538)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (19, 793965914, "ul. Pa�acowa 878, 71-280 M�awa", "Mr�z P.P.O.F", 61793260119920027595557930)

INSERT INTO Product ("ID", "UnitPrice", "CompanyID", "ForAnimal", "AnimalCategory", "Name")
VALUES (0, 11.06, 19, 1, 1, "Pasza dla konia")

INSERT INTO Product ("ID", "UnitPrice", "CompanyID", "ForAnimal", "AnimalCategory", "Name")
VALUES (1, 148.7, 7, 0, NULL, "�opata")

INSERT INTO Product ("ID", "UnitPrice", "CompanyID", "ForAnimal", "AnimalCategory", "Name")
VALUES (2, 16.23, 10, 1, 1, "�ci�ka dla wiewi�rki")

INSERT INTO Product ("ID", "UnitPrice", "CompanyID", "ForAnimal", "AnimalCategory", "Name")
VALUES (3, 377.83, 11, 0, NULL, "R�kawiczki jednorazowe")

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (0, "2001-00-19", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (1, "2001-00-27", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (2, "2001-00-18", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (3, "2001-00-06", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (4, "2001-00-19", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (5, "2001-01-19", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (6, "2001-01-20", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (7, "2001-01-03", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (8, "2001-01-02", 1, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (9, "2001-01-29", 1, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (10, "2001-01-20", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (11, "2001-01-24", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (12, "2001-02-29", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (13, "2001-02-09", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (14, "2001-02-18", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (15, "2001-02-05", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (16, "2001-03-01", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (17, "2001-04-11", 1, 1, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (18, "2001-04-04", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (19, "2001-04-17", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (20, "2001-05-30", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (21, "2001-05-13", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (22, "2001-05-04", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (23, "2001-05-30", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (24, "2001-06-26", 1, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (25, "2001-06-23", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (26, "2001-06-29", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (27, "2001-06-01", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (28, "2001-06-14", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (29, "2001-06-28", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (30, "2001-06-16", 1, 1, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (31, "2001-07-31", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (32, "2001-07-09", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (33, "2001-07-27", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (34, "2001-08-12", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (35, "2001-08-01", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (36, "2001-08-12", 1, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (37, "2001-08-05", 1, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (38, "2001-09-26", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (39, "2001-09-05", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (40, "2001-09-22", 1, 1, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (41, "2001-09-11", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (42, "2001-09-31", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (43, "2001-09-11", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (44, "2001-10-05", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (45, "2001-10-01", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (46, "2001-10-10", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (47, "2001-10-18", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (48, "2001-10-10", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (49, "2001-10-09", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (50, "2001-11-04", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (51, "2001-11-05", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (52, "2001-11-18", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (53, "2001-11-18", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (54, "2001-11-04", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (55, "2001-11-07", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (56, "2001-11-23", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (57, "2002-00-09", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (58, "2002-00-07", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (59, "2002-00-18", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (60, "2002-00-27", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (61, "2002-01-03", 1, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (62, "2002-01-10", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (63, "2002-01-12", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (64, "2002-02-29", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (65, "2002-02-02", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (66, "2002-02-24", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (67, "2002-02-26", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (68, "2002-02-12", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (69, "2002-03-26", 1, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (70, "2002-03-21", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (71, "2002-04-22", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (72, "2002-04-12", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (73, "2002-04-12", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (74, "2002-04-26", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (75, "2002-05-21", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (76, "2002-05-28", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (77, "2002-05-17", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (78, "2002-06-23", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (79, "2002-06-27", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (80, "2002-06-01", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (81, "2002-06-30", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (82, "2002-06-14", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (83, "2002-06-11", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (84, "2002-07-22", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (85, "2002-07-31", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (86, "2002-07-12", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (87, "2002-08-27", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (88, "2002-08-14", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (89, "2002-08-04", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (90, "2002-08-05", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (91, "2002-08-02", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (92, "2002-08-29", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (93, "2002-08-26", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (94, "2002-09-14", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (95, "2002-09-10", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (96, "2002-09-27", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (97, "2002-09-03", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (98, "2002-09-06", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (99, "2002-09-12", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (100, "2002-09-22", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (101, "2002-10-01", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (102, "2002-11-10", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (103, "2002-11-09", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (104, "2002-11-22", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (105, "2002-11-23", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (106, "2003-00-12", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (107, "2003-00-08", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (108, "2003-00-17", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (109, "2003-01-01", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (110, "2003-01-08", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (111, "2003-01-26", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (112, "2003-02-28", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (113, "2003-02-17", 1, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (114, "2003-02-03", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (115, "2003-02-29", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (116, "2003-02-12", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (117, "2003-03-18", 1, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (118, "2003-03-06", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (119, "2003-04-07", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (120, "2003-04-17", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (121, "2003-04-27", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (122, "2003-04-01", 1, 1, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (123, "2003-04-17", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (124, "2003-04-29", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (125, "2003-04-23", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (126, "2003-05-30", 1, 1, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (127, "2003-05-29", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (128, "2003-05-09", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (129, "2003-05-15", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (130, "2003-06-14", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (131, "2003-06-09", 1, 1, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (132, "2003-06-22", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (133, "2003-06-12", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (134, "2003-06-18", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (135, "2003-06-09", 1, 1, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (136, "2003-06-28", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (137, "2003-07-04", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (138, "2003-07-18", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (139, "2003-08-15", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (140, "2003-08-28", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (141, "2003-08-17", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (142, "2003-09-15", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (143, "2003-09-12", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (144, "2003-09-28", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (145, "2003-09-25", 1, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (146, "2003-09-03", 1, 1, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (147, "2003-09-25", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (148, "2003-10-06", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (149, "2003-10-16", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (150, "2003-10-17", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (151, "2003-11-18", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (152, "2003-11-03", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (153, "2003-11-01", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (154, "2003-11-30", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (155, "2004-00-16", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (156, "2004-00-20", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (157, "2004-00-12", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (158, "2004-00-17", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (159, "2004-00-15", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (160, "2004-01-01", 1, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (161, "2004-02-15", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (162, "2004-02-10", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (163, "2004-02-01", 1, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (164, "2004-02-27", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (165, "2004-02-19", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (166, "2004-02-07", 1, 1, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (167, "2004-02-08", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (168, "2004-03-29", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (169, "2004-03-16", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (170, "2004-03-22", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (171, "2004-03-12", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (172, "2004-03-25", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (173, "2004-03-31", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (174, "2004-04-12", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (175, "2004-05-22", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (176, "2004-06-21", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (177, "2004-06-02", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (178, "2004-06-08", 1, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (179, "2004-06-01", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (180, "2004-07-03", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (181, "2004-07-05", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (182, "2004-07-31", 1, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (183, "2004-07-11", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (184, "2004-08-24", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (185, "2004-08-01", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (186, "2004-09-19", 1, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (187, "2004-09-19", 1, 1, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (188, "2004-09-05", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (189, "2004-10-11", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (190, "2004-10-29", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (191, "2004-11-07", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (192, "2004-11-22", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (193, "2005-00-08", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (194, "2005-00-03", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (195, "2005-00-09", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (196, "2005-01-28", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (197, "2005-01-21", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (198, "2005-02-06", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (199, "2005-03-31", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (200, "2005-04-08", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (201, "2005-04-04", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (202, "2005-04-19", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (203, "2005-04-17", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (204, "2005-04-03", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (205, "2005-04-10", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (206, "2005-05-17", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (207, "2005-05-19", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (208, "2005-05-29", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (209, "2005-05-13", 1, 1, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (210, "2005-06-25", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (211, "2005-06-12", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (212, "2005-07-11", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (213, "2005-07-15", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (214, "2005-07-14", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (215, "2005-07-05", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (216, "2005-07-22", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (217, "2005-07-23", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (218, "2005-07-16", 1, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (219, "2005-08-04", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (220, "2005-09-31", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (221, "2005-09-05", 1, 1, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (222, "2005-10-24", 1, 1, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (223, "2005-11-04", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (224, "2005-11-07", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (225, "2005-11-09", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (226, "2005-11-18", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (227, "2005-11-24", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (228, "2006-00-13", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (229, "2006-00-14", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (230, "2006-00-15", 1, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (231, "2006-00-21", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (232, "2006-00-22", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (233, "2006-00-14", 1, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (234, "2006-01-04", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (235, "2006-01-23", 1, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (236, "2006-01-10", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (237, "2006-01-27", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (238, "2006-01-24", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (239, "2006-01-13", 1, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (240, "2006-02-21", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (241, "2006-02-27", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (242, "2006-03-30", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (243, "2006-03-26", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (244, "2006-04-02", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (245, "2006-04-01", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (246, "2006-04-06", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (247, "2006-04-10", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (248, "2006-04-14", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (249, "2006-05-14", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (250, "2006-05-25", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (251, "2006-05-09", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (252, "2006-05-27", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (253, "2006-05-19", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (254, "2006-05-21", 1, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (255, "2006-06-12", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (256, "2006-06-03", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (257, "2006-06-26", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (258, "2006-07-03", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (259, "2006-07-23", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (260, "2006-07-11", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (261, "2006-07-23", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (262, "2006-08-23", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (263, "2006-08-04", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (264, "2006-08-13", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (265, "2006-08-02", 1, 1, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (266, "2006-08-12", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (267, "2006-08-13", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (268, "2006-08-30", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (269, "2006-09-28", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (270, "2006-09-25", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (271, "2006-09-25", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (272, "2006-09-18", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (273, "2006-10-20", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (274, "2006-10-22", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (275, "2006-11-13", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (276, "2006-11-03", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (277, "2007-00-26", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (278, "2007-00-16", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (279, "2007-00-08", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (280, "2007-00-15", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (281, "2007-00-03", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (282, "2007-00-23", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (283, "2007-00-02", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (284, "2007-01-10", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (285, "2007-01-06", 1, 1, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (286, "2007-01-04", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (287, "2007-02-20", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (288, "2007-02-22", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (289, "2007-02-05", 1, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (290, "2007-02-05", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (291, "2007-02-07", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (292, "2007-02-14", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (293, "2007-02-03", 1, 1, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (294, "2007-03-12", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (295, "2007-03-08", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (296, "2007-04-21", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (297, "2007-04-12", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (298, "2007-04-20", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (299, "2007-05-31", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (300, "2007-05-22", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (301, "2007-05-21", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (302, "2007-05-14", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (303, "2007-06-25", 1, 1, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (304, "2007-07-06", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (305, "2007-07-01", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (306, "2007-07-17", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (307, "2007-08-30", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (308, "2007-08-17", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (309, "2007-08-19", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (310, "2007-08-05", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (311, "2007-08-24", 1, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (312, "2007-08-19", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (313, "2007-08-07", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (314, "2007-09-27", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (315, "2007-09-30", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (316, "2007-10-23", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (317, "2007-10-21", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (318, "2007-11-26", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (319, "2007-11-21", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (320, "2007-11-07", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (321, "2008-00-27", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (322, "2008-01-23", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (323, "2008-01-01", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (324, "2008-01-08", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (325, "2008-01-03", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (326, "2008-01-16", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (327, "2008-01-13", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (328, "2008-01-25", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (329, "2008-02-15", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (330, "2008-02-23", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (331, "2008-02-08", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (332, "2008-02-20", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (333, "2008-03-22", 1, 1, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (334, "2008-03-02", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (335, "2008-03-17", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (336, "2008-03-20", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (337, "2008-03-06", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (338, "2008-03-10", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (339, "2008-03-02", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (340, "2008-04-23", 1, 1, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (341, "2008-04-21", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (342, "2008-05-31", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (343, "2008-05-16", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (344, "2008-05-05", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (345, "2008-05-14", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (346, "2008-05-22", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (347, "2008-05-06", 1, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (348, "2008-05-06", 1, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (349, "2008-06-16", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (350, "2008-06-20", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (351, "2008-06-25", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (352, "2008-06-26", 1, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (353, "2008-07-07", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (354, "2008-07-20", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (355, "2008-08-13", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (356, "2008-08-29", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (357, "2008-08-17", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (358, "2008-08-29", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (359, "2008-08-03", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (360, "2008-08-27", 1, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (361, "2008-09-14", 1, 1, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (362, "2008-09-24", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (363, "2008-09-22", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (364, "2008-09-09", 1, 1, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (365, "2008-09-20", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (366, "2008-09-23", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (367, "2008-09-28", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (368, "2008-10-26", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (369, "2008-10-10", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (370, "2008-10-28", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (371, "2008-10-09", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (372, "2008-11-15", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (373, "2008-11-28", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (374, "2009-00-29", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (375, "2009-00-05", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (376, "2009-00-21", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (377, "2009-00-24", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (378, "2009-00-28", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (379, "2009-00-21", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (380, "2009-00-06", 1, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (381, "2009-01-02", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (382, "2009-01-21", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (383, "2009-01-19", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (384, "2009-01-24", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (385, "2009-02-11", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (386, "2009-02-16", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (387, "2009-02-24", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (388, "2009-02-01", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (389, "2009-02-01", 1, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (390, "2009-02-13", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (391, "2009-02-06", 1, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (392, "2009-03-09", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (393, "2009-03-14", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (394, "2009-03-15", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (395, "2009-03-07", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (396, "2009-03-01", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (397, "2009-03-26", 1, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (398, "2009-03-27", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (399, "2009-04-25", 1, 1, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (400, "2009-04-12", 1, 1, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (401, "2009-04-27", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (402, "2009-04-29", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (403, "2009-04-21", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (404, "2009-04-03", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (405, "2009-04-08", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (406, "2009-05-24", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (407, "2009-05-17", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (408, "2009-05-20", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (409, "2009-05-07", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (410, "2009-06-20", 1, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (411, "2009-06-14", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (412, "2009-07-10", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (413, "2009-07-30", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (414, "2009-07-09", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (415, "2009-07-26", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (416, "2009-07-26", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (417, "2009-07-04", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (418, "2009-07-31", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (419, "2009-08-01", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (420, "2009-08-28", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (421, "2009-09-03", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (422, "2009-09-27", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (423, "2009-09-10", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (424, "2009-09-04", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (425, "2009-09-01", 1, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (426, "2009-09-28", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (427, "2009-09-25", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (428, "2009-10-20", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (429, "2009-10-19", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (430, "2009-10-19", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (431, "2009-10-21", 1, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (432, "2009-10-09", 1, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (433, "2009-10-12", 1, 1, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (434, "2009-10-10", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (435, "2009-11-06", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (436, "2009-11-01", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (437, "2009-11-11", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (438, "2009-11-10", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (439, "2009-11-15", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (440, "2010-00-28", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (441, "2010-00-18", 1, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (442, "2010-00-27", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (443, "2010-01-30", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (444, "2010-01-14", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (445, "2010-01-30", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (446, "2010-02-24", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (447, "2010-02-18", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (448, "2010-03-19", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (449, "2010-04-17", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (450, "2010-04-27", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (451, "2010-04-11", 1, 1, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (452, "2010-04-04", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (453, "2010-04-08", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (454, "2010-04-29", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (455, "2010-05-16", 1, 1, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (456, "2010-05-20", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (457, "2010-05-06", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (458, "2010-05-19", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (459, "2010-05-13", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (460, "2010-05-23", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (461, "2010-05-05", 1, 1, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (462, "2010-06-01", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (463, "2010-06-14", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (464, "2010-06-04", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (465, "2010-06-26", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (466, "2010-07-30", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (467, "2010-07-05", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (468, "2010-07-28", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (469, "2010-07-03", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (470, "2010-08-24", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (471, "2010-08-24", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (472, "2010-08-07", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (473, "2010-08-14", 1, 1, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (474, "2010-09-19", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (475, "2010-09-23", 1, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (476, "2010-09-15", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (477, "2010-09-24", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (478, "2010-10-16", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (479, "2010-10-06", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (480, "2010-10-05", 1, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (481, "2010-10-18", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (482, "2010-11-18", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (483, "2010-11-21", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (484, "2011-00-27", 1, 1, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (485, "2011-00-30", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (486, "2011-00-15", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (487, "2011-00-26", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (488, "2011-00-03", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (489, "2011-00-20", 1, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (490, "2011-01-09", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (491, "2011-01-24", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (492, "2011-01-01", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (493, "2011-01-11", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (494, "2011-02-09", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (495, "2011-02-26", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (496, "2011-02-07", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (497, "2011-02-03", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (498, "2011-03-27", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (499, "2011-03-19", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (500, "2011-03-11", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (501, "2011-03-04", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (502, "2011-03-02", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (503, "2011-03-21", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (504, "2011-04-10", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (505, "2011-04-01", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (506, "2011-04-30", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (507, "2011-04-18", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (508, "2011-04-10", 1, 1, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (509, "2011-04-03", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (510, "2011-05-30", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (511, "2011-05-10", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (512, "2011-06-30", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (513, "2011-06-29", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (514, "2011-06-30", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (515, "2011-06-11", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (516, "2011-06-01", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (517, "2011-06-09", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (518, "2011-06-16", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (519, "2011-07-17", 1, 1, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (520, "2011-07-29", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (521, "2011-07-31", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (522, "2011-07-28", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (523, "2011-08-04", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (524, "2011-08-04", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (525, "2011-09-13", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (526, "2011-09-07", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (527, "2011-10-23", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (528, "2011-10-16", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (529, "2011-10-14", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (530, "2011-11-03", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (531, "2011-11-29", 1, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (532, "2011-11-26", 1, 1, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (533, "2011-11-11", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (534, "2012-00-13", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (535, "2012-00-08", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (536, "2012-01-06", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (537, "2012-01-29", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (538, "2012-01-01", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (539, "2012-01-02", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (540, "2012-01-01", 1, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (541, "2012-01-02", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (542, "2012-01-10", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (543, "2012-02-09", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (544, "2012-03-23", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (545, "2012-03-04", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (546, "2012-03-31", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (547, "2012-03-29", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (548, "2012-03-22", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (549, "2012-04-15", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (550, "2012-05-18", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (551, "2012-05-29", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (552, "2012-05-30", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (553, "2012-06-30", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (554, "2012-06-11", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (555, "2012-06-03", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (556, "2012-06-04", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (557, "2012-07-21", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (558, "2012-07-08", 1, 1, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (559, "2012-07-13", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (560, "2012-07-25", 1, 1, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (561, "2012-07-06", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (562, "2012-07-21", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (563, "2012-08-15", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (564, "2012-08-28", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (565, "2012-09-10", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (566, "2012-09-31", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (567, "2012-09-26", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (568, "2012-09-30", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (569, "2012-09-06", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (570, "2012-10-04", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (571, "2012-10-19", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (572, "2012-10-19", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (573, "2012-10-11", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (574, "2012-10-18", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (575, "2012-10-19", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (576, "2012-11-25", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (577, "2012-11-10", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (578, "2012-11-03", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (579, "2012-11-11", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (580, "2012-11-16", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (581, "2012-11-22", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (582, "2012-11-06", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (583, "2013-00-05", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (584, "2013-00-07", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (585, "2013-00-13", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (586, "2013-00-17", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (587, "2013-01-30", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (588, "2013-01-10", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (589, "2013-01-12", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (590, "2013-01-14", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (591, "2013-02-10", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (592, "2013-02-18", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (593, "2013-02-10", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (594, "2013-02-05", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (595, "2013-03-20", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (596, "2013-03-09", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (597, "2013-03-17", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (598, "2013-03-04", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (599, "2013-03-03", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (600, "2013-04-08", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (601, "2013-04-10", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (602, "2013-04-17", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (603, "2013-05-31", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (604, "2013-05-22", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (605, "2013-05-12", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (606, "2013-05-16", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (607, "2013-05-15", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (608, "2013-06-17", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (609, "2013-06-23", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (610, "2013-06-23", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (611, "2013-06-13", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (612, "2013-06-18", 1, 1, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (613, "2013-06-13", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (614, "2013-07-20", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (615, "2013-07-10", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (616, "2013-07-15", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (617, "2013-07-27", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (618, "2013-08-19", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (619, "2013-08-10", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (620, "2013-08-01", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (621, "2013-08-07", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (622, "2013-08-18", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (623, "2013-08-28", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (624, "2013-08-10", 1, 1, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (625, "2013-09-04", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (626, "2013-10-02", 1, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (627, "2013-10-24", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (628, "2013-10-25", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (629, "2013-10-27", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (630, "2013-10-13", 1, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (631, "2013-10-22", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (632, "2013-11-10", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (633, "2013-11-09", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (634, "2014-00-05", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (635, "2014-01-20", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (636, "2014-01-21", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (637, "2014-01-27", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (638, "2014-01-23", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (639, "2014-01-21", 1, 1, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (640, "2014-01-16", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (641, "2014-01-31", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (642, "2014-02-22", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (643, "2014-03-27", 1, 1, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (644, "2014-03-20", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (645, "2014-04-01", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (646, "2014-04-04", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (647, "2014-04-30", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (648, "2014-04-05", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (649, "2014-05-13", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (650, "2014-05-11", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (651, "2014-06-25", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (652, "2014-06-09", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (653, "2014-07-14", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (654, "2014-07-15", 1, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (655, "2014-07-29", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (656, "2014-07-14", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (657, "2014-07-02", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (658, "2014-07-14", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (659, "2014-08-27", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (660, "2014-08-14", 1, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (661, "2014-08-01", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (662, "2014-08-08", 1, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (663, "2014-08-26", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (664, "2014-09-09", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (665, "2014-09-03", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (666, "2014-09-17", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (667, "2014-09-21", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (668, "2014-10-07", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (669, "2014-11-28", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (670, "2014-11-11", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (671, "2014-11-04", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (672, "2015-00-05", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (673, "2015-00-30", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (674, "2015-00-13", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (675, "2015-00-24", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (676, "2015-00-23", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (677, "2015-00-10", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (678, "2015-01-15", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (679, "2015-01-23", 1, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (680, "2015-02-20", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (681, "2015-02-24", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (682, "2015-02-15", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (683, "2015-03-26", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (684, "2015-03-23", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (685, "2015-03-11", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (686, "2015-03-16", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (687, "2015-04-23", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (688, "2015-04-18", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (689, "2015-05-13", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (690, "2015-05-04", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (691, "2015-05-15", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (692, "2015-05-25", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (693, "2015-06-10", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (694, "2015-06-17", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (695, "2015-06-19", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (696, "2015-06-08", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (697, "2015-06-25", 1, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (698, "2015-06-22", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (699, "2015-06-16", 1, 1, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (700, "2015-07-15", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (701, "2015-07-18", 1, 1, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (702, "2015-07-04", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (703, "2015-08-11", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (704, "2015-08-18", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (705, "2015-09-03", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (706, "2015-09-28", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (707, "2015-10-27", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (708, "2015-10-08", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (709, "2015-10-06", 1, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (710, "2015-10-20", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (711, "2015-11-23", 1, 1, 1, 7)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (0, 1, 0, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1, 25.1418, 0, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2, 21.3234, 1, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (3, 10.1142, 1, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (4, 428, 1, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (5, 4, 1, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (6, 27.4453, 2, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (7, 1, 2, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (8, 377, 2, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (9, 10.6435, 2, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (10, 1, 3, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (11, 419, 3, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (12, 27.5565, 3, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (13, 15.006, 3, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (14, 10.847, 4, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (15, 2, 4, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (16, 20.8634, 4, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (17, 279, 4, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (18, 24.9096, 5, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (19, 1, 6, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (20, 5, 7, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (21, 400, 7, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (22, 17.4416, 7, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (23, 482, 8, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (24, 4, 8, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (25, 27.259, 8, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (26, 19.3344, 8, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (27, 387, 9, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (28, 21.2874, 9, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (29, 4, 9, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (30, 16.4253, 9, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (31, 4, 10, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (32, 26.0064, 10, 2, 1, 12.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (33, 20.4397, 10, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (34, 388, 10, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (35, 20.6389, 11, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (36, 28.8775, 11, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (37, 5, 11, 1, 1, 73.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (38, 364, 12, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (39, 26.3408, 12, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (40, 327, 13, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (41, 20.7868, 13, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (42, 2, 13, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (43, 21.2338, 13, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (44, 20.4899, 14, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (45, 3, 14, 1, 1, 76.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (46, 22.0987, 14, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (47, 425, 14, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (48, 23.0845, 15, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (49, 10.4335, 15, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (50, 361, 15, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (51, 5, 15, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (52, 471, 16, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (53, 23.7646, 16, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (54, 4, 16, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (55, 20.3759, 16, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (56, 3, 17, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (57, 27.3428, 17, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (58, 414, 18, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (59, 2, 18, 1, 1, 65.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (60, 19.7571, 18, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (61, 22.1543, 18, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (62, 19.5109, 19, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (63, 406, 19, 3, 1, 192.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (64, 464, 20, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (65, 1, 20, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (66, 24.882, 21, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (67, 351, 21, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (68, 18.5363, 21, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (69, 4, 21, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (70, 4, 22, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (71, 23.526, 22, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (72, 369, 22, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (73, 24.1179, 22, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (74, 3, 23, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (75, 312, 23, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (76, 3, 24, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (77, 29.073, 24, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (78, 12.0591, 24, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (79, 437, 24, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (80, 22.5032, 25, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (81, 20.1983, 25, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (82, 431, 25, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (83, 4, 25, 1, 1, 83.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (84, 27.4622, 26, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (85, 22.7459, 26, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (86, 4, 26, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (87, 485, 26, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (88, 401, 27, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (89, 1, 27, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (90, 19.882, 27, 0, 1, 8.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (91, 20.047, 27, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (92, 1, 28, 1, 1, 76.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (93, 11.7085, 28, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (94, 293, 28, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (95, 22.8875, 28, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (96, 13.8605, 29, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (97, 26.7381, 29, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (98, 1, 29, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (99, 387, 29, 3, 1, 225.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (100, 28.9973, 30, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (101, 4, 30, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (102, 2, 31, 1, 1, 87.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (103, 26.902, 32, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (104, 1, 32, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (105, 269, 32, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (106, 12.0826, 32, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (107, 21.6339, 33, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (108, 17.6932, 33, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (109, 4, 33, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (110, 419, 33, 3, 1, 217.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (111, 25.5712, 34, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (112, 276, 34, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (113, 15.8725, 34, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (114, 4, 34, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (115, 312, 35, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (116, 5, 35, 1, 1, 83.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (117, 25.225, 35, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (118, 13.7339, 35, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (119, 21.6916, 36, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (120, 24.2367, 36, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (121, 365, 36, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (122, 1, 36, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (123, 448, 37, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (124, 20.0363, 37, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (125, 18.9462, 37, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (126, 2, 37, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (127, 17.9653, 38, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (128, 4, 38, 1, 1, 88.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (129, 24.9384, 38, 2, 1, 12.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (130, 250, 38, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (131, 3, 39, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (132, 459, 39, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (133, 16.3413, 39, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (134, 28.5957, 39, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (135, 20.5914, 40, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (136, 486, 40, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (137, 2, 40, 1, 1, 83.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (138, 20.7177, 40, 2, 1, 9.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (139, 23.8043, 41, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (140, 12.2034, 41, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (141, 3, 41, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (142, 296, 41, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (143, 5, 42, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (144, 23.2886, 42, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (145, 1, 43, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (146, 17.612, 43, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (147, 415, 43, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (148, 28.4583, 43, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (149, 25.3655, 44, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (150, 5, 44, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (151, 276, 44, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (152, 16.189, 44, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (153, 4, 45, 1, 1, 71.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (154, 24.1658, 45, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (155, 336, 45, 3, 1, 225.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (156, 449, 46, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (157, 26.2361, 46, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (158, 3, 46, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (159, 23.1798, 46, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (160, 22.5619, 47, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (161, 21.8556, 48, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (162, 4, 48, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (163, 482, 48, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (164, 26.5065, 49, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (165, 498, 49, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (166, 5, 49, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (167, 16.2997, 49, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (168, 1, 50, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (169, 18.7342, 50, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (170, 24.3844, 50, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (171, 277, 50, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (172, 2, 51, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (173, 22.7074, 51, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (174, 461, 51, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (175, 21.6723, 51, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (176, 14.3675, 52, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (177, 5, 53, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (178, 15.1276, 53, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (179, 20.9025, 53, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (180, 370, 53, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (181, 22.0268, 54, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (182, 23.737, 54, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (183, 344, 55, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (184, 3, 55, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (185, 10.079, 55, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (186, 24.447, 55, 2, 1, 13.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (187, 16.8931, 56, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (188, 4, 56, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (189, 390, 56, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (190, 24.5657, 56, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (191, 346, 57, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (192, 12.0095, 57, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (193, 21.9345, 57, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (194, 5, 57, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (195, 5, 58, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (196, 25.187, 58, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (197, 414, 58, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (198, 21.8978, 58, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (199, 12.9582, 59, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (200, 27.8015, 59, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (201, 434, 59, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (202, 4, 59, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (203, 4, 60, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (204, 11.8917, 60, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (205, 400, 60, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (206, 23.3763, 60, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (207, 17.9457, 61, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (208, 464, 61, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (209, 5, 61, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (210, 21.0143, 61, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (211, 1, 62, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (212, 254, 62, 3, 1, 182.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (213, 15.2306, 62, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (214, 27.0458, 62, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (215, 26.6145, 63, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (216, 15.7846, 64, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (217, 1, 64, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (218, 261, 64, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (219, 27.6631, 64, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (220, 21.4546, 65, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (221, 4, 65, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (222, 20.3951, 66, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (223, 392, 66, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (224, 4, 66, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (225, 27.295, 66, 2, 1, 10.65)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (226, 21.2579, 67, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (227, 2, 67, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (228, 263, 68, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (229, 5, 69, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (230, 18.7344, 69, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (231, 406, 69, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (232, 29.1856, 70, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (233, 2, 70, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (234, 20.648, 71, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (235, 19.402, 71, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (236, 403, 71, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (237, 5, 71, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (238, 29.3108, 72, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (239, 5, 72, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (240, 19.0575, 72, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (241, 474, 72, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (242, 17.2834, 73, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (243, 21.4342, 73, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (244, 3, 73, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (245, 470, 73, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (246, 21.2928, 74, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (247, 21.6744, 74, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (248, 3, 75, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (249, 415, 75, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (250, 28.3122, 75, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (251, 14.3973, 75, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (252, 12.0038, 76, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (253, 21.6886, 76, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (254, 5, 76, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (255, 452, 76, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (256, 15.0322, 77, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (257, 28.7448, 77, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (258, 1, 77, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (259, 326, 77, 3, 1, 217.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (260, 26.1527, 78, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (261, 3, 78, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (262, 357, 78, 3, 1, 165.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (263, 13.5123, 78, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (264, 14.8409, 79, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (265, 24.0312, 79, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (266, 4, 79, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (267, 284, 79, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (268, 15.388, 80, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (269, 463, 80, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (270, 1, 80, 1, 1, 79.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (271, 25.8168, 81, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (272, 17.3753, 81, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (273, 344, 81, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (274, 4, 81, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (275, 5, 82, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (276, 399, 82, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (277, 12.7116, 82, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (278, 25.091, 82, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (279, 27.2025, 83, 2, 1, 12.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (280, 19.4191, 83, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (281, 3, 83, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (282, 317, 83, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (283, 373, 84, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (284, 23.1329, 84, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (285, 27.1532, 84, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (286, 3, 84, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (287, 16.6725, 85, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (288, 1, 85, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (289, 20.4674, 85, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (290, 270, 85, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (291, 4, 86, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (292, 492, 86, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (293, 20.9293, 86, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (294, 20.6643, 86, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (295, 3, 87, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (296, 24.7377, 87, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (297, 283, 87, 3, 1, 162.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (298, 13.3052, 87, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (299, 16.3292, 88, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (300, 277, 88, 3, 1, 175.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (301, 28.0348, 88, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (302, 5, 88, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (303, 10.7574, 89, 0, 1, 7.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (304, 257, 89, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (305, 3, 89, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (306, 23.125, 89, 2, 1, 13.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (307, 436, 90, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (308, 16.2467, 90, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (309, 4, 90, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (310, 24.8918, 90, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (311, 5, 91, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (312, 28.9011, 91, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (313, 355, 91, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (314, 20.9441, 91, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (315, 22.8542, 92, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (316, 2, 92, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (317, 28.775, 92, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (318, 347, 92, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (319, 5, 93, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (320, 29.7101, 93, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (321, 17.3443, 93, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (322, 396, 93, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (323, 482, 94, 3, 1, 192.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (324, 5, 94, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (325, 28.2735, 94, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (326, 13.9104, 94, 0, 1, 8.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (327, 22.2475, 95, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (328, 346, 95, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (329, 22.7823, 95, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (330, 5, 95, 1, 1, 65.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (331, 28.768, 96, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (332, 300, 96, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (333, 3, 96, 1, 1, 72.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (334, 267, 97, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (335, 10.3191, 97, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (336, 24.5761, 97, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (337, 1, 97, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (338, 2, 98, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (339, 14.8229, 98, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (340, 295, 98, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (341, 26.866, 98, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (342, 22.0447, 99, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (343, 5, 99, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (344, 15.3575, 99, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (345, 342, 99, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (346, 4, 100, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (347, 15.5701, 100, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (348, 27.4551, 100, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (349, 327, 100, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (350, 2, 101, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (351, 14.3596, 101, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (352, 24.7744, 101, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (353, 410, 101, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (354, 11.9889, 102, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (355, 447, 102, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (356, 3, 102, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (357, 26.0761, 102, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (358, 4, 103, 1, 1, 73.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (359, 487, 103, 3, 1, 182.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (360, 24.3571, 103, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (361, 20.605, 103, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (362, 24.4709, 104, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (363, 294, 104, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (364, 11.0491, 104, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (365, 2, 104, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (366, 17.1715, 105, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (367, 1, 105, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (368, 22.872, 105, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (369, 382, 105, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (370, 4, 106, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (371, 4, 107, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (372, 290, 107, 3, 1, 190.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (373, 21.0672, 107, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (374, 27.0982, 107, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (375, 5, 108, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (376, 24.0687, 108, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (377, 323, 108, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (378, 23.0652, 108, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (379, 21.147, 109, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (380, 2, 109, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (381, 21.1088, 110, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (382, 421, 110, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (383, 1, 110, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (384, 21.6335, 110, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (385, 327, 111, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (386, 3, 111, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (387, 29.9312, 111, 2, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (388, 24.5778, 111, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (389, 1, 112, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (390, 394, 112, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (391, 25.9485, 112, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (392, 23.88, 113, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (393, 4, 113, 1, 1, 86.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (394, 380, 113, 3, 1, 200.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (395, 23.5306, 113, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (396, 477, 114, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (397, 3, 115, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (398, 20.7606, 115, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (399, 295, 116, 3, 1, 180.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (400, 21.5117, 116, 2, 1, 9.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (401, 16.165, 116, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (402, 2, 116, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (403, 16.501, 117, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (404, 20.1356, 117, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (405, 27.7804, 118, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (406, 457, 118, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (407, 17.4665, 118, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (408, 4, 118, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (409, 14.8416, 119, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (410, 4, 119, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (411, 466, 119, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (412, 26.638, 119, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (413, 18.7931, 120, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (414, 472, 120, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (415, 28.6229, 120, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (416, 2, 120, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (417, 21.7851, 121, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (418, 19.0059, 121, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (419, 2, 121, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (420, 368, 121, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (421, 26.2098, 122, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (422, 22.4649, 122, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (423, 2, 122, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (424, 485, 122, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (425, 432, 123, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (426, 25.093, 123, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (427, 4, 123, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (428, 20.2439, 123, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (429, 25.718, 124, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (430, 5, 124, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (431, 20.8036, 124, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (432, 268, 124, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (433, 489, 125, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (434, 17.8816, 125, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (435, 1, 125, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (436, 28.2356, 125, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (437, 319, 126, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (438, 351, 127, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (439, 1, 127, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (440, 19.3062, 127, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (441, 28.2115, 127, 2, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (442, 332, 128, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (443, 20.0312, 128, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (444, 1, 128, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (445, 10.2992, 128, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (446, 414, 129, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (447, 10.333, 129, 0, 1, 8.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (448, 21.7161, 129, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (449, 4, 129, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (450, 1, 130, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (451, 325, 130, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (452, 22.4074, 130, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (453, 11.1895, 130, 0, 1, 7.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (454, 2, 131, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (455, 17.223, 131, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (456, 23.0228, 131, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (457, 350, 131, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (458, 21.0622, 132, 0, 1, 7.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (459, 381, 132, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (460, 21.1267, 132, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (461, 3, 132, 1, 1, 72.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (462, 2, 133, 1, 1, 65.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (463, 22.2685, 133, 0, 1, 8.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (464, 350, 133, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (465, 403, 134, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (466, 20.0599, 134, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (467, 2, 134, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (468, 22.5364, 134, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (469, 412, 135, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (470, 20.7095, 135, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (471, 18.7064, 135, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (472, 5, 135, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (473, 377, 136, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (474, 29.5817, 136, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (475, 1, 136, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (476, 12.9674, 136, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (477, 22.3394, 137, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (478, 20.332, 137, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (479, 2, 137, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (480, 325, 137, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (481, 17.13, 138, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (482, 464, 138, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (483, 292, 139, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (484, 2, 139, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (485, 21.9163, 139, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (486, 26.4996, 139, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (487, 29.3344, 140, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (488, 493, 140, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (489, 18.0743, 140, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (490, 23.3772, 141, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (491, 4, 142, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (492, 346, 142, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (493, 27.5472, 142, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (494, 16.8467, 142, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (495, 26.4034, 143, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (496, 382, 143, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (497, 3, 144, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (498, 338, 144, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (499, 27.358, 144, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (500, 19.5844, 144, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (501, 375, 145, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (502, 13.4398, 146, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (503, 2, 146, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (504, 28.8951, 146, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (505, 281, 146, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (506, 306, 147, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (507, 5, 147, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (508, 14.5191, 147, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (509, 29.7871, 147, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (510, 28.5903, 148, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (511, 3, 148, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (512, 347, 148, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (513, 283, 149, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (514, 10.4692, 149, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (515, 3, 149, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (516, 21.4107, 149, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (517, 21.4742, 150, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (518, 1, 150, 1, 1, 76.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (519, 490, 150, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (520, 13.2371, 150, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (521, 460, 151, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (522, 29.734, 151, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (523, 5, 151, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (524, 21, 151, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (525, 458, 152, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (526, 22.9874, 152, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (527, 5, 152, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (528, 12.0476, 152, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (529, 20.7872, 153, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (530, 25.6937, 154, 2, 1, 10.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (531, 1, 154, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (532, 10.3076, 154, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (533, 442, 154, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (534, 4, 155, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (535, 24.3879, 155, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (536, 11.7912, 155, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (537, 392, 155, 3, 1, 167.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (538, 27.4151, 156, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (539, 3, 156, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (540, 495, 156, 3, 1, 157.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (541, 10.6952, 156, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (542, 23.1933, 157, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (543, 13.5431, 157, 0, 1, 8.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (544, 3, 157, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (545, 422, 157, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (546, 289, 158, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (547, 22.2973, 158, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (548, 17.4873, 158, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (549, 1, 158, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (550, 21.6604, 159, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (551, 379, 159, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (552, 1, 159, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (553, 20.6632, 159, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (554, 1, 160, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (555, 267, 160, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (556, 22.0788, 160, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (557, 22.7988, 160, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (558, 347, 161, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (559, 21.8868, 161, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (560, 24.322, 161, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (561, 5, 161, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (562, 13.3341, 162, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (563, 405, 162, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (564, 1, 162, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (565, 4, 163, 1, 1, 68.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (566, 29.4749, 163, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (567, 21.1233, 163, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (568, 421, 163, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (569, 14.266, 164, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (570, 451, 164, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (571, 2, 164, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (572, 22.2713, 164, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (573, 13.8555, 165, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (574, 425, 166, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (575, 24.0016, 166, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (576, 16.0411, 166, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (577, 3, 166, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (578, 13.783, 167, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (579, 478, 167, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (580, 24.3241, 167, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (581, 1, 167, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (582, 21.4126, 168, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (583, 15.1005, 168, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (584, 418, 168, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (585, 4, 168, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (586, 16.1694, 169, 0, 1, 8.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (587, 3, 169, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (588, 387, 169, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (589, 27.0446, 169, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (590, 24.599, 170, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (591, 405, 170, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (592, 17.7555, 170, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (593, 15.7057, 171, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (594, 5, 171, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (595, 312, 171, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (596, 4, 172, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (597, 15.0962, 172, 0, 1, 8.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (598, 21.7498, 172, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (599, 496, 172, 3, 1, 180.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (600, 419, 173, 3, 1, 180.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (601, 22.4782, 173, 2, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (602, 11.0229, 173, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (603, 3, 173, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (604, 12.0815, 174, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (605, 29.4707, 174, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (606, 367, 174, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (607, 2, 174, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (608, 5, 175, 1, 1, 71.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (609, 497, 175, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (610, 24.3064, 175, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (611, 27.7221, 175, 2, 1, 10.65)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (612, 24.9448, 176, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (613, 275, 176, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (614, 29.8832, 176, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (615, 3, 176, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (616, 20.2666, 177, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (617, 259, 177, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (618, 2, 177, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (619, 20.4003, 177, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (620, 27.9541, 178, 2, 1, 12.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (621, 474, 178, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (622, 4, 178, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (623, 22.7987, 178, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (624, 20.8229, 179, 0, 1, 8.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (625, 21.4737, 179, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (626, 5, 179, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (627, 402, 179, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (628, 28.3003, 180, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (629, 3, 180, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (630, 16.9645, 180, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (631, 457, 180, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (632, 25.0946, 181, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (633, 12.146, 181, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (634, 424, 181, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (635, 1, 181, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (636, 14.1426, 182, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (637, 472, 182, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (638, 2, 182, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (639, 24.6316, 182, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (640, 2, 183, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (641, 279, 183, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (642, 23.4265, 183, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (643, 17.9018, 184, 0, 1, 8.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (644, 408, 184, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (645, 366, 185, 3, 1, 157.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (646, 16.0024, 185, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (647, 14.4539, 186, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (648, 23.6009, 186, 2, 1, 11.85)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (649, 2, 186, 1, 1, 72.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (650, 291, 186, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (651, 20.8461, 187, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (652, 21.6504, 187, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (653, 339, 187, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (654, 1, 187, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (655, 428, 188, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (656, 4, 188, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (657, 16.2297, 188, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (658, 22.1736, 188, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (659, 457, 189, 3, 1, 167.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (660, 20.3931, 189, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (661, 5, 189, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (662, 25.7825, 189, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (663, 22.5743, 190, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (664, 410, 190, 3, 1, 155.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (665, 3, 190, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (666, 12.8397, 190, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (667, 15.8934, 191, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (668, 399, 191, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (669, 22.7176, 191, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (670, 5, 191, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (671, 455, 192, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (672, 23.0801, 192, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (673, 13.1925, 192, 0, 1, 8.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (674, 4, 193, 1, 1, 86.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (675, 369, 193, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (676, 1, 194, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (677, 409, 194, 3, 1, 155.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (678, 13.8757, 194, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (679, 20.3583, 194, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (680, 5, 195, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (681, 368, 195, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (682, 266, 196, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (683, 26.2524, 196, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (684, 2, 196, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (685, 10.1788, 196, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (686, 20.1142, 197, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (687, 15.2747, 197, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (688, 442, 197, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (689, 3, 197, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (690, 1, 198, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (691, 342, 198, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (692, 22.584, 198, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (693, 2, 199, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (694, 14.6763, 199, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (695, 24.0603, 199, 2, 1, 9.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (696, 394, 200, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (697, 1, 200, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (698, 24.5331, 200, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (699, 24.1722, 200, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (700, 16.3659, 201, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (701, 355, 201, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (702, 25.3538, 201, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (703, 4, 201, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (704, 304, 202, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (705, 22.3278, 202, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (706, 22.4188, 202, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (707, 1, 202, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (708, 13.7816, 203, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (709, 398, 203, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (710, 2, 203, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (711, 24.8367, 203, 2, 1, 13.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (712, 17.3894, 204, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (713, 337, 204, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (714, 23.0413, 204, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (715, 3, 204, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (716, 3, 205, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (717, 2, 206, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (718, 26.6681, 206, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (719, 285, 206, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (720, 18.7702, 206, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (721, 324, 207, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (722, 26.8838, 207, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (723, 4, 207, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (724, 23.8456, 207, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (725, 29.3089, 208, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (726, 251, 208, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (727, 20.2077, 208, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (728, 3, 208, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (729, 496, 209, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (730, 16.9522, 209, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (731, 4, 209, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (732, 26.8375, 209, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (733, 13.3834, 210, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (734, 25.6858, 210, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (735, 465, 210, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (736, 5, 210, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (737, 397, 211, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (738, 15.4261, 211, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (739, 315, 212, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (740, 11.2601, 212, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (741, 25.6069, 212, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (742, 4, 212, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (743, 24.7323, 213, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (744, 5, 213, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (745, 22.6048, 213, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (746, 491, 213, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (747, 371, 214, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (748, 10.1808, 214, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (749, 25.0039, 214, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (750, 4, 214, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (751, 26.1797, 215, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (752, 359, 215, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (753, 10.186, 215, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (754, 1, 215, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (755, 355, 216, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (756, 22.4175, 217, 0, 1, 6.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (757, 3, 217, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (758, 27.7346, 218, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (759, 4, 219, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (760, 18.1577, 219, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (761, 25.8955, 219, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (762, 447, 219, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (763, 14.3491, 220, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (764, 21.5461, 221, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (765, 416, 221, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (766, 15.8225, 221, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (767, 3, 221, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (768, 418, 222, 3, 1, 200.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (769, 22.5087, 222, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (770, 2, 222, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (771, 24.5351, 222, 2, 1, 11.85)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (772, 28.6845, 223, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (773, 10.6473, 223, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (774, 341, 223, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (775, 26.4693, 224, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (776, 15.833, 224, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (777, 4, 224, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (778, 307, 224, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (779, 491, 225, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (780, 26.3945, 225, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (781, 5, 225, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (782, 23.9613, 226, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (783, 3, 226, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (784, 265, 226, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (785, 14.7266, 227, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (786, 329, 227, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (787, 25.0554, 227, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (788, 2, 227, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (789, 16.6556, 228, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (790, 4, 229, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (791, 256, 229, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (792, 24.5148, 229, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (793, 13.9267, 229, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (794, 15.1818, 230, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (795, 24.6825, 230, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (796, 278, 230, 3, 1, 217.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (797, 3, 230, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (798, 23.4091, 231, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (799, 472, 231, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (800, 3, 231, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (801, 20.5757, 232, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (802, 402, 232, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (803, 13.9265, 232, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (804, 2, 232, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (805, 452, 233, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (806, 5, 233, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (807, 18.8594, 233, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (808, 26.3714, 233, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (809, 340, 234, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (810, 26.1639, 234, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (811, 21.6212, 234, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (812, 4, 234, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (813, 19.0941, 235, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (814, 5, 235, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (815, 27.0685, 235, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (816, 28.0824, 236, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (817, 439, 236, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (818, 11.5092, 236, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (819, 2, 236, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (820, 18.3919, 237, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (821, 2, 237, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (822, 20.6959, 237, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (823, 356, 237, 3, 1, 200.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (824, 21.8071, 238, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (825, 1, 238, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (826, 21.8846, 238, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (827, 439, 238, 3, 1, 157.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (828, 21.6996, 239, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (829, 422, 239, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (830, 22.8476, 239, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (831, 2, 239, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (832, 2, 240, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (833, 478, 240, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (834, 22.5445, 240, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (835, 18.5741, 240, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (836, 18.4621, 241, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (837, 21.3469, 242, 0, 1, 8.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (838, 26.4782, 242, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (839, 398, 243, 3, 1, 167.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (840, 23.7818, 243, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (841, 3, 243, 1, 1, 65.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (842, 20.8358, 243, 0, 1, 7.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (843, 24.4009, 244, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (844, 358, 244, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (845, 455, 245, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (846, 1, 246, 1, 1, 75.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (847, 29.4796, 246, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (848, 15.0933, 246, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (849, 252, 246, 3, 1, 190.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (850, 22.026, 247, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (851, 340, 247, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (852, 19.9126, 247, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (853, 1, 247, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (854, 461, 248, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (855, 5, 248, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (856, 20.0241, 248, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (857, 20.7324, 248, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (858, 4, 249, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (859, 23.4199, 249, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (860, 363, 249, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (861, 23.9307, 249, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (862, 27.5136, 250, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (863, 1, 250, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (864, 19.7363, 250, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (865, 258, 250, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (866, 26.605, 251, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (867, 2, 251, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (868, 499, 251, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (869, 19.0991, 251, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (870, 23.382, 252, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (871, 25.1225, 252, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (872, 488, 252, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (873, 1, 252, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (874, 2, 253, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (875, 435, 254, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (876, 2, 254, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (877, 29.3696, 254, 2, 1, 12.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (878, 19.5547, 254, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (879, 309, 255, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (880, 451, 256, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (881, 5, 256, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (882, 21.32, 256, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (883, 13.1635, 256, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (884, 5, 257, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (885, 297, 257, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (886, 1, 258, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (887, 23.5135, 258, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (888, 17.3734, 258, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (889, 307, 258, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (890, 24.2477, 259, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (891, 18.822, 259, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (892, 4, 260, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (893, 28.6464, 260, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (894, 455, 260, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (895, 19.1403, 261, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (896, 1, 261, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (897, 23.8911, 261, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (898, 471, 261, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (899, 17.7416, 262, 0, 1, 7.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (900, 24.2385, 263, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (901, 22.9888, 263, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (902, 4, 264, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (903, 10.4986, 265, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (904, 20.9958, 265, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (905, 4, 265, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (906, 369, 265, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (907, 2, 266, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (908, 255, 266, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (909, 21.454, 266, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (910, 25.3273, 266, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (911, 20.5408, 267, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (912, 4, 267, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (913, 474, 267, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (914, 25.3116, 267, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (915, 19.454, 268, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (916, 23.6772, 268, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (917, 397, 268, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (918, 1, 268, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (919, 1, 269, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (920, 23.9807, 269, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (921, 27.0051, 269, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (922, 411, 269, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (923, 19.8234, 270, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (924, 328, 270, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (925, 26.2093, 270, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (926, 3, 270, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (927, 23.6689, 271, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (928, 427, 272, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (929, 29.997, 272, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (930, 18.1888, 272, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (931, 1, 272, 1, 1, 87.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (932, 28.7865, 273, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (933, 3, 273, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (934, 20.5027, 273, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (935, 335, 274, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (936, 26.4448, 274, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (937, 4, 274, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (938, 20.0681, 275, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (939, 23.405, 275, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (940, 397, 275, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (941, 3, 275, 1, 1, 68.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (942, 448, 276, 3, 1, 182.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (943, 24.5366, 276, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (944, 1, 276, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (945, 13.9114, 276, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (946, 11.6449, 277, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (947, 20.5675, 277, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (948, 357, 277, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (949, 5, 277, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (950, 372, 278, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (951, 4, 278, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (952, 23.9427, 278, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (953, 28.116, 278, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (954, 490, 279, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (955, 27.5186, 279, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (956, 15.4597, 279, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (957, 3, 279, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (958, 1, 280, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (959, 22.6547, 280, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (960, 465, 280, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (961, 10.4326, 280, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (962, 403, 281, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (963, 15.1178, 281, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (964, 24.5548, 281, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (965, 4, 281, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (966, 20.8767, 282, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (967, 5, 282, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (968, 23.5387, 282, 0, 1, 8.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (969, 339, 282, 3, 1, 217.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (970, 20.9704, 283, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (971, 1, 283, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (972, 389, 283, 3, 1, 190.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (973, 27.4741, 283, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (974, 24.1139, 284, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (975, 17.5187, 284, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (976, 412, 284, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (977, 1, 284, 1, 1, 87.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (978, 4, 285, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (979, 26.7352, 285, 2, 1, 10.65)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (980, 328, 285, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (981, 14.2494, 285, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (982, 4, 286, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (983, 433, 287, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (984, 23.5312, 287, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (985, 5, 287, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (986, 12.8665, 287, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (987, 20.4678, 288, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (988, 25.4476, 289, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (989, 2, 289, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (990, 373, 289, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (991, 15.3619, 289, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (992, 13.3743, 290, 0, 1, 7.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (993, 2, 290, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (994, 441, 290, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (995, 21.8186, 290, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (996, 3, 291, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (997, 20.4287, 291, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (998, 442, 291, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (999, 18.9451, 291, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1000, 481, 292, 3, 1, 162.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1001, 15.0989, 292, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1002, 2, 292, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1003, 26.105, 292, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1004, 22.5152, 293, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1005, 357, 293, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1006, 17.2525, 293, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1007, 2, 293, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1008, 21.4544, 294, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1009, 339, 294, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1010, 5, 294, 1, 1, 71.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1011, 11.5437, 294, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1012, 4, 295, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1013, 23.4504, 295, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1014, 19.185, 295, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1015, 405, 296, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1016, 25.4899, 296, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1017, 5, 296, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1018, 10.1797, 296, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1019, 25.2585, 297, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1020, 3, 297, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1021, 24.3806, 297, 0, 1, 7.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1022, 327, 297, 3, 1, 210.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1023, 24.6374, 298, 2, 1, 10.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1024, 18.4305, 298, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1025, 366, 298, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1026, 1, 298, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1027, 380, 299, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1028, 1, 299, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1029, 20.0898, 299, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1030, 24.396, 299, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1031, 435, 300, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1032, 16.9303, 300, 0, 1, 8.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1033, 22.9461, 300, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1034, 2, 300, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1035, 22.4627, 301, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1036, 294, 301, 3, 1, 225.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1037, 1, 301, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1038, 10.2997, 301, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1039, 2, 302, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1040, 255, 302, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1041, 16.2567, 302, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1042, 23.0756, 302, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1043, 3, 303, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1044, 320, 303, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1045, 26.8872, 303, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1046, 4, 304, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1047, 21.4255, 304, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1048, 29.3063, 304, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1049, 468, 304, 3, 1, 210.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1050, 25.5412, 305, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1051, 5, 305, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1052, 337, 305, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1053, 18.448, 305, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1054, 288, 306, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1055, 24.9223, 307, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1056, 435, 307, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1057, 5, 307, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1058, 11.7856, 307, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1059, 455, 308, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1060, 26.2668, 308, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1061, 3, 308, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1062, 18.5435, 308, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1063, 21.4756, 309, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1064, 10.2477, 309, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1065, 23.3909, 310, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1066, 22.6001, 311, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1067, 2, 311, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1068, 28.931, 311, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1069, 380, 311, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1070, 22.9126, 312, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1071, 20.7877, 312, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1072, 428, 312, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1073, 5, 312, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1074, 2, 313, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1075, 390, 313, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1076, 28.584, 314, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1077, 4, 314, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1078, 250, 314, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1079, 12.0637, 314, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1080, 26.6877, 315, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1081, 12.3742, 315, 0, 1, 8.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1082, 3, 315, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1083, 445, 315, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1084, 354, 316, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1085, 22.358, 316, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1086, 14.2996, 316, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1087, 2, 316, 1, 1, 71.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1088, 493, 317, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1089, 27.6677, 317, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1090, 14.3903, 317, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1091, 1, 318, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1092, 335, 318, 3, 1, 180.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1093, 16.3156, 318, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1094, 21.9467, 318, 2, 1, 13.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1095, 23.9798, 319, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1096, 281, 320, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1097, 3, 320, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1098, 24.8178, 320, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1099, 26.0693, 320, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1100, 306, 321, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1101, 26.4178, 321, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1102, 18.4609, 321, 0, 1, 6.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1103, 2, 321, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1104, 29.1866, 322, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1105, 5, 322, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1106, 312, 322, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1107, 379, 323, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1108, 14.7489, 323, 0, 1, 7.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1109, 1, 323, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1110, 20.5751, 323, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1111, 426, 324, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1112, 20.1683, 324, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1113, 21.5215, 324, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1114, 4, 324, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1115, 2, 325, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1116, 318, 326, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1117, 19.0039, 326, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1118, 29.6773, 326, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1119, 4, 326, 1, 1, 88.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1120, 17.817, 327, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1121, 407, 327, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1122, 2, 327, 1, 1, 76.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1123, 29.5243, 327, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1124, 27.0883, 328, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1125, 424, 329, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1126, 5, 329, 1, 1, 71.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1127, 265, 330, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1128, 5, 330, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1129, 11.1282, 330, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1130, 22.6773, 330, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1131, 328, 331, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1132, 2, 331, 1, 1, 75.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1133, 25.9263, 331, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1134, 18.7004, 331, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1135, 24.9131, 332, 0, 1, 7.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1136, 485, 332, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1137, 2, 332, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1138, 20.7416, 332, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1139, 3, 333, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1140, 28.1006, 333, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1141, 408, 333, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1142, 19.5933, 333, 0, 1, 7.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1143, 19.3406, 334, 0, 1, 7.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1144, 29.0515, 334, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1145, 463, 334, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1146, 4, 334, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1147, 12.9023, 335, 0, 1, 6.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1148, 5, 335, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1149, 327, 335, 3, 1, 157.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1150, 5, 336, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1151, 22.376, 336, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1152, 20.3883, 336, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1153, 448, 336, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1154, 403, 337, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1155, 20.0885, 337, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1156, 11.1171, 337, 0, 1, 6.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1157, 3, 337, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1158, 304, 338, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1159, 22.021, 339, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1160, 22.7, 340, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1161, 27.4468, 340, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1162, 430, 340, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1163, 1, 340, 1, 1, 86.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1164, 18.178, 341, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1165, 478, 341, 3, 1, 157.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1166, 2, 341, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1167, 20.5148, 341, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1168, 1, 342, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1169, 277, 342, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1170, 24.1274, 342, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1171, 18.9534, 343, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1172, 367, 343, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1173, 26.2758, 343, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1174, 4, 343, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1175, 19.1283, 344, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1176, 28.4316, 344, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1177, 24.8179, 345, 0, 1, 7.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1178, 28.5593, 345, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1179, 385, 345, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1180, 2, 345, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1181, 356, 346, 3, 1, 190.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1182, 2, 346, 1, 1, 87.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1183, 18.0868, 346, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1184, 27.2062, 346, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1185, 23.4862, 347, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1186, 414, 347, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1187, 3, 347, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1188, 28.5281, 347, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1189, 346, 348, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1190, 25.0302, 348, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1191, 4, 348, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1192, 10.7873, 348, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1193, 21.414, 349, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1194, 13.6141, 349, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1195, 272, 349, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1196, 3, 349, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1197, 27.9123, 350, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1198, 21.4699, 351, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1199, 20.6231, 351, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1200, 428, 351, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1201, 20.1841, 352, 0, 1, 7.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1202, 4, 352, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1203, 21.7434, 352, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1204, 481, 352, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1205, 408, 353, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1206, 1, 353, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1207, 29.5964, 353, 2, 1, 13.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1208, 18.7654, 354, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1209, 26.3216, 354, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1210, 1, 354, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1211, 394, 354, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1212, 13.984, 355, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1213, 358, 355, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1214, 423, 356, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1215, 24.6197, 356, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1216, 16.4275, 356, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1217, 5, 356, 1, 1, 72.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1218, 19.0406, 357, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1219, 3, 357, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1220, 25.058, 357, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1221, 315, 357, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1222, 438, 358, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1223, 22.7481, 358, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1224, 265, 359, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1225, 22.7782, 359, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1226, 3, 359, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1227, 11.6973, 359, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1228, 18.9552, 360, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1229, 5, 360, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1230, 26.4984, 361, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1231, 21.4554, 361, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1232, 370, 361, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1233, 5, 361, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1234, 12.7301, 362, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1235, 12.1389, 363, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1236, 24.9739, 363, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1237, 1, 363, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1238, 308, 363, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1239, 29.1515, 364, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1240, 11.0822, 364, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1241, 424, 364, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1242, 2, 364, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1243, 17.9647, 365, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1244, 22.5769, 365, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1245, 5, 365, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1246, 366, 365, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1247, 25.1093, 366, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1248, 23.8689, 366, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1249, 427, 366, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1250, 2, 366, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1251, 2, 367, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1252, 21.7501, 367, 0, 1, 7.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1253, 278, 367, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1254, 20.7955, 367, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1255, 310, 368, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1256, 10.2624, 368, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1257, 27.7443, 368, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1258, 4, 368, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1259, 317, 369, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1260, 24.2181, 369, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1261, 5, 369, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1262, 12.6768, 369, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1263, 22.5943, 370, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1264, 23.9308, 370, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1265, 5, 370, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1266, 490, 370, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1267, 13.3959, 371, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1268, 451, 371, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1269, 2, 371, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1270, 20.0104, 371, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1271, 21.5074, 372, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1272, 433, 372, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1273, 1, 373, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1274, 488, 373, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1275, 21.1282, 373, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1276, 23.8911, 373, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1277, 28.4369, 374, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1278, 5, 374, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1279, 314, 374, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1280, 21.8703, 374, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1281, 410, 375, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1282, 4, 375, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1283, 21.1107, 375, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1284, 23.5883, 375, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1285, 29.6567, 376, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1286, 4, 376, 1, 1, 88.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1287, 294, 376, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1288, 24.7007, 376, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1289, 23.5002, 377, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1290, 433, 377, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1291, 22.9358, 378, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1292, 2, 378, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1293, 14.0619, 378, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1294, 275, 378, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1295, 5, 379, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1296, 500, 380, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1297, 11.3256, 381, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1298, 3, 381, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1299, 28.2777, 381, 2, 1, 10.65)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1300, 336, 381, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1301, 21.9677, 382, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1302, 26.9189, 382, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1303, 2, 382, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1304, 251, 382, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1305, 28.168, 383, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1306, 327, 383, 3, 1, 225.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1307, 15.3542, 383, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1308, 2, 383, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1309, 349, 384, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1310, 4, 384, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1311, 21.4609, 384, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1312, 15.9595, 384, 0, 1, 6.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1313, 283, 385, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1314, 25.1862, 385, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1315, 4, 385, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1316, 11.2328, 385, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1317, 2, 386, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1318, 409, 386, 3, 1, 167.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1319, 21.6728, 386, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1320, 13.9098, 386, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1321, 21.8883, 387, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1322, 14.9588, 387, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1323, 329, 387, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1324, 1, 387, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1325, 5, 388, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1326, 21.3826, 388, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1327, 298, 388, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1328, 21.4295, 388, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1329, 2, 389, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1330, 26.9244, 389, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1331, 290, 389, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1332, 20.4601, 390, 2, 1, 13.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1333, 10.1644, 390, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1334, 25.6723, 391, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1335, 18.2748, 391, 0, 1, 7.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1336, 4, 391, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1337, 370, 391, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1338, 28.6034, 392, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1339, 12.6307, 392, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1340, 422, 392, 3, 1, 200.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1341, 5, 392, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1342, 26.7332, 393, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1343, 421, 393, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1344, 18.7422, 393, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1345, 1, 393, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1346, 316, 394, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1347, 29.1981, 394, 2, 1, 13.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1348, 19.885, 394, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1349, 2, 394, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1350, 494, 395, 3, 1, 157.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1351, 15.8627, 395, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1352, 24.4263, 395, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1353, 3, 395, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1354, 22.6807, 396, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1355, 26.926, 396, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1356, 451, 396, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1357, 2, 396, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1358, 325, 397, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1359, 14.0492, 397, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1360, 3, 397, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1361, 20.0184, 397, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1362, 18.4442, 398, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1363, 269, 398, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1364, 4, 398, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1365, 26.6911, 398, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1366, 1, 399, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1367, 355, 399, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1368, 15.7756, 399, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1369, 437, 400, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1370, 20.7607, 400, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1371, 1, 400, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1372, 252, 401, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1373, 27.2974, 401, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1374, 1, 401, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1375, 12.529, 401, 0, 1, 7.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1376, 294, 402, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1377, 22.0006, 402, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1378, 4, 402, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1379, 461, 403, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1380, 29.0639, 403, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1381, 3, 403, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1382, 13.7101, 403, 0, 1, 7.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1383, 26.1808, 404, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1384, 478, 404, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1385, 10.7909, 404, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1386, 1, 404, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1387, 26.5331, 405, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1388, 22.2644, 405, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1389, 430, 405, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1390, 5, 405, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1391, 23.436, 406, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1392, 29.7623, 406, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1393, 20.443, 407, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1394, 14.1638, 407, 0, 1, 7.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1395, 387, 407, 3, 1, 225.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1396, 4, 407, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1397, 319, 408, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1398, 1, 408, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1399, 20.5557, 408, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1400, 12.0946, 408, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1401, 2, 409, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1402, 20.1648, 409, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1403, 387, 409, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1404, 23.4379, 409, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1405, 29.5917, 410, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1406, 2, 410, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1407, 383, 410, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1408, 24.3978, 410, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1409, 25.1391, 411, 2, 1, 12.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1410, 12.3974, 411, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1411, 5, 411, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1412, 348, 411, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1413, 4, 412, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1414, 25.4588, 412, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1415, 13.2421, 412, 0, 1, 7.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1416, 273, 412, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1417, 24.1796, 413, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1418, 1, 413, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1419, 313, 413, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1420, 13.9291, 413, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1421, 454, 414, 3, 1, 210.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1422, 22.9597, 414, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1423, 12.1337, 415, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1424, 23.2049, 415, 2, 1, 10.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1425, 19.719, 416, 0, 1, 7.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1426, 1, 416, 1, 1, 71.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1427, 24.2773, 416, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1428, 294, 416, 3, 1, 162.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1429, 16.8802, 417, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1430, 4, 417, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1431, 307, 417, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1432, 23.4776, 417, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1433, 2, 418, 1, 1, 75.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1434, 21.1524, 418, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1435, 274, 418, 3, 1, 180.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1436, 463, 419, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1437, 4, 419, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1438, 15.546, 419, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1439, 21.6954, 419, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1440, 21.8745, 420, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1441, 292, 420, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1442, 5, 420, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1443, 15.2637, 421, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1444, 378, 422, 3, 1, 160.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1445, 29.5309, 422, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1446, 2, 422, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1447, 12.3142, 422, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1448, 259, 423, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1449, 21.0271, 423, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1450, 22.1639, 423, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1451, 2, 423, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1452, 454, 424, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1453, 1, 424, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1454, 17.7562, 424, 0, 1, 8.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1455, 22.9193, 424, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1456, 24.7608, 425, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1457, 21.8773, 425, 0, 1, 8.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1458, 447, 425, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1459, 4, 425, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1460, 26.1893, 426, 2, 1, 13.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1461, 11.884, 426, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1462, 332, 426, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1463, 1, 426, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1464, 10.3276, 427, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1465, 318, 427, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1466, 27.996, 428, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1467, 258, 428, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1468, 3, 428, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1469, 12.75, 428, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1470, 285, 429, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1471, 3, 429, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1472, 23.1367, 429, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1473, 22.1001, 429, 0, 1, 6.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1474, 24.3941, 430, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1475, 3, 430, 1, 1, 75.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1476, 25.9186, 430, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1477, 1, 431, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1478, 19.6279, 431, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1479, 27.6682, 431, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1480, 303, 431, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1481, 1, 432, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1482, 29.6604, 432, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1483, 13.4934, 432, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1484, 4, 433, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1485, 28.2553, 433, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1486, 20.5558, 434, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1487, 489, 434, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1488, 17.7283, 434, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1489, 3, 434, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1490, 27.7493, 435, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1491, 252, 435, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1492, 20.9224, 436, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1493, 282, 436, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1494, 5, 436, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1495, 21.015, 436, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1496, 17.2599, 437, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1497, 467, 438, 3, 1, 162.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1498, 22.1668, 438, 0, 1, 7.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1499, 27.3151, 438, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1500, 2, 438, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1501, 1, 439, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1502, 27.2557, 439, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1503, 17.9448, 439, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1504, 416, 439, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1505, 23.9242, 440, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1506, 445, 440, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1507, 1, 440, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1508, 23.1656, 440, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1509, 29.1746, 441, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1510, 23.546, 442, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1511, 5, 442, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1512, 11.8878, 442, 0, 1, 8.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1513, 438, 442, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1514, 1, 443, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1515, 449, 443, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1516, 24.5389, 443, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1517, 23.2027, 443, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1518, 279, 444, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1519, 5, 445, 1, 1, 72.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1520, 458, 445, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1521, 25.2688, 446, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1522, 375, 446, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1523, 17.7433, 446, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1524, 3, 446, 1, 1, 75.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1525, 422, 447, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1526, 20.8142, 447, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1527, 17.7799, 448, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1528, 28.0592, 448, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1529, 464, 448, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1530, 2, 448, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1531, 28.0016, 449, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1532, 320, 449, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1533, 4, 450, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1534, 339, 450, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1535, 5, 451, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1536, 13.5702, 451, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1537, 20.1523, 451, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1538, 441, 451, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1539, 5, 452, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1540, 18.9513, 452, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1541, 29.3019, 452, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1542, 302, 453, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1543, 14.5442, 453, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1544, 1, 453, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1545, 26.5237, 453, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1546, 15.2696, 454, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1547, 1, 454, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1548, 319, 454, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1549, 28.5021, 454, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1550, 11.6362, 455, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1551, 29.358, 455, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1552, 5, 456, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1553, 21.032, 456, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1554, 13.632, 456, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1555, 315, 456, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1556, 289, 457, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1557, 22.7733, 457, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1558, 29.0123, 457, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1559, 3, 457, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1560, 23.0068, 458, 0, 1, 7.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1561, 4, 458, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1562, 24.8575, 458, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1563, 396, 458, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1564, 27.0507, 459, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1565, 466, 459, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1566, 4, 459, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1567, 20.9128, 459, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1568, 12.733, 460, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1569, 3, 460, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1570, 410, 460, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1571, 20.9787, 460, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1572, 444, 461, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1573, 28.7078, 462, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1574, 259, 463, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1575, 18.138, 463, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1576, 24.0773, 463, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1577, 5, 463, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1578, 18.6428, 464, 0, 1, 8.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1579, 22.0807, 464, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1580, 5, 464, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1581, 466, 464, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1582, 3, 465, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1583, 18.7616, 465, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1584, 328, 465, 3, 1, 162.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1585, 23.4147, 465, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1586, 20.3621, 466, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1587, 4, 466, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1588, 340, 466, 3, 1, 182.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1589, 11.8935, 466, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1590, 412, 467, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1591, 2, 467, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1592, 25.8185, 467, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1593, 23.972, 467, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1594, 26.8253, 468, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1595, 15.7653, 468, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1596, 4, 468, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1597, 424, 468, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1598, 5, 469, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1599, 27.8418, 469, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1600, 322, 469, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1601, 15.6988, 469, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1602, 20.9895, 470, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1603, 5, 470, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1604, 24.2571, 470, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1605, 410, 470, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1606, 392, 471, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1607, 27.6052, 471, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1608, 14.8977, 471, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1609, 5, 471, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1610, 4, 472, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1611, 18.7094, 472, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1612, 29.7392, 472, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1613, 379, 472, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1614, 306, 473, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1615, 13.7382, 473, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1616, 1, 473, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1617, 25.6865, 473, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1618, 26.9898, 474, 2, 1, 13.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1619, 408, 474, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1620, 18.504, 474, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1621, 3, 474, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1622, 28.3714, 475, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1623, 4, 475, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1624, 20.6804, 475, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1625, 449, 475, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1626, 1, 476, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1627, 396, 477, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1628, 23.1056, 477, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1629, 5, 477, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1630, 24.447, 477, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1631, 21.2316, 478, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1632, 12.1645, 478, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1633, 2, 478, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1634, 292, 478, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1635, 27.9562, 479, 2, 1, 12.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1636, 489, 479, 3, 1, 160.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1637, 19.1954, 479, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1638, 5, 479, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1639, 21.9692, 480, 0, 1, 7.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1640, 5, 480, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1641, 23.9835, 480, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1642, 484, 480, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1643, 27.9535, 481, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1644, 20.6105, 481, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1645, 2, 481, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1646, 313, 481, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1647, 291, 482, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1648, 2, 482, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1649, 26.6136, 482, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1650, 21.8725, 482, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1651, 16.1885, 483, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1652, 3, 483, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1653, 27.3446, 483, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1654, 287, 483, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1655, 4, 484, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1656, 456, 484, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1657, 27.9876, 484, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1658, 13.4644, 484, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1659, 262, 485, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1660, 23.0458, 485, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1661, 22.0809, 485, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1662, 4, 485, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1663, 21.5171, 486, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1664, 3, 487, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1665, 26.4665, 487, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1666, 13.8422, 487, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1667, 394, 487, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1668, 15.8456, 488, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1669, 497, 488, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1670, 23.1563, 488, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1671, 5, 488, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1672, 25.4753, 489, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1673, 352, 489, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1674, 12.1421, 489, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1675, 1, 489, 1, 1, 75.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1676, 23.1816, 490, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1677, 413, 490, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1678, 24.9975, 490, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1679, 1, 490, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1680, 29.9331, 491, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1681, 23.8075, 491, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1682, 2, 491, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1683, 26.4042, 492, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1684, 381, 492, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1685, 18.3211, 492, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1686, 1, 492, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1687, 20.9795, 493, 2, 1, 13.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1688, 2, 493, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1689, 454, 493, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1690, 18.1908, 493, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1691, 4, 494, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1692, 429, 494, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1693, 27.5314, 494, 2, 1, 9.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1694, 22.3911, 494, 0, 1, 7.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1695, 3, 495, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1696, 361, 495, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1697, 23.7959, 495, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1698, 12.6458, 495, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1699, 19.9882, 496, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1700, 436, 496, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1701, 2, 496, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1702, 26.8823, 496, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1703, 348, 497, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1704, 24.0635, 497, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1705, 21.1492, 498, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1706, 20.0426, 498, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1707, 405, 498, 3, 1, 165.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1708, 5, 498, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1709, 28.0136, 499, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1710, 19.5183, 499, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1711, 2, 499, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1712, 283, 499, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1713, 5, 500, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1714, 370, 500, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1715, 440, 501, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1716, 2, 501, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1717, 20.0691, 501, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1718, 19.9507, 501, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1719, 4, 502, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1720, 29.3516, 502, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1721, 432, 502, 3, 1, 192.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1722, 15.9819, 502, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1723, 384, 503, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1724, 10.3525, 503, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1725, 24.8818, 503, 2, 1, 10.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1726, 4, 504, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1727, 388, 504, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1728, 16.032, 504, 0, 1, 8.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1729, 24.4116, 504, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1730, 5, 505, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1731, 29.5869, 505, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1732, 20.6791, 505, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1733, 368, 505, 3, 1, 225.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1734, 427, 506, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1735, 23.2289, 506, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1736, 1, 506, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1737, 3, 507, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1738, 22.1643, 507, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1739, 361, 507, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1740, 20.1304, 507, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1741, 4, 508, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1742, 465, 508, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1743, 3, 509, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1744, 22.4583, 509, 0, 1, 7.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1745, 27.9159, 510, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1746, 500, 510, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1747, 18.2247, 510, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1748, 4, 510, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1749, 4, 511, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1750, 20.5415, 511, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1751, 20.0984, 511, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1752, 284, 511, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1753, 256, 512, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1754, 26.7277, 512, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1755, 19.2423, 512, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1756, 1, 512, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1757, 5, 513, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1758, 11.1375, 513, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1759, 21.7752, 513, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1760, 482, 513, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1761, 4, 514, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1762, 12.9986, 514, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1763, 275, 514, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1764, 22.7549, 514, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1765, 21.5775, 515, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1766, 23.4052, 515, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1767, 342, 515, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1768, 1, 515, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1769, 333, 516, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1770, 22.4851, 516, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1771, 27.1795, 516, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1772, 2, 516, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1773, 20.0148, 517, 2, 1, 9.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1774, 14.3022, 517, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1775, 436, 517, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1776, 5, 517, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1777, 279, 518, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1778, 22.1486, 518, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1779, 21.5176, 518, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1780, 4, 518, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1781, 24.2819, 519, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1782, 3, 519, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1783, 13.142, 519, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1784, 486, 520, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1785, 29.2669, 520, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1786, 2, 520, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1787, 23.6282, 520, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1788, 340, 521, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1789, 4, 521, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1790, 22.2392, 521, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1791, 20.5442, 521, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1792, 1, 522, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1793, 350, 522, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1794, 23.3585, 522, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1795, 21.1568, 522, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1796, 29.8458, 523, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1797, 21.8586, 523, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1798, 4, 523, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1799, 469, 523, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1800, 320, 524, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1801, 2, 524, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1802, 29.2166, 524, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1803, 24.5338, 524, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1804, 5, 525, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1805, 283, 525, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1806, 24.6424, 525, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1807, 27.9865, 525, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1808, 5, 526, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1809, 22.3533, 526, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1810, 23.9913, 527, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1811, 3, 527, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1812, 289, 528, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1813, 20.9466, 528, 2, 1, 12.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1814, 21.1803, 528, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1815, 1, 528, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1816, 20.939, 529, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1817, 24.6312, 529, 2, 1, 12.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1818, 4, 529, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1819, 354, 529, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1820, 10.3037, 530, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1821, 25.7538, 530, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1822, 261, 530, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1823, 1, 530, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1824, 3, 531, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1825, 27.6669, 531, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1826, 319, 531, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1827, 22.8825, 531, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1828, 24.8501, 532, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1829, 367, 532, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1830, 1, 532, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1831, 23.3557, 532, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1832, 3, 533, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1833, 286, 533, 3, 1, 200.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1834, 26.2919, 533, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1835, 12.9783, 533, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1836, 13.7826, 534, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1837, 369, 534, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1838, 22.0391, 534, 2, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1839, 5, 534, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1840, 3, 535, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1841, 479, 535, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1842, 21.8687, 535, 2, 1, 12.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1843, 24.4963, 535, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1844, 1, 536, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1845, 20.3649, 536, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1846, 410, 536, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1847, 16.9335, 536, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1848, 17.9945, 537, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1849, 29.6789, 537, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1850, 469, 537, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1851, 4, 537, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1852, 274, 538, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1853, 27.7517, 538, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1854, 2, 538, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1855, 16.2844, 538, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1856, 409, 539, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1857, 17.7795, 539, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1858, 21.5952, 539, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1859, 2, 539, 1, 1, 65.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1860, 1, 540, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1861, 25.6521, 540, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1862, 376, 540, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1863, 19.7432, 540, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1864, 2, 541, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1865, 344, 541, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1866, 22.2878, 541, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1867, 17.4835, 541, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1868, 425, 542, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1869, 5, 542, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1870, 24.292, 542, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1871, 24.0278, 542, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1872, 4, 543, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1873, 11.4362, 543, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1874, 495, 543, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1875, 24.7791, 543, 2, 1, 13.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1876, 24.5288, 544, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1877, 448, 544, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1878, 20.5709, 544, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1879, 5, 544, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1880, 25.0854, 545, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1881, 5, 545, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1882, 431, 545, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1883, 24.6933, 545, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1884, 21.2511, 546, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1885, 26.2148, 546, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1886, 2, 546, 1, 1, 72.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1887, 275, 546, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1888, 29.7826, 547, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1889, 20.6089, 547, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1890, 290, 547, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1891, 1, 547, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1892, 1, 548, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1893, 15.167, 548, 0, 1, 7.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1894, 461, 548, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1895, 26.4938, 548, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1896, 406, 549, 3, 1, 162.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1897, 10.6913, 549, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1898, 27.8109, 549, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1899, 3, 549, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1900, 415, 550, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1901, 5, 550, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1902, 25.1057, 550, 2, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1903, 19.1675, 550, 0, 1, 7.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1904, 15.4227, 551, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1905, 21.4117, 551, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1906, 3, 551, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1907, 5, 552, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1908, 330, 552, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1909, 15.8575, 552, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1910, 26.3411, 552, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1911, 23.7957, 553, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1912, 389, 553, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1913, 3, 553, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1914, 28.3648, 553, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1915, 21.7187, 554, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1916, 5, 554, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1917, 15.0621, 554, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1918, 326, 554, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1919, 21.3769, 555, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1920, 29.6061, 555, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1921, 361, 555, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1922, 4, 555, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1923, 2, 556, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1924, 461, 556, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1925, 24.5219, 556, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1926, 24.0882, 556, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1927, 25.1976, 557, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1928, 24.3763, 558, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1929, 25.8516, 558, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1930, 263, 558, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1931, 3, 558, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1932, 22.487, 559, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1933, 343, 559, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1934, 29.233, 560, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1935, 24.4868, 560, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1936, 464, 560, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1937, 2, 560, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1938, 27.3917, 561, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1939, 3, 561, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1940, 300, 561, 3, 1, 217.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1941, 18.2031, 561, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1942, 20.2771, 562, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1943, 21.179, 562, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1944, 16.4324, 563, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1945, 28.836, 563, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1946, 16.0521, 564, 0, 1, 6.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1947, 261, 564, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1948, 2, 564, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1949, 27.03, 564, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1950, 23.7939, 565, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1951, 17.0568, 565, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1952, 334, 565, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1953, 4, 565, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1954, 23.8762, 566, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1955, 256, 566, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1956, 22.0739, 566, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1957, 5, 566, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1958, 325, 567, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1959, 22.907, 567, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1960, 24.9351, 567, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1961, 3, 567, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1962, 447, 568, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1963, 4, 568, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1964, 20.8046, 568, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1965, 14.3129, 568, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1966, 23.4876, 569, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1967, 417, 569, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1968, 5, 569, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1969, 27.4598, 569, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1970, 1, 570, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1971, 29.3504, 570, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1972, 287, 570, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1973, 11.4525, 570, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1974, 454, 571, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1975, 21.6443, 571, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1976, 29.193, 571, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1977, 2, 571, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1978, 5, 572, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1979, 21.8052, 573, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1980, 3, 573, 1, 1, 71.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1981, 3, 574, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1982, 15.6797, 574, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1983, 20.2088, 575, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1984, 22.6549, 575, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1985, 252, 575, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1986, 1, 575, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1987, 475, 576, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1988, 5, 576, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1989, 17.9008, 576, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1990, 21.6151, 576, 2, 1, 13.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1991, 370, 577, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1992, 3, 577, 1, 1, 87.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1993, 19.8374, 577, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1994, 29.7255, 577, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1995, 19.3537, 578, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1996, 313, 578, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1997, 22.0991, 578, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1998, 3, 578, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1999, 23.4814, 579, 0, 1, 8.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2000, 5, 579, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2001, 21.5137, 579, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2002, 384, 579, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2003, 25.8887, 580, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2004, 389, 580, 3, 1, 160.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2005, 22.832, 580, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2006, 4, 580, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2007, 17.6811, 581, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2008, 359, 581, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2009, 2, 581, 1, 1, 68.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2010, 23.4911, 581, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2011, 352, 582, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2012, 14.9692, 582, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2013, 27.0328, 582, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2014, 4, 582, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2015, 16.067, 583, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2016, 2, 583, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2017, 26.2818, 583, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2018, 457, 583, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2019, 10.6629, 584, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2020, 5, 584, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2021, 27.4485, 584, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2022, 408, 584, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2023, 4, 585, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2024, 14.6365, 585, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2025, 1, 586, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2026, 26.2673, 586, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2027, 12.4415, 587, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2028, 1, 587, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2029, 26.0521, 587, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2030, 285, 587, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2031, 5, 588, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2032, 301, 588, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2033, 26.6621, 588, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2034, 28.5605, 589, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2035, 359, 589, 3, 1, 192.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2036, 17.4243, 590, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2037, 5, 590, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2038, 298, 590, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2039, 21.1537, 590, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2040, 11.1103, 591, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2041, 484, 591, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2042, 2, 591, 1, 1, 86.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2043, 21.647, 591, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2044, 4, 592, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2045, 334, 592, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2046, 23.2405, 592, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2047, 17.3526, 592, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2048, 22.3684, 593, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2049, 17.3796, 593, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2050, 16.9488, 594, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2051, 3, 594, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2052, 27.0088, 595, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2053, 3, 595, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2054, 402, 595, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2055, 23.0445, 595, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2056, 22.2243, 596, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2057, 4, 597, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2058, 18.6984, 597, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2059, 366, 597, 3, 1, 165.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2060, 27.9575, 597, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2061, 19.5379, 598, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2062, 472, 598, 3, 1, 160.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2063, 20.2921, 598, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2064, 5, 598, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2065, 12.0884, 599, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2066, 458, 599, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2067, 25.0879, 599, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2068, 2, 599, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2069, 22.0305, 600, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2070, 27.9265, 601, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2071, 19.0122, 601, 0, 1, 6.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2072, 4, 601, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2073, 23.1408, 602, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2074, 438, 602, 3, 1, 175.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2075, 23.134, 602, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2076, 25.1118, 603, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2077, 22.2202, 603, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2078, 254, 603, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2079, 5, 604, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2080, 22.8379, 604, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2081, 10.5829, 604, 0, 1, 8.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2082, 254, 604, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2083, 319, 605, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2084, 3, 606, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2085, 262, 606, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2086, 15.3712, 606, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2087, 23.4384, 606, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2088, 26.5294, 607, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2089, 2, 607, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2090, 1, 608, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2091, 368, 608, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2092, 11.0473, 608, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2093, 3, 609, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2094, 14.8872, 609, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2095, 479, 609, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2096, 28.7436, 609, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2097, 23.5752, 610, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2098, 384, 610, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2099, 16.6037, 610, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2100, 1, 610, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2101, 320, 611, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2102, 5, 611, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2103, 24.4321, 611, 2, 1, 11.85)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2104, 269, 612, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2105, 4, 612, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2106, 17.5567, 612, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2107, 21.619, 612, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2108, 274, 613, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2109, 28.3519, 613, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2110, 5, 613, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2111, 16.4155, 613, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2112, 4, 614, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2113, 22.4353, 614, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2114, 352, 614, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2115, 22.463, 614, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2116, 311, 615, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2117, 5, 616, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2118, 28.9962, 616, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2119, 22.3539, 616, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2120, 435, 616, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2121, 1, 617, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2122, 19.7991, 617, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2123, 3, 618, 1, 1, 88.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2124, 381, 618, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2125, 23.2922, 618, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2126, 28.7847, 618, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2127, 14.1995, 619, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2128, 256, 619, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2129, 1, 619, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2130, 22.4124, 619, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2131, 366, 620, 3, 1, 182.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2132, 27.42, 620, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2133, 22.7258, 620, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2134, 5, 620, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2135, 23.9884, 621, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2136, 19.2962, 621, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2137, 2, 621, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2138, 476, 621, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2139, 29.5518, 622, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2140, 362, 622, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2141, 20.2849, 622, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2142, 1, 622, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2143, 4, 623, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2144, 29.6468, 623, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2145, 17.168, 624, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2146, 5, 624, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2147, 12.9551, 625, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2148, 468, 625, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2149, 21.624, 625, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2150, 1, 626, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2151, 21.5993, 626, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2152, 10.6551, 626, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2153, 354, 626, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2154, 19.9184, 627, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2155, 24.0783, 627, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2156, 418, 627, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2157, 1, 627, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2158, 28.0225, 628, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2159, 2, 629, 1, 1, 79.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2160, 498, 629, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2161, 268, 630, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2162, 11.8332, 630, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2163, 4, 630, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2164, 20.9797, 630, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2165, 19.913, 631, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2166, 403, 631, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2167, 5, 631, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2168, 27.0536, 631, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2169, 20.641, 632, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2170, 5, 632, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2171, 11.4602, 632, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2172, 281, 632, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2173, 26.1406, 633, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2174, 12.5243, 633, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2175, 22.1384, 634, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2176, 4, 634, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2177, 1, 635, 1, 1, 88.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2178, 14.2228, 635, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2179, 28.6736, 635, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2180, 251, 635, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2181, 4, 636, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2182, 17.3803, 636, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2183, 367, 636, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2184, 20.1356, 636, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2185, 25.6327, 637, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2186, 4, 637, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2187, 411, 637, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2188, 10.6362, 637, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2189, 355, 638, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2190, 21.2286, 638, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2191, 1, 638, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2192, 10.1044, 638, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2193, 29.8389, 639, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2194, 19.6487, 639, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2195, 4, 639, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2196, 338, 639, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2197, 24.5539, 640, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2198, 25.4716, 640, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2199, 1, 641, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2200, 20.7669, 641, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2201, 12.7564, 642, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2202, 3, 643, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2203, 448, 643, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2204, 24.5226, 643, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2205, 23.969, 643, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2206, 1, 644, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2207, 21.8378, 644, 2, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2208, 19.472, 644, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2209, 427, 644, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2210, 5, 645, 1, 1, 73.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2211, 356, 645, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2212, 22.0796, 645, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2213, 1, 646, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2214, 23.5355, 646, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2215, 458, 647, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2216, 1, 647, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2217, 16.0514, 647, 0, 1, 7.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2218, 24.9432, 647, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2219, 21.9581, 648, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2220, 5, 648, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2221, 482, 648, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2222, 24.2594, 648, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2223, 13.6927, 649, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2224, 22.3351, 650, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2225, 5, 650, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2226, 432, 650, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2227, 26.7115, 650, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2228, 321, 651, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2229, 22.0115, 651, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2230, 27.1042, 651, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2231, 2, 651, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2232, 18.559, 652, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2233, 361, 653, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2234, 4, 653, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2235, 14.1696, 653, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2236, 28.4926, 653, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2237, 281, 654, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2238, 15.1446, 654, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2239, 5, 654, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2240, 23.3967, 654, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2241, 254, 655, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2242, 18.4783, 655, 0, 1, 8.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2243, 26.4541, 655, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2244, 4, 655, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2245, 24.1209, 656, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2246, 29.2914, 656, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2247, 23.9784, 657, 0, 1, 7.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2248, 339, 657, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2249, 2, 657, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2250, 23.7381, 657, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2251, 5, 658, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2252, 256, 658, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2253, 23.8127, 659, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2254, 3, 659, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2255, 25.3074, 659, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2256, 492, 659, 3, 1, 167.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2257, 3, 660, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2258, 11.277, 660, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2259, 5, 661, 1, 1, 88.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2260, 25.4988, 661, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2261, 369, 661, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2262, 12.0661, 661, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2263, 4, 662, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2264, 26.5919, 662, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2265, 19.5207, 662, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2266, 375, 662, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2267, 381, 663, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2268, 25.4278, 663, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2269, 2, 663, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2270, 18.7868, 663, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2271, 5, 664, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2272, 21.8102, 664, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2273, 353, 664, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2274, 17.5002, 664, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2275, 377, 665, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2276, 23.7938, 665, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2277, 23.5465, 665, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2278, 4, 665, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2279, 262, 666, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2280, 1, 666, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2281, 21.364, 666, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2282, 19.0342, 666, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2283, 331, 667, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2284, 21.9582, 667, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2285, 1, 668, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2286, 27.3997, 668, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2287, 478, 668, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2288, 21.1441, 668, 0, 1, 8.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2289, 11.605, 669, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2290, 21.3163, 669, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2291, 452, 669, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2292, 1, 669, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2293, 20.1554, 670, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2294, 4, 670, 1, 1, 72.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2295, 20.9854, 670, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2296, 23.7449, 671, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2297, 17.4183, 672, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2298, 355, 673, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2299, 27.5934, 673, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2300, 5, 673, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2301, 18.6049, 673, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2302, 5, 674, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2303, 10.9973, 674, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2304, 24.2701, 674, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2305, 483, 674, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2306, 1, 675, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2307, 24.8585, 676, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2308, 25.7274, 676, 2, 1, 13.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2309, 355, 676, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2310, 4, 676, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2311, 423, 677, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2312, 4, 677, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2313, 15.279, 677, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2314, 28.9859, 677, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2315, 401, 678, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2316, 25.4922, 678, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2317, 16.7739, 678, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2318, 3, 678, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2319, 22.0004, 679, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2320, 24.0369, 679, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2321, 454, 679, 3, 1, 165.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2322, 5, 679, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2323, 11.7544, 680, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2324, 5, 680, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2325, 470, 680, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2326, 23.3226, 680, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2327, 2, 681, 1, 1, 79.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2328, 24.0927, 681, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2329, 455, 681, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2330, 17.4511, 681, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2331, 1, 682, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2332, 13.7475, 682, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2333, 333, 682, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2334, 22.3046, 682, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2335, 386, 683, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2336, 20.6183, 683, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2337, 29.9, 683, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2338, 3, 683, 1, 1, 73.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2339, 20.1385, 684, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2340, 5, 684, 1, 1, 72.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2341, 340, 684, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2342, 20.2605, 684, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2343, 3, 685, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2344, 451, 685, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2345, 12.8619, 685, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2346, 24.3452, 685, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2347, 26.4966, 686, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2348, 20.1276, 687, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2349, 11.9141, 687, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2350, 3, 687, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2351, 335, 687, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2352, 1, 688, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2353, 485, 689, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2354, 22.0216, 689, 0, 1, 8.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2355, 26.0692, 689, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2356, 4, 689, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2357, 1, 690, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2358, 21.654, 690, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2359, 23.3625, 690, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2360, 485, 690, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2361, 16.9435, 691, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2362, 24.7181, 692, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2363, 19.1977, 693, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2364, 4, 694, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2365, 28.7309, 694, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2366, 23.7559, 694, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2367, 297, 694, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2368, 492, 695, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2369, 22.7746, 695, 2, 1, 12.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2370, 4, 695, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2371, 14.1226, 695, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2372, 361, 696, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2373, 27.0748, 696, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2374, 13.5346, 696, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2375, 3, 696, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2376, 19.9348, 697, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2377, 484, 697, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2378, 4, 697, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2379, 20.0171, 697, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2380, 376, 698, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2381, 1, 698, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2382, 18.3019, 698, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2383, 22.8764, 699, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2384, 1, 699, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2385, 427, 699, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2386, 21.0601, 699, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2387, 21.1771, 700, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2388, 468, 700, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2389, 18.2806, 700, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2390, 4, 700, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2391, 23.2124, 701, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2392, 425, 701, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2393, 3, 701, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2394, 20.2093, 701, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2395, 24.5888, 702, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2396, 16.3778, 702, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2397, 401, 702, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2398, 2, 703, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2399, 11.4752, 703, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2400, 25.0446, 703, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2401, 294, 703, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2402, 24.7146, 704, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2403, 352, 704, 3, 1, 210.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2404, 1, 704, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2405, 13.546, 704, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2406, 16.5811, 705, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2407, 312, 705, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2408, 25.5177, 705, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2409, 28.6791, 706, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2410, 362, 706, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2411, 19.1149, 706, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2412, 2, 706, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2413, 18.9828, 707, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2414, 20.023, 707, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2415, 395, 707, 3, 1, 217.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2416, 5, 707, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2417, 26.487, 708, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2418, 253, 708, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2419, 14.1713, 708, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2420, 4, 708, 1, 1, 68.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2421, 4, 709, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2422, 23.1349, 709, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2423, 22.1293, 709, 2, 1, 10.65)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2424, 285, 709, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2425, 17.1434, 710, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2426, 367, 710, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2427, 27.7116, 710, 2, 0, NULL)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (0, 0, NULL, 1, 2, 2)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (1, 0, NULL, 1, 1, 10)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (2, 1, 7, 0, NULL, 12)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (3, 1, 5, 0, NULL, 51)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (4, 1, 10, 0, NULL, 66)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (5, 0, NULL, 1, 1, 81)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (6, 1, 3, 0, NULL, 109)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (7, 1, 5, 0, NULL, 112)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (8, 0, NULL, 1, 2, 118)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (9, 0, NULL, 1, 2, 121)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (10, 0, NULL, 1, 2, 124)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (11, 1, 7, 1, 2, 132)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (12, 0, NULL, 1, 2, 134)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (13, 0, NULL, 1, 2, 159)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (14, 0, NULL, 1, 1, 160)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (15, 1, 2, 0, NULL, 172)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (16, 0, NULL, 1, 1, 182)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (17, 1, 6, 0, NULL, 183)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (18, 1, 4, 0, NULL, 187)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (19, 1, 3, 0, NULL, 203)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (20, 0, NULL, 1, 2, 204)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (21, 0, NULL, 1, 2, 214)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (22, 1, 4, 0, NULL, 221)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (23, 1, 2, 0, NULL, 229)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (24, 1, 6, 1, 2, 276)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (25, 0, NULL, 1, 1, 278)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (26, 1, 5, 0, NULL, 280)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (27, 0, NULL, 1, 2, 298)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (28, 1, 7, 0, NULL, 306)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (29, 0, NULL, 1, 1, 309)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (30, 1, 3, 0, NULL, 311)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (31, 0, NULL, 1, 1, 330)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (32, 1, 1, 0, NULL, 343)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (33, 1, 8, 0, NULL, 364)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (34, 0, NULL, 1, 2, 368)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (35, 1, 3, 0, NULL, 372)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (36, 0, NULL, 1, 1, 375)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (37, 1, 7, 0, NULL, 381)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (38, 1, 4, 0, NULL, 408)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (39, 1, 4, 0, NULL, 418)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (40, 1, 7, 0, NULL, 441)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (41, 1, 4, 0, NULL, 442)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (42, 1, 5, 0, NULL, 446)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (43, 0, NULL, 1, 1, 460)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (44, 1, 3, 0, NULL, 470)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (45, 0, NULL, 1, 2, 471)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (46, 1, 2, 1, 2, 484)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (47, 1, 1, 0, NULL, 497)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (48, 0, NULL, 1, 1, 503)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (49, 0, NULL, 1, 1, 518)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (50, 1, 2, 0, NULL, 527)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (51, 1, 7, 1, 1, 547)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (52, 0, NULL, 1, 1, 564)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (53, 1, 2, 0, NULL, 571)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (54, 1, 9, 0, NULL, 574)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (55, 0, NULL, 1, 1, 578)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (56, 1, 8, 0, NULL, 601)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (57, 1, 5, 0, NULL, 606)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (58, 1, 2, 0, NULL, 625)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (59, 1, 1, 1, 1, 629)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (60, 0, NULL, 1, 1, 634)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (61, 0, NULL, 1, 1, 635)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (62, 0, NULL, 1, 2, 652)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (63, 1, 10, 0, NULL, 681)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (64, 0, NULL, 1, 2, 695)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (65, 0, NULL, 1, 1, 697)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (66, 0, NULL, 1, 1, 707)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (67, 1, 1, 0, NULL, 710)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (68, 1, 2, 0, NULL, 764)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (69, 1, 1, 0, NULL, 770)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (70, 0, NULL, 1, 1, 776)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (71, 0, NULL, 1, 2, 780)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (72, 0, NULL, 1, 2, 801)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (73, 0, NULL, 1, 1, 814)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (74, 0, NULL, 1, 2, 818)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (75, 1, 6, 0, NULL, 822)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (76, 1, 5, 0, NULL, 823)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (77, 1, 10, 0, NULL, 836)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (78, 0, NULL, 1, 2, 855)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (79, 0, NULL, 1, 1, 860)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (80, 1, 6, 0, NULL, 864)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (81, 0, NULL, 1, 1, 869)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (82, 1, 8, 0, NULL, 895)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (83, 1, 2, 0, NULL, 897)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (84, 1, 3, 0, NULL, 902)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (85, 1, 8, 0, NULL, 911)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (86, 1, 7, 0, NULL, 938)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (87, 0, NULL, 1, 1, 943)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (88, 1, 10, 0, NULL, 947)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (89, 1, 1, 0, NULL, 948)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (90, 1, 8, 0, NULL, 966)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (91, 0, NULL, 1, 1, 983)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (92, 0, NULL, 1, 1, 989)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (93, 0, NULL, 1, 2, 996)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (94, 0, NULL, 1, 2, 1000)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (95, 0, NULL, 1, 1, 1010)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (96, 0, NULL, 1, 1, 1011)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (97, 1, 3, 0, NULL, 1029)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (98, 1, 4, 0, NULL, 1031)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (99, 1, 1, 0, NULL, 1052)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (100, 0, NULL, 1, 2, 1057)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (101, 0, NULL, 1, 2, 1065)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (102, 0, NULL, 1, 1, 1069)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (103, 1, 5, 0, NULL, 1073)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (104, 0, NULL, 1, 1, 1079)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (105, 1, 6, 0, NULL, 1080)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (106, 0, NULL, 1, 2, 1085)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (107, 1, 7, 1, 1, 1096)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (108, 1, 1, 0, NULL, 1097)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (109, 0, NULL, 1, 2, 1098)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (110, 1, 1, 0, NULL, 1099)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (111, 1, 7, 0, NULL, 1101)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (112, 1, 2, 0, NULL, 1105)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (113, 1, 8, 0, NULL, 1106)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (114, 1, 1, 0, NULL, 1127)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (115, 1, 2, 0, NULL, 1138)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (116, 1, 7, 0, NULL, 1143)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (117, 0, NULL, 1, 2, 1172)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (118, 1, 6, 0, NULL, 1190)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (119, 0, NULL, 1, 1, 1196)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (120, 0, NULL, 1, 1, 1206)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (121, 1, 1, 0, NULL, 1210)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (122, 1, 10, 0, NULL, 1222)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (123, 1, 1, 0, NULL, 1226)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (124, 1, 1, 0, NULL, 1231)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (125, 0, NULL, 1, 2, 1242)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (126, 0, NULL, 1, 1, 1249)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (127, 1, 2, 0, NULL, 1251)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (128, 0, NULL, 1, 1, 1259)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (129, 0, NULL, 1, 1, 1276)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (130, 0, NULL, 1, 2, 1284)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (131, 1, 2, 0, NULL, 1288)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (132, 1, 3, 0, NULL, 1289)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (133, 1, 10, 0, NULL, 1291)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (134, 0, NULL, 1, 1, 1310)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (135, 1, 1, 0, NULL, 1324)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (136, 0, NULL, 1, 2, 1349)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (137, 1, 8, 0, NULL, 1350)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (138, 0, NULL, 1, 2, 1353)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (139, 1, 2, 0, NULL, 1363)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (140, 1, 1, 0, NULL, 1369)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (141, 1, 1, 0, NULL, 1371)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (142, 1, 1, 0, NULL, 1378)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (143, 0, NULL, 1, 1, 1389)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (144, 1, 7, 0, NULL, 1395)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (145, 0, NULL, 1, 2, 1404)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (146, 0, NULL, 1, 2, 1406)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (147, 1, 1, 0, NULL, 1408)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (148, 1, 2, 0, NULL, 1436)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (149, 1, 3, 0, NULL, 1439)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (150, 1, 3, 0, NULL, 1471)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (151, 0, NULL, 1, 2, 1472)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (152, 1, 5, 1, 1, 1482)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (153, 1, 1, 0, NULL, 1501)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (154, 1, 6, 0, NULL, 1508)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (155, 0, NULL, 1, 2, 1511)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (156, 1, 8, 0, NULL, 1520)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (157, 0, NULL, 1, 1, 1528)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (158, 1, 4, 0, NULL, 1540)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (159, 1, 3, 0, NULL, 1548)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (160, 0, NULL, 1, 2, 1550)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (161, 1, 7, 0, NULL, 1553)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (162, 0, NULL, 1, 2, 1560)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (163, 1, 2, 0, NULL, 1575)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (164, 1, 10, 0, NULL, 1585)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (165, 0, NULL, 1, 1, 1586)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (166, 0, NULL, 1, 1, 1637)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (167, 0, NULL, 1, 1, 1651)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (168, 0, NULL, 1, 2, 1663)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (169, 0, NULL, 1, 2, 1671)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (170, 1, 2, 0, NULL, 1688)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (171, 0, NULL, 1, 2, 1692)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (172, 1, 10, 0, NULL, 1721)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (173, 0, NULL, 1, 1, 1726)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (174, 0, NULL, 1, 1, 1736)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (175, 1, 4, 0, NULL, 1738)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (176, 0, NULL, 1, 1, 1743)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (177, 0, NULL, 1, 1, 1767)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (178, 0, NULL, 1, 1, 1772)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (179, 0, NULL, 1, 2, 1803)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (180, 0, NULL, 1, 2, 1817)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (181, 1, 3, 0, NULL, 1826)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (182, 1, 9, 0, NULL, 1827)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (183, 1, 10, 0, NULL, 1828)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (184, 0, NULL, 1, 2, 1836)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (185, 1, 3, 0, NULL, 1847)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (186, 1, 1, 0, NULL, 1851)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (187, 1, 10, 1, 1, 1858)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (188, 1, 1, 0, NULL, 1868)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (189, 1, 3, 0, NULL, 1884)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (190, 1, 2, 0, NULL, 1886)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (191, 1, 4, 0, NULL, 1915)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (192, 1, 9, 0, NULL, 1926)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (193, 0, NULL, 1, 1, 1927)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (194, 0, NULL, 1, 1, 1935)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (195, 1, 2, 0, NULL, 1941)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (196, 1, 1, 0, NULL, 1950)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (197, 1, 1, 0, NULL, 1973)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (198, 1, 3, 0, NULL, 1992)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (199, 1, 10, 0, NULL, 1994)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (200, 1, 1, 0, NULL, 1998)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (201, 1, 9, 0, NULL, 2010)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (202, 0, NULL, 1, 2, 2021)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (203, 1, 6, 0, NULL, 2022)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (204, 0, NULL, 1, 2, 2024)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (205, 0, NULL, 1, 2, 2039)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (206, 0, NULL, 1, 1, 2059)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (207, 0, NULL, 1, 1, 2062)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (208, 1, 3, 0, NULL, 2074)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (209, 1, 8, 0, NULL, 2076)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (210, 1, 1, 0, NULL, 2077)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (211, 1, 1, 0, NULL, 2082)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (212, 1, 7, 0, NULL, 2098)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (213, 1, 2, 0, NULL, 2107)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (214, 0, NULL, 1, 1, 2109)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (215, 1, 5, 0, NULL, 2110)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (216, 0, NULL, 1, 1, 2118)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (217, 0, NULL, 1, 1, 2124)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (218, 1, 1, 0, NULL, 2129)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (219, 0, NULL, 1, 1, 2132)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (220, 1, 5, 0, NULL, 2140)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (221, 0, NULL, 1, 1, 2152)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (222, 0, NULL, 1, 1, 2167)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (223, 0, NULL, 1, 1, 2172)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (224, 0, NULL, 1, 1, 2175)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (225, 1, 3, 0, NULL, 2181)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (226, 1, 1, 0, NULL, 2189)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (227, 1, 8, 1, 1, 2190)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (228, 1, 8, 0, NULL, 2204)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (229, 0, NULL, 1, 1, 2218)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (230, 1, 5, 0, NULL, 2232)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (231, 0, NULL, 1, 1, 2255)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (232, 1, 7, 0, NULL, 2275)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (233, 0, NULL, 1, 1, 2284)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (234, 1, 10, 0, NULL, 2287)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (235, 1, 9, 0, NULL, 2293)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (236, 1, 4, 0, NULL, 2300)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (237, 1, 6, 0, NULL, 2320)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (238, 0, NULL, 1, 1, 2336)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (239, 1, 2, 0, NULL, 2342)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (240, 0, NULL, 1, 2, 2361)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (241, 0, NULL, 1, 1, 2365)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (242, 0, NULL, 1, 1, 2373)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (243, 1, 1, 0, NULL, 2378)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (244, 1, 8, 0, NULL, 2385)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (245, 1, 2, 0, NULL, 2402)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (246, 1, 6, 0, NULL, 2424)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (247, 1, 2, 0, NULL, 2425)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (0, 27.4453, 0, NULL, "2001-00-18", 0, NULL, 6)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1, 1, 0, NULL, "2001-00-18", 0, NULL, 7)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (2, 377, 0, NULL, "2001-00-18", 0, NULL, 8)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (3, 10.6435, 1, "2002-00-18", "2001-00-18", 1, NULL, 9)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (4, 0, 0, NULL, "2001-00-06", 0, NULL, 10)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (5, 419, 0, NULL, "2001-00-06", 0, NULL, 11)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (6, 20.5565, 0, NULL, "2001-00-06", 0, NULL, 12)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (7, 15.006, 1, "2002-00-06", "2001-00-06", 1, NULL, 13)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (8, 10.847, 1, "2002-00-19", "2001-00-19", 1, NULL, 14)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (9, 2, 0, NULL, "2001-00-19", 0, NULL, 15)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (10, 20.8634, 0, NULL, "2001-00-19", 0, NULL, 16)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (11, 279, 0, NULL, "2001-00-19", 0, NULL, 17)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (12, 24.9096, 0, NULL, "2001-01-19", 0, NULL, 18)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (13, 1, 0, NULL, "2001-01-20", 0, NULL, 19)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (14, 5, 0, NULL, "2001-01-03", 0, NULL, 20)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (15, 400, 0, NULL, "2001-01-03", 0, NULL, 21)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (16, 17.4416, 1, "2002-01-03", "2001-01-03", 1, NULL, 22)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (17, 4, 0, NULL, "2001-01-20", 0, NULL, 31)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (18, 26.0064, 0, NULL, "2001-01-20", 0, NULL, 32)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (19, 20.4397, 1, "2002-01-20", "2001-01-20", 1, NULL, 33)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (20, 388, 0, NULL, "2001-01-20", 0, NULL, 34)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (21, 20.6389, 1, "2002-01-24", "2001-01-24", 1, NULL, 35)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (22, 28.8775, 0, NULL, "2001-01-24", 0, NULL, 36)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (23, 5, 0, NULL, "2001-01-24", 0, NULL, 37)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (24, 364, 0, NULL, "2001-02-29", 0, NULL, 38)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (25, 26.3408, 0, NULL, "2001-02-29", 0, NULL, 39)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (26, 20.4899, 1, "2002-02-18", "2001-02-18", 1, NULL, 44)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (27, 3, 0, NULL, "2001-02-18", 0, NULL, 45)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (28, 22.0987, 0, NULL, "2001-02-18", 0, NULL, 46)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (29, 425, 0, NULL, "2001-02-18", 0, NULL, 47)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (30, 23.0845, 0, NULL, "2001-02-05", 0, NULL, 48)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (31, 10.4335, 1, "2002-02-05", "2001-02-05", 1, NULL, 49)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (32, 361, 0, NULL, "2001-02-05", 0, NULL, 50)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (33, 0, 0, NULL, "2001-02-05", 0, NULL, 51)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (34, 471, 0, NULL, "2001-03-01", 0, NULL, 52)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (35, 23.7646, 0, NULL, "2001-03-01", 0, NULL, 53)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (36, 4, 0, NULL, "2001-03-01", 0, NULL, 54)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (37, 20.3759, 1, "2002-03-01", "2001-03-01", 1, NULL, 55)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (38, 19.5109, 1, "2002-04-17", "2001-04-17", 1, NULL, 62)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (39, 406, 0, NULL, "2001-04-17", 0, NULL, 63)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (40, 464, 0, NULL, "2001-05-30", 0, NULL, 64)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (41, 1, 0, NULL, "2001-05-30", 0, NULL, 65)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (42, 14.882, 0, NULL, "2001-05-13", 0, NULL, 66)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (43, 351, 0, NULL, "2001-05-13", 0, NULL, 67)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (44, 18.5363, 1, "2002-05-13", "2001-05-13", 1, NULL, 68)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (45, 4, 0, NULL, "2001-05-13", 0, NULL, 69)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (46, 4, 0, NULL, "2001-05-04", 0, NULL, 70)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (47, 23.526, 1, "2002-05-04", "2001-05-04", 1, NULL, 71)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (48, 369, 0, NULL, "2001-05-04", 0, NULL, 72)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (49, 24.1179, 0, NULL, "2001-05-04", 0, NULL, 73)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (50, 22.5032, 0, NULL, "2001-06-23", 0, NULL, 80)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (51, 19.1983, 1, "2002-06-23", "2001-06-23", 1, NULL, 81)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (52, 431, 0, NULL, "2001-06-23", 0, NULL, 82)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (53, 4, 0, NULL, "2001-06-23", 0, NULL, 83)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (54, 27.4622, 0, NULL, "2001-06-29", 0, NULL, 84)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (55, 22.7459, 1, "2002-06-29", "2001-06-29", 1, NULL, 85)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (56, 4, 0, NULL, "2001-06-29", 0, NULL, 86)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (57, 485, 0, NULL, "2001-06-29", 0, NULL, 87)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (58, 401, 0, NULL, "2001-06-01", 0, NULL, 88)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (59, 1, 0, NULL, "2001-06-01", 0, NULL, 89)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (60, 19.882, 1, "2002-06-01", "2001-06-01", 1, NULL, 90)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (61, 20.047, 0, NULL, "2001-06-01", 0, NULL, 91)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (62, 1, 0, NULL, "2001-06-14", 0, NULL, 92)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (63, 11.7085, 1, "2002-06-14", "2001-06-14", 1, NULL, 93)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (64, 293, 0, NULL, "2001-06-14", 0, NULL, 94)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (65, 22.8875, 0, NULL, "2001-06-14", 0, NULL, 95)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (66, 13.8605, 1, "2002-06-28", "2001-06-28", 1, NULL, 96)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (67, 26.7381, 0, NULL, "2001-06-28", 0, NULL, 97)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (68, 1, 0, NULL, "2001-06-28", 0, NULL, 98)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (69, 387, 0, NULL, "2001-06-28", 0, NULL, 99)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (70, 2, 0, NULL, "2001-07-31", 0, NULL, 102)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (71, 26.902, 0, NULL, "2001-07-09", 0, NULL, 103)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (72, 1, 0, NULL, "2001-07-09", 0, NULL, 104)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (73, 269, 0, NULL, "2001-07-09", 0, NULL, 105)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (74, 12.0826, 1, "2002-07-09", "2001-07-09", 1, NULL, 106)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (75, 21.6339, 0, NULL, "2001-07-27", 0, NULL, 107)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (76, 17.6932, 1, "2002-07-27", "2001-07-27", 1, NULL, 108)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (77, 1, 0, NULL, "2001-07-27", 0, NULL, 109)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (78, 419, 0, NULL, "2001-07-27", 0, NULL, 110)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (79, 25.5712, 0, NULL, "2001-08-12", 0, NULL, 111)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (80, 271, 0, NULL, "2001-08-12", 0, NULL, 112)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (81, 15.8725, 1, "2002-08-12", "2001-08-12", 1, NULL, 113)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (82, 4, 0, NULL, "2001-08-12", 0, NULL, 114)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (83, 3, 0, NULL, "2001-09-05", 0, NULL, 131)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (84, 450, 0, NULL, "2001-09-05", 0, NULL, 132)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (85, 16.3413, 1, "2002-09-05", "2001-09-05", 1, NULL, 133)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (86, 26.5957, 0, NULL, "2001-09-05", 0, NULL, 134)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (87, 23.8043, 0, NULL, "2001-09-11", 0, NULL, 139)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (88, 12.2034, 1, "2002-09-11", "2001-09-11", 1, NULL, 140)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (89, 3, 0, NULL, "2001-09-11", 0, NULL, 141)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (90, 296, 0, NULL, "2001-09-11", 0, NULL, 142)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (91, 5, 0, NULL, "2001-09-31", 0, NULL, 143)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (92, 23.2886, 1, "2002-09-31", "2001-09-31", 1, NULL, 144)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (93, 1, 0, NULL, "2001-09-11", 0, NULL, 145)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (94, 17.612, 1, "2002-09-11", "2001-09-11", 1, NULL, 146)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (95, 415, 0, NULL, "2001-09-11", 0, NULL, 147)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (96, 28.4583, 0, NULL, "2001-09-11", 0, NULL, 148)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (97, 449, 0, NULL, "2001-10-10", 0, NULL, 156)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (98, 26.2361, 0, NULL, "2001-10-10", 0, NULL, 157)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (99, 3, 0, NULL, "2001-10-10", 0, NULL, 158)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (100, 21.1798, 1, "2002-10-10", "2001-10-10", 1, NULL, 159)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (101, 21.5619, 0, NULL, "2001-10-18", 0, NULL, 160)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (102, 21.8556, 1, "2002-10-10", "2001-10-10", 1, NULL, 161)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (103, 4, 0, NULL, "2001-10-10", 0, NULL, 162)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (104, 482, 0, NULL, "2001-10-10", 0, NULL, 163)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (105, 26.5065, 0, NULL, "2001-10-09", 0, NULL, 164)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (106, 498, 0, NULL, "2001-10-09", 0, NULL, 165)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (107, 5, 0, NULL, "2001-10-09", 0, NULL, 166)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (108, 16.2997, 1, "2002-10-09", "2001-10-09", 1, NULL, 167)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (109, 1, 0, NULL, "2001-11-04", 0, NULL, 168)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (110, 18.7342, 1, "2002-11-04", "2001-11-04", 1, NULL, 169)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (111, 24.3844, 0, NULL, "2001-11-04", 0, NULL, 170)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (112, 277, 0, NULL, "2001-11-04", 0, NULL, 171)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (113, 14.3675, 1, "2002-11-18", "2001-11-18", 1, NULL, 176)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (114, 5, 0, NULL, "2001-11-18", 0, NULL, 177)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (115, 15.1276, 1, "2002-11-18", "2001-11-18", 1, NULL, 178)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (116, 20.9025, 0, NULL, "2001-11-18", 0, NULL, 179)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (117, 370, 0, NULL, "2001-11-18", 0, NULL, 180)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (118, 22.0268, 0, NULL, "2001-11-04", 0, NULL, 181)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (119, 22.737, 1, "2002-11-04", "2001-11-04", 1, NULL, 182)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (120, 338, 0, NULL, "2001-11-07", 0, NULL, 183)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (121, 3, 0, NULL, "2001-11-07", 0, NULL, 184)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (122, 10.079, 1, "2002-11-07", "2001-11-07", 1, NULL, 185)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (123, 24.447, 0, NULL, "2001-11-07", 0, NULL, 186)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (124, 12.8931, 1, "2002-11-23", "2001-11-23", 1, NULL, 187)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (125, 4, 0, NULL, "2001-11-23", 0, NULL, 188)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (126, 390, 0, NULL, "2001-11-23", 0, NULL, 189)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (127, 24.5657, 0, NULL, "2001-11-23", 0, NULL, 190)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (128, 346, 0, NULL, "2002-00-09", 0, NULL, 191)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (129, 12.0095, 1, "2003-00-09", "2002-00-09", 1, NULL, 192)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (130, 21.9345, 0, NULL, "2002-00-09", 0, NULL, 193)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (131, 5, 0, NULL, "2002-00-09", 0, NULL, 194)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (132, 5, 0, NULL, "2002-00-07", 0, NULL, 195)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (133, 25.187, 0, NULL, "2002-00-07", 0, NULL, 196)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (134, 414, 0, NULL, "2002-00-07", 0, NULL, 197)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (135, 21.8978, 1, "2003-00-07", "2002-00-07", 1, NULL, 198)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (136, 12.9582, 1, "2003-00-18", "2002-00-18", 1, NULL, 199)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (137, 27.8015, 0, NULL, "2002-00-18", 0, NULL, 200)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (138, 434, 0, NULL, "2002-00-18", 0, NULL, 201)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (139, 4, 0, NULL, "2002-00-18", 0, NULL, 202)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (140, 1, 0, NULL, "2002-00-27", 0, NULL, 203)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (141, 9.8917, 1, "2003-00-27", "2002-00-27", 1, NULL, 204)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (142, 400, 0, NULL, "2002-00-27", 0, NULL, 205)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (143, 23.3763, 0, NULL, "2002-00-27", 0, NULL, 206)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (144, 1, 0, NULL, "2002-01-10", 0, NULL, 211)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (145, 254, 0, NULL, "2002-01-10", 0, NULL, 212)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (146, 15.2306, 1, "2003-01-10", "2002-01-10", 1, NULL, 213)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (147, 25.0458, 0, NULL, "2002-01-10", 0, NULL, 214)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (148, 26.6145, 0, NULL, "2002-01-12", 0, NULL, 215)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (149, 15.7846, 1, "2003-02-29", "2002-02-29", 1, NULL, 216)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (150, 1, 0, NULL, "2002-02-29", 0, NULL, 217)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (151, 261, 0, NULL, "2002-02-29", 0, NULL, 218)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (152, 27.6631, 0, NULL, "2002-02-29", 0, NULL, 219)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (153, 21.4546, 0, NULL, "2002-02-02", 0, NULL, 220)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (154, 0, 0, NULL, "2002-02-02", 0, NULL, 221)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (155, 20.3951, 1, "2003-02-24", "2002-02-24", 1, NULL, 222)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (156, 392, 0, NULL, "2002-02-24", 0, NULL, 223)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (157, 4, 0, NULL, "2002-02-24", 0, NULL, 224)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (158, 27.295, 0, NULL, "2002-02-24", 0, NULL, 225)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (159, 21.2579, 0, NULL, "2002-02-26", 0, NULL, 226)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (160, 2, 0, NULL, "2002-02-26", 0, NULL, 227)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (161, 263, 0, NULL, "2002-02-12", 0, NULL, 228)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (162, 29.1856, 0, NULL, "2002-03-21", 0, NULL, 232)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (163, 2, 0, NULL, "2002-03-21", 0, NULL, 233)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (164, 20.648, 0, NULL, "2002-04-22", 0, NULL, 234)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (165, 19.402, 1, "2003-04-22", "2002-04-22", 1, NULL, 235)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (166, 403, 0, NULL, "2002-04-22", 0, NULL, 236)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (167, 5, 0, NULL, "2002-04-22", 0, NULL, 237)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (168, 29.3108, 0, NULL, "2002-04-12", 0, NULL, 238)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (169, 5, 0, NULL, "2002-04-12", 0, NULL, 239)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (170, 19.0575, 1, "2003-04-12", "2002-04-12", 1, NULL, 240)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (171, 474, 0, NULL, "2002-04-12", 0, NULL, 241)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (172, 17.2834, 1, "2003-04-12", "2002-04-12", 1, NULL, 242)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (173, 21.4342, 0, NULL, "2002-04-12", 0, NULL, 243)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (174, 3, 0, NULL, "2002-04-12", 0, NULL, 244)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (175, 470, 0, NULL, "2002-04-12", 0, NULL, 245)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (176, 21.2928, 0, NULL, "2002-04-26", 0, NULL, 246)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (177, 21.6744, 1, "2003-04-26", "2002-04-26", 1, NULL, 247)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (178, 3, 0, NULL, "2002-05-21", 0, NULL, 248)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (179, 415, 0, NULL, "2002-05-21", 0, NULL, 249)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (180, 28.3122, 0, NULL, "2002-05-21", 0, NULL, 250)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (181, 14.3973, 1, "2003-05-21", "2002-05-21", 1, NULL, 251)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (182, 15.0322, 1, "2003-05-17", "2002-05-17", 1, NULL, 256)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (183, 28.7448, 0, NULL, "2002-05-17", 0, NULL, 257)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (184, 1, 0, NULL, "2002-05-17", 0, NULL, 258)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (185, 326, 0, NULL, "2002-05-17", 0, NULL, 259)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (186, 14.8409, 1, "2003-06-27", "2002-06-27", 1, NULL, 264)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (187, 24.0312, 0, NULL, "2002-06-27", 0, NULL, 265)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (188, 4, 0, NULL, "2002-06-27", 0, NULL, 266)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (189, 284, 0, NULL, "2002-06-27", 0, NULL, 267)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (190, 15.388, 1, "2003-06-01", "2002-06-01", 1, NULL, 268)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (191, 463, 0, NULL, "2002-06-01", 0, NULL, 269)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (192, 1, 0, NULL, "2002-06-01", 0, NULL, 270)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (193, 25.8168, 0, NULL, "2002-06-30", 0, NULL, 271)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (194, 17.3753, 1, "2003-06-30", "2002-06-30", 1, NULL, 272)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (195, 344, 0, NULL, "2002-06-30", 0, NULL, 273)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (196, 4, 0, NULL, "2002-06-30", 0, NULL, 274)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (197, 5, 0, NULL, "2002-06-14", 0, NULL, 275)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (198, 391, 0, NULL, "2002-06-14", 0, NULL, 276)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (199, 12.7116, 1, "2003-06-14", "2002-06-14", 1, NULL, 277)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (200, 24.091, 0, NULL, "2002-06-14", 0, NULL, 278)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (201, 27.2025, 0, NULL, "2002-06-11", 0, NULL, 279)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (202, 14.4191, 1, "2003-06-11", "2002-06-11", 1, NULL, 280)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (203, 3, 0, NULL, "2002-06-11", 0, NULL, 281)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (204, 317, 0, NULL, "2002-06-11", 0, NULL, 282)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (205, 373, 0, NULL, "2002-07-22", 0, NULL, 283)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (206, 23.1329, 1, "2003-07-22", "2002-07-22", 1, NULL, 284)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (207, 27.1532, 0, NULL, "2002-07-22", 0, NULL, 285)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (208, 3, 0, NULL, "2002-07-22", 0, NULL, 286)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (209, 16.6725, 1, "2003-07-31", "2002-07-31", 1, NULL, 287)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (210, 1, 0, NULL, "2002-07-31", 0, NULL, 288)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (211, 20.4674, 0, NULL, "2002-07-31", 0, NULL, 289)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (212, 270, 0, NULL, "2002-07-31", 0, NULL, 290)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (213, 4, 0, NULL, "2002-07-12", 0, NULL, 291)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (214, 492, 0, NULL, "2002-07-12", 0, NULL, 292)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (215, 20.9293, 0, NULL, "2002-07-12", 0, NULL, 293)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (216, 20.6643, 1, "2003-07-12", "2002-07-12", 1, NULL, 294)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (217, 3, 0, NULL, "2002-08-27", 0, NULL, 295)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (218, 24.7377, 0, NULL, "2002-08-27", 0, NULL, 296)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (219, 283, 0, NULL, "2002-08-27", 0, NULL, 297)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (220, 11.3052, 1, "2003-08-27", "2002-08-27", 1, NULL, 298)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (221, 16.3292, 1, "2003-08-14", "2002-08-14", 1, NULL, 299)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (222, 277, 0, NULL, "2002-08-14", 0, NULL, 300)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (223, 28.0348, 0, NULL, "2002-08-14", 0, NULL, 301)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (224, 5, 0, NULL, "2002-08-14", 0, NULL, 302)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (225, 10.7574, 1, "2003-08-04", "2002-08-04", 1, NULL, 303)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (226, 257, 0, NULL, "2002-08-04", 0, NULL, 304)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (227, 3, 0, NULL, "2002-08-04", 0, NULL, 305)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (228, 16.125, 0, NULL, "2002-08-04", 0, NULL, 306)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (229, 436, 0, NULL, "2002-08-05", 0, NULL, 307)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (230, 16.2467, 1, "2003-08-05", "2002-08-05", 1, NULL, 308)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (231, 3, 0, NULL, "2002-08-05", 0, NULL, 309)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (232, 24.8918, 0, NULL, "2002-08-05", 0, NULL, 310)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (233, 2, 0, NULL, "2002-08-02", 0, NULL, 311)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (234, 28.9011, 0, NULL, "2002-08-02", 0, NULL, 312)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (235, 355, 0, NULL, "2002-08-02", 0, NULL, 313)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (236, 20.9441, 1, "2003-08-02", "2002-08-02", 1, NULL, 314)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (237, 22.8542, 1, "2003-08-29", "2002-08-29", 1, NULL, 315)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (238, 2, 0, NULL, "2002-08-29", 0, NULL, 316)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (239, 28.775, 0, NULL, "2002-08-29", 0, NULL, 317)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (240, 347, 0, NULL, "2002-08-29", 0, NULL, 318)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (241, 5, 0, NULL, "2002-08-26", 0, NULL, 319)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (242, 29.7101, 0, NULL, "2002-08-26", 0, NULL, 320)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (243, 17.3443, 1, "2003-08-26", "2002-08-26", 1, NULL, 321)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (244, 396, 0, NULL, "2002-08-26", 0, NULL, 322)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (245, 22.2475, 0, NULL, "2002-09-10", 0, NULL, 327)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (246, 346, 0, NULL, "2002-09-10", 0, NULL, 328)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (247, 22.7823, 1, "2003-09-10", "2002-09-10", 1, NULL, 329)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (248, 4, 0, NULL, "2002-09-10", 0, NULL, 330)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (249, 28.768, 0, NULL, "2002-09-27", 0, NULL, 331)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (250, 300, 0, NULL, "2002-09-27", 0, NULL, 332)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (251, 3, 0, NULL, "2002-09-27", 0, NULL, 333)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (252, 2, 0, NULL, "2002-09-06", 0, NULL, 338)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (253, 14.8229, 1, "2003-09-06", "2002-09-06", 1, NULL, 339)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (254, 295, 0, NULL, "2002-09-06", 0, NULL, 340)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (255, 26.866, 0, NULL, "2002-09-06", 0, NULL, 341)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (256, 22.0447, 0, NULL, "2002-09-12", 0, NULL, 342)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (257, 4, 0, NULL, "2002-09-12", 0, NULL, 343)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (258, 15.3575, 1, "2003-09-12", "2002-09-12", 1, NULL, 344)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (259, 342, 0, NULL, "2002-09-12", 0, NULL, 345)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (260, 4, 0, NULL, "2002-09-22", 0, NULL, 346)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (261, 15.5701, 1, "2003-09-22", "2002-09-22", 1, NULL, 347)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (262, 27.4551, 0, NULL, "2002-09-22", 0, NULL, 348)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (263, 327, 0, NULL, "2002-09-22", 0, NULL, 349)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (264, 2, 0, NULL, "2002-10-01", 0, NULL, 350)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (265, 14.3596, 1, "2003-10-01", "2002-10-01", 1, NULL, 351)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (266, 24.7744, 0, NULL, "2002-10-01", 0, NULL, 352)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (267, 410, 0, NULL, "2002-10-01", 0, NULL, 353)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (268, 24.4709, 0, NULL, "2002-11-22", 0, NULL, 362)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (269, 294, 0, NULL, "2002-11-22", 0, NULL, 363)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (270, 3.0491, 1, "2003-11-22", "2002-11-22", 1, NULL, 364)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (271, 2, 0, NULL, "2002-11-22", 0, NULL, 365)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (272, 17.1715, 1, "2003-11-23", "2002-11-23", 1, NULL, 366)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (273, 1, 0, NULL, "2002-11-23", 0, NULL, 367)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (274, 20.872, 0, NULL, "2002-11-23", 0, NULL, 368)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (275, 382, 0, NULL, "2002-11-23", 0, NULL, 369)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (276, 4, 0, NULL, "2003-00-12", 0, NULL, 370)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (277, 4, 0, NULL, "2003-00-08", 0, NULL, 371)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (278, 287, 0, NULL, "2003-00-08", 0, NULL, 372)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (279, 21.0672, 1, "2004-00-08", "2003-00-08", 1, NULL, 373)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (280, 27.0982, 0, NULL, "2003-00-08", 0, NULL, 374)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (281, 4, 0, NULL, "2003-00-17", 0, NULL, 375)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (282, 24.0687, 1, "2004-00-17", "2003-00-17", 1, NULL, 376)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (283, 323, 0, NULL, "2003-00-17", 0, NULL, 377)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (284, 23.0652, 0, NULL, "2003-00-17", 0, NULL, 378)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (285, 14.1088, 0, NULL, "2003-01-08", 0, NULL, 381)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (286, 421, 0, NULL, "2003-01-08", 0, NULL, 382)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (287, 1, 0, NULL, "2003-01-08", 0, NULL, 383)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (288, 21.6335, 1, "2004-01-08", "2003-01-08", 1, NULL, 384)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (289, 327, 0, NULL, "2003-01-26", 0, NULL, 385)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (290, 3, 0, NULL, "2003-01-26", 0, NULL, 386)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (291, 29.9312, 0, NULL, "2003-01-26", 0, NULL, 387)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (292, 24.5778, 1, "2004-01-26", "2003-01-26", 1, NULL, 388)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (293, 1, 0, NULL, "2003-02-28", 0, NULL, 389)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (294, 394, 0, NULL, "2003-02-28", 0, NULL, 390)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (295, 25.9485, 0, NULL, "2003-02-28", 0, NULL, 391)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (296, 477, 0, NULL, "2003-02-03", 0, NULL, 396)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (297, 295, 0, NULL, "2003-02-12", 0, NULL, 399)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (298, 21.5117, 0, NULL, "2003-02-12", 0, NULL, 400)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (299, 16.165, 1, "2004-02-12", "2003-02-12", 1, NULL, 401)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (300, 2, 0, NULL, "2003-02-12", 0, NULL, 402)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (301, 27.7804, 0, NULL, "2003-03-06", 0, NULL, 405)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (302, 457, 0, NULL, "2003-03-06", 0, NULL, 406)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (303, 17.4665, 1, "2004-03-06", "2003-03-06", 1, NULL, 407)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (304, 0, 0, NULL, "2003-03-06", 0, NULL, 408)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (305, 14.8416, 1, "2004-04-07", "2003-04-07", 1, NULL, 409)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (306, 4, 0, NULL, "2003-04-07", 0, NULL, 410)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (307, 466, 0, NULL, "2003-04-07", 0, NULL, 411)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (308, 26.638, 0, NULL, "2003-04-07", 0, NULL, 412)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (309, 18.7931, 1, "2004-04-17", "2003-04-17", 1, NULL, 413)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (310, 472, 0, NULL, "2003-04-17", 0, NULL, 414)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (311, 28.6229, 0, NULL, "2003-04-17", 0, NULL, 415)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (312, 2, 0, NULL, "2003-04-17", 0, NULL, 416)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (313, 21.7851, 0, NULL, "2003-04-27", 0, NULL, 417)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (314, 15.0059, 1, "2004-04-27", "2003-04-27", 1, NULL, 418)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (315, 2, 0, NULL, "2003-04-27", 0, NULL, 419)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (316, 368, 0, NULL, "2003-04-27", 0, NULL, 420)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (317, 25.718, 0, NULL, "2003-04-29", 0, NULL, 429)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (318, 5, 0, NULL, "2003-04-29", 0, NULL, 430)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (319, 20.8036, 1, "2004-04-29", "2003-04-29", 1, NULL, 431)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (320, 268, 0, NULL, "2003-04-29", 0, NULL, 432)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (321, 489, 0, NULL, "2003-04-23", 0, NULL, 433)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (322, 17.8816, 1, "2004-04-23", "2003-04-23", 1, NULL, 434)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (323, 1, 0, NULL, "2003-04-23", 0, NULL, 435)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (324, 28.2356, 0, NULL, "2003-04-23", 0, NULL, 436)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (325, 351, 0, NULL, "2003-05-29", 0, NULL, 438)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (326, 1, 0, NULL, "2003-05-29", 0, NULL, 439)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (327, 19.3062, 1, "2004-05-29", "2003-05-29", 1, NULL, 440)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (328, 21.2115, 0, NULL, "2003-05-29", 0, NULL, 441)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (329, 328, 0, NULL, "2003-05-09", 0, NULL, 442)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (330, 20.0312, 0, NULL, "2003-05-09", 0, NULL, 443)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (331, 1, 0, NULL, "2003-05-09", 0, NULL, 444)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (332, 10.2992, 1, "2004-05-09", "2003-05-09", 1, NULL, 445)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (333, 409, 0, NULL, "2003-05-15", 0, NULL, 446)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (334, 10.333, 1, "2004-05-15", "2003-05-15", 1, NULL, 447)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (335, 21.7161, 0, NULL, "2003-05-15", 0, NULL, 448)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (336, 4, 0, NULL, "2003-05-15", 0, NULL, 449)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (337, 1, 0, NULL, "2003-06-14", 0, NULL, 450)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (338, 325, 0, NULL, "2003-06-14", 0, NULL, 451)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (339, 22.4074, 0, NULL, "2003-06-14", 0, NULL, 452)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (340, 11.1895, 1, "2004-06-14", "2003-06-14", 1, NULL, 453)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (341, 21.0622, 1, "2004-06-22", "2003-06-22", 1, NULL, 458)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (342, 381, 0, NULL, "2003-06-22", 0, NULL, 459)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (343, 20.1267, 0, NULL, "2003-06-22", 0, NULL, 460)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (344, 3, 0, NULL, "2003-06-22", 0, NULL, 461)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (345, 2, 0, NULL, "2003-06-12", 0, NULL, 462)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (346, 22.2685, 1, "2004-06-12", "2003-06-12", 1, NULL, 463)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (347, 350, 0, NULL, "2003-06-12", 0, NULL, 464)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (348, 403, 0, NULL, "2003-06-18", 0, NULL, 465)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (349, 20.0599, 1, "2004-06-18", "2003-06-18", 1, NULL, 466)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (350, 2, 0, NULL, "2003-06-18", 0, NULL, 467)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (351, 22.5364, 0, NULL, "2003-06-18", 0, NULL, 468)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (352, 377, 0, NULL, "2003-06-28", 0, NULL, 473)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (353, 29.5817, 0, NULL, "2003-06-28", 0, NULL, 474)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (354, 1, 0, NULL, "2003-06-28", 0, NULL, 475)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (355, 12.9674, 1, "2004-06-28", "2003-06-28", 1, NULL, 476)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (356, 29.3344, 0, NULL, "2003-08-28", 0, NULL, 487)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (357, 493, 0, NULL, "2003-08-28", 0, NULL, 488)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (358, 18.0743, 1, "2004-08-28", "2003-08-28", 1, NULL, 489)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (359, 23.3772, 1, "2004-08-17", "2003-08-17", 1, NULL, 490)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (360, 26.4034, 0, NULL, "2003-09-12", 0, NULL, 495)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (361, 382, 0, NULL, "2003-09-12", 0, NULL, 496)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (362, 306, 0, NULL, "2003-09-25", 0, NULL, 506)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (363, 5, 0, NULL, "2003-09-25", 0, NULL, 507)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (364, 14.5191, 1, "2004-09-25", "2003-09-25", 1, NULL, 508)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (365, 29.7871, 0, NULL, "2003-09-25", 0, NULL, 509)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (366, 28.5903, 0, NULL, "2003-10-06", 0, NULL, 510)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (367, 3, 0, NULL, "2003-10-06", 0, NULL, 511)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (368, 347, 0, NULL, "2003-10-06", 0, NULL, 512)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (369, 283, 0, NULL, "2003-10-16", 0, NULL, 513)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (370, 10.4692, 1, "2004-10-16", "2003-10-16", 1, NULL, 514)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (371, 3, 0, NULL, "2003-10-16", 0, NULL, 515)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (372, 21.4107, 0, NULL, "2003-10-16", 0, NULL, 516)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (373, 460, 0, NULL, "2003-11-18", 0, NULL, 521)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (374, 29.734, 0, NULL, "2003-11-18", 0, NULL, 522)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (375, 5, 0, NULL, "2003-11-18", 0, NULL, 523)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (376, 21, 1, "2004-11-18", "2003-11-18", 1, NULL, 524)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (377, 458, 0, NULL, "2003-11-03", 0, NULL, 525)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (378, 22.9874, 0, NULL, "2003-11-03", 0, NULL, 526)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (379, 3, 0, NULL, "2003-11-03", 0, NULL, 527)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (380, 12.0476, 1, "2004-11-03", "2003-11-03", 1, NULL, 528)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (381, 20.7872, 1, "2004-11-01", "2003-11-01", 1, NULL, 529)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (382, 25.6937, 0, NULL, "2003-11-30", 0, NULL, 530)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (383, 1, 0, NULL, "2003-11-30", 0, NULL, 531)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (384, 10.3076, 1, "2004-11-30", "2003-11-30", 1, NULL, 532)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (385, 442, 0, NULL, "2003-11-30", 0, NULL, 533)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (386, 4, 0, NULL, "2004-00-16", 0, NULL, 534)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (387, 24.3879, 0, NULL, "2004-00-16", 0, NULL, 535)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (388, 11.7912, 1, "2005-00-16", "2004-00-16", 1, NULL, 536)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (389, 392, 0, NULL, "2004-00-16", 0, NULL, 537)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (390, 27.4151, 0, NULL, "2004-00-20", 0, NULL, 538)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (391, 3, 0, NULL, "2004-00-20", 0, NULL, 539)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (392, 495, 0, NULL, "2004-00-20", 0, NULL, 540)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (393, 10.6952, 1, "2005-00-20", "2004-00-20", 1, NULL, 541)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (394, 23.1933, 0, NULL, "2004-00-12", 0, NULL, 542)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (395, 13.5431, 1, "2005-00-12", "2004-00-12", 1, NULL, 543)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (396, 3, 0, NULL, "2004-00-12", 0, NULL, 544)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (397, 422, 0, NULL, "2004-00-12", 0, NULL, 545)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (398, 289, 0, NULL, "2004-00-17", 0, NULL, 546)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (399, 14.2973, 0, NULL, "2004-00-17", 0, NULL, 547)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (400, 17.4873, 1, "2005-00-17", "2004-00-17", 1, NULL, 548)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (401, 1, 0, NULL, "2004-00-17", 0, NULL, 549)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (402, 21.6604, 1, "2005-00-15", "2004-00-15", 1, NULL, 550)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (403, 379, 0, NULL, "2004-00-15", 0, NULL, 551)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (404, 1, 0, NULL, "2004-00-15", 0, NULL, 552)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (405, 20.6632, 0, NULL, "2004-00-15", 0, NULL, 553)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (406, 347, 0, NULL, "2004-02-15", 0, NULL, 558)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (407, 21.8868, 0, NULL, "2004-02-15", 0, NULL, 559)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (408, 24.322, 1, "2005-02-15", "2004-02-15", 1, NULL, 560)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (409, 5, 0, NULL, "2004-02-15", 0, NULL, 561)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (410, 13.3341, 1, "2005-02-10", "2004-02-10", 1, NULL, 562)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (411, 405, 0, NULL, "2004-02-10", 0, NULL, 563)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (412, 0, 0, NULL, "2004-02-10", 0, NULL, 564)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (413, 14.266, 1, "2005-02-27", "2004-02-27", 1, NULL, 569)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (414, 451, 0, NULL, "2004-02-27", 0, NULL, 570)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (415, 0, 0, NULL, "2004-02-27", 0, NULL, 571)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (416, 22.2713, 0, NULL, "2004-02-27", 0, NULL, 572)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (417, 13.8555, 1, "2005-02-19", "2004-02-19", 1, NULL, 573)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (418, 12.783, 1, "2005-02-08", "2004-02-08", 1, NULL, 578)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (419, 478, 0, NULL, "2004-02-08", 0, NULL, 579)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (420, 24.3241, 0, NULL, "2004-02-08", 0, NULL, 580)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (421, 1, 0, NULL, "2004-02-08", 0, NULL, 581)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (422, 16.1694, 1, "2005-03-16", "2004-03-16", 1, NULL, 586)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (423, 3, 0, NULL, "2004-03-16", 0, NULL, 587)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (424, 387, 0, NULL, "2004-03-16", 0, NULL, 588)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (425, 27.0446, 0, NULL, "2004-03-16", 0, NULL, 589)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (426, 24.599, 0, NULL, "2004-03-22", 0, NULL, 590)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (427, 405, 0, NULL, "2004-03-22", 0, NULL, 591)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (428, 17.7555, 1, "2005-03-22", "2004-03-22", 1, NULL, 592)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (429, 4, 0, NULL, "2004-03-25", 0, NULL, 596)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (430, 15.0962, 1, "2005-03-25", "2004-03-25", 1, NULL, 597)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (431, 21.7498, 0, NULL, "2004-03-25", 0, NULL, 598)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (432, 496, 0, NULL, "2004-03-25", 0, NULL, 599)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (433, 419, 0, NULL, "2004-03-31", 0, NULL, 600)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (434, 14.4782, 0, NULL, "2004-03-31", 0, NULL, 601)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (435, 11.0229, 1, "2005-03-31", "2004-03-31", 1, NULL, 602)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (436, 3, 0, NULL, "2004-03-31", 0, NULL, 603)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (437, 5, 0, NULL, "2004-05-22", 0, NULL, 608)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (438, 497, 0, NULL, "2004-05-22", 0, NULL, 609)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (439, 24.3064, 1, "2005-05-22", "2004-05-22", 1, NULL, 610)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (440, 27.7221, 0, NULL, "2004-05-22", 0, NULL, 611)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (441, 24.9448, 1, "2005-06-21", "2004-06-21", 1, NULL, 612)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (442, 275, 0, NULL, "2004-06-21", 0, NULL, 613)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (443, 29.8832, 0, NULL, "2004-06-21", 0, NULL, 614)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (444, 3, 0, NULL, "2004-06-21", 0, NULL, 615)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (445, 20.2666, 0, NULL, "2004-06-02", 0, NULL, 616)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (446, 259, 0, NULL, "2004-06-02", 0, NULL, 617)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (447, 2, 0, NULL, "2004-06-02", 0, NULL, 618)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (448, 20.4003, 1, "2005-06-02", "2004-06-02", 1, NULL, 619)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (449, 20.8229, 1, "2005-06-01", "2004-06-01", 1, NULL, 624)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (450, 19.4737, 0, NULL, "2004-06-01", 0, NULL, 625)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (451, 5, 0, NULL, "2004-06-01", 0, NULL, 626)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (452, 402, 0, NULL, "2004-06-01", 0, NULL, 627)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (453, 25.0946, 0, NULL, "2004-07-05", 0, NULL, 632)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (454, 12.146, 1, "2005-07-05", "2004-07-05", 1, NULL, 633)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (455, 423, 0, NULL, "2004-07-05", 0, NULL, 634)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (456, 0, 0, NULL, "2004-07-05", 0, NULL, 635)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (457, 2, 0, NULL, "2004-07-11", 0, NULL, 640)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (458, 279, 0, NULL, "2004-07-11", 0, NULL, 641)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (459, 23.4265, 0, NULL, "2004-07-11", 0, NULL, 642)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (460, 17.9018, 1, "2005-08-24", "2004-08-24", 1, NULL, 643)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (461, 408, 0, NULL, "2004-08-24", 0, NULL, 644)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (462, 366, 0, NULL, "2004-08-01", 0, NULL, 645)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (463, 16.0024, 1, "2005-08-01", "2004-08-01", 1, NULL, 646)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (464, 428, 0, NULL, "2004-09-05", 0, NULL, 655)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (465, 4, 0, NULL, "2004-09-05", 0, NULL, 656)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (466, 16.2297, 1, "2005-09-05", "2004-09-05", 1, NULL, 657)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (467, 22.1736, 0, NULL, "2004-09-05", 0, NULL, 658)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (468, 457, 0, NULL, "2004-10-11", 0, NULL, 659)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (469, 20.3931, 1, "2005-10-11", "2004-10-11", 1, NULL, 660)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (470, 5, 0, NULL, "2004-10-11", 0, NULL, 661)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (471, 25.7825, 0, NULL, "2004-10-11", 0, NULL, 662)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (472, 15.8934, 1, "2005-11-07", "2004-11-07", 1, NULL, 667)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (473, 399, 0, NULL, "2004-11-07", 0, NULL, 668)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (474, 22.7176, 0, NULL, "2004-11-07", 0, NULL, 669)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (475, 5, 0, NULL, "2004-11-07", 0, NULL, 670)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (476, 455, 0, NULL, "2004-11-22", 0, NULL, 671)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (477, 23.0801, 0, NULL, "2004-11-22", 0, NULL, 672)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (478, 13.1925, 1, "2005-11-22", "2004-11-22", 1, NULL, 673)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (479, 4, 0, NULL, "2005-00-08", 0, NULL, 674)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (480, 369, 0, NULL, "2005-00-08", 0, NULL, 675)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (481, 266, 0, NULL, "2005-01-28", 0, NULL, 682)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (482, 26.2524, 0, NULL, "2005-01-28", 0, NULL, 683)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (483, 2, 0, NULL, "2005-01-28", 0, NULL, 684)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (484, 10.1788, 1, "2006-01-28", "2005-01-28", 1, NULL, 685)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (485, 20.1142, 0, NULL, "2005-01-21", 0, NULL, 686)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (486, 15.2747, 1, "2006-01-21", "2005-01-21", 1, NULL, 687)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (487, 442, 0, NULL, "2005-01-21", 0, NULL, 688)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (488, 3, 0, NULL, "2005-01-21", 0, NULL, 689)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (489, 1, 0, NULL, "2005-02-06", 0, NULL, 690)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (490, 342, 0, NULL, "2005-02-06", 0, NULL, 691)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (491, 22.584, 0, NULL, "2005-02-06", 0, NULL, 692)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (492, 2, 0, NULL, "2005-03-31", 0, NULL, 693)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (493, 14.6763, 1, "2006-03-31", "2005-03-31", 1, NULL, 694)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (494, 22.0603, 0, NULL, "2005-03-31", 0, NULL, 695)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (495, 394, 0, NULL, "2005-04-08", 0, NULL, 696)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (496, 0, 0, NULL, "2005-04-08", 0, NULL, 697)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (497, 24.5331, 0, NULL, "2005-04-08", 0, NULL, 698)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (498, 24.1722, 1, "2006-04-08", "2005-04-08", 1, NULL, 699)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (499, 304, 0, NULL, "2005-04-19", 0, NULL, 704)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (500, 22.3278, 0, NULL, "2005-04-19", 0, NULL, 705)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (501, 22.4188, 1, "2006-04-19", "2005-04-19", 1, NULL, 706)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (502, 0, 0, NULL, "2005-04-19", 0, NULL, 707)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (503, 17.3894, 1, "2006-04-03", "2005-04-03", 1, NULL, 712)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (504, 337, 0, NULL, "2005-04-03", 0, NULL, 713)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (505, 23.0413, 0, NULL, "2005-04-03", 0, NULL, 714)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (506, 3, 0, NULL, "2005-04-03", 0, NULL, 715)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (507, 3, 0, NULL, "2005-04-10", 0, NULL, 716)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (508, 2, 0, NULL, "2005-05-17", 0, NULL, 717)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (509, 26.6681, 0, NULL, "2005-05-17", 0, NULL, 718)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (510, 285, 0, NULL, "2005-05-17", 0, NULL, 719)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (511, 18.7702, 1, "2006-05-17", "2005-05-17", 1, NULL, 720)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (512, 324, 0, NULL, "2005-05-19", 0, NULL, 721)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (513, 26.8838, 0, NULL, "2005-05-19", 0, NULL, 722)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (514, 4, 0, NULL, "2005-05-19", 0, NULL, 723)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (515, 23.8456, 1, "2006-05-19", "2005-05-19", 1, NULL, 724)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (516, 29.3089, 0, NULL, "2005-05-29", 0, NULL, 725)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (517, 251, 0, NULL, "2005-05-29", 0, NULL, 726)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (518, 20.2077, 1, "2006-05-29", "2005-05-29", 1, NULL, 727)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (519, 3, 0, NULL, "2005-05-29", 0, NULL, 728)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (520, 13.3834, 1, "2006-06-25", "2005-06-25", 1, NULL, 733)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (521, 25.6858, 0, NULL, "2005-06-25", 0, NULL, 734)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (522, 465, 0, NULL, "2005-06-25", 0, NULL, 735)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (523, 5, 0, NULL, "2005-06-25", 0, NULL, 736)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (524, 397, 0, NULL, "2005-06-12", 0, NULL, 737)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (525, 15.4261, 1, "2006-06-12", "2005-06-12", 1, NULL, 738)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (526, 315, 0, NULL, "2005-07-11", 0, NULL, 739)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (527, 11.2601, 1, "2006-07-11", "2005-07-11", 1, NULL, 740)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (528, 25.6069, 0, NULL, "2005-07-11", 0, NULL, 741)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (529, 4, 0, NULL, "2005-07-11", 0, NULL, 742)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (530, 24.7323, 1, "2006-07-15", "2005-07-15", 1, NULL, 743)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (531, 5, 0, NULL, "2005-07-15", 0, NULL, 744)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (532, 22.6048, 0, NULL, "2005-07-15", 0, NULL, 745)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (533, 491, 0, NULL, "2005-07-15", 0, NULL, 746)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (534, 371, 0, NULL, "2005-07-14", 0, NULL, 747)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (535, 10.1808, 1, "2006-07-14", "2005-07-14", 1, NULL, 748)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (536, 25.0039, 0, NULL, "2005-07-14", 0, NULL, 749)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (537, 4, 0, NULL, "2005-07-14", 0, NULL, 750)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (538, 22.4175, 1, "2006-07-23", "2005-07-23", 1, NULL, 756)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (539, 3, 0, NULL, "2005-07-23", 0, NULL, 757)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (540, 14.3491, 1, "2006-09-31", "2005-09-31", 1, NULL, 763)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (541, 28.6845, 0, NULL, "2005-11-04", 0, NULL, 772)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (542, 10.6473, 1, "2006-11-04", "2005-11-04", 1, NULL, 773)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (543, 341, 0, NULL, "2005-11-04", 0, NULL, 774)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (544, 491, 0, NULL, "2005-11-09", 0, NULL, 779)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (545, 24.3945, 0, NULL, "2005-11-09", 0, NULL, 780)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (546, 5, 0, NULL, "2005-11-09", 0, NULL, 781)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (547, 16.6556, 1, "2007-00-13", "2006-00-13", 1, NULL, 789)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (548, 4, 0, NULL, "2006-00-14", 0, NULL, 790)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (549, 256, 0, NULL, "2006-00-14", 0, NULL, 791)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (550, 24.5148, 0, NULL, "2006-00-14", 0, NULL, 792)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (551, 13.9267, 1, "2007-00-14", "2006-00-14", 1, NULL, 793)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (552, 23.4091, 0, NULL, "2006-00-21", 0, NULL, 798)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (553, 472, 0, NULL, "2006-00-21", 0, NULL, 799)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (554, 3, 0, NULL, "2006-00-21", 0, NULL, 800)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (555, 18.5757, 0, NULL, "2006-00-22", 0, NULL, 801)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (556, 402, 0, NULL, "2006-00-22", 0, NULL, 802)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (557, 13.9265, 1, "2007-00-22", "2006-00-22", 1, NULL, 803)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (558, 2, 0, NULL, "2006-00-22", 0, NULL, 804)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (559, 340, 0, NULL, "2006-01-04", 0, NULL, 809)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (560, 26.1639, 0, NULL, "2006-01-04", 0, NULL, 810)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (561, 21.6212, 1, "2007-01-04", "2006-01-04", 1, NULL, 811)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (562, 4, 0, NULL, "2006-01-04", 0, NULL, 812)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (563, 28.0824, 0, NULL, "2006-01-10", 0, NULL, 816)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (564, 439, 0, NULL, "2006-01-10", 0, NULL, 817)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (565, 9.5092, 1, "2007-01-10", "2006-01-10", 1, NULL, 818)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (566, 2, 0, NULL, "2006-01-10", 0, NULL, 819)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (567, 18.3919, 1, "2007-01-27", "2006-01-27", 1, NULL, 820)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (568, 2, 0, NULL, "2006-01-27", 0, NULL, 821)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (569, 14.6959, 0, NULL, "2006-01-27", 0, NULL, 822)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (570, 351, 0, NULL, "2006-01-27", 0, NULL, 823)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (571, 21.8071, 1, "2007-01-24", "2006-01-24", 1, NULL, 824)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (572, 1, 0, NULL, "2006-01-24", 0, NULL, 825)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (573, 21.8846, 0, NULL, "2006-01-24", 0, NULL, 826)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (574, 439, 0, NULL, "2006-01-24", 0, NULL, 827)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (575, 2, 0, NULL, "2006-02-21", 0, NULL, 832)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (576, 478, 0, NULL, "2006-02-21", 0, NULL, 833)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (577, 22.5445, 0, NULL, "2006-02-21", 0, NULL, 834)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (578, 18.5741, 1, "2007-02-21", "2006-02-21", 1, NULL, 835)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (579, 21.3469, 1, "2007-03-30", "2006-03-30", 1, NULL, 837)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (580, 26.4782, 0, NULL, "2006-03-30", 0, NULL, 838)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (581, 398, 0, NULL, "2006-03-26", 0, NULL, 839)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (582, 23.7818, 0, NULL, "2006-03-26", 0, NULL, 840)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (583, 3, 0, NULL, "2006-03-26", 0, NULL, 841)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (584, 20.8358, 1, "2007-03-26", "2006-03-26", 1, NULL, 842)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (585, 24.4009, 0, NULL, "2006-04-02", 0, NULL, 843)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (586, 358, 0, NULL, "2006-04-02", 0, NULL, 844)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (587, 455, 0, NULL, "2006-04-01", 0, NULL, 845)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (588, 1, 0, NULL, "2006-04-06", 0, NULL, 846)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (589, 29.4796, 0, NULL, "2006-04-06", 0, NULL, 847)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (590, 15.0933, 1, "2007-04-06", "2006-04-06", 1, NULL, 848)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (591, 252, 0, NULL, "2006-04-06", 0, NULL, 849)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (592, 22.026, 0, NULL, "2006-04-10", 0, NULL, 850)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (593, 340, 0, NULL, "2006-04-10", 0, NULL, 851)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (594, 19.9126, 1, "2007-04-10", "2006-04-10", 1, NULL, 852)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (595, 1, 0, NULL, "2006-04-10", 0, NULL, 853)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (596, 461, 0, NULL, "2006-04-14", 0, NULL, 854)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (597, 3, 0, NULL, "2006-04-14", 0, NULL, 855)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (598, 20.0241, 0, NULL, "2006-04-14", 0, NULL, 856)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (599, 20.7324, 1, "2007-04-14", "2006-04-14", 1, NULL, 857)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (600, 4, 0, NULL, "2006-05-14", 0, NULL, 858)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (601, 23.4199, 1, "2007-05-14", "2006-05-14", 1, NULL, 859)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (602, 362, 0, NULL, "2006-05-14", 0, NULL, 860)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (603, 23.9307, 0, NULL, "2006-05-14", 0, NULL, 861)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (604, 27.5136, 0, NULL, "2006-05-25", 0, NULL, 862)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (605, 1, 0, NULL, "2006-05-25", 0, NULL, 863)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (606, 13.7363, 1, "2007-05-25", "2006-05-25", 1, NULL, 864)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (607, 258, 0, NULL, "2006-05-25", 0, NULL, 865)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (608, 26.605, 0, NULL, "2006-05-09", 0, NULL, 866)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (609, 2, 0, NULL, "2006-05-09", 0, NULL, 867)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (610, 499, 0, NULL, "2006-05-09", 0, NULL, 868)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (611, 18.0991, 1, "2007-05-09", "2006-05-09", 1, NULL, 869)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (612, 23.382, 1, "2007-05-27", "2006-05-27", 1, NULL, 870)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (613, 25.1225, 0, NULL, "2006-05-27", 0, NULL, 871)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (614, 488, 0, NULL, "2006-05-27", 0, NULL, 872)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (615, 1, 0, NULL, "2006-05-27", 0, NULL, 873)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (616, 2, 0, NULL, "2006-05-19", 0, NULL, 874)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (617, 309, 0, NULL, "2006-06-12", 0, NULL, 879)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (618, 451, 0, NULL, "2006-06-03", 0, NULL, 880)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (619, 5, 0, NULL, "2006-06-03", 0, NULL, 881)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (620, 21.32, 0, NULL, "2006-06-03", 0, NULL, 882)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (621, 13.1635, 1, "2007-06-03", "2006-06-03", 1, NULL, 883)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (622, 1, 0, NULL, "2006-07-03", 0, NULL, 886)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (623, 23.5135, 0, NULL, "2006-07-03", 0, NULL, 887)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (624, 17.3734, 1, "2007-07-03", "2006-07-03", 1, NULL, 888)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (625, 307, 0, NULL, "2006-07-03", 0, NULL, 889)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (626, 24.2477, 0, NULL, "2006-07-23", 0, NULL, 890)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (627, 18.822, 1, "2007-07-23", "2006-07-23", 1, NULL, 891)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (628, 4, 0, NULL, "2006-07-11", 0, NULL, 892)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (629, 28.6464, 0, NULL, "2006-07-11", 0, NULL, 893)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (630, 455, 0, NULL, "2006-07-11", 0, NULL, 894)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (631, 11.1403, 1, "2007-07-23", "2006-07-23", 1, NULL, 895)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (632, 1, 0, NULL, "2006-07-23", 0, NULL, 896)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (633, 21.8911, 0, NULL, "2006-07-23", 0, NULL, 897)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (634, 471, 0, NULL, "2006-07-23", 0, NULL, 898)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (635, 17.7416, 1, "2007-08-23", "2006-08-23", 1, NULL, 899)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (636, 24.2385, 0, NULL, "2006-08-04", 0, NULL, 900)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (637, 22.9888, 1, "2007-08-04", "2006-08-04", 1, NULL, 901)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (638, 2, 0, NULL, "2006-08-12", 0, NULL, 907)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (639, 255, 0, NULL, "2006-08-12", 0, NULL, 908)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (640, 21.454, 1, "2007-08-12", "2006-08-12", 1, NULL, 909)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (641, 25.3273, 0, NULL, "2006-08-12", 0, NULL, 910)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (642, 12.5408, 1, "2007-08-13", "2006-08-13", 1, NULL, 911)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (643, 4, 0, NULL, "2006-08-13", 0, NULL, 912)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (644, 474, 0, NULL, "2006-08-13", 0, NULL, 913)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (645, 25.3116, 0, NULL, "2006-08-13", 0, NULL, 914)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (646, 19.454, 1, "2007-08-30", "2006-08-30", 1, NULL, 915)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (647, 23.6772, 0, NULL, "2006-08-30", 0, NULL, 916)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (648, 397, 0, NULL, "2006-08-30", 0, NULL, 917)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (649, 1, 0, NULL, "2006-08-30", 0, NULL, 918)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (650, 1, 0, NULL, "2006-09-28", 0, NULL, 919)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (651, 23.9807, 1, "2007-09-28", "2006-09-28", 1, NULL, 920)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (652, 27.0051, 0, NULL, "2006-09-28", 0, NULL, 921)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (653, 411, 0, NULL, "2006-09-28", 0, NULL, 922)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (654, 19.8234, 1, "2007-09-25", "2006-09-25", 1, NULL, 923)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (655, 328, 0, NULL, "2006-09-25", 0, NULL, 924)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (656, 26.2093, 0, NULL, "2006-09-25", 0, NULL, 925)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (657, 3, 0, NULL, "2006-09-25", 0, NULL, 926)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (658, 427, 0, NULL, "2006-09-18", 0, NULL, 928)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (659, 29.997, 0, NULL, "2006-09-18", 0, NULL, 929)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (660, 18.1888, 1, "2007-09-18", "2006-09-18", 1, NULL, 930)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (661, 1, 0, NULL, "2006-09-18", 0, NULL, 931)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (662, 28.7865, 0, NULL, "2006-10-20", 0, NULL, 932)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (663, 3, 0, NULL, "2006-10-20", 0, NULL, 933)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (664, 20.5027, 1, "2007-10-20", "2006-10-20", 1, NULL, 934)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (665, 335, 0, NULL, "2006-10-22", 0, NULL, 935)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (666, 26.4448, 0, NULL, "2006-10-22", 0, NULL, 936)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (667, 4, 0, NULL, "2006-10-22", 0, NULL, 937)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (668, 448, 0, NULL, "2006-11-03", 0, NULL, 942)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (669, 23.5366, 0, NULL, "2006-11-03", 0, NULL, 943)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (670, 1, 0, NULL, "2006-11-03", 0, NULL, 944)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (671, 13.9114, 1, "2007-11-03", "2006-11-03", 1, NULL, 945)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (672, 11.6449, 1, "2008-00-26", "2007-00-26", 1, NULL, 946)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (673, 10.5675, 0, NULL, "2007-00-26", 0, NULL, 947)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (674, 356, 0, NULL, "2007-00-26", 0, NULL, 948)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (675, 5, 0, NULL, "2007-00-26", 0, NULL, 949)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (676, 372, 0, NULL, "2007-00-16", 0, NULL, 950)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (677, 4, 0, NULL, "2007-00-16", 0, NULL, 951)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (678, 23.9427, 1, "2008-00-16", "2007-00-16", 1, NULL, 952)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (679, 28.116, 0, NULL, "2007-00-16", 0, NULL, 953)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (680, 1, 0, NULL, "2007-00-15", 0, NULL, 958)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (681, 22.6547, 0, NULL, "2007-00-15", 0, NULL, 959)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (682, 465, 0, NULL, "2007-00-15", 0, NULL, 960)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (683, 10.4326, 1, "2008-00-15", "2007-00-15", 1, NULL, 961)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (684, 403, 0, NULL, "2007-00-03", 0, NULL, 962)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (685, 15.1178, 1, "2008-00-03", "2007-00-03", 1, NULL, 963)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (686, 24.5548, 0, NULL, "2007-00-03", 0, NULL, 964)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (687, 4, 0, NULL, "2007-00-03", 0, NULL, 965)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (688, 20.9704, 1, "2008-00-02", "2007-00-02", 1, NULL, 970)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (689, 1, 0, NULL, "2007-00-02", 0, NULL, 971)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (690, 389, 0, NULL, "2007-00-02", 0, NULL, 972)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (691, 27.4741, 0, NULL, "2007-00-02", 0, NULL, 973)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (692, 24.1139, 0, NULL, "2007-01-10", 0, NULL, 974)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (693, 17.5187, 1, "2008-01-10", "2007-01-10", 1, NULL, 975)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (694, 412, 0, NULL, "2007-01-10", 0, NULL, 976)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (695, 1, 0, NULL, "2007-01-10", 0, NULL, 977)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (696, 4, 0, NULL, "2007-01-04", 0, NULL, 982)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (697, 432, 0, NULL, "2007-02-20", 0, NULL, 983)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (698, 23.5312, 0, NULL, "2007-02-20", 0, NULL, 984)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (699, 5, 0, NULL, "2007-02-20", 0, NULL, 985)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (700, 12.8665, 1, "2008-02-20", "2007-02-20", 1, NULL, 986)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (701, 1, 0, NULL, "2007-02-07", 0, NULL, 996)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (702, 20.4287, 0, NULL, "2007-02-07", 0, NULL, 997)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (703, 442, 0, NULL, "2007-02-07", 0, NULL, 998)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (704, 18.9451, 1, "2008-02-07", "2007-02-07", 1, NULL, 999)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (705, 479, 0, NULL, "2007-02-14", 0, NULL, 1000)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (706, 15.0989, 1, "2008-02-14", "2007-02-14", 1, NULL, 1001)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (707, 2, 0, NULL, "2007-02-14", 0, NULL, 1002)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (708, 26.105, 0, NULL, "2007-02-14", 0, NULL, 1003)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (709, 21.4544, 0, NULL, "2007-03-12", 0, NULL, 1008)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (710, 339, 0, NULL, "2007-03-12", 0, NULL, 1009)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (711, 4, 0, NULL, "2007-03-12", 0, NULL, 1010)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (712, 10.5437, 1, "2008-03-12", "2007-03-12", 1, NULL, 1011)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (713, 4, 0, NULL, "2007-03-08", 0, NULL, 1012)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (714, 23.4504, 0, NULL, "2007-03-08", 0, NULL, 1013)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (715, 19.185, 1, "2008-03-08", "2007-03-08", 1, NULL, 1014)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (716, 405, 0, NULL, "2007-04-21", 0, NULL, 1015)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (717, 25.4899, 0, NULL, "2007-04-21", 0, NULL, 1016)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (718, 5, 0, NULL, "2007-04-21", 0, NULL, 1017)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (719, 10.1797, 1, "2008-04-21", "2007-04-21", 1, NULL, 1018)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (720, 25.2585, 0, NULL, "2007-04-12", 0, NULL, 1019)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (721, 3, 0, NULL, "2007-04-12", 0, NULL, 1020)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (722, 24.3806, 1, "2008-04-12", "2007-04-12", 1, NULL, 1021)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (723, 327, 0, NULL, "2007-04-12", 0, NULL, 1022)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (724, 22.4627, 0, NULL, "2007-05-21", 0, NULL, 1035)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (725, 294, 0, NULL, "2007-05-21", 0, NULL, 1036)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (726, 1, 0, NULL, "2007-05-21", 0, NULL, 1037)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (727, 10.2997, 1, "2008-05-21", "2007-05-21", 1, NULL, 1038)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (728, 2, 0, NULL, "2007-05-14", 0, NULL, 1039)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (729, 255, 0, NULL, "2007-05-14", 0, NULL, 1040)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (730, 16.2567, 1, "2008-05-14", "2007-05-14", 1, NULL, 1041)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (731, 23.0756, 0, NULL, "2007-05-14", 0, NULL, 1042)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (732, 4, 0, NULL, "2007-07-06", 0, NULL, 1046)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (733, 21.4255, 1, "2008-07-06", "2007-07-06", 1, NULL, 1047)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (734, 29.3063, 0, NULL, "2007-07-06", 0, NULL, 1048)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (735, 468, 0, NULL, "2007-07-06", 0, NULL, 1049)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (736, 24.9223, 0, NULL, "2007-08-30", 0, NULL, 1055)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (737, 435, 0, NULL, "2007-08-30", 0, NULL, 1056)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (738, 3, 0, NULL, "2007-08-30", 0, NULL, 1057)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (739, 11.7856, 1, "2008-08-30", "2007-08-30", 1, NULL, 1058)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (740, 21.4756, 0, NULL, "2007-08-19", 0, NULL, 1063)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (741, 10.2477, 1, "2008-08-19", "2007-08-19", 1, NULL, 1064)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (742, 21.3909, 0, NULL, "2007-08-05", 0, NULL, 1065)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (743, 22.9126, 1, "2008-08-19", "2007-08-19", 1, NULL, 1070)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (744, 20.7877, 0, NULL, "2007-08-19", 0, NULL, 1071)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (745, 428, 0, NULL, "2007-08-19", 0, NULL, 1072)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (746, 0, 0, NULL, "2007-08-19", 0, NULL, 1073)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (747, 28.584, 0, NULL, "2007-09-27", 0, NULL, 1076)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (748, 4, 0, NULL, "2007-09-27", 0, NULL, 1077)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (749, 250, 0, NULL, "2007-09-27", 0, NULL, 1078)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (750, 11.0637, 1, "2008-09-27", "2007-09-27", 1, NULL, 1079)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (751, 20.6877, 0, NULL, "2007-09-30", 0, NULL, 1080)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (752, 12.3742, 1, "2008-09-30", "2007-09-30", 1, NULL, 1081)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (753, 3, 0, NULL, "2007-09-30", 0, NULL, 1082)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (754, 445, 0, NULL, "2007-09-30", 0, NULL, 1083)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (755, 493, 0, NULL, "2007-10-21", 0, NULL, 1088)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (756, 27.6677, 0, NULL, "2007-10-21", 0, NULL, 1089)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (757, 14.3903, 1, "2008-10-21", "2007-10-21", 1, NULL, 1090)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (758, 1, 0, NULL, "2007-11-26", 0, NULL, 1091)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (759, 335, 0, NULL, "2007-11-26", 0, NULL, 1092)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (760, 16.3156, 1, "2008-11-26", "2007-11-26", 1, NULL, 1093)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (761, 21.9467, 0, NULL, "2007-11-26", 0, NULL, 1094)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (762, 273, 0, NULL, "2007-11-07", 0, NULL, 1096)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (763, 2, 0, NULL, "2007-11-07", 0, NULL, 1097)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (764, 22.8178, 1, "2008-11-07", "2007-11-07", 1, NULL, 1098)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (765, 25.0693, 0, NULL, "2007-11-07", 0, NULL, 1099)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (766, 426, 0, NULL, "2008-01-08", 0, NULL, 1111)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (767, 20.1683, 0, NULL, "2008-01-08", 0, NULL, 1112)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (768, 21.5215, 1, "2009-01-08", "2008-01-08", 1, NULL, 1113)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (769, 4, 0, NULL, "2008-01-08", 0, NULL, 1114)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (770, 2, 0, NULL, "2008-01-03", 0, NULL, 1115)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (771, 318, 0, NULL, "2008-01-16", 0, NULL, 1116)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (772, 19.0039, 1, "2009-01-16", "2008-01-16", 1, NULL, 1117)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (773, 29.6773, 0, NULL, "2008-01-16", 0, NULL, 1118)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (774, 4, 0, NULL, "2008-01-16", 0, NULL, 1119)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (775, 17.817, 1, "2009-01-13", "2008-01-13", 1, NULL, 1120)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (776, 407, 0, NULL, "2008-01-13", 0, NULL, 1121)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (777, 2, 0, NULL, "2008-01-13", 0, NULL, 1122)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (778, 29.5243, 0, NULL, "2008-01-13", 0, NULL, 1123)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (779, 27.0883, 0, NULL, "2008-01-25", 0, NULL, 1124)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (780, 424, 0, NULL, "2008-02-15", 0, NULL, 1125)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (781, 5, 0, NULL, "2008-02-15", 0, NULL, 1126)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (782, 328, 0, NULL, "2008-02-08", 0, NULL, 1131)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (783, 2, 0, NULL, "2008-02-08", 0, NULL, 1132)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (784, 25.9263, 0, NULL, "2008-02-08", 0, NULL, 1133)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (785, 18.7004, 1, "2009-02-08", "2008-02-08", 1, NULL, 1134)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (786, 12.3406, 1, "2009-03-02", "2008-03-02", 1, NULL, 1143)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (787, 29.0515, 0, NULL, "2008-03-02", 0, NULL, 1144)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (788, 463, 0, NULL, "2008-03-02", 0, NULL, 1145)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (789, 4, 0, NULL, "2008-03-02", 0, NULL, 1146)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (790, 12.9023, 1, "2009-03-17", "2008-03-17", 1, NULL, 1147)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (791, 5, 0, NULL, "2008-03-17", 0, NULL, 1148)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (792, 327, 0, NULL, "2008-03-17", 0, NULL, 1149)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (793, 403, 0, NULL, "2008-03-06", 0, NULL, 1154)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (794, 20.0885, 0, NULL, "2008-03-06", 0, NULL, 1155)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (795, 11.1171, 1, "2009-03-06", "2008-03-06", 1, NULL, 1156)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (796, 3, 0, NULL, "2008-03-06", 0, NULL, 1157)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (797, 22.021, 0, NULL, "2008-03-02", 0, NULL, 1159)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (798, 1, 0, NULL, "2008-05-31", 0, NULL, 1168)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (799, 277, 0, NULL, "2008-05-31", 0, NULL, 1169)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (800, 24.1274, 1, "2009-05-31", "2008-05-31", 1, NULL, 1170)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (801, 18.9534, 1, "2009-05-16", "2008-05-16", 1, NULL, 1171)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (802, 365, 0, NULL, "2008-05-16", 0, NULL, 1172)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (803, 26.2758, 0, NULL, "2008-05-16", 0, NULL, 1173)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (804, 4, 0, NULL, "2008-05-16", 0, NULL, 1174)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (805, 19.1283, 1, "2009-05-05", "2008-05-05", 1, NULL, 1175)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (806, 28.4316, 0, NULL, "2008-05-05", 0, NULL, 1176)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (807, 356, 0, NULL, "2008-05-22", 0, NULL, 1181)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (808, 2, 0, NULL, "2008-05-22", 0, NULL, 1182)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (809, 18.0868, 1, "2009-05-22", "2008-05-22", 1, NULL, 1183)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (810, 27.2062, 0, NULL, "2008-05-22", 0, NULL, 1184)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (811, 21.414, 0, NULL, "2008-06-16", 0, NULL, 1193)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (812, 13.6141, 1, "2009-06-16", "2008-06-16", 1, NULL, 1194)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (813, 272, 0, NULL, "2008-06-16", 0, NULL, 1195)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (814, 2, 0, NULL, "2008-06-16", 0, NULL, 1196)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (815, 27.9123, 0, NULL, "2008-06-20", 0, NULL, 1197)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (816, 21.4699, 1, "2009-06-25", "2008-06-25", 1, NULL, 1198)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (817, 20.6231, 0, NULL, "2008-06-25", 0, NULL, 1199)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (818, 428, 0, NULL, "2008-06-25", 0, NULL, 1200)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (819, 408, 0, NULL, "2008-07-07", 0, NULL, 1205)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (820, 0, 0, NULL, "2008-07-07", 0, NULL, 1206)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (821, 29.5964, 0, NULL, "2008-07-07", 0, NULL, 1207)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (822, 18.7654, 1, "2009-07-20", "2008-07-20", 1, NULL, 1208)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (823, 26.3216, 0, NULL, "2008-07-20", 0, NULL, 1209)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (824, 0, 0, NULL, "2008-07-20", 0, NULL, 1210)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (825, 394, 0, NULL, "2008-07-20", 0, NULL, 1211)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (826, 13.984, 1, "2009-08-13", "2008-08-13", 1, NULL, 1212)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (827, 358, 0, NULL, "2008-08-13", 0, NULL, 1213)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (828, 423, 0, NULL, "2008-08-29", 0, NULL, 1214)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (829, 24.6197, 0, NULL, "2008-08-29", 0, NULL, 1215)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (830, 16.4275, 1, "2009-08-29", "2008-08-29", 1, NULL, 1216)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (831, 5, 0, NULL, "2008-08-29", 0, NULL, 1217)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (832, 428, 0, NULL, "2008-08-29", 0, NULL, 1222)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (833, 22.7481, 1, "2009-08-29", "2008-08-29", 1, NULL, 1223)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (834, 12.7301, 1, "2009-09-24", "2008-09-24", 1, NULL, 1234)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (835, 12.1389, 1, "2009-09-22", "2008-09-22", 1, NULL, 1235)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (836, 24.9739, 0, NULL, "2008-09-22", 0, NULL, 1236)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (837, 1, 0, NULL, "2008-09-22", 0, NULL, 1237)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (838, 308, 0, NULL, "2008-09-22", 0, NULL, 1238)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (839, 17.9647, 1, "2009-09-20", "2008-09-20", 1, NULL, 1243)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (840, 22.5769, 0, NULL, "2008-09-20", 0, NULL, 1244)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (841, 5, 0, NULL, "2008-09-20", 0, NULL, 1245)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (842, 366, 0, NULL, "2008-09-20", 0, NULL, 1246)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (843, 25.1093, 0, NULL, "2008-09-23", 0, NULL, 1247)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (844, 23.8689, 1, "2009-09-23", "2008-09-23", 1, NULL, 1248)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (845, 426, 0, NULL, "2008-09-23", 0, NULL, 1249)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (846, 2, 0, NULL, "2008-09-23", 0, NULL, 1250)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (847, 0, 0, NULL, "2008-09-28", 0, NULL, 1251)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (848, 21.7501, 1, "2009-09-28", "2008-09-28", 1, NULL, 1252)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (849, 278, 0, NULL, "2008-09-28", 0, NULL, 1253)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (850, 20.7955, 0, NULL, "2008-09-28", 0, NULL, 1254)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (851, 310, 0, NULL, "2008-10-26", 0, NULL, 1255)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (852, 10.2624, 1, "2009-10-26", "2008-10-26", 1, NULL, 1256)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (853, 27.7443, 0, NULL, "2008-10-26", 0, NULL, 1257)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (854, 4, 0, NULL, "2008-10-26", 0, NULL, 1258)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (855, 316, 0, NULL, "2008-10-10", 0, NULL, 1259)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (856, 24.2181, 0, NULL, "2008-10-10", 0, NULL, 1260)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (857, 5, 0, NULL, "2008-10-10", 0, NULL, 1261)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (858, 12.6768, 1, "2009-10-10", "2008-10-10", 1, NULL, 1262)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (859, 13.3959, 1, "2009-10-09", "2008-10-09", 1, NULL, 1267)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (860, 451, 0, NULL, "2008-10-09", 0, NULL, 1268)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (861, 2, 0, NULL, "2008-10-09", 0, NULL, 1269)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (862, 20.0104, 0, NULL, "2008-10-09", 0, NULL, 1270)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (863, 21.5074, 0, NULL, "2008-11-15", 0, NULL, 1271)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (864, 433, 0, NULL, "2008-11-15", 0, NULL, 1272)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (865, 1, 0, NULL, "2008-11-28", 0, NULL, 1273)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (866, 488, 0, NULL, "2008-11-28", 0, NULL, 1274)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (867, 21.1282, 1, "2009-11-28", "2008-11-28", 1, NULL, 1275)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (868, 22.8911, 0, NULL, "2008-11-28", 0, NULL, 1276)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (869, 28.4369, 0, NULL, "2009-00-29", 0, NULL, 1277)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (870, 5, 0, NULL, "2009-00-29", 0, NULL, 1278)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (871, 314, 0, NULL, "2009-00-29", 0, NULL, 1279)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (872, 21.8703, 1, "20010-00-29", "2009-00-29", 1, NULL, 1280)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (873, 410, 0, NULL, "2009-00-05", 0, NULL, 1281)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (874, 4, 0, NULL, "2009-00-05", 0, NULL, 1282)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (875, 21.1107, 1, "20010-00-05", "2009-00-05", 1, NULL, 1283)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (876, 21.5883, 0, NULL, "2009-00-05", 0, NULL, 1284)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (877, 20.5002, 1, "20010-00-24", "2009-00-24", 1, NULL, 1289)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (878, 433, 0, NULL, "2009-00-24", 0, NULL, 1290)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (879, 5, 0, NULL, "2009-00-21", 0, NULL, 1295)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (880, 11.3256, 1, "20010-01-02", "2009-01-02", 1, NULL, 1297)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (881, 3, 0, NULL, "2009-01-02", 0, NULL, 1298)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (882, 28.2777, 0, NULL, "2009-01-02", 0, NULL, 1299)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (883, 336, 0, NULL, "2009-01-02", 0, NULL, 1300)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (884, 28.168, 0, NULL, "2009-01-19", 0, NULL, 1305)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (885, 327, 0, NULL, "2009-01-19", 0, NULL, 1306)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (886, 15.3542, 1, "20010-01-19", "2009-01-19", 1, NULL, 1307)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (887, 2, 0, NULL, "2009-01-19", 0, NULL, 1308)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (888, 349, 0, NULL, "2009-01-24", 0, NULL, 1309)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (889, 3, 0, NULL, "2009-01-24", 0, NULL, 1310)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (890, 21.4609, 0, NULL, "2009-01-24", 0, NULL, 1311)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (891, 15.9595, 1, "20010-01-24", "2009-01-24", 1, NULL, 1312)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (892, 283, 0, NULL, "2009-02-11", 0, NULL, 1313)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (893, 25.1862, 0, NULL, "2009-02-11", 0, NULL, 1314)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (894, 4, 0, NULL, "2009-02-11", 0, NULL, 1315)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (895, 11.2328, 1, "20010-02-11", "2009-02-11", 1, NULL, 1316)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (896, 5, 0, NULL, "2009-02-01", 0, NULL, 1325)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (897, 21.3826, 1, "20010-02-01", "2009-02-01", 1, NULL, 1326)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (898, 298, 0, NULL, "2009-02-01", 0, NULL, 1327)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (899, 21.4295, 0, NULL, "2009-02-01", 0, NULL, 1328)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (900, 20.4601, 0, NULL, "2009-02-13", 0, NULL, 1332)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (901, 10.1644, 1, "20010-02-13", "2009-02-13", 1, NULL, 1333)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (902, 28.6034, 0, NULL, "2009-03-09", 0, NULL, 1338)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (903, 12.6307, 1, "20010-03-09", "2009-03-09", 1, NULL, 1339)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (904, 422, 0, NULL, "2009-03-09", 0, NULL, 1340)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (905, 5, 0, NULL, "2009-03-09", 0, NULL, 1341)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (906, 26.7332, 0, NULL, "2009-03-14", 0, NULL, 1342)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (907, 421, 0, NULL, "2009-03-14", 0, NULL, 1343)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (908, 18.7422, 1, "20010-03-14", "2009-03-14", 1, NULL, 1344)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (909, 1, 0, NULL, "2009-03-14", 0, NULL, 1345)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (910, 486, 0, NULL, "2009-03-07", 0, NULL, 1350)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (911, 15.8627, 1, "20010-03-07", "2009-03-07", 1, NULL, 1351)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (912, 24.4263, 0, NULL, "2009-03-07", 0, NULL, 1352)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (913, 1, 0, NULL, "2009-03-07", 0, NULL, 1353)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (914, 294, 0, NULL, "2009-04-29", 0, NULL, 1376)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (915, 22.0006, 0, NULL, "2009-04-29", 0, NULL, 1377)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (916, 3, 0, NULL, "2009-04-29", 0, NULL, 1378)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (917, 461, 0, NULL, "2009-04-21", 0, NULL, 1379)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (918, 29.0639, 0, NULL, "2009-04-21", 0, NULL, 1380)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (919, 3, 0, NULL, "2009-04-21", 0, NULL, 1381)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (920, 13.7101, 1, "20010-04-21", "2009-04-21", 1, NULL, 1382)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (921, 26.1808, 0, NULL, "2009-04-03", 0, NULL, 1383)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (922, 478, 0, NULL, "2009-04-03", 0, NULL, 1384)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (923, 10.7909, 1, "20010-04-03", "2009-04-03", 1, NULL, 1385)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (924, 1, 0, NULL, "2009-04-03", 0, NULL, 1386)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (925, 26.5331, 0, NULL, "2009-04-08", 0, NULL, 1387)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (926, 22.2644, 1, "20010-04-08", "2009-04-08", 1, NULL, 1388)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (927, 429, 0, NULL, "2009-04-08", 0, NULL, 1389)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (928, 5, 0, NULL, "2009-04-08", 0, NULL, 1390)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (929, 23.436, 1, "20010-05-24", "2009-05-24", 1, NULL, 1391)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (930, 29.7623, 0, NULL, "2009-05-24", 0, NULL, 1392)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (931, 20.443, 0, NULL, "2009-05-17", 0, NULL, 1393)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (932, 14.1638, 1, "20010-05-17", "2009-05-17", 1, NULL, 1394)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (933, 380, 0, NULL, "2009-05-17", 0, NULL, 1395)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (934, 4, 0, NULL, "2009-05-17", 0, NULL, 1396)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (935, 319, 0, NULL, "2009-05-20", 0, NULL, 1397)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (936, 1, 0, NULL, "2009-05-20", 0, NULL, 1398)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (937, 20.5557, 0, NULL, "2009-05-20", 0, NULL, 1399)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (938, 12.0946, 1, "20010-05-20", "2009-05-20", 1, NULL, 1400)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (939, 2, 0, NULL, "2009-05-07", 0, NULL, 1401)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (940, 20.1648, 1, "20010-05-07", "2009-05-07", 1, NULL, 1402)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (941, 387, 0, NULL, "2009-05-07", 0, NULL, 1403)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (942, 21.4379, 0, NULL, "2009-05-07", 0, NULL, 1404)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (943, 25.1391, 0, NULL, "2009-06-14", 0, NULL, 1409)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (944, 12.3974, 1, "20010-06-14", "2009-06-14", 1, NULL, 1410)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (945, 5, 0, NULL, "2009-06-14", 0, NULL, 1411)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (946, 348, 0, NULL, "2009-06-14", 0, NULL, 1412)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (947, 4, 0, NULL, "2009-07-10", 0, NULL, 1413)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (948, 25.4588, 0, NULL, "2009-07-10", 0, NULL, 1414)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (949, 13.2421, 1, "20010-07-10", "2009-07-10", 1, NULL, 1415)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (950, 273, 0, NULL, "2009-07-10", 0, NULL, 1416)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (951, 24.1796, 0, NULL, "2009-07-30", 0, NULL, 1417)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (952, 1, 0, NULL, "2009-07-30", 0, NULL, 1418)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (953, 313, 0, NULL, "2009-07-30", 0, NULL, 1419)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (954, 13.9291, 1, "20010-07-30", "2009-07-30", 1, NULL, 1420)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (955, 12.1337, 1, "20010-07-26", "2009-07-26", 1, NULL, 1423)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (956, 23.2049, 0, NULL, "2009-07-26", 0, NULL, 1424)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (957, 19.719, 1, "20010-07-26", "2009-07-26", 1, NULL, 1425)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (958, 1, 0, NULL, "2009-07-26", 0, NULL, 1426)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (959, 24.2773, 0, NULL, "2009-07-26", 0, NULL, 1427)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (960, 294, 0, NULL, "2009-07-26", 0, NULL, 1428)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (961, 16.8802, 1, "20010-07-04", "2009-07-04", 1, NULL, 1429)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (962, 4, 0, NULL, "2009-07-04", 0, NULL, 1430)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (963, 307, 0, NULL, "2009-07-04", 0, NULL, 1431)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (964, 23.4776, 0, NULL, "2009-07-04", 0, NULL, 1432)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (965, 2, 0, NULL, "2009-07-31", 0, NULL, 1433)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (966, 21.1524, 0, NULL, "2009-07-31", 0, NULL, 1434)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (967, 274, 0, NULL, "2009-07-31", 0, NULL, 1435)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (968, 21.8745, 0, NULL, "2009-08-28", 0, NULL, 1440)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (969, 292, 0, NULL, "2009-08-28", 0, NULL, 1441)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (970, 5, 0, NULL, "2009-08-28", 0, NULL, 1442)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (971, 15.2637, 1, "20010-09-03", "2009-09-03", 1, NULL, 1443)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (972, 378, 0, NULL, "2009-09-27", 0, NULL, 1444)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (973, 29.5309, 0, NULL, "2009-09-27", 0, NULL, 1445)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (974, 2, 0, NULL, "2009-09-27", 0, NULL, 1446)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (975, 12.3142, 1, "20010-09-27", "2009-09-27", 1, NULL, 1447)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (976, 259, 0, NULL, "2009-09-10", 0, NULL, 1448)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (977, 21.0271, 1, "20010-09-10", "2009-09-10", 1, NULL, 1449)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (978, 22.1639, 0, NULL, "2009-09-10", 0, NULL, 1450)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (979, 2, 0, NULL, "2009-09-10", 0, NULL, 1451)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (980, 454, 0, NULL, "2009-09-04", 0, NULL, 1452)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (981, 1, 0, NULL, "2009-09-04", 0, NULL, 1453)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (982, 17.7562, 1, "20010-09-04", "2009-09-04", 1, NULL, 1454)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (983, 22.9193, 0, NULL, "2009-09-04", 0, NULL, 1455)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (984, 26.1893, 0, NULL, "2009-09-28", 0, NULL, 1460)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (985, 11.884, 1, "20010-09-28", "2009-09-28", 1, NULL, 1461)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (986, 332, 0, NULL, "2009-09-28", 0, NULL, 1462)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (987, 1, 0, NULL, "2009-09-28", 0, NULL, 1463)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (988, 10.3276, 1, "20010-09-25", "2009-09-25", 1, NULL, 1464)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (989, 318, 0, NULL, "2009-09-25", 0, NULL, 1465)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (990, 285, 0, NULL, "2009-10-19", 0, NULL, 1470)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (991, 0, 0, NULL, "2009-10-19", 0, NULL, 1471)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (992, 21.1367, 0, NULL, "2009-10-19", 0, NULL, 1472)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (993, 22.1001, 1, "20010-10-19", "2009-10-19", 1, NULL, 1473)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (994, 20.5558, 0, NULL, "2009-10-10", 0, NULL, 1486)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (995, 489, 0, NULL, "2009-10-10", 0, NULL, 1487)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (996, 17.7283, 1, "20010-10-10", "2009-10-10", 1, NULL, 1488)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (997, 3, 0, NULL, "2009-10-10", 0, NULL, 1489)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (998, 20.9224, 1, "20010-11-01", "2009-11-01", 1, NULL, 1492)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (999, 282, 0, NULL, "2009-11-01", 0, NULL, 1493)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1000, 5, 0, NULL, "2009-11-01", 0, NULL, 1494)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1001, 21.015, 0, NULL, "2009-11-01", 0, NULL, 1495)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1002, 17.2599, 1, "20010-11-11", "2009-11-11", 1, NULL, 1496)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1003, 467, 0, NULL, "2009-11-10", 0, NULL, 1497)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1004, 22.1668, 1, "20010-11-10", "2009-11-10", 1, NULL, 1498)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1005, 27.3151, 0, NULL, "2009-11-10", 0, NULL, 1499)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1006, 2, 0, NULL, "2009-11-10", 0, NULL, 1500)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1007, 0, 0, NULL, "2009-11-15", 0, NULL, 1501)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1008, 27.2557, 0, NULL, "2009-11-15", 0, NULL, 1502)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1009, 17.9448, 1, "20010-11-15", "2009-11-15", 1, NULL, 1503)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1010, 416, 0, NULL, "2009-11-15", 0, NULL, 1504)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1011, 23.9242, 1, "2011-00-28", "2010-00-28", 1, NULL, 1505)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1012, 445, 0, NULL, "2010-00-28", 0, NULL, 1506)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1013, 1, 0, NULL, "2010-00-28", 0, NULL, 1507)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1014, 17.1656, 0, NULL, "2010-00-28", 0, NULL, 1508)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1015, 279, 0, NULL, "2010-01-14", 0, NULL, 1518)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1016, 5, 0, NULL, "2010-01-30", 0, NULL, 1519)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1017, 450, 0, NULL, "2010-01-30", 0, NULL, 1520)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1018, 25.2688, 0, NULL, "2010-02-24", 0, NULL, 1521)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1019, 375, 0, NULL, "2010-02-24", 0, NULL, 1522)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1020, 17.7433, 1, "2011-02-24", "2010-02-24", 1, NULL, 1523)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1021, 3, 0, NULL, "2010-02-24", 0, NULL, 1524)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1022, 28.0016, 0, NULL, "2010-04-17", 0, NULL, 1531)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1023, 320, 0, NULL, "2010-04-17", 0, NULL, 1532)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1024, 4, 0, NULL, "2010-04-27", 0, NULL, 1533)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1025, 339, 0, NULL, "2010-04-27", 0, NULL, 1534)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1026, 15.2696, 1, "2011-04-29", "2010-04-29", 1, NULL, 1546)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1027, 1, 0, NULL, "2010-04-29", 0, NULL, 1547)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1028, 316, 0, NULL, "2010-04-29", 0, NULL, 1548)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1029, 28.5021, 0, NULL, "2010-04-29", 0, NULL, 1549)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1030, 5, 0, NULL, "2010-05-20", 0, NULL, 1552)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1031, 14.032, 0, NULL, "2010-05-20", 0, NULL, 1553)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1032, 13.632, 1, "2011-05-20", "2010-05-20", 1, NULL, 1554)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1033, 315, 0, NULL, "2010-05-20", 0, NULL, 1555)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1034, 289, 0, NULL, "2010-05-06", 0, NULL, 1556)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1035, 22.7733, 1, "2011-05-06", "2010-05-06", 1, NULL, 1557)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1036, 29.0123, 0, NULL, "2010-05-06", 0, NULL, 1558)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1037, 3, 0, NULL, "2010-05-06", 0, NULL, 1559)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1038, 21.0068, 1, "2011-05-19", "2010-05-19", 1, NULL, 1560)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1039, 4, 0, NULL, "2010-05-19", 0, NULL, 1561)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1040, 24.8575, 0, NULL, "2010-05-19", 0, NULL, 1562)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1041, 396, 0, NULL, "2010-05-19", 0, NULL, 1563)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1042, 12.733, 1, "2011-05-23", "2010-05-23", 1, NULL, 1568)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1043, 3, 0, NULL, "2010-05-23", 0, NULL, 1569)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1044, 410, 0, NULL, "2010-05-23", 0, NULL, 1570)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1045, 20.9787, 0, NULL, "2010-05-23", 0, NULL, 1571)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1046, 28.7078, 0, NULL, "2010-06-01", 0, NULL, 1573)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1047, 259, 0, NULL, "2010-06-14", 0, NULL, 1574)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1048, 16.138, 1, "2011-06-14", "2010-06-14", 1, NULL, 1575)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1049, 24.0773, 0, NULL, "2010-06-14", 0, NULL, 1576)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1050, 5, 0, NULL, "2010-06-14", 0, NULL, 1577)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1051, 19.3621, 0, NULL, "2010-07-30", 0, NULL, 1586)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1052, 4, 0, NULL, "2010-07-30", 0, NULL, 1587)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1053, 340, 0, NULL, "2010-07-30", 0, NULL, 1588)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1054, 11.8935, 1, "2011-07-30", "2010-07-30", 1, NULL, 1589)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1055, 412, 0, NULL, "2010-07-05", 0, NULL, 1590)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1056, 2, 0, NULL, "2010-07-05", 0, NULL, 1591)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1057, 25.8185, 0, NULL, "2010-07-05", 0, NULL, 1592)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1058, 23.972, 1, "2011-07-05", "2010-07-05", 1, NULL, 1593)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1059, 26.8253, 0, NULL, "2010-07-28", 0, NULL, 1594)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1060, 15.7653, 1, "2011-07-28", "2010-07-28", 1, NULL, 1595)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1061, 4, 0, NULL, "2010-07-28", 0, NULL, 1596)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1062, 424, 0, NULL, "2010-07-28", 0, NULL, 1597)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1063, 20.9895, 0, NULL, "2010-08-24", 0, NULL, 1602)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1064, 5, 0, NULL, "2010-08-24", 0, NULL, 1603)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1065, 24.2571, 1, "2011-08-24", "2010-08-24", 1, NULL, 1604)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1066, 410, 0, NULL, "2010-08-24", 0, NULL, 1605)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1067, 4, 0, NULL, "2010-08-07", 0, NULL, 1610)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1068, 18.7094, 1, "2011-08-07", "2010-08-07", 1, NULL, 1611)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1069, 29.7392, 0, NULL, "2010-08-07", 0, NULL, 1612)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1070, 379, 0, NULL, "2010-08-07", 0, NULL, 1613)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1071, 1, 0, NULL, "2010-09-15", 0, NULL, 1626)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1072, 21.2316, 0, NULL, "2010-10-16", 0, NULL, 1631)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1073, 12.1645, 1, "2011-10-16", "2010-10-16", 1, NULL, 1632)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1074, 2, 0, NULL, "2010-10-16", 0, NULL, 1633)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1075, 292, 0, NULL, "2010-10-16", 0, NULL, 1634)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1076, 291, 0, NULL, "2010-11-18", 0, NULL, 1647)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1077, 2, 0, NULL, "2010-11-18", 0, NULL, 1648)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1078, 26.6136, 0, NULL, "2010-11-18", 0, NULL, 1649)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1079, 21.8725, 1, "2011-11-18", "2010-11-18", 1, NULL, 1650)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1080, 15.1885, 1, "2011-11-21", "2010-11-21", 1, NULL, 1651)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1081, 3, 0, NULL, "2010-11-21", 0, NULL, 1652)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1082, 27.3446, 0, NULL, "2010-11-21", 0, NULL, 1653)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1083, 287, 0, NULL, "2010-11-21", 0, NULL, 1654)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1084, 262, 0, NULL, "2011-00-30", 0, NULL, 1659)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1085, 23.0458, 0, NULL, "2011-00-30", 0, NULL, 1660)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1086, 22.0809, 1, "2012-00-30", "2011-00-30", 1, NULL, 1661)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1087, 4, 0, NULL, "2011-00-30", 0, NULL, 1662)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1088, 19.5171, 1, "2012-00-15", "2011-00-15", 1, NULL, 1663)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1089, 3, 0, NULL, "2011-00-26", 0, NULL, 1664)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1090, 26.4665, 0, NULL, "2011-00-26", 0, NULL, 1665)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1091, 13.8422, 1, "2012-00-26", "2011-00-26", 1, NULL, 1666)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1092, 394, 0, NULL, "2011-00-26", 0, NULL, 1667)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1093, 15.8456, 1, "2012-00-03", "2011-00-03", 1, NULL, 1668)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1094, 497, 0, NULL, "2011-00-03", 0, NULL, 1669)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1095, 23.1563, 0, NULL, "2011-00-03", 0, NULL, 1670)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1096, 3, 0, NULL, "2011-00-03", 0, NULL, 1671)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1097, 29.9331, 0, NULL, "2011-01-24", 0, NULL, 1680)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1098, 23.8075, 1, "2012-01-24", "2011-01-24", 1, NULL, 1681)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1099, 2, 0, NULL, "2011-01-24", 0, NULL, 1682)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1100, 26.4042, 0, NULL, "2011-01-01", 0, NULL, 1683)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1101, 381, 0, NULL, "2011-01-01", 0, NULL, 1684)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1102, 18.3211, 1, "2012-01-01", "2011-01-01", 1, NULL, 1685)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1103, 1, 0, NULL, "2011-01-01", 0, NULL, 1686)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1104, 20.9795, 0, NULL, "2011-01-11", 0, NULL, 1687)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1105, 0, 0, NULL, "2011-01-11", 0, NULL, 1688)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1106, 454, 0, NULL, "2011-01-11", 0, NULL, 1689)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1107, 18.1908, 1, "2012-01-11", "2011-01-11", 1, NULL, 1690)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1108, 4, 0, NULL, "2011-02-09", 0, NULL, 1691)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1109, 427, 0, NULL, "2011-02-09", 0, NULL, 1692)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1110, 27.5314, 0, NULL, "2011-02-09", 0, NULL, 1693)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1111, 22.3911, 1, "2012-02-09", "2011-02-09", 1, NULL, 1694)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1112, 3, 0, NULL, "2011-02-26", 0, NULL, 1695)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1113, 361, 0, NULL, "2011-02-26", 0, NULL, 1696)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1114, 23.7959, 0, NULL, "2011-02-26", 0, NULL, 1697)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1115, 12.6458, 1, "2012-02-26", "2011-02-26", 1, NULL, 1698)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1116, 19.9882, 1, "2012-02-07", "2011-02-07", 1, NULL, 1699)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1117, 436, 0, NULL, "2011-02-07", 0, NULL, 1700)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1118, 2, 0, NULL, "2011-02-07", 0, NULL, 1701)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1119, 26.8823, 0, NULL, "2011-02-07", 0, NULL, 1702)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1120, 348, 0, NULL, "2011-02-03", 0, NULL, 1703)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1121, 24.0635, 0, NULL, "2011-02-03", 0, NULL, 1704)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1122, 21.1492, 1, "2012-03-27", "2011-03-27", 1, NULL, 1705)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1123, 20.0426, 0, NULL, "2011-03-27", 0, NULL, 1706)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1124, 405, 0, NULL, "2011-03-27", 0, NULL, 1707)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1125, 5, 0, NULL, "2011-03-27", 0, NULL, 1708)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1126, 28.0136, 0, NULL, "2011-03-19", 0, NULL, 1709)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1127, 19.5183, 1, "2012-03-19", "2011-03-19", 1, NULL, 1710)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1128, 2, 0, NULL, "2011-03-19", 0, NULL, 1711)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1129, 283, 0, NULL, "2011-03-19", 0, NULL, 1712)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1130, 5, 0, NULL, "2011-03-11", 0, NULL, 1713)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1131, 370, 0, NULL, "2011-03-11", 0, NULL, 1714)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1132, 440, 0, NULL, "2011-03-04", 0, NULL, 1715)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1133, 2, 0, NULL, "2011-03-04", 0, NULL, 1716)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1134, 20.0691, 0, NULL, "2011-03-04", 0, NULL, 1717)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1135, 19.9507, 1, "2012-03-04", "2011-03-04", 1, NULL, 1718)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1136, 4, 0, NULL, "2011-03-02", 0, NULL, 1719)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1137, 29.3516, 0, NULL, "2011-03-02", 0, NULL, 1720)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1138, 422, 0, NULL, "2011-03-02", 0, NULL, 1721)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1139, 15.9819, 1, "2012-03-02", "2011-03-02", 1, NULL, 1722)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1140, 384, 0, NULL, "2011-03-21", 0, NULL, 1723)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1141, 10.3525, 1, "2012-03-21", "2011-03-21", 1, NULL, 1724)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1142, 24.8818, 0, NULL, "2011-03-21", 0, NULL, 1725)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1143, 3, 0, NULL, "2011-04-10", 0, NULL, 1726)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1144, 388, 0, NULL, "2011-04-10", 0, NULL, 1727)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1145, 16.032, 1, "2012-04-10", "2011-04-10", 1, NULL, 1728)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1146, 24.4116, 0, NULL, "2011-04-10", 0, NULL, 1729)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1147, 5, 0, NULL, "2011-04-01", 0, NULL, 1730)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1148, 29.5869, 0, NULL, "2011-04-01", 0, NULL, 1731)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1149, 20.6791, 1, "2012-04-01", "2011-04-01", 1, NULL, 1732)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1150, 368, 0, NULL, "2011-04-01", 0, NULL, 1733)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1151, 427, 0, NULL, "2011-04-30", 0, NULL, 1734)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1152, 23.2289, 1, "2012-04-30", "2011-04-30", 1, NULL, 1735)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1153, 0, 0, NULL, "2011-04-30", 0, NULL, 1736)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1154, 3, 0, NULL, "2011-04-18", 0, NULL, 1737)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1155, 18.1643, 1, "2012-04-18", "2011-04-18", 1, NULL, 1738)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1156, 361, 0, NULL, "2011-04-18", 0, NULL, 1739)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1157, 20.1304, 0, NULL, "2011-04-18", 0, NULL, 1740)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1158, 2, 0, NULL, "2011-04-03", 0, NULL, 1743)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1159, 22.4583, 1, "2012-04-03", "2011-04-03", 1, NULL, 1744)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1160, 27.9159, 0, NULL, "2011-05-30", 0, NULL, 1745)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1161, 500, 0, NULL, "2011-05-30", 0, NULL, 1746)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1162, 18.2247, 1, "2012-05-30", "2011-05-30", 1, NULL, 1747)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1163, 4, 0, NULL, "2011-05-30", 0, NULL, 1748)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1164, 256, 0, NULL, "2011-06-30", 0, NULL, 1753)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1165, 26.7277, 0, NULL, "2011-06-30", 0, NULL, 1754)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1166, 19.2423, 1, "2012-06-30", "2011-06-30", 1, NULL, 1755)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1167, 1, 0, NULL, "2011-06-30", 0, NULL, 1756)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1168, 5, 0, NULL, "2011-06-29", 0, NULL, 1757)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1169, 11.1375, 1, "2012-06-29", "2011-06-29", 1, NULL, 1758)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1170, 21.7752, 0, NULL, "2011-06-29", 0, NULL, 1759)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1171, 482, 0, NULL, "2011-06-29", 0, NULL, 1760)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1172, 4, 0, NULL, "2011-06-30", 0, NULL, 1761)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1173, 12.9986, 1, "2012-06-30", "2011-06-30", 1, NULL, 1762)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1174, 275, 0, NULL, "2011-06-30", 0, NULL, 1763)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1175, 22.7549, 0, NULL, "2011-06-30", 0, NULL, 1764)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1176, 21.5775, 1, "2012-06-11", "2011-06-11", 1, NULL, 1765)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1177, 23.4052, 0, NULL, "2011-06-11", 0, NULL, 1766)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1178, 341, 0, NULL, "2011-06-11", 0, NULL, 1767)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1179, 1, 0, NULL, "2011-06-11", 0, NULL, 1768)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1180, 20.0148, 0, NULL, "2011-06-09", 0, NULL, 1773)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1181, 14.3022, 1, "2012-06-09", "2011-06-09", 1, NULL, 1774)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1182, 436, 0, NULL, "2011-06-09", 0, NULL, 1775)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1183, 5, 0, NULL, "2011-06-09", 0, NULL, 1776)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1184, 279, 0, NULL, "2011-06-16", 0, NULL, 1777)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1185, 22.1486, 1, "2012-06-16", "2011-06-16", 1, NULL, 1778)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1186, 21.5176, 0, NULL, "2011-06-16", 0, NULL, 1779)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1187, 4, 0, NULL, "2011-06-16", 0, NULL, 1780)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1188, 340, 0, NULL, "2011-07-31", 0, NULL, 1788)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1189, 4, 0, NULL, "2011-07-31", 0, NULL, 1789)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1190, 22.2392, 1, "2012-07-31", "2011-07-31", 1, NULL, 1790)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1191, 20.5442, 0, NULL, "2011-07-31", 0, NULL, 1791)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1192, 1, 0, NULL, "2011-07-28", 0, NULL, 1792)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1193, 350, 0, NULL, "2011-07-28", 0, NULL, 1793)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1194, 23.3585, 1, "2012-07-28", "2011-07-28", 1, NULL, 1794)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1195, 21.1568, 0, NULL, "2011-07-28", 0, NULL, 1795)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1196, 29.8458, 0, NULL, "2011-08-04", 0, NULL, 1796)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1197, 21.8586, 1, "2012-08-04", "2011-08-04", 1, NULL, 1797)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1198, 4, 0, NULL, "2011-08-04", 0, NULL, 1798)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1199, 469, 0, NULL, "2011-08-04", 0, NULL, 1799)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1200, 320, 0, NULL, "2011-08-04", 0, NULL, 1800)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1201, 2, 0, NULL, "2011-08-04", 0, NULL, 1801)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1202, 29.2166, 0, NULL, "2011-08-04", 0, NULL, 1802)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1203, 22.5338, 1, "2012-08-04", "2011-08-04", 1, NULL, 1803)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1204, 5, 0, NULL, "2011-09-07", 0, NULL, 1808)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1205, 22.3533, 1, "2012-09-07", "2011-09-07", 1, NULL, 1809)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1206, 23.9913, 0, NULL, "2011-10-23", 0, NULL, 1810)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1207, 3, 0, NULL, "2011-10-23", 0, NULL, 1811)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1208, 289, 0, NULL, "2011-10-16", 0, NULL, 1812)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1209, 20.9466, 0, NULL, "2011-10-16", 0, NULL, 1813)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1210, 21.1803, 1, "2012-10-16", "2011-10-16", 1, NULL, 1814)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1211, 1, 0, NULL, "2011-10-16", 0, NULL, 1815)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1212, 10.3037, 1, "2012-11-03", "2011-11-03", 1, NULL, 1820)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1213, 25.7538, 0, NULL, "2011-11-03", 0, NULL, 1821)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1214, 261, 0, NULL, "2011-11-03", 0, NULL, 1822)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1215, 1, 0, NULL, "2011-11-03", 0, NULL, 1823)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1216, 3, 0, NULL, "2011-11-11", 0, NULL, 1832)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1217, 286, 0, NULL, "2011-11-11", 0, NULL, 1833)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1218, 26.2919, 0, NULL, "2011-11-11", 0, NULL, 1834)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1219, 12.9783, 1, "2012-11-11", "2011-11-11", 1, NULL, 1835)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1220, 11.7826, 1, "2013-00-13", "2012-00-13", 1, NULL, 1836)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1221, 369, 0, NULL, "2012-00-13", 0, NULL, 1837)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1222, 22.0391, 0, NULL, "2012-00-13", 0, NULL, 1838)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1223, 5, 0, NULL, "2012-00-13", 0, NULL, 1839)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1224, 1, 0, NULL, "2012-01-06", 0, NULL, 1844)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1225, 20.3649, 0, NULL, "2012-01-06", 0, NULL, 1845)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1226, 410, 0, NULL, "2012-01-06", 0, NULL, 1846)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1227, 13.9335, 1, "2013-01-06", "2012-01-06", 1, NULL, 1847)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1228, 17.9945, 1, "2013-01-29", "2012-01-29", 1, NULL, 1848)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1229, 29.6789, 0, NULL, "2012-01-29", 0, NULL, 1849)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1230, 469, 0, NULL, "2012-01-29", 0, NULL, 1850)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1231, 3, 0, NULL, "2012-01-29", 0, NULL, 1851)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1232, 2, 0, NULL, "2012-01-02", 0, NULL, 1864)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1233, 344, 0, NULL, "2012-01-02", 0, NULL, 1865)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1234, 22.2878, 0, NULL, "2012-01-02", 0, NULL, 1866)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1235, 17.4835, 1, "2013-01-02", "2012-01-02", 1, NULL, 1867)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1236, 424, 0, NULL, "2012-01-10", 0, NULL, 1868)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1237, 5, 0, NULL, "2012-01-10", 0, NULL, 1869)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1238, 24.292, 0, NULL, "2012-01-10", 0, NULL, 1870)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1239, 24.0278, 1, "2013-01-10", "2012-01-10", 1, NULL, 1871)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1240, 4, 0, NULL, "2012-02-09", 0, NULL, 1872)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1241, 11.4362, 1, "2013-02-09", "2012-02-09", 1, NULL, 1873)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1242, 495, 0, NULL, "2012-02-09", 0, NULL, 1874)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1243, 24.7791, 0, NULL, "2012-02-09", 0, NULL, 1875)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1244, 24.5288, 0, NULL, "2012-03-23", 0, NULL, 1876)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1245, 448, 0, NULL, "2012-03-23", 0, NULL, 1877)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1246, 20.5709, 1, "2013-03-23", "2012-03-23", 1, NULL, 1878)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1247, 5, 0, NULL, "2012-03-23", 0, NULL, 1879)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1248, 25.0854, 0, NULL, "2012-03-04", 0, NULL, 1880)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1249, 5, 0, NULL, "2012-03-04", 0, NULL, 1881)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1250, 431, 0, NULL, "2012-03-04", 0, NULL, 1882)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1251, 24.6933, 1, "2013-03-04", "2012-03-04", 1, NULL, 1883)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1252, 18.2511, 1, "2013-03-31", "2012-03-31", 1, NULL, 1884)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1253, 26.2148, 0, NULL, "2012-03-31", 0, NULL, 1885)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1254, 0, 0, NULL, "2012-03-31", 0, NULL, 1886)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1255, 275, 0, NULL, "2012-03-31", 0, NULL, 1887)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1256, 29.7826, 0, NULL, "2012-03-29", 0, NULL, 1888)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1257, 20.6089, 1, "2013-03-29", "2012-03-29", 1, NULL, 1889)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1258, 290, 0, NULL, "2012-03-29", 0, NULL, 1890)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1259, 1, 0, NULL, "2012-03-29", 0, NULL, 1891)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1260, 1, 0, NULL, "2012-03-22", 0, NULL, 1892)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1261, 15.167, 1, "2013-03-22", "2012-03-22", 1, NULL, 1893)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1262, 461, 0, NULL, "2012-03-22", 0, NULL, 1894)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1263, 26.4938, 0, NULL, "2012-03-22", 0, NULL, 1895)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1264, 5, 0, NULL, "2012-05-30", 0, NULL, 1907)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1265, 330, 0, NULL, "2012-05-30", 0, NULL, 1908)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1266, 15.8575, 1, "2013-05-30", "2012-05-30", 1, NULL, 1909)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1267, 26.3411, 0, NULL, "2012-05-30", 0, NULL, 1910)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1268, 23.7957, 1, "2013-06-30", "2012-06-30", 1, NULL, 1911)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1269, 389, 0, NULL, "2012-06-30", 0, NULL, 1912)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1270, 3, 0, NULL, "2012-06-30", 0, NULL, 1913)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1271, 28.3648, 0, NULL, "2012-06-30", 0, NULL, 1914)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1272, 21.3769, 1, "2013-06-03", "2012-06-03", 1, NULL, 1919)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1273, 29.6061, 0, NULL, "2012-06-03", 0, NULL, 1920)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1274, 361, 0, NULL, "2012-06-03", 0, NULL, 1921)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1275, 4, 0, NULL, "2012-06-03", 0, NULL, 1922)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1276, 2, 0, NULL, "2012-06-04", 0, NULL, 1923)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1277, 461, 0, NULL, "2012-06-04", 0, NULL, 1924)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1278, 24.5219, 1, "2013-06-04", "2012-06-04", 1, NULL, 1925)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1279, 15.0882, 0, NULL, "2012-06-04", 0, NULL, 1926)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1280, 24.1976, 0, NULL, "2012-07-21", 0, NULL, 1927)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1281, 22.487, 0, NULL, "2012-07-13", 0, NULL, 1932)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1282, 343, 0, NULL, "2012-07-13", 0, NULL, 1933)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1283, 27.3917, 0, NULL, "2012-07-06", 0, NULL, 1938)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1284, 3, 0, NULL, "2012-07-06", 0, NULL, 1939)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1285, 300, 0, NULL, "2012-07-06", 0, NULL, 1940)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1286, 16.2031, 1, "2013-07-06", "2012-07-06", 1, NULL, 1941)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1287, 16.4324, 1, "2013-08-15", "2012-08-15", 1, NULL, 1944)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1288, 28.836, 0, NULL, "2012-08-15", 0, NULL, 1945)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1289, 16.0521, 1, "2013-08-28", "2012-08-28", 1, NULL, 1946)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1290, 261, 0, NULL, "2012-08-28", 0, NULL, 1947)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1291, 2, 0, NULL, "2012-08-28", 0, NULL, 1948)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1292, 27.03, 0, NULL, "2012-08-28", 0, NULL, 1949)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1293, 22.7939, 0, NULL, "2012-09-10", 0, NULL, 1950)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1294, 17.0568, 1, "2013-09-10", "2012-09-10", 1, NULL, 1951)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1295, 334, 0, NULL, "2012-09-10", 0, NULL, 1952)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1296, 4, 0, NULL, "2012-09-10", 0, NULL, 1953)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1297, 23.8762, 1, "2013-09-31", "2012-09-31", 1, NULL, 1954)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1298, 256, 0, NULL, "2012-09-31", 0, NULL, 1955)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1299, 22.0739, 0, NULL, "2012-09-31", 0, NULL, 1956)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1300, 5, 0, NULL, "2012-09-31", 0, NULL, 1957)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1301, 325, 0, NULL, "2012-09-26", 0, NULL, 1958)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1302, 22.907, 0, NULL, "2012-09-26", 0, NULL, 1959)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1303, 24.9351, 1, "2013-09-26", "2012-09-26", 1, NULL, 1960)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1304, 3, 0, NULL, "2012-09-26", 0, NULL, 1961)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1305, 447, 0, NULL, "2012-09-30", 0, NULL, 1962)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1306, 4, 0, NULL, "2012-09-30", 0, NULL, 1963)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1307, 20.8046, 0, NULL, "2012-09-30", 0, NULL, 1964)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1308, 14.3129, 1, "2013-09-30", "2012-09-30", 1, NULL, 1965)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1309, 23.4876, 1, "2013-09-06", "2012-09-06", 1, NULL, 1966)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1310, 417, 0, NULL, "2012-09-06", 0, NULL, 1967)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1311, 5, 0, NULL, "2012-09-06", 0, NULL, 1968)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1312, 27.4598, 0, NULL, "2012-09-06", 0, NULL, 1969)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1313, 1, 0, NULL, "2012-10-04", 0, NULL, 1970)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1314, 29.3504, 0, NULL, "2012-10-04", 0, NULL, 1971)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1315, 287, 0, NULL, "2012-10-04", 0, NULL, 1972)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1316, 10.4525, 1, "2013-10-04", "2012-10-04", 1, NULL, 1973)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1317, 454, 0, NULL, "2012-10-19", 0, NULL, 1974)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1318, 21.6443, 1, "2013-10-19", "2012-10-19", 1, NULL, 1975)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1319, 29.193, 0, NULL, "2012-10-19", 0, NULL, 1976)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1320, 2, 0, NULL, "2012-10-19", 0, NULL, 1977)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1321, 3, 0, NULL, "2012-10-18", 0, NULL, 1981)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1322, 15.6797, 1, "2013-10-18", "2012-10-18", 1, NULL, 1982)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1323, 20.2088, 1, "2013-10-19", "2012-10-19", 1, NULL, 1983)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1324, 22.6549, 0, NULL, "2012-10-19", 0, NULL, 1984)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1325, 252, 0, NULL, "2012-10-19", 0, NULL, 1985)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1326, 1, 0, NULL, "2012-10-19", 0, NULL, 1986)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1327, 475, 0, NULL, "2012-11-25", 0, NULL, 1987)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1328, 5, 0, NULL, "2012-11-25", 0, NULL, 1988)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1329, 17.9008, 1, "2013-11-25", "2012-11-25", 1, NULL, 1989)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1330, 21.6151, 0, NULL, "2012-11-25", 0, NULL, 1990)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1331, 370, 0, NULL, "2012-11-10", 0, NULL, 1991)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1332, 0, 0, NULL, "2012-11-10", 0, NULL, 1992)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1333, 19.8374, 1, "2013-11-10", "2012-11-10", 1, NULL, 1993)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1334, 19.7255, 0, NULL, "2012-11-10", 0, NULL, 1994)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1335, 23.4814, 1, "2013-11-11", "2012-11-11", 1, NULL, 1999)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1336, 5, 0, NULL, "2012-11-11", 0, NULL, 2000)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1337, 21.5137, 0, NULL, "2012-11-11", 0, NULL, 2001)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1338, 384, 0, NULL, "2012-11-11", 0, NULL, 2002)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1339, 17.6811, 1, "2013-11-22", "2012-11-22", 1, NULL, 2007)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1340, 359, 0, NULL, "2012-11-22", 0, NULL, 2008)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1341, 2, 0, NULL, "2012-11-22", 0, NULL, 2009)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1342, 14.4911, 0, NULL, "2012-11-22", 0, NULL, 2010)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1343, 352, 0, NULL, "2012-11-06", 0, NULL, 2011)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1344, 14.9692, 1, "2013-11-06", "2012-11-06", 1, NULL, 2012)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1345, 27.0328, 0, NULL, "2012-11-06", 0, NULL, 2013)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1346, 4, 0, NULL, "2012-11-06", 0, NULL, 2014)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1347, 16.067, 1, "2014-00-05", "2013-00-05", 1, NULL, 2015)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1348, 2, 0, NULL, "2013-00-05", 0, NULL, 2016)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1349, 26.2818, 0, NULL, "2013-00-05", 0, NULL, 2017)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1350, 457, 0, NULL, "2013-00-05", 0, NULL, 2018)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1351, 10.6629, 1, "2014-00-07", "2013-00-07", 1, NULL, 2019)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1352, 5, 0, NULL, "2013-00-07", 0, NULL, 2020)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1353, 25.4485, 0, NULL, "2013-00-07", 0, NULL, 2021)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1354, 402, 0, NULL, "2013-00-07", 0, NULL, 2022)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1355, 1, 0, NULL, "2013-00-17", 0, NULL, 2025)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1356, 26.2673, 0, NULL, "2013-00-17", 0, NULL, 2026)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1357, 12.4415, 1, "2014-01-30", "2013-01-30", 1, NULL, 2027)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1358, 1, 0, NULL, "2013-01-30", 0, NULL, 2028)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1359, 26.0521, 0, NULL, "2013-01-30", 0, NULL, 2029)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1360, 285, 0, NULL, "2013-01-30", 0, NULL, 2030)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1361, 5, 0, NULL, "2013-01-10", 0, NULL, 2031)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1362, 301, 0, NULL, "2013-01-10", 0, NULL, 2032)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1363, 26.6621, 0, NULL, "2013-01-10", 0, NULL, 2033)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1364, 28.5605, 0, NULL, "2013-01-12", 0, NULL, 2034)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1365, 359, 0, NULL, "2013-01-12", 0, NULL, 2035)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1366, 17.4243, 1, "2014-01-14", "2013-01-14", 1, NULL, 2036)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1367, 5, 0, NULL, "2013-01-14", 0, NULL, 2037)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1368, 298, 0, NULL, "2013-01-14", 0, NULL, 2038)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1369, 19.1537, 0, NULL, "2013-01-14", 0, NULL, 2039)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1370, 11.1103, 1, "2014-02-10", "2013-02-10", 1, NULL, 2040)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1371, 484, 0, NULL, "2013-02-10", 0, NULL, 2041)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1372, 2, 0, NULL, "2013-02-10", 0, NULL, 2042)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1373, 21.647, 0, NULL, "2013-02-10", 0, NULL, 2043)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1374, 4, 0, NULL, "2013-02-18", 0, NULL, 2044)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1375, 334, 0, NULL, "2013-02-18", 0, NULL, 2045)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1376, 23.2405, 0, NULL, "2013-02-18", 0, NULL, 2046)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1377, 17.3526, 1, "2014-02-18", "2013-02-18", 1, NULL, 2047)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1378, 22.3684, 0, NULL, "2013-02-10", 0, NULL, 2048)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1379, 17.3796, 1, "2014-02-10", "2013-02-10", 1, NULL, 2049)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1380, 16.9488, 1, "2014-02-05", "2013-02-05", 1, NULL, 2050)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1381, 3, 0, NULL, "2013-02-05", 0, NULL, 2051)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1382, 27.0088, 0, NULL, "2013-03-20", 0, NULL, 2052)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1383, 3, 0, NULL, "2013-03-20", 0, NULL, 2053)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1384, 402, 0, NULL, "2013-03-20", 0, NULL, 2054)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1385, 23.0445, 1, "2014-03-20", "2013-03-20", 1, NULL, 2055)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1386, 22.2243, 1, "2014-03-09", "2013-03-09", 1, NULL, 2056)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1387, 19.5379, 1, "2014-03-04", "2013-03-04", 1, NULL, 2061)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1388, 471, 0, NULL, "2013-03-04", 0, NULL, 2062)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1389, 20.2921, 0, NULL, "2013-03-04", 0, NULL, 2063)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1390, 5, 0, NULL, "2013-03-04", 0, NULL, 2064)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1391, 22.0305, 0, NULL, "2013-04-08", 0, NULL, 2069)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1392, 27.9265, 0, NULL, "2013-04-10", 0, NULL, 2070)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1393, 19.0122, 1, "2014-04-10", "2013-04-10", 1, NULL, 2071)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1394, 4, 0, NULL, "2013-04-10", 0, NULL, 2072)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1395, 23.1408, 1, "2014-04-17", "2013-04-17", 1, NULL, 2073)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1396, 435, 0, NULL, "2013-04-17", 0, NULL, 2074)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1397, 23.134, 0, NULL, "2013-04-17", 0, NULL, 2075)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1398, 17.1118, 0, NULL, "2013-05-31", 0, NULL, 2076)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1399, 21.2202, 1, "2014-05-31", "2013-05-31", 1, NULL, 2077)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1400, 254, 0, NULL, "2013-05-31", 0, NULL, 2078)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1401, 5, 0, NULL, "2013-05-22", 0, NULL, 2079)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1402, 22.8379, 0, NULL, "2013-05-22", 0, NULL, 2080)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1403, 10.5829, 1, "2014-05-22", "2013-05-22", 1, NULL, 2081)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1404, 253, 0, NULL, "2013-05-22", 0, NULL, 2082)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1405, 319, 0, NULL, "2013-05-12", 0, NULL, 2083)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1406, 3, 0, NULL, "2013-05-16", 0, NULL, 2084)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1407, 262, 0, NULL, "2013-05-16", 0, NULL, 2085)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1408, 15.3712, 1, "2014-05-16", "2013-05-16", 1, NULL, 2086)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1409, 23.4384, 0, NULL, "2013-05-16", 0, NULL, 2087)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1410, 3, 0, NULL, "2013-06-23", 0, NULL, 2093)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1411, 14.8872, 1, "2014-06-23", "2013-06-23", 1, NULL, 2094)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1412, 479, 0, NULL, "2013-06-23", 0, NULL, 2095)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1413, 28.7436, 0, NULL, "2013-06-23", 0, NULL, 2096)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1414, 23.5752, 0, NULL, "2013-06-23", 0, NULL, 2097)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1415, 377, 0, NULL, "2013-06-23", 0, NULL, 2098)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1416, 16.6037, 1, "2014-06-23", "2013-06-23", 1, NULL, 2099)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1417, 1, 0, NULL, "2013-06-23", 0, NULL, 2100)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1418, 320, 0, NULL, "2013-06-13", 0, NULL, 2101)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1419, 5, 0, NULL, "2013-06-13", 0, NULL, 2102)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1420, 24.4321, 0, NULL, "2013-06-13", 0, NULL, 2103)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1421, 274, 0, NULL, "2013-06-13", 0, NULL, 2108)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1422, 27.3519, 0, NULL, "2013-06-13", 0, NULL, 2109)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1423, 0, 0, NULL, "2013-06-13", 0, NULL, 2110)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1424, 16.4155, 1, "2014-06-13", "2013-06-13", 1, NULL, 2111)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1425, 4, 0, NULL, "2013-07-20", 0, NULL, 2112)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1426, 22.4353, 0, NULL, "2013-07-20", 0, NULL, 2113)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1427, 352, 0, NULL, "2013-07-20", 0, NULL, 2114)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1428, 22.463, 1, "2014-07-20", "2013-07-20", 1, NULL, 2115)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1429, 311, 0, NULL, "2013-07-10", 0, NULL, 2116)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1430, 5, 0, NULL, "2013-07-15", 0, NULL, 2117)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1431, 27.9962, 0, NULL, "2013-07-15", 0, NULL, 2118)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1432, 22.3539, 1, "2014-07-15", "2013-07-15", 1, NULL, 2119)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1433, 435, 0, NULL, "2013-07-15", 0, NULL, 2120)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1434, 3, 0, NULL, "2013-08-19", 0, NULL, 2123)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1435, 380, 0, NULL, "2013-08-19", 0, NULL, 2124)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1436, 23.2922, 1, "2014-08-19", "2013-08-19", 1, NULL, 2125)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1437, 28.7847, 0, NULL, "2013-08-19", 0, NULL, 2126)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1438, 14.1995, 1, "2014-08-10", "2013-08-10", 1, NULL, 2127)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1439, 256, 0, NULL, "2013-08-10", 0, NULL, 2128)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1440, 0, 0, NULL, "2013-08-10", 0, NULL, 2129)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1441, 22.4124, 0, NULL, "2013-08-10", 0, NULL, 2130)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1442, 366, 0, NULL, "2013-08-01", 0, NULL, 2131)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1443, 26.42, 0, NULL, "2013-08-01", 0, NULL, 2132)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1444, 22.7258, 1, "2014-08-01", "2013-08-01", 1, NULL, 2133)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1445, 5, 0, NULL, "2013-08-01", 0, NULL, 2134)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1446, 23.9884, 0, NULL, "2013-08-07", 0, NULL, 2135)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1447, 19.2962, 1, "2014-08-07", "2013-08-07", 1, NULL, 2136)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1448, 2, 0, NULL, "2013-08-07", 0, NULL, 2137)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1449, 476, 0, NULL, "2013-08-07", 0, NULL, 2138)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1450, 29.5518, 0, NULL, "2013-08-18", 0, NULL, 2139)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1451, 357, 0, NULL, "2013-08-18", 0, NULL, 2140)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1452, 20.2849, 1, "2014-08-18", "2013-08-18", 1, NULL, 2141)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1453, 1, 0, NULL, "2013-08-18", 0, NULL, 2142)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1454, 4, 0, NULL, "2013-08-28", 0, NULL, 2143)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1455, 29.6468, 0, NULL, "2013-08-28", 0, NULL, 2144)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1456, 28.0225, 0, NULL, "2013-10-25", 0, NULL, 2158)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1457, 2, 0, NULL, "2013-10-27", 0, NULL, 2159)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1458, 498, 0, NULL, "2013-10-27", 0, NULL, 2160)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1459, 19.913, 1, "2014-10-22", "2013-10-22", 1, NULL, 2165)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1460, 403, 0, NULL, "2013-10-22", 0, NULL, 2166)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1461, 4, 0, NULL, "2013-10-22", 0, NULL, 2167)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1462, 27.0536, 0, NULL, "2013-10-22", 0, NULL, 2168)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1463, 20.641, 0, NULL, "2013-11-10", 0, NULL, 2169)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1464, 5, 0, NULL, "2013-11-10", 0, NULL, 2170)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1465, 11.4602, 1, "2014-11-10", "2013-11-10", 1, NULL, 2171)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1466, 280, 0, NULL, "2013-11-10", 0, NULL, 2172)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1467, 26.1406, 0, NULL, "2013-11-09", 0, NULL, 2173)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1468, 12.5243, 1, "2014-11-09", "2013-11-09", 1, NULL, 2174)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1469, 1, 0, NULL, "2014-01-21", 0, NULL, 2181)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1470, 17.3803, 1, "2015-01-21", "2014-01-21", 1, NULL, 2182)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1471, 367, 0, NULL, "2014-01-21", 0, NULL, 2183)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1472, 20.1356, 0, NULL, "2014-01-21", 0, NULL, 2184)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1473, 25.6327, 0, NULL, "2014-01-27", 0, NULL, 2185)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1474, 4, 0, NULL, "2014-01-27", 0, NULL, 2186)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1475, 411, 0, NULL, "2014-01-27", 0, NULL, 2187)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1476, 10.6362, 1, "2015-01-27", "2014-01-27", 1, NULL, 2188)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1477, 354, 0, NULL, "2014-01-23", 0, NULL, 2189)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1478, 12.2286, 0, NULL, "2014-01-23", 0, NULL, 2190)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1479, 1, 0, NULL, "2014-01-23", 0, NULL, 2191)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1480, 10.1044, 1, "2015-01-23", "2014-01-23", 1, NULL, 2192)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1481, 1, 0, NULL, "2014-01-31", 0, NULL, 2199)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1482, 20.7669, 0, NULL, "2014-01-31", 0, NULL, 2200)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1483, 12.7564, 1, "2015-02-22", "2014-02-22", 1, NULL, 2201)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1484, 1, 0, NULL, "2014-03-20", 0, NULL, 2206)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1485, 21.8378, 0, NULL, "2014-03-20", 0, NULL, 2207)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1486, 19.472, 1, "2015-03-20", "2014-03-20", 1, NULL, 2208)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1487, 427, 0, NULL, "2014-03-20", 0, NULL, 2209)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1488, 5, 0, NULL, "2014-04-01", 0, NULL, 2210)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1489, 356, 0, NULL, "2014-04-01", 0, NULL, 2211)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1490, 22.0796, 0, NULL, "2014-04-01", 0, NULL, 2212)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1491, 1, 0, NULL, "2014-04-04", 0, NULL, 2213)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1492, 23.5355, 0, NULL, "2014-04-04", 0, NULL, 2214)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1493, 458, 0, NULL, "2014-04-30", 0, NULL, 2215)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1494, 1, 0, NULL, "2014-04-30", 0, NULL, 2216)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1495, 16.0514, 1, "2015-04-30", "2014-04-30", 1, NULL, 2217)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1496, 23.9432, 0, NULL, "2014-04-30", 0, NULL, 2218)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1497, 21.9581, 0, NULL, "2014-04-05", 0, NULL, 2219)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1498, 5, 0, NULL, "2014-04-05", 0, NULL, 2220)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1499, 482, 0, NULL, "2014-04-05", 0, NULL, 2221)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1500, 24.2594, 1, "2015-04-05", "2014-04-05", 1, NULL, 2222)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1501, 13.6927, 1, "2015-05-13", "2014-05-13", 1, NULL, 2223)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1502, 22.3351, 1, "2015-05-11", "2014-05-11", 1, NULL, 2224)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1503, 5, 0, NULL, "2014-05-11", 0, NULL, 2225)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1504, 432, 0, NULL, "2014-05-11", 0, NULL, 2226)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1505, 26.7115, 0, NULL, "2014-05-11", 0, NULL, 2227)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1506, 321, 0, NULL, "2014-06-25", 0, NULL, 2228)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1507, 22.0115, 1, "2015-06-25", "2014-06-25", 1, NULL, 2229)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1508, 27.1042, 0, NULL, "2014-06-25", 0, NULL, 2230)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1509, 2, 0, NULL, "2014-06-25", 0, NULL, 2231)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1510, 13.559, 1, "2015-06-09", "2014-06-09", 1, NULL, 2232)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1511, 254, 0, NULL, "2014-07-29", 0, NULL, 2241)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1512, 18.4783, 1, "2015-07-29", "2014-07-29", 1, NULL, 2242)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1513, 26.4541, 0, NULL, "2014-07-29", 0, NULL, 2243)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1514, 4, 0, NULL, "2014-07-29", 0, NULL, 2244)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1515, 24.1209, 1, "2015-07-14", "2014-07-14", 1, NULL, 2245)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1516, 29.2914, 0, NULL, "2014-07-14", 0, NULL, 2246)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1517, 5, 0, NULL, "2014-07-14", 0, NULL, 2251)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1518, 256, 0, NULL, "2014-07-14", 0, NULL, 2252)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1519, 23.8127, 1, "2015-08-27", "2014-08-27", 1, NULL, 2253)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1520, 3, 0, NULL, "2014-08-27", 0, NULL, 2254)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1521, 24.3074, 0, NULL, "2014-08-27", 0, NULL, 2255)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1522, 492, 0, NULL, "2014-08-27", 0, NULL, 2256)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1523, 5, 0, NULL, "2014-08-01", 0, NULL, 2259)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1524, 25.4988, 0, NULL, "2014-08-01", 0, NULL, 2260)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1525, 369, 0, NULL, "2014-08-01", 0, NULL, 2261)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1526, 12.0661, 1, "2015-08-01", "2014-08-01", 1, NULL, 2262)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1527, 381, 0, NULL, "2014-08-26", 0, NULL, 2267)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1528, 25.4278, 0, NULL, "2014-08-26", 0, NULL, 2268)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1529, 2, 0, NULL, "2014-08-26", 0, NULL, 2269)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1530, 18.7868, 1, "2015-08-26", "2014-08-26", 1, NULL, 2270)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1531, 5, 0, NULL, "2014-09-09", 0, NULL, 2271)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1532, 21.8102, 0, NULL, "2014-09-09", 0, NULL, 2272)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1533, 353, 0, NULL, "2014-09-09", 0, NULL, 2273)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1534, 17.5002, 1, "2015-09-09", "2014-09-09", 1, NULL, 2274)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1535, 370, 0, NULL, "2014-09-03", 0, NULL, 2275)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1536, 23.7938, 0, NULL, "2014-09-03", 0, NULL, 2276)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1537, 23.5465, 1, "2015-09-03", "2014-09-03", 1, NULL, 2277)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1538, 4, 0, NULL, "2014-09-03", 0, NULL, 2278)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1539, 262, 0, NULL, "2014-09-17", 0, NULL, 2279)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1540, 1, 0, NULL, "2014-09-17", 0, NULL, 2280)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1541, 21.364, 0, NULL, "2014-09-17", 0, NULL, 2281)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1542, 19.0342, 1, "2015-09-17", "2014-09-17", 1, NULL, 2282)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1543, 331, 0, NULL, "2014-09-21", 0, NULL, 2283)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1544, 20.9582, 0, NULL, "2014-09-21", 0, NULL, 2284)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1545, 1, 0, NULL, "2014-10-07", 0, NULL, 2285)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1546, 27.3997, 0, NULL, "2014-10-07", 0, NULL, 2286)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1547, 468, 0, NULL, "2014-10-07", 0, NULL, 2287)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1548, 21.1441, 1, "2015-10-07", "2014-10-07", 1, NULL, 2288)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1549, 11.605, 1, "2015-11-28", "2014-11-28", 1, NULL, 2289)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1550, 21.3163, 0, NULL, "2014-11-28", 0, NULL, 2290)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1551, 452, 0, NULL, "2014-11-28", 0, NULL, 2291)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1552, 1, 0, NULL, "2014-11-28", 0, NULL, 2292)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1553, 11.1554, 1, "2015-11-11", "2014-11-11", 1, NULL, 2293)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1554, 4, 0, NULL, "2014-11-11", 0, NULL, 2294)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1555, 20.9854, 0, NULL, "2014-11-11", 0, NULL, 2295)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1556, 23.7449, 1, "2015-11-04", "2014-11-04", 1, NULL, 2296)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1557, 17.4183, 1, "2016-00-05", "2015-00-05", 1, NULL, 2297)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1558, 5, 0, NULL, "2015-00-13", 0, NULL, 2302)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1559, 10.9973, 1, "2016-00-13", "2015-00-13", 1, NULL, 2303)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1560, 24.2701, 0, NULL, "2015-00-13", 0, NULL, 2304)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1561, 483, 0, NULL, "2015-00-13", 0, NULL, 2305)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1562, 1, 0, NULL, "2015-00-24", 0, NULL, 2306)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1563, 401, 0, NULL, "2015-01-15", 0, NULL, 2315)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1564, 25.4922, 0, NULL, "2015-01-15", 0, NULL, 2316)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1565, 16.7739, 1, "2016-01-15", "2015-01-15", 1, NULL, 2317)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1566, 3, 0, NULL, "2015-01-15", 0, NULL, 2318)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1567, 11.7544, 1, "2016-02-20", "2015-02-20", 1, NULL, 2323)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1568, 5, 0, NULL, "2015-02-20", 0, NULL, 2324)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1569, 470, 0, NULL, "2015-02-20", 0, NULL, 2325)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1570, 23.3226, 0, NULL, "2015-02-20", 0, NULL, 2326)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1571, 2, 0, NULL, "2015-02-24", 0, NULL, 2327)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1572, 24.0927, 0, NULL, "2015-02-24", 0, NULL, 2328)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1573, 455, 0, NULL, "2015-02-24", 0, NULL, 2329)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1574, 17.4511, 1, "2016-02-24", "2015-02-24", 1, NULL, 2330)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1575, 386, 0, NULL, "2015-03-26", 0, NULL, 2335)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1576, 19.6183, 1, "2016-03-26", "2015-03-26", 1, NULL, 2336)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1577, 29.9, 0, NULL, "2015-03-26", 0, NULL, 2337)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1578, 3, 0, NULL, "2015-03-26", 0, NULL, 2338)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1579, 3, 0, NULL, "2015-03-11", 0, NULL, 2343)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1580, 451, 0, NULL, "2015-03-11", 0, NULL, 2344)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1581, 12.8619, 1, "2016-03-11", "2015-03-11", 1, NULL, 2345)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1582, 24.3452, 0, NULL, "2015-03-11", 0, NULL, 2346)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1583, 26.4966, 0, NULL, "2015-03-16", 0, NULL, 2347)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1584, 20.1276, 0, NULL, "2015-04-23", 0, NULL, 2348)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1585, 11.9141, 1, "2016-04-23", "2015-04-23", 1, NULL, 2349)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1586, 3, 0, NULL, "2015-04-23", 0, NULL, 2350)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1587, 335, 0, NULL, "2015-04-23", 0, NULL, 2351)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1588, 1, 0, NULL, "2015-04-18", 0, NULL, 2352)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1589, 1, 0, NULL, "2015-05-04", 0, NULL, 2357)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1590, 21.654, 0, NULL, "2015-05-04", 0, NULL, 2358)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1591, 23.3625, 1, "2016-05-04", "2015-05-04", 1, NULL, 2359)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1592, 485, 0, NULL, "2015-05-04", 0, NULL, 2360)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1593, 14.9435, 1, "2016-05-15", "2015-05-15", 1, NULL, 2361)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1594, 24.7181, 0, NULL, "2015-05-25", 0, NULL, 2362)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1595, 19.1977, 1, "2016-06-10", "2015-06-10", 1, NULL, 2363)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1596, 361, 0, NULL, "2015-06-08", 0, NULL, 2372)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1597, 26.0748, 0, NULL, "2015-06-08", 0, NULL, 2373)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1598, 13.5346, 1, "2016-06-08", "2015-06-08", 1, NULL, 2374)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1599, 3, 0, NULL, "2015-06-08", 0, NULL, 2375)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1600, 21.1771, 0, NULL, "2015-07-15", 0, NULL, 2387)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1601, 468, 0, NULL, "2015-07-15", 0, NULL, 2388)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1602, 18.2806, 1, "2016-07-15", "2015-07-15", 1, NULL, 2389)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1603, 4, 0, NULL, "2015-07-15", 0, NULL, 2390)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1604, 2, 0, NULL, "2015-08-11", 0, NULL, 2398)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1605, 11.4752, 1, "2016-08-11", "2015-08-11", 1, NULL, 2399)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1606, 25.0446, 0, NULL, "2015-08-11", 0, NULL, 2400)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1607, 294, 0, NULL, "2015-08-11", 0, NULL, 2401)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1608, 22.7146, 0, NULL, "2015-08-18", 0, NULL, 2402)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1609, 352, 0, NULL, "2015-08-18", 0, NULL, 2403)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1610, 1, 0, NULL, "2015-08-18", 0, NULL, 2404)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1611, 13.546, 1, "2016-08-18", "2015-08-18", 1, NULL, 2405)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1612, 28.6791, 0, NULL, "2015-09-28", 0, NULL, 2409)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1613, 362, 0, NULL, "2015-09-28", 0, NULL, 2410)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1614, 19.1149, 1, "2016-09-28", "2015-09-28", 1, NULL, 2411)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1615, 2, 0, NULL, "2015-09-28", 0, NULL, 2412)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (0, "2016-00-24", 1, 1, 1, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (1, "2016-00-05", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (2, "2016-00-27", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (3, "2016-00-03", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (4, "2016-01-10", 1, 1, 1, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (5, "2016-02-25", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (6, "2016-02-23", 1, 1, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (7, "2016-02-01", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (8, "2016-02-18", 1, 1, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (9, "2016-02-05", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (10, "2016-02-27", 1, 1, 1, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (11, "2016-02-17", 1, 0, 0, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (12, "2016-03-25", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (13, "2016-04-01", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (14, "2016-04-01", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (15, "2016-04-22", 1, 1, 1, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (16, "2016-04-29", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (17, "2016-04-14", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (18, "2016-05-11", 1, 1, 1, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (19, "2016-06-02", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (20, "2016-06-13", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (21, "2016-06-06", 1, 1, 1, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (22, "2016-06-26", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (23, "2016-07-27", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (24, "2016-07-04", 1, 1, 1, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (25, "2016-07-23", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (26, "2016-07-27", 1, 1, 1, 16)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (27, "2016-07-17", 1, 0, 0, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (28, "2016-08-18", 1, 1, 1, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (29, "2016-08-21", 1, 1, 1, 14)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (30, "2016-08-20", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (31, "2016-08-25", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (32, "2016-08-15", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (33, "2016-08-14", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (34, "2016-08-13", 0, 0, 0, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (35, "2016-09-04", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (36, "2016-09-24", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (37, "2016-09-10", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (38, "2016-09-01", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (39, "2016-09-17", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (40, "2016-09-08", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (41, "2016-09-29", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (42, "2016-10-18", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (43, "2016-10-20", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (44, "2016-10-03", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (45, "2016-10-18", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (46, "2016-10-11", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (47, "2016-10-28", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (48, "2016-10-16", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (49, "2016-11-03", 1, 1, 1, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (50, "2016-11-13", 1, 1, 1, 16)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (51, "2016-11-29", 1, 1, 1, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (52, "2016-11-25", 1, 1, 1, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (53, "2016-11-26", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (54, "2017-00-20", 0, 0, 0, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (55, "2017-00-17", 1, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (56, "2017-00-08", 0, 0, 0, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (57, "2017-00-14", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (58, "2017-01-04", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (59, "2017-01-16", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (60, "2017-02-14", 1, 1, 1, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (61, "2017-02-01", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (62, "2017-03-01", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (63, "2017-03-16", 0, 0, 0, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (64, "2017-04-11", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (65, "2017-05-14", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (66, "2017-05-09", 1, 1, 1, 13)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (67, "2017-05-15", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (68, "2017-06-10", 1, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (69, "2017-06-27", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (70, "2017-06-02", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (71, "2017-07-27", 1, 1, 1, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (72, "2017-07-02", 0, 0, 0, 13)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (73, "2017-07-15", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (74, "2017-07-14", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (75, "2017-07-05", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (76, "2017-07-30", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (77, "2017-07-01", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (78, "2017-08-01", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (79, "2017-09-13", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (80, "2017-09-12", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (81, "2017-09-08", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (82, "2017-09-27", 1, 1, 1, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (83, "2017-09-03", 1, 0, 0, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (84, "2017-09-07", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (85, "2017-10-18", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (86, "2017-10-04", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (87, "2017-10-06", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (88, "2017-10-08", 1, 1, 1, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (89, "2017-11-13", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (90, "2017-11-26", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (91, "2017-11-13", 0, 0, 0, 16)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (92, "2017-11-04", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (93, "2017-11-19", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (94, "2017-11-03", 1, 1, 1, 2)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (0, 2, 0, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1, 11.8733, 0, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2, 20.1061, 0, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (3, 335, 0, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (4, 404, 1, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (5, 3, 1, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (6, 22.7953, 1, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (7, 13.9925, 1, 0, 1, 6.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (8, 3, 2, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (9, 12.01, 2, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (10, 21.8325, 2, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (11, 480, 2, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (12, 2, 3, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (13, 29.4305, 3, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (14, 393, 3, 3, 1, 167.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (15, 18.3437, 3, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (16, 27.8427, 4, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (17, 3, 4, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (18, 305, 4, 3, 1, 155.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (19, 19.0671, 4, 0, 1, 7.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (20, 27.4952, 5, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (21, 3, 5, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (22, 26.6323, 6, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (23, 1, 6, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (24, 15.0807, 6, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (25, 441, 6, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (26, 1, 7, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (27, 378, 8, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (28, 250, 9, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (29, 4, 9, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (30, 28.1863, 9, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (31, 15.9128, 9, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (32, 402, 10, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (33, 29.0732, 10, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (34, 19.9352, 10, 0, 1, 7.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (35, 3, 10, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (36, 1, 11, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (37, 16.4162, 11, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (38, 369, 11, 3, 1, 165.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (39, 22.6477, 11, 2, 1, 11.85)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (40, 290, 12, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (41, 22.6342, 12, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (42, 1, 13, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (43, 24.0595, 13, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (44, 15.8362, 14, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (45, 1, 14, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (46, 470, 14, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (47, 24.4424, 14, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (48, 4, 15, 1, 1, 83.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (49, 473, 15, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (50, 25.6132, 15, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (51, 19.1342, 15, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (52, 481, 16, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (53, 21.9947, 16, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (54, 18.592, 16, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (55, 5, 17, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (56, 24.809, 17, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (57, 272, 17, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (58, 13.8969, 17, 0, 1, 8.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (59, 475, 18, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (60, 12.0811, 18, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (61, 5, 18, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (62, 29.8738, 18, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (63, 23.755, 19, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (64, 3, 20, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (65, 18.2394, 20, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (66, 430, 20, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (67, 27.3938, 20, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (68, 22.1034, 21, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (69, 22.3422, 21, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (70, 436, 21, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (71, 2, 21, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (72, 24.433, 22, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (73, 431, 22, 3, 1, 182.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (74, 29.968, 22, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (75, 3, 22, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (76, 279, 23, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (77, 4, 23, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (78, 12.4815, 23, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (79, 23.6254, 23, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (80, 24.5285, 24, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (81, 20.2826, 24, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (82, 4, 24, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (83, 494, 24, 3, 1, 192.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (84, 410, 25, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (85, 22.4686, 25, 2, 1, 12.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (86, 5, 25, 1, 1, 68.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (87, 14.3538, 25, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (88, 11.7749, 26, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (89, 2, 26, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (90, 22.7041, 26, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (91, 17.6675, 27, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (92, 25.1598, 27, 2, 1, 9.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (93, 3, 27, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (94, 397, 28, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (95, 28.9687, 28, 2, 1, 12.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (96, 23.8214, 28, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (97, 1, 28, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (98, 3, 29, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (99, 21.1833, 29, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (100, 19.8396, 29, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (101, 298, 29, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (102, 2, 30, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (103, 255, 30, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (104, 26.1869, 30, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (105, 16.9772, 30, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (106, 353, 31, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (107, 24.2553, 31, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (108, 5, 31, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (109, 24.9084, 31, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (110, 28.295, 32, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (111, 16.9803, 32, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (112, 21.4224, 33, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (113, 16.6322, 33, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (114, 355, 33, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (115, 5, 33, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (116, 19.4701, 34, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (117, 437, 34, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (118, 27.5582, 34, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (119, 1, 34, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (120, 10.9831, 35, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (121, 436, 35, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (122, 26.9592, 35, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (123, 4, 35, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (124, 16.6385, 36, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (125, 20.733, 36, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (126, 406, 36, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (127, 5, 36, 1, 1, 76.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (128, 313, 37, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (129, 3, 37, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (130, 17.0692, 37, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (131, 29.6047, 37, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (132, 24.9946, 38, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (133, 26.8801, 38, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (134, 3, 38, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (135, 255, 38, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (136, 26.2551, 39, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (137, 4, 39, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (138, 11.7471, 39, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (139, 448, 39, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (140, 20.1281, 40, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (141, 27.7937, 41, 2, 1, 12.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (142, 5, 41, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (143, 458, 41, 3, 1, 157.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (144, 11.5809, 41, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (145, 276, 42, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (146, 10.0017, 42, 0, 1, 8.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (147, 4, 42, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (148, 27.3611, 42, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (149, 352, 43, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (150, 3, 43, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (151, 10.5039, 43, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (152, 26.3313, 43, 2, 1, 11.85)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (153, 3, 44, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (154, 486, 44, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (155, 22.1091, 44, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (156, 12.3424, 44, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (157, 29.9579, 45, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (158, 13.1922, 45, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (159, 1, 45, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (160, 329, 45, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (161, 13.9098, 46, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (162, 290, 46, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (163, 22.3692, 47, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (164, 452, 47, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (165, 22.0654, 47, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (166, 15.7925, 48, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (167, 20.5949, 48, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (168, 2, 48, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (169, 26.9787, 49, 2, 1, 9.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (170, 444, 49, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (171, 20.3243, 50, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (172, 336, 50, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (173, 21.5582, 50, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (174, 27.8321, 51, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (175, 356, 51, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (176, 20.306, 52, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (177, 1, 52, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (178, 26.264, 52, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (179, 306, 52, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (180, 18.0702, 53, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (181, 21.3219, 53, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (182, 2, 53, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (183, 304, 53, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (184, 24.1578, 54, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (185, 1, 54, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (186, 21.8332, 54, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (187, 431, 54, 3, 1, 175.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (188, 20.3126, 55, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (189, 451, 55, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (190, 3, 55, 1, 1, 86.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (191, 19.3046, 55, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (192, 281, 56, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (193, 1, 56, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (194, 13.8209, 56, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (195, 23.3293, 56, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (196, 4, 57, 1, 1, 76.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (197, 11.0416, 58, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (198, 1, 58, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (199, 255, 58, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (200, 26.5954, 58, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (201, 24.6023, 59, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (202, 15.3414, 59, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (203, 2, 59, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (204, 404, 59, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (205, 4, 60, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (206, 353, 61, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (207, 27.974, 61, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (208, 5, 62, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (209, 23.6376, 62, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (210, 24.4255, 62, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (211, 486, 62, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (212, 21.811, 63, 2, 1, 12.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (213, 19.72, 63, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (214, 397, 63, 3, 1, 190.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (215, 292, 64, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (216, 372, 65, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (217, 23.8322, 65, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (218, 18.4064, 65, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (219, 13.4862, 66, 0, 1, 8.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (220, 4, 66, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (221, 20.8318, 67, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (222, 451, 67, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (223, 25.8508, 67, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (224, 1, 67, 1, 1, 68.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (225, 22.631, 68, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (226, 29.4764, 68, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (227, 305, 68, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (228, 23.8349, 69, 2, 1, 10.65)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (229, 263, 69, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (230, 20.5354, 69, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (231, 2, 69, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (232, 4, 70, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (233, 16.3428, 71, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (234, 2, 71, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (235, 27.8255, 71, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (236, 297, 71, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (237, 23.5387, 72, 2, 1, 13.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (238, 253, 72, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (239, 5, 72, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (240, 19.5766, 72, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (241, 19.9186, 73, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (242, 28.6952, 73, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (243, 3, 73, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (244, 14.9085, 74, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (245, 1, 74, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (246, 427, 74, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (247, 22.7277, 74, 2, 1, 11.85)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (248, 21.3157, 75, 2, 1, 12.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (249, 434, 76, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (250, 24.9456, 76, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (251, 1, 76, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (252, 21.6554, 77, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (253, 280, 78, 3, 1, 217.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (254, 24.034, 78, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (255, 17.4205, 78, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (256, 23.3873, 79, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (257, 345, 79, 3, 1, 157.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (258, 4, 79, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (259, 20.0792, 79, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (260, 341, 80, 3, 1, 210.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (261, 12.1381, 81, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (262, 294, 81, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (263, 20.1403, 81, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (264, 2, 81, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (265, 21.2109, 82, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (266, 14.0642, 82, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (267, 4, 82, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (268, 363, 82, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (269, 2, 83, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (270, 22.9275, 83, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (271, 19.1362, 83, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (272, 443, 83, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (273, 383, 84, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (274, 5, 84, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (275, 22.0232, 84, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (276, 15.8293, 84, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (277, 20.1144, 85, 2, 1, 12.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (278, 24.8589, 85, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (279, 483, 85, 3, 1, 155.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (280, 4, 85, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (281, 1, 86, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (282, 26.3851, 86, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (283, 492, 86, 3, 1, 180.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (284, 11.4139, 86, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (285, 22.6444, 87, 0, 1, 8.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (286, 5, 87, 1, 1, 88.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (287, 26.9256, 87, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (288, 311, 87, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (289, 20.6587, 88, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (290, 487, 88, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (291, 2, 88, 1, 1, 71.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (292, 16.8158, 88, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (293, 17.4647, 89, 0, 1, 7.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (294, 297, 89, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (295, 19.0249, 90, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (296, 26.1491, 90, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (297, 416, 90, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (298, 24.4215, 91, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (299, 3, 92, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (300, 23.4574, 92, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (301, 22.5453, 93, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (302, 1, 93, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (303, 19.8042, 93, 0, 0, NULL)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (0, 1, 2, 0, NULL, 16)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (1, 1, 1, 0, NULL, 33)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (2, 0, NULL, 1, 1, 45)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (3, 0, NULL, 1, 1, 58)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (4, 1, 6, 0, NULL, 68)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (5, 1, 2, 0, NULL, 102)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (6, 0, NULL, 1, 2, 111)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (7, 0, NULL, 1, 2, 113)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (8, 1, 9, 0, NULL, 116)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (9, 1, 2, 0, NULL, 117)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (10, 0, NULL, 1, 2, 120)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (11, 1, 6, 1, 1, 121)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (12, 1, 2, 0, NULL, 134)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (13, 1, 10, 0, NULL, 136)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (14, 1, 1, 1, 2, 137)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (15, 0, NULL, 1, 1, 145)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (16, 1, 2, 0, NULL, 183)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (17, 1, 7, 0, NULL, 191)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (18, 0, NULL, 1, 1, 192)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (19, 0, NULL, 1, 1, 196)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (20, 0, NULL, 1, 1, 224)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (21, 0, NULL, 1, 2, 232)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (22, 1, 8, 0, NULL, 235)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (23, 1, 9, 0, NULL, 242)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (24, 0, NULL, 1, 1, 246)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (25, 0, NULL, 1, 1, 268)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (26, 0, NULL, 1, 2, 275)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (27, 0, NULL, 1, 2, 295)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (0, 2, 0, NULL, "2016-00-24", 0, NULL, 0)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1, 11.8733, 1, "2017-00-24", "2016-00-24", 1, NULL, 1)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (2, 20.1061, 0, NULL, "2016-00-24", 0, NULL, 2)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (3, 335, 0, NULL, "2016-00-24", 0, NULL, 3)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (4, 404, 0, NULL, "2016-00-05", 0, NULL, 4)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (5, 3, 0, NULL, "2016-00-05", 0, NULL, 5)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (6, 22.7953, 0, NULL, "2016-00-05", 0, NULL, 6)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (7, 13.9925, 1, "2017-00-05", "2016-00-05", 1, NULL, 7)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (8, 3, 0, NULL, "2016-00-27", 0, NULL, 8)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (9, 12.01, 1, "2017-00-27", "2016-00-27", 1, NULL, 9)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (10, 21.8325, 0, NULL, "2016-00-27", 0, NULL, 10)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (11, 480, 0, NULL, "2016-00-27", 0, NULL, 11)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (12, 2, 0, NULL, "2016-00-03", 0, NULL, 12)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (13, 29.4305, 0, NULL, "2016-00-03", 0, NULL, 13)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (14, 393, 0, NULL, "2016-00-03", 0, NULL, 14)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (15, 18.3437, 1, "2017-00-03", "2016-00-03", 1, NULL, 15)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (16, 25.8427, 0, NULL, "2016-01-10", 0, NULL, 16)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (17, 3, 0, NULL, "2016-01-10", 0, NULL, 17)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (18, 305, 0, NULL, "2016-01-10", 0, NULL, 18)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (19, 19.0671, 1, "2017-01-10", "2016-01-10", 1, NULL, 19)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (20, 27.4952, 0, NULL, "2016-02-25", 0, NULL, 20)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (21, 3, 0, NULL, "2016-02-25", 0, NULL, 21)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (22, 250, 0, NULL, "2016-02-05", 0, NULL, 28)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (23, 4, 0, NULL, "2016-02-05", 0, NULL, 29)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (24, 28.1863, 0, NULL, "2016-02-05", 0, NULL, 30)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (25, 15.9128, 1, "2017-02-05", "2016-02-05", 1, NULL, 31)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (26, 402, 0, NULL, "2016-02-27", 0, NULL, 32)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (27, 28.0732, 0, NULL, "2016-02-27", 0, NULL, 33)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (28, 19.9352, 1, "2017-02-27", "2016-02-27", 1, NULL, 34)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (29, 3, 0, NULL, "2016-02-27", 0, NULL, 35)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (30, 1, 0, NULL, "2016-04-01", 0, NULL, 42)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (31, 24.0595, 1, "2017-04-01", "2016-04-01", 1, NULL, 43)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (32, 4, 0, NULL, "2016-04-22", 0, NULL, 48)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (33, 473, 0, NULL, "2016-04-22", 0, NULL, 49)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (34, 25.6132, 0, NULL, "2016-04-22", 0, NULL, 50)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (35, 19.1342, 1, "2017-04-22", "2016-04-22", 1, NULL, 51)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (36, 481, 0, NULL, "2016-04-29", 0, NULL, 52)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (37, 21.9947, 0, NULL, "2016-04-29", 0, NULL, 53)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (38, 18.592, 1, "2017-04-29", "2016-04-29", 1, NULL, 54)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (39, 5, 0, NULL, "2016-04-14", 0, NULL, 55)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (40, 24.809, 0, NULL, "2016-04-14", 0, NULL, 56)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (41, 272, 0, NULL, "2016-04-14", 0, NULL, 57)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (42, 12.8969, 1, "2017-04-14", "2016-04-14", 1, NULL, 58)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (43, 475, 0, NULL, "2016-05-11", 0, NULL, 59)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (44, 12.0811, 1, "2017-05-11", "2016-05-11", 1, NULL, 60)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (45, 5, 0, NULL, "2016-05-11", 0, NULL, 61)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (46, 29.8738, 0, NULL, "2016-05-11", 0, NULL, 62)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (47, 3, 0, NULL, "2016-06-13", 0, NULL, 64)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (48, 18.2394, 1, "2017-06-13", "2016-06-13", 1, NULL, 65)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (49, 430, 0, NULL, "2016-06-13", 0, NULL, 66)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (50, 27.3938, 0, NULL, "2016-06-13", 0, NULL, 67)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (51, 16.1034, 0, NULL, "2016-06-06", 0, NULL, 68)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (52, 22.3422, 1, "2017-06-06", "2016-06-06", 1, NULL, 69)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (53, 436, 0, NULL, "2016-06-06", 0, NULL, 70)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (54, 2, 0, NULL, "2016-06-06", 0, NULL, 71)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (55, 279, 0, NULL, "2016-07-27", 0, NULL, 76)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (56, 4, 0, NULL, "2016-07-27", 0, NULL, 77)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (57, 12.4815, 1, "2017-07-27", "2016-07-27", 1, NULL, 78)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (58, 23.6254, 0, NULL, "2016-07-27", 0, NULL, 79)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (59, 24.5285, 1, "2017-07-04", "2016-07-04", 1, NULL, 80)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (60, 20.2826, 0, NULL, "2016-07-04", 0, NULL, 81)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (61, 4, 0, NULL, "2016-07-04", 0, NULL, 82)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (62, 494, 0, NULL, "2016-07-04", 0, NULL, 83)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (63, 11.7749, 1, "2017-07-27", "2016-07-27", 1, NULL, 88)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (64, 2, 0, NULL, "2016-07-27", 0, NULL, 89)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (65, 22.7041, 0, NULL, "2016-07-27", 0, NULL, 90)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (66, 397, 0, NULL, "2016-08-18", 0, NULL, 94)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (67, 28.9687, 0, NULL, "2016-08-18", 0, NULL, 95)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (68, 23.8214, 1, "2017-08-18", "2016-08-18", 1, NULL, 96)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (69, 1, 0, NULL, "2016-08-18", 0, NULL, 97)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (70, 3, 0, NULL, "2016-08-21", 0, NULL, 98)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (71, 21.1833, 0, NULL, "2016-08-21", 0, NULL, 99)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (72, 19.8396, 1, "2017-08-21", "2016-08-21", 1, NULL, 100)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (73, 298, 0, NULL, "2016-08-21", 0, NULL, 101)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (74, 0, 0, NULL, "2016-08-20", 0, NULL, 102)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (75, 255, 0, NULL, "2016-08-20", 0, NULL, 103)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (76, 26.1869, 0, NULL, "2016-08-20", 0, NULL, 104)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (77, 16.9772, 1, "2017-08-20", "2016-08-20", 1, NULL, 105)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (78, 28.295, 0, NULL, "2016-08-15", 0, NULL, 110)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (79, 14.9803, 1, "2017-08-15", "2016-08-15", 1, NULL, 111)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (80, 21.4224, 0, NULL, "2016-08-14", 0, NULL, 112)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (81, 14.6322, 1, "2017-08-14", "2016-08-14", 1, NULL, 113)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (82, 355, 0, NULL, "2016-08-14", 0, NULL, 114)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (83, 5, 0, NULL, "2016-08-14", 0, NULL, 115)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (84, 8.9831, 1, "2017-09-04", "2016-09-04", 1, NULL, 120)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (85, 429, 0, NULL, "2016-09-04", 0, NULL, 121)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (86, 26.9592, 0, NULL, "2016-09-04", 0, NULL, 122)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (87, 4, 0, NULL, "2016-09-04", 0, NULL, 123)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (88, 16.6385, 1, "2017-09-24", "2016-09-24", 1, NULL, 124)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (89, 20.733, 0, NULL, "2016-09-24", 0, NULL, 125)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (90, 406, 0, NULL, "2016-09-24", 0, NULL, 126)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (91, 5, 0, NULL, "2016-09-24", 0, NULL, 127)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (92, 24.9946, 1, "2017-09-01", "2016-09-01", 1, NULL, 132)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (93, 26.8801, 0, NULL, "2016-09-01", 0, NULL, 133)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (94, 1, 0, NULL, "2016-09-01", 0, NULL, 134)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (95, 255, 0, NULL, "2016-09-01", 0, NULL, 135)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (96, 16.2551, 0, NULL, "2016-09-17", 0, NULL, 136)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (97, 1, 0, NULL, "2016-09-17", 0, NULL, 137)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (98, 11.7471, 1, "2017-09-17", "2016-09-17", 1, NULL, 138)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (99, 448, 0, NULL, "2016-09-17", 0, NULL, 139)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (100, 20.1281, 0, NULL, "2016-09-08", 0, NULL, 140)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (101, 27.7937, 0, NULL, "2016-09-29", 0, NULL, 141)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (102, 5, 0, NULL, "2016-09-29", 0, NULL, 142)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (103, 458, 0, NULL, "2016-09-29", 0, NULL, 143)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (104, 11.5809, 1, "2017-09-29", "2016-09-29", 1, NULL, 144)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (105, 352, 0, NULL, "2016-10-20", 0, NULL, 149)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (106, 3, 0, NULL, "2016-10-20", 0, NULL, 150)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (107, 10.5039, 1, "2017-10-20", "2016-10-20", 1, NULL, 151)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (108, 26.3313, 0, NULL, "2016-10-20", 0, NULL, 152)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (109, 3, 0, NULL, "2016-10-03", 0, NULL, 153)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (110, 486, 0, NULL, "2016-10-03", 0, NULL, 154)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (111, 22.1091, 0, NULL, "2016-10-03", 0, NULL, 155)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (112, 12.3424, 1, "2017-10-03", "2016-10-03", 1, NULL, 156)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (113, 29.9579, 0, NULL, "2016-10-18", 0, NULL, 157)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (114, 13.1922, 1, "2017-10-18", "2016-10-18", 1, NULL, 158)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (115, 1, 0, NULL, "2016-10-18", 0, NULL, 159)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (116, 329, 0, NULL, "2016-10-18", 0, NULL, 160)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (117, 13.9098, 1, "2017-10-11", "2016-10-11", 1, NULL, 161)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (118, 290, 0, NULL, "2016-10-11", 0, NULL, 162)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (119, 22.3692, 0, NULL, "2016-10-28", 0, NULL, 163)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (120, 452, 0, NULL, "2016-10-28", 0, NULL, 164)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (121, 22.0654, 1, "2017-10-28", "2016-10-28", 1, NULL, 165)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (122, 26.9787, 0, NULL, "2016-11-03", 0, NULL, 169)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (123, 444, 0, NULL, "2016-11-03", 0, NULL, 170)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (124, 20.3243, 0, NULL, "2016-11-13", 0, NULL, 171)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (125, 336, 0, NULL, "2016-11-13", 0, NULL, 172)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (126, 21.5582, 1, "2017-11-13", "2016-11-13", 1, NULL, 173)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (127, 27.8321, 0, NULL, "2016-11-29", 0, NULL, 174)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (128, 356, 0, NULL, "2016-11-29", 0, NULL, 175)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (129, 20.306, 1, "2017-11-25", "2016-11-25", 1, NULL, 176)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (130, 1, 0, NULL, "2016-11-25", 0, NULL, 177)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (131, 26.264, 0, NULL, "2016-11-25", 0, NULL, 178)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (132, 306, 0, NULL, "2016-11-25", 0, NULL, 179)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (133, 18.0702, 1, "2017-11-26", "2016-11-26", 1, NULL, 180)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (134, 21.3219, 0, NULL, "2016-11-26", 0, NULL, 181)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (135, 2, 0, NULL, "2016-11-26", 0, NULL, 182)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (136, 302, 0, NULL, "2016-11-26", 0, NULL, 183)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (137, 3, 0, NULL, "2017-00-14", 0, NULL, 196)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (138, 11.0416, 1, "2018-01-04", "2017-01-04", 1, NULL, 197)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (139, 1, 0, NULL, "2017-01-04", 0, NULL, 198)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (140, 255, 0, NULL, "2017-01-04", 0, NULL, 199)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (141, 26.5954, 0, NULL, "2017-01-04", 0, NULL, 200)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (142, 4, 0, NULL, "2017-02-14", 0, NULL, 205)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (143, 353, 0, NULL, "2017-02-01", 0, NULL, 206)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (144, 27.974, 0, NULL, "2017-02-01", 0, NULL, 207)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (145, 5, 0, NULL, "2017-03-01", 0, NULL, 208)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (146, 23.6376, 0, NULL, "2017-03-01", 0, NULL, 209)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (147, 24.4255, 1, "2018-03-01", "2017-03-01", 1, NULL, 210)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (148, 486, 0, NULL, "2017-03-01", 0, NULL, 211)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (149, 292, 0, NULL, "2017-04-11", 0, NULL, 215)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (150, 13.4862, 1, "2018-05-09", "2017-05-09", 1, NULL, 219)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (151, 4, 0, NULL, "2017-05-09", 0, NULL, 220)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (152, 20.8318, 1, "2018-05-15", "2017-05-15", 1, NULL, 221)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (153, 451, 0, NULL, "2017-05-15", 0, NULL, 222)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (154, 25.8508, 0, NULL, "2017-05-15", 0, NULL, 223)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (155, 0, 0, NULL, "2017-05-15", 0, NULL, 224)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (156, 23.8349, 0, NULL, "2017-06-27", 0, NULL, 228)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (157, 263, 0, NULL, "2017-06-27", 0, NULL, 229)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (158, 20.5354, 1, "2018-06-27", "2017-06-27", 1, NULL, 230)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (159, 2, 0, NULL, "2017-06-27", 0, NULL, 231)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (160, 2, 0, NULL, "2017-06-02", 0, NULL, 232)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (161, 16.3428, 1, "2018-07-27", "2017-07-27", 1, NULL, 233)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (162, 2, 0, NULL, "2017-07-27", 0, NULL, 234)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (163, 19.8255, 0, NULL, "2017-07-27", 0, NULL, 235)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (164, 297, 0, NULL, "2017-07-27", 0, NULL, 236)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (165, 14.9085, 1, "2018-07-14", "2017-07-14", 1, NULL, 244)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (166, 1, 0, NULL, "2017-07-14", 0, NULL, 245)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (167, 426, 0, NULL, "2017-07-14", 0, NULL, 246)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (168, 22.7277, 0, NULL, "2017-07-14", 0, NULL, 247)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (169, 21.3157, 0, NULL, "2017-07-05", 0, NULL, 248)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (170, 280, 0, NULL, "2017-08-01", 0, NULL, 253)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (171, 24.034, 0, NULL, "2017-08-01", 0, NULL, 254)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (172, 17.4205, 1, "2018-08-01", "2017-08-01", 1, NULL, 255)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (173, 23.3873, 0, NULL, "2017-09-13", 0, NULL, 256)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (174, 345, 0, NULL, "2017-09-13", 0, NULL, 257)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (175, 4, 0, NULL, "2017-09-13", 0, NULL, 258)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (176, 20.0792, 1, "2018-09-13", "2017-09-13", 1, NULL, 259)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (177, 341, 0, NULL, "2017-09-12", 0, NULL, 260)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (178, 12.1381, 1, "2018-09-08", "2017-09-08", 1, NULL, 261)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (179, 294, 0, NULL, "2017-09-08", 0, NULL, 262)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (180, 20.1403, 0, NULL, "2017-09-08", 0, NULL, 263)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (181, 2, 0, NULL, "2017-09-08", 0, NULL, 264)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (182, 21.2109, 0, NULL, "2017-09-27", 0, NULL, 265)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (183, 14.0642, 1, "2018-09-27", "2017-09-27", 1, NULL, 266)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (184, 4, 0, NULL, "2017-09-27", 0, NULL, 267)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (185, 362, 0, NULL, "2017-09-27", 0, NULL, 268)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (186, 383, 0, NULL, "2017-09-07", 0, NULL, 273)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (187, 5, 0, NULL, "2017-09-07", 0, NULL, 274)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (188, 20.0232, 0, NULL, "2017-09-07", 0, NULL, 275)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (189, 15.8293, 1, "2018-09-07", "2017-09-07", 1, NULL, 276)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (190, 1, 0, NULL, "2017-10-04", 0, NULL, 281)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (191, 26.3851, 0, NULL, "2017-10-04", 0, NULL, 282)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (192, 492, 0, NULL, "2017-10-04", 0, NULL, 283)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (193, 11.4139, 1, "2018-10-04", "2017-10-04", 1, NULL, 284)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (194, 22.6444, 1, "2018-10-06", "2017-10-06", 1, NULL, 285)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (195, 5, 0, NULL, "2017-10-06", 0, NULL, 286)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (196, 26.9256, 0, NULL, "2017-10-06", 0, NULL, 287)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (197, 311, 0, NULL, "2017-10-06", 0, NULL, 288)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (198, 20.6587, 0, NULL, "2017-10-08", 0, NULL, 289)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (199, 487, 0, NULL, "2017-10-08", 0, NULL, 290)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (200, 2, 0, NULL, "2017-10-08", 0, NULL, 291)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (201, 16.8158, 1, "2018-10-08", "2017-10-08", 1, NULL, 292)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (202, 17.0249, 1, "2018-11-26", "2017-11-26", 1, NULL, 295)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (203, 26.1491, 0, NULL, "2017-11-26", 0, NULL, 296)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (204, 416, 0, NULL, "2017-11-26", 0, NULL, 297)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (205, 3, 0, NULL, "2017-11-04", 0, NULL, 299)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (206, 23.4574, 1, "2018-11-04", "2017-11-04", 1, NULL, 300)

