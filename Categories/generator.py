def generate():
    file_categories = open("Categories/categories.txt", encoding="utf8")
    categories = []
    for line in file_categories:
        if line.count('\n') > 0:
            categories.append(line[:-1])
        else:
            categories.append(line)
    return categories
