import pandas as pd


def from_excel(file):
    excel = pd.read_excel(file, sheet_name="Arkusz 3", index_col=None)
    categories = []
    for category in excel["Nazwa"]:
        categories.append(category)
    return categories


def to_excel(categories, writer):
    categories_dataframe = pd.DataFrame(data={
        "Nazwa": categories
    })
    categories_dataframe.to_excel(writer, "Arkusz 3", index_label="Numer")
