import datetime
import os

import Workers
import Categories
import Animals
import Database
import pandas as pd

current_datetime = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
os.mkdir(current_datetime)
filename = current_datetime + "/zoo"

t0_year = 2001
t1_year = 2016
t2_year = 2018


writer = pd.ExcelWriter(filename + "-t1.xlsx")

workers_data = Workers.generate(t0_year, t1_year)
Workers.to_excel(workers_data, writer)

animals_data = Animals.generate()
Animals.to_excel(animals_data, writer)

categories_data = Categories.generate()
Categories.to_excel(categories_data, writer)

writer.save()

database_data = Database.generate(filename, t0_year, t1_year, workers_data[len(workers_data)-1]['worker_id'])

writer = pd.ExcelWriter(filename + "-t2.xlsx")

Workers.modify(workers_data, t1_year, t2_year)
Workers.to_excel(workers_data, writer)

Animals.modify(animals_data)
Animals.to_excel(animals_data, writer)

Categories.to_excel(categories_data, writer)

writer.save()

Database.generate(filename, t1_year, t2_year, workers_data[len(workers_data)-1]['worker_id'], modify=True)
