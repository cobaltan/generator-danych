import pandas as pd


def from_excel(file):
    excel = pd.read_excel(file, sheet_name="Arkusz 1", index_col=None)
    excel = excel.where((pd.notnull(excel)), None)
    workers = []
    for i in range(0, len(excel["ID"])):
        workers.append({
            "worker_id": excel["ID"][i],
            "pesel": excel["Pesel"][i],
            "first_name": excel["Imię"][i],
            "last_name": excel["Nazwisko"][i],
            "birth_date": excel["Data urodzenia"][i],
            "education": excel["Wykształcenie"][i],
            "position": excel["Stanowisko"][i],
            "acceptance_date": excel["Data zatrudnienia"][i],
            "sacking_date": excel["Data zwolnienia"][i]
        })
    return workers


def to_excel(workers, writer):
    worker_id = []
    pesel = []
    first_name = []
    last_name = []
    birth_date = []
    education = []
    position = []
    acceptance_date = []
    sacking_date = []

    for worker in workers:
        worker_id.append(worker["worker_id"])
        pesel.append(worker["pesel"])
        first_name.append(worker["first_name"])
        last_name.append(worker["last_name"])
        birth_date.append(worker["birth_date"])
        education.append(worker["education"])
        position.append(worker["position"])
        acceptance_date.append(worker["acceptance_date"])
        sacking_date.append(worker["sacking_date"])

    workers_dataframe = pd.DataFrame(data={
        "ID": worker_id,
        "Pesel": pesel,
        "Imię": first_name,
        "Nazwisko": last_name,
        "Data urodzenia": birth_date,
        "Wykształcenie": education,
        "Stanowisko": position,
        "Data zatrudnienia": acceptance_date,
        "Data zwolnienia": sacking_date

    })
    workers_dataframe.to_excel(writer, "Arkusz 1", index=False)
