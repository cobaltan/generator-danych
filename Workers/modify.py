import random
from Workers.utils import generate_position, generate_date, generate_pesel, generate_last_name, generate_first_name, generate_education


def modify(data, t1_year, t2_year, chance_to_promotion=0.4, chance_to_fire=0.4, chance_to_hire=0.5, max_hire=3, birth_year_start=1958, birth_year_end=1999, chance_to_women=0.5):
    last_worker_id = data[len(data) - 1]['worker_id']

    new_workers = []

    for worker in data:
        if worker['sacking_date'] is None:
            if random.random() < chance_to_promotion:
                new_pos = generate_position()
                while new_pos == worker['position']:
                    new_pos = generate_position()
                changing_date = generate_date(t1_year, t2_year)
                worker['sacking_date'] = changing_date
                last_worker_id += 1
                new_workers.append({
                    "worker_id": last_worker_id,
                    "pesel": worker['pesel'],
                    "first_name": worker['first_name'],
                    "last_name": worker['last_name'],
                    "birth_date": worker['birth_date'],
                    "education": worker['education'],
                    "position": new_pos,
                    "acceptance_date": changing_date,
                    "sacking_date": None
                })
            elif random.random() < chance_to_fire:
                fire_date = generate_date(t1_year, t2_year)
                worker['sacking_date'] = fire_date

    for i in range(max_hire):
        if random.random() < chance_to_hire:
            last_worker_id += 1
            birth_date = generate_date(birth_year_start, birth_year_end)
            new_workers.append({
                "worker_id": last_worker_id,
                "pesel": generate_pesel(birth_date),
                "first_name": generate_first_name(chance_to_women),
                "last_name": generate_last_name(),
                "birth_date": birth_date,
                "education": generate_education(),
                "position": generate_position(),
                "acceptance_date": generate_date(t1_year, t2_year),
                "sacking_date": None
            })

    for worker in new_workers:
        data.append(worker)
