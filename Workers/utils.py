import random


def generate_first_name(chance_to_women):
    file_first_f = open("Workers/first-f.txt", 'r', encoding="utf8")
    file_first_m = open("Workers/first-m.txt", 'r', encoding="utf8")
    first_f = []
    first_m = []
    for line in file_first_f:
        if line.count('\n') > 0:
            first_f.append(line[:-1])
        else:
            first_f.append(line)
    for line in file_first_m:
        if line.count('\n') > 0:
            first_m.append(line[:-1])
        else:
            first_m.append(line)
    file_first_f.close()
    file_first_m.close()
    if random.random() < chance_to_women:
        first_name = first_f[random.randint(0, len(first_f)-1)]
    else:
        first_name = first_m[random.randint(0, len(first_m)-1)]

    return first_name


def generate_last_name():
    file_last = open("Workers/last.txt", 'r', encoding="utf8")
    last = []
    for line in file_last:
        if line.count('\n') > 0:
            last.append(line[:-1])
        else:
            last.append(line)
    file_last.close()
    return last[random.randint(0, len(last)-1)]


def generate_date(start, end):
    birth_year = random.randint(start, end)
    birth_month = random.randint(1, 12)
    if birth_month == 2:
        birth_day = random.randint(1, 29)
    elif birth_month%2 == 0:
        birth_day = random.randint(1, 30)
    else:
        birth_day = random.randint(1, 31)

    if birth_month < 10:
        birth_month_str = "0" + str(birth_month)
    else:
        birth_month_str = str(birth_month)
    if birth_day < 10:
        birth_day_str = "0" + str(birth_day)
    else:
        birth_day_str = str(birth_day)

    return str(birth_year) + "-" + birth_month_str + "-" + birth_day_str


def generate_education():
    file_edu = open("Workers/education.txt", 'r', encoding="utf8")
    education = []
    for line in file_edu:
        if line.count('\n') > 0:
            education.append(line[:-1])
        else:
            education.append(line)
    file_edu.close()
    return education[random.randint(0, len(education)-1)]


def generate_position():
    file_pos = open("Workers/position.txt", 'r', encoding="utf8")
    position = []
    for line in file_pos:
        if line.count('\n') > 0:
            position.append(line[:-1])
        else:
            position.append(line)
    file_pos.close()
    return position[random.randint(0, len(position)-1)]


def generate_pesel(birth):
    number_str = str(random.randint(0, 99999))
    while len(number_str) < 5:
        number_str = '0' + number_str
    return str(birth[2:4])+str(birth[5:7])+str(birth[8:10])+number_str
