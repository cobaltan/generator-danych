import random
from Workers.utils import generate_date, generate_position, generate_education, generate_first_name, generate_last_name, generate_pesel


def generate(t0_year, t1_year, number_of_workers=10, last_worker_id=-1, chance_to_women=0.5, chance_to_sack=0.3, chance_to_promotion=0.4,
             birth_year_start=1958, birth_year_end=1999):

    workers = []
    for i in range(0, number_of_workers):
        birth_date = generate_date(birth_year_start, birth_year_end)
        pesel = generate_pesel(birth_date)
        first_name = generate_first_name(chance_to_women)
        last_name = generate_last_name()
        edu = generate_education()
        pos = [generate_position()]
        acceptance_date = generate_date(t0_year, t1_year)
        if random.random() < chance_to_sack and int(acceptance_date[0:4]) < t1_year:
            sacking_date = generate_date(int(acceptance_date[0:4]), t1_year)
        elif random.random() < chance_to_promotion and int(acceptance_date[0:4]) < t1_year:
            sacking_date = generate_date(int(acceptance_date[0:4]), t1_year)
            pos.append(generate_position())
            while pos[0] == pos[1]:
                pos[1] = generate_position()
        else:
            sacking_date = None

        last_worker_id += 1
        workers.append({
            "worker_id": last_worker_id,
            "pesel": pesel,
            "first_name": first_name,
            "last_name": last_name,
            "birth_date": birth_date,
            "education": edu,
            "position": pos[0],
            "acceptance_date": acceptance_date,
            "sacking_date": sacking_date
        })

        if len(pos) > 1:
            last_worker_id += 1
            workers.append({
                "worker_id": last_worker_id,
                "pesel": pesel,
                "first_name": first_name,
                "last_name": last_name,
                "birth_date": birth_date,
                "education": edu,
                "position": pos[1],
                "acceptance_date": sacking_date,
                "sacking_date": None
            })
    return workers
