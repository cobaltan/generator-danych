INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (0, 190501071, "al. P�nocna 80, 93-390 Gorz�w Wielkopolski", "Krajewski-Olszewski", 53612498629803506886931711)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (1, 447424402, "pl. Towarowa 85/28, 87-838 �ory", "Nowak", 12113459395289835486622597)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (2, 443780247, "aleja Daszy�skiego 15/64, 39-445 Pabianice", "Kaczmarek", 70623782520106876458156885)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (3, 225596350, "pl. Grzybowa 60/56, 61-521 Z�bki", "Wr�bel", 44420238969177699915727668)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (4, 413220194, "pl. R�zana 17, 45-750 Tarnobrzeg", "Szyma�ski", 97663843828298568694153938)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (5, 445999921, "plac Floriana 67/19, 11-887 Ko�cian", "Wilk sp. k.", 15035866108055276784852863)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (6, 788182716, "al. Dworska 37, 62-098 M�awa", "Sikorska sp. p.", 44736609074643679184249751)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (7, 874934177, "ul. Toru�ska 54, 18-472 Chojnice", "Nowakowska-Baranowska", 65966363295075067994569373)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (8, 454976864, "plac Szeroka 02, 22-451 O�awa", "Sikora", 63157006835509245403927777)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (9, 872233422, "ul. �wirowa 827, 99-859 Tczew", "G�owacki sp. z o.o.", 55937475084210961781859617)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (10, 481786069, "ulica Pu�askiego 20/04, 10-159 Mi�sk Mazowiecki", "Grupa Michalska", 21155025582903809412759505)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (11, 748608619, "ulica Krucza 773, 49-385 Opole", "Olszewska s. c.", 57061314199823644795136992)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (12, 234300405, "al. Lipca 74/17, 31-019 Malbork", "Fundacja G�rska", 15052841142325681563264513)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (13, 657924930, "ulica Gda�ska 474, 56-559 S�upsk", "Wilk-Brzezi�ski", 80002308277552434940634838)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (14, 809540317, "ul. �urawia 77, 58-304 Inowroc�aw", "Duda sp. j.", 31533438801934259303915473)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (15, 966011044, "aleja Cedrowa 30, 94-322 Lubart�w", "Sp�dzielnia W�odarczyk", 59180417248744141388261310)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (16, 105559375, "al. Handlowa 103, 21-003 M�awa", "Jaworski-Mr�z", 47366609911072106075381393)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (17, 658926509, "aleja Matejki 933, 63-847 Dzier�oni�w", "Laskowska-Wojciechowska", 36633182296400682129367934)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (18, 655402634, "ulica Letniskowa 03, 07-419 Kutno", "Wysocka sp. z o.o.", 10619581974209579617573622)

INSERT INTO Company ("ID", "PhoneNumber", "Address:", "Name", "AccountNumber")
VALUES (19, 121286439, "ulica Podmiejska 34, 09-031 Pruszk�w", "Mr�z P.P.O.F", 19730482057705224581641489)

INSERT INTO Product ("ID", "UnitPrice", "CompanyID", "ForAnimal", "AnimalCategory", "Name")
VALUES (0, 24.85, 14, 1, 1, "Pasza dla konia")

INSERT INTO Product ("ID", "UnitPrice", "CompanyID", "ForAnimal", "AnimalCategory", "Name")
VALUES (1, 144.11, 4, 0, NULL, "�opata")

INSERT INTO Product ("ID", "UnitPrice", "CompanyID", "ForAnimal", "AnimalCategory", "Name")
VALUES (2, 15.16, 16, 1, 1, "�ci�ka dla wiewi�rki")

INSERT INTO Product ("ID", "UnitPrice", "CompanyID", "ForAnimal", "AnimalCategory", "Name")
VALUES (3, 386.9, 16, 0, NULL, "R�kawiczki jednorazowe")

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (0, "2001-00-10", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (1, "2001-00-16", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (2, "2001-01-15", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (3, "2001-01-22", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (4, "2001-01-15", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (5, "2001-01-03", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (6, "2001-02-06", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (7, "2001-02-10", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (8, "2001-03-21", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (9, "2001-03-13", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (10, "2001-03-11", 1, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (11, "2001-03-17", 1, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (12, "2001-03-05", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (13, "2001-04-02", 1, 1, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (14, "2001-04-19", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (15, "2001-04-15", 1, 1, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (16, "2001-04-06", 1, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (17, "2001-05-27", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (18, "2001-05-30", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (19, "2001-05-05", 1, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (20, "2001-05-30", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (21, "2001-05-09", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (22, "2001-06-16", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (23, "2001-06-22", 1, 1, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (24, "2001-07-29", 1, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (25, "2001-07-30", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (26, "2001-07-29", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (27, "2001-07-20", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (28, "2001-08-15", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (29, "2001-08-25", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (30, "2001-08-18", 1, 1, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (31, "2001-08-13", 1, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (32, "2001-08-07", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (33, "2001-09-14", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (34, "2001-09-16", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (35, "2001-09-20", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (36, "2001-10-28", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (37, "2001-10-30", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (38, "2001-10-06", 1, 1, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (39, "2001-10-23", 1, 1, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (40, "2001-10-27", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (41, "2001-10-02", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (42, "2001-11-19", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (43, "2001-11-25", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (44, "2001-11-23", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (45, "2001-11-01", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (46, "2001-11-16", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (47, "2001-11-12", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (48, "2001-11-15", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (49, "2002-00-02", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (50, "2002-00-27", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (51, "2002-00-09", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (52, "2002-00-12", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (53, "2002-00-01", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (54, "2002-01-14", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (55, "2002-01-29", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (56, "2002-01-27", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (57, "2002-01-19", 1, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (58, "2002-01-22", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (59, "2002-01-02", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (60, "2002-01-16", 1, 1, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (61, "2002-02-11", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (62, "2002-02-03", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (63, "2002-02-08", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (64, "2002-02-13", 1, 1, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (65, "2002-02-28", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (66, "2002-03-29", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (67, "2002-03-14", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (68, "2002-03-20", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (69, "2002-03-26", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (70, "2002-03-19", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (71, "2002-04-06", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (72, "2002-04-09", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (73, "2002-04-12", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (74, "2002-05-22", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (75, "2002-05-07", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (76, "2002-05-14", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (77, "2002-06-22", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (78, "2002-06-08", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (79, "2002-07-25", 1, 1, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (80, "2002-07-01", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (81, "2002-07-17", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (82, "2002-07-24", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (83, "2002-07-31", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (84, "2002-08-24", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (85, "2002-09-14", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (86, "2002-09-07", 1, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (87, "2002-09-28", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (88, "2002-10-07", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (89, "2002-10-08", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (90, "2002-10-18", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (91, "2002-10-18", 1, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (92, "2002-10-05", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (93, "2002-10-23", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (94, "2002-10-16", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (95, "2002-11-19", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (96, "2002-11-12", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (97, "2002-11-22", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (98, "2002-11-17", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (99, "2002-11-13", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (100, "2002-11-09", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (101, "2002-11-31", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (102, "2003-00-14", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (103, "2003-00-26", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (104, "2003-00-28", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (105, "2003-00-21", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (106, "2003-00-23", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (107, "2003-01-16", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (108, "2003-02-23", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (109, "2003-02-28", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (110, "2003-02-05", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (111, "2003-02-14", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (112, "2003-03-25", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (113, "2003-03-31", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (114, "2003-03-11", 1, 1, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (115, "2003-03-21", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (116, "2003-03-29", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (117, "2003-03-08", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (118, "2003-03-18", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (119, "2003-04-29", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (120, "2003-04-30", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (121, "2003-04-03", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (122, "2003-04-29", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (123, "2003-05-21", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (124, "2003-05-25", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (125, "2003-05-03", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (126, "2003-05-24", 1, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (127, "2003-05-16", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (128, "2003-05-26", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (129, "2003-05-18", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (130, "2003-06-29", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (131, "2003-06-16", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (132, "2003-06-08", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (133, "2003-07-09", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (134, "2003-07-11", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (135, "2003-07-08", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (136, "2003-07-14", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (137, "2003-07-21", 1, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (138, "2003-07-15", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (139, "2003-08-17", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (140, "2003-08-24", 1, 1, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (141, "2003-08-12", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (142, "2003-09-26", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (143, "2003-09-23", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (144, "2003-10-29", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (145, "2003-10-17", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (146, "2003-10-17", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (147, "2003-10-21", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (148, "2003-10-23", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (149, "2003-10-04", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (150, "2003-10-17", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (151, "2003-11-12", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (152, "2003-11-21", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (153, "2003-11-10", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (154, "2003-11-09", 1, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (155, "2003-11-09", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (156, "2003-11-19", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (157, "2004-00-20", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (158, "2004-00-23", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (159, "2004-00-11", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (160, "2004-00-04", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (161, "2004-00-06", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (162, "2004-00-30", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (163, "2004-00-26", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (164, "2004-01-04", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (165, "2004-01-17", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (166, "2004-01-17", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (167, "2004-01-06", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (168, "2004-01-01", 1, 1, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (169, "2004-02-03", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (170, "2004-03-20", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (171, "2004-03-09", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (172, "2004-03-17", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (173, "2004-03-25", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (174, "2004-03-19", 1, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (175, "2004-03-03", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (176, "2004-04-09", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (177, "2004-04-03", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (178, "2004-04-27", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (179, "2004-05-19", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (180, "2004-05-16", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (181, "2004-05-12", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (182, "2004-05-25", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (183, "2004-05-08", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (184, "2004-05-23", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (185, "2004-06-08", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (186, "2004-06-22", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (187, "2004-06-08", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (188, "2004-06-06", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (189, "2004-07-04", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (190, "2004-07-29", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (191, "2004-07-01", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (192, "2004-07-22", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (193, "2004-08-10", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (194, "2004-08-14", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (195, "2004-09-05", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (196, "2004-10-02", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (197, "2004-10-20", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (198, "2004-10-16", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (199, "2004-10-12", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (200, "2004-10-10", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (201, "2004-10-27", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (202, "2004-10-23", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (203, "2004-11-29", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (204, "2004-11-16", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (205, "2005-00-23", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (206, "2005-00-06", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (207, "2005-00-08", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (208, "2005-01-17", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (209, "2005-01-27", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (210, "2005-01-08", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (211, "2005-02-06", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (212, "2005-03-11", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (213, "2005-03-27", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (214, "2005-03-30", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (215, "2005-03-08", 1, 1, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (216, "2005-04-20", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (217, "2005-04-23", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (218, "2005-04-01", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (219, "2005-04-13", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (220, "2005-04-10", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (221, "2005-05-02", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (222, "2005-05-11", 1, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (223, "2005-05-04", 1, 1, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (224, "2005-05-11", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (225, "2005-05-13", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (226, "2005-05-29", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (227, "2005-05-31", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (228, "2005-06-08", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (229, "2005-06-28", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (230, "2005-06-01", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (231, "2005-06-21", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (232, "2005-06-07", 1, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (233, "2005-06-10", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (234, "2005-07-08", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (235, "2005-07-28", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (236, "2005-07-19", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (237, "2005-07-07", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (238, "2005-07-09", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (239, "2005-08-22", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (240, "2005-08-07", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (241, "2005-08-08", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (242, "2005-08-13", 1, 1, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (243, "2005-08-15", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (244, "2005-08-02", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (245, "2005-09-31", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (246, "2005-09-06", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (247, "2005-09-25", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (248, "2005-09-02", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (249, "2005-10-01", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (250, "2005-10-13", 1, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (251, "2005-11-19", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (252, "2005-11-31", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (253, "2005-11-31", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (254, "2005-11-03", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (255, "2005-11-25", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (256, "2005-11-21", 1, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (257, "2005-11-16", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (258, "2006-00-08", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (259, "2006-00-26", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (260, "2006-00-12", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (261, "2006-00-02", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (262, "2006-00-30", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (263, "2006-00-02", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (264, "2006-01-14", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (265, "2006-01-23", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (266, "2006-01-16", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (267, "2006-01-04", 1, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (268, "2006-01-11", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (269, "2006-02-26", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (270, "2006-02-29", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (271, "2006-02-11", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (272, "2006-02-05", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (273, "2006-02-28", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (274, "2006-03-10", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (275, "2006-03-02", 1, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (276, "2006-04-21", 1, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (277, "2006-04-12", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (278, "2006-04-26", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (279, "2006-04-01", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (280, "2006-04-08", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (281, "2006-04-10", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (282, "2006-04-23", 1, 1, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (283, "2006-05-11", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (284, "2006-05-22", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (285, "2006-05-26", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (286, "2006-06-04", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (287, "2006-06-22", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (288, "2006-07-01", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (289, "2006-07-30", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (290, "2006-07-21", 1, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (291, "2006-07-25", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (292, "2006-08-11", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (293, "2006-08-01", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (294, "2006-08-15", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (295, "2006-09-21", 1, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (296, "2006-09-08", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (297, "2006-10-26", 1, 1, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (298, "2006-10-16", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (299, "2006-11-12", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (300, "2006-11-04", 1, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (301, "2006-11-26", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (302, "2006-11-04", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (303, "2006-11-03", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (304, "2007-00-28", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (305, "2007-01-28", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (306, "2007-01-05", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (307, "2007-01-21", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (308, "2007-01-18", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (309, "2007-01-05", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (310, "2007-02-29", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (311, "2007-02-03", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (312, "2007-02-26", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (313, "2007-02-11", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (314, "2007-02-16", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (315, "2007-03-11", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (316, "2007-03-27", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (317, "2007-03-11", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (318, "2007-03-13", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (319, "2007-03-12", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (320, "2007-03-13", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (321, "2007-04-30", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (322, "2007-04-07", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (323, "2007-04-10", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (324, "2007-05-27", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (325, "2007-05-16", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (326, "2007-05-25", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (327, "2007-05-13", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (328, "2007-05-28", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (329, "2007-05-01", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (330, "2007-05-09", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (331, "2007-06-05", 1, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (332, "2007-06-23", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (333, "2007-06-26", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (334, "2007-06-20", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (335, "2007-07-13", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (336, "2007-07-15", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (337, "2007-07-19", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (338, "2007-07-25", 1, 1, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (339, "2007-08-06", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (340, "2007-08-28", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (341, "2007-08-09", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (342, "2007-08-05", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (343, "2007-09-13", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (344, "2007-09-14", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (345, "2007-09-05", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (346, "2007-09-23", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (347, "2007-09-13", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (348, "2007-09-08", 1, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (349, "2007-09-30", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (350, "2007-10-25", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (351, "2007-10-02", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (352, "2007-11-22", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (353, "2008-00-20", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (354, "2008-01-24", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (355, "2008-01-12", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (356, "2008-01-02", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (357, "2008-01-31", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (358, "2008-01-01", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (359, "2008-02-28", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (360, "2008-02-07", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (361, "2008-02-06", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (362, "2008-02-19", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (363, "2008-03-22", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (364, "2008-03-29", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (365, "2008-03-20", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (366, "2008-03-05", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (367, "2008-03-30", 1, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (368, "2008-03-04", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (369, "2008-03-03", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (370, "2008-04-28", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (371, "2008-05-02", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (372, "2008-05-24", 1, 1, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (373, "2008-06-18", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (374, "2008-06-23", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (375, "2008-06-17", 1, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (376, "2008-07-30", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (377, "2008-07-08", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (378, "2008-07-25", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (379, "2008-07-03", 1, 1, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (380, "2008-07-01", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (381, "2008-07-11", 1, 1, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (382, "2008-08-17", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (383, "2008-08-25", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (384, "2008-08-07", 1, 1, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (385, "2008-08-04", 1, 1, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (386, "2008-09-31", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (387, "2008-09-06", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (388, "2008-09-06", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (389, "2008-10-01", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (390, "2008-10-01", 1, 1, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (391, "2008-10-22", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (392, "2008-10-23", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (393, "2008-11-18", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (394, "2009-00-26", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (395, "2009-00-24", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (396, "2009-00-21", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (397, "2009-01-27", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (398, "2009-01-09", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (399, "2009-01-01", 1, 1, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (400, "2009-01-21", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (401, "2009-01-08", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (402, "2009-01-30", 1, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (403, "2009-01-30", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (404, "2009-02-17", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (405, "2009-02-13", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (406, "2009-02-15", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (407, "2009-02-26", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (408, "2009-02-24", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (409, "2009-02-07", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (410, "2009-03-02", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (411, "2009-03-31", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (412, "2009-04-27", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (413, "2009-04-14", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (414, "2009-04-20", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (415, "2009-04-19", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (416, "2009-04-20", 1, 1, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (417, "2009-05-02", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (418, "2009-05-03", 1, 1, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (419, "2009-05-05", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (420, "2009-06-07", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (421, "2009-07-04", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (422, "2009-07-18", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (423, "2009-07-14", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (424, "2009-07-07", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (425, "2009-07-03", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (426, "2009-07-02", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (427, "2009-08-03", 1, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (428, "2009-08-21", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (429, "2009-08-20", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (430, "2009-08-07", 1, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (431, "2009-08-17", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (432, "2009-08-07", 1, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (433, "2009-09-31", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (434, "2009-09-09", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (435, "2009-09-20", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (436, "2009-09-31", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (437, "2009-09-14", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (438, "2009-09-12", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (439, "2009-09-26", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (440, "2009-10-02", 1, 1, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (441, "2009-10-12", 1, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (442, "2009-11-18", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (443, "2009-11-20", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (444, "2009-11-02", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (445, "2009-11-14", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (446, "2009-11-16", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (447, "2009-11-10", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (448, "2009-11-27", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (449, "2010-00-09", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (450, "2010-00-25", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (451, "2010-01-04", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (452, "2010-01-26", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (453, "2010-01-08", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (454, "2010-01-03", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (455, "2010-01-22", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (456, "2010-02-19", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (457, "2010-02-11", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (458, "2010-02-10", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (459, "2010-03-11", 1, 1, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (460, "2010-03-01", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (461, "2010-04-09", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (462, "2010-05-30", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (463, "2010-05-13", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (464, "2010-06-29", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (465, "2010-06-11", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (466, "2010-06-24", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (467, "2010-07-17", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (468, "2010-07-01", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (469, "2010-07-28", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (470, "2010-07-19", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (471, "2010-07-03", 1, 1, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (472, "2010-07-05", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (473, "2010-07-10", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (474, "2010-08-16", 1, 1, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (475, "2010-08-30", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (476, "2010-08-19", 1, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (477, "2010-08-15", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (478, "2010-08-12", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (479, "2010-08-24", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (480, "2010-09-28", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (481, "2010-10-04", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (482, "2010-10-28", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (483, "2010-10-11", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (484, "2010-10-17", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (485, "2010-11-03", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (486, "2010-11-05", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (487, "2010-11-04", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (488, "2010-11-15", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (489, "2010-11-22", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (490, "2010-11-27", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (491, "2011-00-01", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (492, "2011-00-13", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (493, "2011-00-25", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (494, "2011-00-27", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (495, "2011-00-09", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (496, "2011-01-20", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (497, "2011-01-08", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (498, "2011-01-23", 1, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (499, "2011-02-15", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (500, "2011-02-18", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (501, "2011-02-07", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (502, "2011-02-19", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (503, "2011-02-01", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (504, "2011-03-23", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (505, "2011-03-15", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (506, "2011-03-17", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (507, "2011-03-18", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (508, "2011-04-16", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (509, "2011-05-01", 1, 1, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (510, "2011-05-20", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (511, "2011-05-26", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (512, "2011-05-24", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (513, "2011-05-27", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (514, "2011-06-28", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (515, "2011-06-13", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (516, "2011-06-06", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (517, "2011-06-01", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (518, "2011-07-12", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (519, "2011-08-02", 1, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (520, "2011-08-21", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (521, "2011-08-15", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (522, "2011-08-28", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (523, "2011-08-18", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (524, "2011-08-26", 1, 1, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (525, "2011-09-30", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (526, "2011-09-29", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (527, "2011-09-19", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (528, "2011-10-16", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (529, "2011-10-15", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (530, "2011-10-09", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (531, "2011-10-10", 1, 1, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (532, "2011-10-12", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (533, "2011-10-10", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (534, "2011-11-16", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (535, "2011-11-07", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (536, "2011-11-06", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (537, "2011-11-21", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (538, "2011-11-26", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (539, "2012-00-08", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (540, "2012-00-13", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (541, "2012-00-16", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (542, "2012-00-18", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (543, "2012-00-17", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (544, "2012-01-07", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (545, "2012-01-26", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (546, "2012-01-01", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (547, "2012-02-08", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (548, "2012-02-13", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (549, "2012-02-02", 1, 1, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (550, "2012-02-23", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (551, "2012-03-31", 1, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (552, "2012-03-13", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (553, "2012-03-24", 1, 1, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (554, "2012-03-11", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (555, "2012-03-06", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (556, "2012-03-22", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (557, "2012-04-25", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (558, "2012-04-07", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (559, "2012-04-11", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (560, "2012-04-28", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (561, "2012-04-03", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (562, "2012-05-09", 1, 1, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (563, "2012-05-11", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (564, "2012-05-10", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (565, "2012-05-11", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (566, "2012-05-03", 1, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (567, "2012-05-22", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (568, "2012-06-19", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (569, "2012-06-27", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (570, "2012-06-04", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (571, "2012-06-17", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (572, "2012-06-21", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (573, "2012-06-25", 1, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (574, "2012-07-18", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (575, "2012-07-31", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (576, "2012-07-11", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (577, "2012-07-02", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (578, "2012-07-06", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (579, "2012-07-10", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (580, "2012-08-19", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (581, "2012-08-08", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (582, "2012-08-29", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (583, "2012-08-11", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (584, "2012-08-04", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (585, "2012-08-22", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (586, "2012-08-08", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (587, "2012-09-22", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (588, "2012-09-18", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (589, "2012-09-17", 1, 1, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (590, "2012-09-09", 1, 1, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (591, "2012-09-30", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (592, "2012-10-26", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (593, "2012-10-13", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (594, "2012-10-29", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (595, "2012-11-03", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (596, "2012-11-06", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (597, "2012-11-19", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (598, "2012-11-31", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (599, "2012-11-13", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (600, "2013-00-24", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (601, "2013-00-30", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (602, "2013-00-30", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (603, "2013-00-10", 1, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (604, "2013-00-18", 1, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (605, "2013-00-24", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (606, "2013-01-17", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (607, "2013-01-02", 1, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (608, "2013-02-16", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (609, "2013-02-22", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (610, "2013-03-12", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (611, "2013-03-08", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (612, "2013-03-26", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (613, "2013-04-19", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (614, "2013-04-16", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (615, "2013-04-13", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (616, "2013-04-26", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (617, "2013-04-05", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (618, "2013-05-09", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (619, "2013-05-29", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (620, "2013-05-20", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (621, "2013-05-23", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (622, "2013-05-01", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (623, "2013-05-04", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (624, "2013-06-25", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (625, "2013-06-17", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (626, "2013-06-06", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (627, "2013-06-04", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (628, "2013-06-29", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (629, "2013-07-28", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (630, "2013-07-08", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (631, "2013-07-25", 1, 1, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (632, "2013-07-19", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (633, "2013-07-15", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (634, "2013-07-05", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (635, "2013-07-07", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (636, "2013-08-29", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (637, "2013-08-13", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (638, "2013-09-01", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (639, "2013-09-21", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (640, "2013-09-14", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (641, "2013-09-20", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (642, "2013-09-25", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (643, "2013-09-02", 1, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (644, "2013-10-19", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (645, "2013-10-24", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (646, "2013-10-10", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (647, "2013-10-21", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (648, "2013-11-29", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (649, "2013-11-28", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (650, "2013-11-15", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (651, "2013-11-27", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (652, "2013-11-19", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (653, "2013-11-07", 0, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (654, "2014-00-25", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (655, "2014-01-15", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (656, "2014-01-18", 1, 1, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (657, "2014-02-01", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (658, "2014-02-02", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (659, "2014-02-28", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (660, "2014-02-13", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (661, "2014-03-10", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (662, "2014-04-12", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (663, "2014-04-04", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (664, "2014-04-12", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (665, "2014-04-20", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (666, "2014-04-03", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (667, "2014-04-05", 1, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (668, "2014-05-30", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (669, "2014-05-30", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (670, "2014-05-17", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (671, "2014-06-05", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (672, "2014-07-08", 1, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (673, "2014-07-25", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (674, "2014-07-02", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (675, "2014-07-17", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (676, "2014-07-06", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (677, "2014-07-02", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (678, "2014-08-03", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (679, "2014-08-14", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (680, "2014-08-13", 1, 1, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (681, "2014-08-05", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (682, "2014-08-20", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (683, "2014-08-08", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (684, "2014-09-19", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (685, "2014-09-26", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (686, "2014-09-23", 1, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (687, "2014-09-07", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (688, "2014-10-22", 1, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (689, "2014-10-19", 1, 1, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (690, "2014-10-04", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (691, "2014-11-01", 1, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (692, "2014-11-11", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (693, "2014-11-16", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (694, "2015-00-09", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (695, "2015-00-12", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (696, "2015-01-11", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (697, "2015-01-19", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (698, "2015-01-11", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (699, "2015-01-23", 1, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (700, "2015-01-05", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (701, "2015-01-14", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (702, "2015-01-30", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (703, "2015-02-28", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (704, "2015-02-20", 1, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (705, "2015-02-26", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (706, "2015-02-05", 0, 0, 0, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (707, "2015-03-17", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (708, "2015-03-27", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (709, "2015-03-29", 1, 1, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (710, "2015-03-09", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (711, "2015-03-01", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (712, "2015-03-17", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (713, "2015-03-28", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (714, "2015-04-03", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (715, "2015-04-21", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (716, "2015-04-10", 1, 1, 1, 8)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (717, "2015-04-20", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (718, "2015-04-19", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (719, "2015-04-27", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (720, "2015-05-01", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (721, "2015-05-16", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (722, "2015-05-29", 0, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (723, "2015-06-22", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (724, "2015-06-03", 1, 0, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (725, "2015-06-25", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (726, "2015-06-07", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (727, "2015-06-28", 1, 1, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (728, "2015-07-06", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (729, "2015-07-17", 1, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (730, "2015-07-20", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (731, "2015-07-05", 0, 0, 0, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (732, "2015-07-31", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (733, "2015-07-11", 1, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (734, "2015-07-23", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (735, "2015-08-07", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (736, "2015-09-21", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (737, "2015-09-28", 1, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (738, "2015-09-05", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (739, "2015-09-08", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (740, "2015-10-06", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (741, "2015-10-25", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (742, "2015-10-15", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (743, "2015-11-18", 1, 1, 1, 3)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (0, 2, 0, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1, 20.7341, 1, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2, 2, 2, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (3, 374, 2, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (4, 10.8194, 2, 0, 1, 7.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (5, 27.8063, 2, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (6, 4, 3, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (7, 24.4587, 3, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (8, 358, 3, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (9, 15.5011, 3, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (10, 24.0707, 4, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (11, 2, 4, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (12, 308, 4, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (13, 4, 5, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (14, 24.3115, 6, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (15, 5, 6, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (16, 12.6099, 6, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (17, 311, 6, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (18, 326, 7, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (19, 23.4346, 7, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (20, 24.5892, 7, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (21, 5, 7, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (22, 26.008, 8, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (23, 18.4154, 8, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (24, 1, 8, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (25, 474, 8, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (26, 5, 9, 1, 1, 65.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (27, 326, 9, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (28, 365, 10, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (29, 1, 10, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (30, 25.9278, 10, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (31, 10.4582, 10, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (32, 11.9425, 11, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (33, 4, 11, 1, 1, 71.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (34, 316, 11, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (35, 3, 12, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (36, 302, 12, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (37, 28.1811, 12, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (38, 14.2777, 12, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (39, 23.4788, 13, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (40, 29.4659, 13, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (41, 18.9227, 14, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (42, 377, 14, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (43, 3, 14, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (44, 29.7003, 14, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (45, 3, 15, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (46, 22.5516, 15, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (47, 348, 15, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (48, 21.5714, 15, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (49, 1, 16, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (50, 358, 16, 3, 1, 162.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (51, 19.3692, 16, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (52, 26.814, 16, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (53, 489, 17, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (54, 21.062, 17, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (55, 29.2346, 17, 2, 1, 12.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (56, 3, 17, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (57, 21.0362, 18, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (58, 327, 18, 3, 1, 155.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (59, 3, 18, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (60, 17.1803, 18, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (61, 16.2403, 19, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (62, 5, 19, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (63, 290, 19, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (64, 28.0082, 20, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (65, 257, 20, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (66, 4, 20, 1, 1, 87.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (67, 20.4485, 20, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (68, 1, 21, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (69, 379, 21, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (70, 23.9766, 22, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (71, 382, 22, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (72, 19.4929, 22, 0, 1, 7.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (73, 5, 22, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (74, 4, 23, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (75, 491, 23, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (76, 12.2535, 23, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (77, 22.5356, 23, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (78, 16.4951, 24, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (79, 276, 24, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (80, 1, 24, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (81, 29.1607, 24, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (82, 493, 25, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (83, 338, 26, 3, 1, 167.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (84, 22.1172, 26, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (85, 3, 26, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (86, 10.828, 26, 0, 1, 8.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (87, 364, 27, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (88, 1, 28, 1, 1, 87.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (89, 21.0536, 29, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (90, 12.2812, 29, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (91, 5, 29, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (92, 474, 29, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (93, 5, 30, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (94, 10.3128, 31, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (95, 3, 32, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (96, 21.7796, 32, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (97, 24.9205, 32, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (98, 4, 33, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (99, 348, 33, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (100, 21.1455, 33, 0, 1, 8.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (101, 28.9219, 33, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (102, 16.382, 34, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (103, 3, 34, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (104, 335, 34, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (105, 24.7446, 34, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (106, 3, 35, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (107, 476, 35, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (108, 28.8978, 35, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (109, 15.7402, 35, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (110, 10.4032, 36, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (111, 252, 37, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (112, 14.429, 37, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (113, 29.1439, 37, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (114, 1, 37, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (115, 348, 38, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (116, 27.5564, 38, 2, 1, 12.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (117, 10.1787, 38, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (118, 24.8349, 39, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (119, 464, 39, 3, 1, 192.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (120, 2, 39, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (121, 10.3492, 39, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (122, 22.031, 40, 0, 1, 8.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (123, 5, 40, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (124, 413, 40, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (125, 28.1728, 40, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (126, 14.5071, 41, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (127, 1, 41, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (128, 401, 41, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (129, 29.1413, 41, 2, 1, 13.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (130, 1, 42, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (131, 497, 42, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (132, 20.9026, 42, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (133, 19.0612, 42, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (134, 5, 43, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (135, 18.8888, 44, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (136, 27.663, 44, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (137, 3, 44, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (138, 271, 44, 3, 1, 175.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (139, 26.9991, 45, 2, 1, 12.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (140, 19.422, 45, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (141, 290, 45, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (142, 4, 45, 1, 1, 79.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (143, 260, 46, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (144, 27.973, 46, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (145, 3, 46, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (146, 408, 47, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (147, 4, 47, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (148, 13.51, 47, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (149, 27.9942, 47, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (150, 22.9268, 48, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (151, 290, 48, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (152, 19.0059, 48, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (153, 5, 48, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (154, 1, 49, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (155, 16.3636, 49, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (156, 29.9456, 49, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (157, 443, 49, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (158, 18.6114, 50, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (159, 452, 51, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (160, 4, 51, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (161, 23.1937, 51, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (162, 20.36, 51, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (163, 1, 52, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (164, 27.548, 52, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (165, 23.6198, 52, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (166, 407, 52, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (167, 363, 53, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (168, 28.7446, 53, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (169, 5, 53, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (170, 19.7242, 53, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (171, 24.7747, 54, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (172, 21.2803, 54, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (173, 348, 54, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (174, 5, 54, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (175, 254, 55, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (176, 293, 56, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (177, 16.9965, 56, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (178, 22.3538, 56, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (179, 5, 56, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (180, 5, 57, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (181, 23.5276, 58, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (182, 4, 58, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (183, 314, 58, 3, 1, 190.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (184, 21.8599, 58, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (185, 20.0132, 59, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (186, 23.69, 59, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (187, 3, 60, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (188, 12.2109, 60, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (189, 20.4977, 60, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (190, 326, 60, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (191, 3, 61, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (192, 374, 62, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (193, 463, 63, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (194, 14.6104, 63, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (195, 15.8356, 64, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (196, 479, 64, 3, 1, 167.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (197, 1, 64, 1, 1, 68.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (198, 25.0342, 64, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (199, 282, 65, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (200, 10.6159, 65, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (201, 266, 66, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (202, 2, 66, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (203, 16.8071, 66, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (204, 317, 67, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (205, 19.8405, 67, 0, 1, 7.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (206, 27.2935, 67, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (207, 2, 67, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (208, 271, 68, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (209, 5, 68, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (210, 21.1048, 68, 0, 1, 7.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (211, 22.2518, 68, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (212, 22.6252, 69, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (213, 3, 69, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (214, 328, 69, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (215, 12.3786, 69, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (216, 301, 70, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (217, 1, 70, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (218, 25.35, 70, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (219, 11.484, 70, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (220, 295, 71, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (221, 5, 71, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (222, 18.6838, 71, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (223, 23.6259, 71, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (224, 17.3329, 72, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (225, 1, 72, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (226, 22.4121, 72, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (227, 472, 72, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (228, 24.4163, 73, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (229, 1, 73, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (230, 10.0931, 73, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (231, 468, 73, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (232, 4, 74, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (233, 494, 74, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (234, 22.37, 74, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (235, 436, 75, 3, 1, 200.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (236, 21.2894, 76, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (237, 25.6165, 77, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (238, 18.5886, 77, 0, 1, 7.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (239, 2, 77, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (240, 372, 77, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (241, 11.6804, 78, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (242, 20.2692, 78, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (243, 445, 78, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (244, 2, 78, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (245, 285, 79, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (246, 20.9739, 79, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (247, 4, 80, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (248, 21.8354, 80, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (249, 21.0823, 80, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (250, 369, 80, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (251, 22.3471, 81, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (252, 5, 81, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (253, 19.6676, 81, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (254, 433, 81, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (255, 10.1645, 82, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (256, 254, 82, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (257, 2, 82, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (258, 25.2546, 82, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (259, 27.5724, 83, 2, 1, 13.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (260, 1, 83, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (261, 17.2221, 83, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (262, 305, 83, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (263, 21.9191, 84, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (264, 1, 84, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (265, 20.5522, 84, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (266, 309, 84, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (267, 3, 85, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (268, 360, 85, 3, 1, 225.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (269, 27.9019, 85, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (270, 14.3727, 85, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (271, 398, 86, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (272, 22.2192, 86, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (273, 11.9585, 86, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (274, 4, 86, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (275, 18.6449, 87, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (276, 2, 87, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (277, 281, 87, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (278, 29.908, 87, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (279, 20.7049, 88, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (280, 20.1737, 88, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (281, 259, 88, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (282, 5, 88, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (283, 21.0358, 89, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (284, 4, 89, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (285, 286, 89, 3, 1, 190.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (286, 29.1331, 89, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (287, 3, 90, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (288, 24.9402, 90, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (289, 15.345, 90, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (290, 462, 90, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (291, 352, 91, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (292, 28.9182, 91, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (293, 19.4632, 91, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (294, 4, 91, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (295, 3, 92, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (296, 497, 92, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (297, 16.3551, 92, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (298, 27.6393, 92, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (299, 1, 93, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (300, 384, 93, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (301, 18.8658, 93, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (302, 22.3544, 93, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (303, 365, 94, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (304, 26.342, 94, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (305, 20.0409, 94, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (306, 5, 94, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (307, 24.5077, 95, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (308, 347, 95, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (309, 4, 95, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (310, 20.8463, 95, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (311, 338, 96, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (312, 26.5864, 96, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (313, 23.404, 96, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (314, 4, 96, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (315, 375, 97, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (316, 26.0514, 97, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (317, 13.8649, 97, 0, 1, 8.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (318, 2, 97, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (319, 20.0706, 98, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (320, 23.1492, 98, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (321, 5, 98, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (322, 318, 98, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (323, 5, 99, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (324, 25.5881, 99, 2, 1, 13.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (325, 20.1367, 99, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (326, 303, 99, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (327, 4, 100, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (328, 20.6109, 100, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (329, 15.6467, 100, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (330, 349, 100, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (331, 1, 101, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (332, 393, 101, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (333, 23.578, 101, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (334, 21.3157, 101, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (335, 4, 102, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (336, 21.9558, 102, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (337, 11.215, 102, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (338, 464, 102, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (339, 18.8897, 103, 0, 1, 7.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (340, 412, 103, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (341, 11.3519, 104, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (342, 392, 104, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (343, 2, 105, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (344, 12.6687, 105, 0, 1, 7.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (345, 23.3807, 106, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (346, 18.9665, 106, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (347, 391, 106, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (348, 5, 106, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (349, 11.7391, 107, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (350, 367, 107, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (351, 1, 107, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (352, 20.4293, 107, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (353, 5, 108, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (354, 18.5921, 108, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (355, 319, 108, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (356, 28.5889, 108, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (357, 23.5578, 109, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (358, 27.1559, 109, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (359, 2, 109, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (360, 477, 109, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (361, 4, 110, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (362, 20.3123, 110, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (363, 496, 110, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (364, 14.3635, 110, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (365, 18.3405, 111, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (366, 26.7119, 111, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (367, 2, 111, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (368, 268, 111, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (369, 14.4964, 112, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (370, 26.6227, 113, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (371, 22.8645, 113, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (372, 1, 113, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (373, 452, 113, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (374, 23.5753, 114, 2, 1, 13.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (375, 5, 114, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (376, 428, 114, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (377, 21.6605, 114, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (378, 12.1259, 115, 0, 1, 7.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (379, 499, 115, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (380, 1, 115, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (381, 28.6907, 115, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (382, 3, 116, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (383, 365, 117, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (384, 4, 117, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (385, 29.1135, 117, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (386, 24.7208, 117, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (387, 15.0046, 118, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (388, 25.6272, 118, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (389, 4, 118, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (390, 488, 118, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (391, 17.2717, 119, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (392, 421, 119, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (393, 2, 119, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (394, 26.5537, 119, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (395, 25.5386, 120, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (396, 15.5603, 121, 0, 1, 8.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (397, 364, 121, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (398, 1, 122, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (399, 18.5717, 122, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (400, 5, 123, 1, 1, 75.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (401, 361, 123, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (402, 29.5791, 123, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (403, 17.2399, 123, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (404, 1, 124, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (405, 16.5006, 124, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (406, 24.7422, 124, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (407, 325, 124, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (408, 4, 125, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (409, 297, 125, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (410, 19.6598, 125, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (411, 25.7851, 125, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (412, 14.3008, 126, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (413, 21.5873, 126, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (414, 2, 126, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (415, 26.3787, 127, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (416, 18.3339, 127, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (417, 2, 127, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (418, 425, 127, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (419, 12.1099, 128, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (420, 25.6223, 128, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (421, 276, 128, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (422, 2, 128, 1, 1, 68.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (423, 264, 129, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (424, 17.9489, 129, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (425, 1, 129, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (426, 29.505, 129, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (427, 20.6447, 130, 0, 1, 7.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (428, 25.5785, 130, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (429, 5, 130, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (430, 491, 130, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (431, 449, 131, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (432, 3, 131, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (433, 21.6354, 131, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (434, 16.257, 131, 0, 1, 8.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (435, 295, 132, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (436, 13.5216, 132, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (437, 3, 132, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (438, 24.9021, 132, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (439, 283, 133, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (440, 25.5111, 133, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (441, 324, 134, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (442, 14.4823, 134, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (443, 27.2398, 134, 2, 1, 12.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (444, 3, 134, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (445, 17.5524, 135, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (446, 25.5456, 136, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (447, 18.9916, 136, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (448, 2, 137, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (449, 20.1669, 137, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (450, 426, 138, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (451, 5, 138, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (452, 23.9886, 138, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (453, 15.0146, 138, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (454, 22.9093, 139, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (455, 419, 139, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (456, 3, 139, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (457, 14.6755, 139, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (458, 3, 140, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (459, 3, 141, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (460, 320, 141, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (461, 10.0915, 141, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (462, 26.8218, 141, 2, 1, 13.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (463, 334, 142, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (464, 12.901, 142, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (465, 5, 142, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (466, 27.3306, 142, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (467, 4, 143, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (468, 369, 143, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (469, 16.6053, 143, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (470, 26.135, 143, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (471, 23.3476, 144, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (472, 482, 144, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (473, 10.4895, 144, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (474, 5, 144, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (475, 295, 145, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (476, 2, 145, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (477, 19.6998, 145, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (478, 24.85, 145, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (479, 1, 146, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (480, 26.1072, 146, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (481, 405, 146, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (482, 18.1542, 146, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (483, 3, 147, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (484, 12.1533, 147, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (485, 20.1755, 147, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (486, 439, 148, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (487, 26.3996, 148, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (488, 23.2275, 149, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (489, 13.2079, 150, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (490, 471, 150, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (491, 23.2045, 150, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (492, 22.0061, 151, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (493, 20.1597, 152, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (494, 489, 152, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (495, 13.4109, 152, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (496, 3, 152, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (497, 10.9468, 153, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (498, 29.8878, 153, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (499, 2, 153, 1, 1, 83.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (500, 273, 153, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (501, 27.7306, 154, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (502, 382, 154, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (503, 13.9512, 154, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (504, 4, 154, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (505, 3, 155, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (506, 296, 155, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (507, 20.5304, 155, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (508, 28.2075, 155, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (509, 21.8171, 156, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (510, 4, 156, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (511, 19.7596, 156, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (512, 270, 156, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (513, 305, 157, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (514, 25.1833, 157, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (515, 5, 157, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (516, 24.6517, 157, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (517, 406, 158, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (518, 14.2151, 158, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (519, 20.6365, 158, 2, 1, 12.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (520, 4, 158, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (521, 29.8692, 159, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (522, 293, 159, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (523, 399, 160, 3, 1, 217.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (524, 22.8937, 160, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (525, 21.3983, 160, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (526, 4, 160, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (527, 26.3286, 161, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (528, 24.9716, 161, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (529, 370, 162, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (530, 20.3639, 162, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (531, 23.0519, 162, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (532, 3, 162, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (533, 347, 163, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (534, 20.3733, 163, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (535, 2, 163, 1, 1, 72.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (536, 4, 164, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (537, 22.4893, 164, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (538, 267, 164, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (539, 17.8267, 164, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (540, 11.537, 165, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (541, 331, 165, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (542, 3, 166, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (543, 413, 166, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (544, 11.3466, 166, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (545, 26.1874, 166, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (546, 1, 167, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (547, 389, 167, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (548, 16.5633, 167, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (549, 28.0315, 167, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (550, 2, 168, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (551, 14.7226, 168, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (552, 399, 168, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (553, 22.87, 168, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (554, 2, 169, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (555, 14.6869, 169, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (556, 412, 169, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (557, 23.4466, 169, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (558, 27.0427, 170, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (559, 3, 170, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (560, 10.8569, 170, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (561, 465, 170, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (562, 413, 171, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (563, 2, 171, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (564, 20.4381, 171, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (565, 15.074, 171, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (566, 14.7579, 172, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (567, 1, 172, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (568, 1, 173, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (569, 21.79, 173, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (570, 471, 173, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (571, 11.4202, 173, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (572, 27.5069, 174, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (573, 10.6703, 175, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (574, 28.323, 175, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (575, 458, 175, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (576, 5, 175, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (577, 10.7557, 176, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (578, 336, 176, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (579, 25.4386, 176, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (580, 3, 176, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (581, 4, 177, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (582, 463, 177, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (583, 20.0751, 177, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (584, 21.7928, 177, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (585, 26.2882, 178, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (586, 1, 178, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (587, 21.1948, 178, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (588, 331, 178, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (589, 4, 179, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (590, 3, 180, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (591, 372, 180, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (592, 21.3687, 180, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (593, 10.3592, 180, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (594, 299, 181, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (595, 5, 181, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (596, 27.4743, 181, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (597, 17.6798, 181, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (598, 20.3059, 182, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (599, 16.5679, 183, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (600, 294, 183, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (601, 376, 184, 3, 1, 190.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (602, 23.7509, 184, 0, 1, 7.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (603, 1, 184, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (604, 410, 185, 3, 1, 217.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (605, 422, 186, 3, 1, 155.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (606, 4, 186, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (607, 27.2531, 186, 2, 1, 13.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (608, 18.9938, 186, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (609, 26.7255, 187, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (610, 4, 187, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (611, 493, 187, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (612, 2, 188, 1, 1, 86.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (613, 24.7643, 188, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (614, 482, 189, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (615, 4, 189, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (616, 11.2454, 190, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (617, 2, 190, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (618, 20.1833, 190, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (619, 403, 190, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (620, 26.4751, 191, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (621, 22.9783, 191, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (622, 353, 191, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (623, 1, 191, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (624, 19.7855, 192, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (625, 29.206, 192, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (626, 438, 192, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (627, 5, 192, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (628, 4, 193, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (629, 353, 193, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (630, 23.8661, 194, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (631, 2, 194, 1, 1, 79.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (632, 21.9519, 194, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (633, 307, 194, 3, 1, 192.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (634, 20.8511, 195, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (635, 439, 195, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (636, 3, 195, 1, 1, 83.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (637, 21.5287, 195, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (638, 11.6509, 196, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (639, 1, 196, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (640, 23.6216, 196, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (641, 290, 196, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (642, 25.0987, 197, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (643, 494, 197, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (644, 3, 197, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (645, 12.0555, 197, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (646, 24.4986, 198, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (647, 4, 198, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (648, 447, 198, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (649, 15.2811, 199, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (650, 497, 200, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (651, 25.6632, 200, 2, 1, 10.65)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (652, 15.9612, 200, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (653, 3, 200, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (654, 4, 201, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (655, 409, 201, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (656, 16.3285, 201, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (657, 26.7714, 201, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (658, 28.2682, 202, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (659, 1, 202, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (660, 19.6349, 202, 0, 1, 8.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (661, 404, 202, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (662, 488, 203, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (663, 20.4563, 203, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (664, 3, 203, 1, 1, 73.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (665, 5, 204, 1, 1, 73.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (666, 15.5897, 204, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (667, 21.094, 204, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (668, 455, 204, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (669, 1, 205, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (670, 454, 205, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (671, 20.972, 205, 2, 1, 12.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (672, 19.1469, 205, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (673, 28.3377, 206, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (674, 1, 206, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (675, 439, 206, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (676, 5, 207, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (677, 397, 208, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (678, 1, 209, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (679, 22.5511, 209, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (680, 26.4986, 209, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (681, 475, 209, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (682, 2, 210, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (683, 27.4303, 210, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (684, 454, 210, 3, 1, 210.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (685, 10.8148, 210, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (686, 21.565, 211, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (687, 2, 211, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (688, 23.2904, 211, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (689, 475, 211, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (690, 25.2571, 212, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (691, 5, 212, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (692, 18.2416, 213, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (693, 23.0207, 213, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (694, 24.6716, 214, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (695, 3, 214, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (696, 14.1689, 214, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (697, 355, 214, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (698, 13.0903, 215, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (699, 4, 215, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (700, 21.2899, 215, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (701, 262, 215, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (702, 25.7833, 216, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (703, 24.6627, 216, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (704, 5, 216, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (705, 269, 216, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (706, 2, 217, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (707, 27.3641, 217, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (708, 382, 217, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (709, 24.1432, 217, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (710, 381, 218, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (711, 14.1399, 218, 0, 1, 8.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (712, 280, 219, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (713, 14.506, 219, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (714, 2, 219, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (715, 26.8026, 219, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (716, 5, 220, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (717, 282, 221, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (718, 16.9494, 222, 0, 1, 8.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (719, 2, 222, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (720, 349, 222, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (721, 29.2324, 222, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (722, 12.7297, 223, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (723, 21.4836, 224, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (724, 360, 224, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (725, 16.3394, 224, 0, 1, 6.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (726, 5, 224, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (727, 276, 225, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (728, 14.2474, 225, 0, 1, 8.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (729, 21.958, 225, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (730, 2, 225, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (731, 21.1618, 226, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (732, 1, 226, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (733, 424, 226, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (734, 1, 227, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (735, 324, 227, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (736, 19.9762, 227, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (737, 25.1724, 227, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (738, 27.2055, 228, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (739, 2, 228, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (740, 17.0428, 228, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (741, 296, 228, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (742, 4, 229, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (743, 408, 230, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (744, 4, 230, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (745, 21.1347, 230, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (746, 17.9478, 230, 0, 1, 8.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (747, 460, 231, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (748, 22.4177, 232, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (749, 24.2176, 232, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (750, 349, 232, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (751, 1, 232, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (752, 466, 233, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (753, 10.6807, 234, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (754, 426, 234, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (755, 2, 234, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (756, 29.8074, 234, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (757, 29.9589, 235, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (758, 12.2269, 235, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (759, 252, 235, 3, 1, 192.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (760, 2, 235, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (761, 13.1016, 236, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (762, 411, 236, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (763, 5, 236, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (764, 28.9572, 236, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (765, 22.2761, 237, 2, 1, 10.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (766, 24.9229, 237, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (767, 3, 237, 1, 1, 75.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (768, 319, 237, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (769, 4, 238, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (770, 350, 238, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (771, 24.878, 238, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (772, 12.3781, 238, 0, 1, 8.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (773, 433, 239, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (774, 24.8108, 239, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (775, 3, 239, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (776, 21.535, 239, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (777, 18.9531, 240, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (778, 5, 241, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (779, 356, 241, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (780, 22.0886, 241, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (781, 20.1146, 241, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (782, 24.4559, 242, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (783, 2, 242, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (784, 12.2801, 242, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (785, 270, 242, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (786, 20.545, 243, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (787, 2, 243, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (788, 494, 243, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (789, 5, 244, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (790, 262, 244, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (791, 10.9546, 244, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (792, 496, 245, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (793, 25.6873, 245, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (794, 10.9067, 245, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (795, 4, 245, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (796, 27.493, 246, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (797, 368, 246, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (798, 20.5912, 246, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (799, 5, 246, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (800, 16.2295, 247, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (801, 3, 247, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (802, 370, 247, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (803, 20.493, 247, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (804, 1, 248, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (805, 20.2826, 248, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (806, 269, 249, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (807, 16.5526, 249, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (808, 4, 249, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (809, 22.9572, 249, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (810, 22.1478, 250, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (811, 28.5914, 250, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (812, 2, 250, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (813, 366, 250, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (814, 27.2748, 251, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (815, 330, 251, 3, 1, 180.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (816, 4, 251, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (817, 19.8516, 251, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (818, 399, 252, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (819, 1, 252, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (820, 17.8003, 252, 0, 1, 8.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (821, 22.4699, 252, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (822, 14.223, 253, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (823, 29.2484, 253, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (824, 310, 253, 3, 1, 175.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (825, 4, 253, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (826, 11.9325, 254, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (827, 24.3923, 254, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (828, 4, 255, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (829, 15.3783, 255, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (830, 25.2382, 255, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (831, 457, 255, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (832, 329, 256, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (833, 1, 256, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (834, 13.2232, 256, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (835, 28.7104, 256, 2, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (836, 384, 257, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (837, 18.8915, 257, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (838, 3, 257, 1, 1, 76.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (839, 2, 258, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (840, 10.8505, 258, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (841, 23.2652, 258, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (842, 28.5218, 259, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (843, 2, 259, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (844, 1, 260, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (845, 26.8408, 260, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (846, 370, 260, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (847, 24.0961, 261, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (848, 29.8795, 262, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (849, 448, 262, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (850, 18.6642, 262, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (851, 5, 262, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (852, 15.5827, 263, 0, 1, 8.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (853, 2, 264, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (854, 22.2004, 264, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (855, 20.7001, 264, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (856, 316, 264, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (857, 13.978, 265, 0, 1, 7.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (858, 301, 265, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (859, 2, 265, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (860, 25.5301, 265, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (861, 21.6254, 266, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (862, 497, 266, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (863, 1, 266, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (864, 21.9773, 266, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (865, 445, 267, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (866, 10.2052, 267, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (867, 23.6532, 267, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (868, 2, 267, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (869, 29.5614, 268, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (870, 4, 268, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (871, 23.1447, 269, 2, 1, 12.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (872, 2, 269, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (873, 312, 269, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (874, 20.3764, 269, 0, 1, 7.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (875, 28.7885, 270, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (876, 1, 270, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (877, 417, 270, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (878, 15.8122, 270, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (879, 16.7396, 271, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (880, 20.5578, 271, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (881, 2, 271, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (882, 11.0986, 272, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (883, 28.9245, 272, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (884, 4, 272, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (885, 329, 272, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (886, 457, 273, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (887, 1, 273, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (888, 24.2823, 273, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (889, 12.2391, 273, 0, 1, 6.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (890, 385, 274, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (891, 23.0799, 274, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (892, 3, 274, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (893, 325, 275, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (894, 27.2572, 275, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (895, 4, 275, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (896, 12.1846, 275, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (897, 20.3872, 276, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (898, 1, 276, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (899, 442, 276, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (900, 28.5784, 276, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (901, 19.8887, 277, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (902, 1, 277, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (903, 261, 277, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (904, 28.6977, 277, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (905, 2, 278, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (906, 21.7368, 278, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (907, 331, 278, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (908, 21.9044, 278, 2, 1, 9.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (909, 22.4298, 279, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (910, 4, 279, 1, 1, 87.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (911, 24.8472, 279, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (912, 282, 279, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (913, 19.2475, 280, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (914, 276, 280, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (915, 3, 280, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (916, 23.4672, 280, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (917, 451, 281, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (918, 5, 281, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (919, 14.3086, 282, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (920, 2, 282, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (921, 425, 283, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (922, 5, 283, 1, 1, 87.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (923, 14.9437, 283, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (924, 27.9338, 283, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (925, 22.3439, 284, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (926, 11.0124, 284, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (927, 1, 284, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (928, 403, 284, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (929, 3, 285, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (930, 22.51, 285, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (931, 18.342, 285, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (932, 251, 285, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (933, 20.1828, 286, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (934, 4, 286, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (935, 423, 286, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (936, 19.4715, 286, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (937, 5, 287, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (938, 282, 287, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (939, 11.5812, 287, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (940, 27.2192, 288, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (941, 15.5743, 288, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (942, 2, 288, 1, 1, 79.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (943, 453, 288, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (944, 22.7723, 289, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (945, 449, 289, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (946, 27.166, 289, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (947, 1, 289, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (948, 22.8267, 290, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (949, 26.4671, 290, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (950, 447, 290, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (951, 5, 290, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (952, 24.1507, 291, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (953, 27.4184, 291, 2, 1, 13.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (954, 4, 291, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (955, 479, 291, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (956, 382, 292, 3, 1, 175.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (957, 2, 292, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (958, 25.2571, 292, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (959, 15.6001, 292, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (960, 321, 293, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (961, 28.0922, 293, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (962, 3, 293, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (963, 11.1292, 293, 0, 1, 7.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (964, 22.3797, 294, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (965, 22.3956, 295, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (966, 389, 296, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (967, 17.8406, 297, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (968, 309, 297, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (969, 27.6453, 297, 2, 1, 9.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (970, 16.83, 298, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (971, 311, 298, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (972, 1, 298, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (973, 26.1027, 298, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (974, 5, 299, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (975, 319, 299, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (976, 11.9661, 299, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (977, 18.4182, 300, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (978, 24.47, 300, 2, 1, 13.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (979, 448, 301, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (980, 17.3858, 301, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (981, 5, 301, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (982, 25.1231, 301, 2, 1, 12.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (983, 23.167, 302, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (984, 2, 302, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (985, 24.4595, 303, 0, 1, 8.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (986, 3, 303, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (987, 23.7688, 303, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (988, 255, 303, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (989, 4, 304, 1, 1, 88.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (990, 25.3601, 304, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (991, 17.5244, 305, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (992, 3, 305, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (993, 407, 305, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (994, 22.6124, 305, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (995, 25.1838, 306, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (996, 377, 306, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (997, 24.4974, 306, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (998, 3, 306, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (999, 3, 307, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1000, 26.988, 307, 2, 1, 10.65)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1001, 251, 308, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1002, 20.752, 308, 0, 1, 7.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1003, 3, 308, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1004, 26.904, 308, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1005, 25.1185, 309, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1006, 268, 309, 3, 1, 225.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1007, 12.489, 309, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1008, 308, 310, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1009, 22.9625, 310, 2, 1, 13.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1010, 3, 310, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1011, 23.7956, 310, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1012, 4, 311, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1013, 22.4693, 311, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1014, 310, 311, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1015, 18.7354, 311, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1016, 404, 312, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1017, 23.1591, 312, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1018, 22.0141, 312, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1019, 3, 312, 1, 1, 68.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1020, 288, 313, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1021, 14.4029, 313, 0, 1, 7.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1022, 14.2836, 314, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1023, 5, 314, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1024, 29.7126, 314, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1025, 301, 314, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1026, 17.5808, 315, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1027, 3, 315, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1028, 257, 315, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1029, 29.8375, 315, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1030, 258, 316, 3, 1, 225.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1031, 28.048, 317, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1032, 494, 317, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1033, 20.421, 317, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1034, 3, 317, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1035, 3, 318, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1036, 26.2771, 318, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1037, 16.1221, 318, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1038, 475, 318, 3, 1, 160.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1039, 14.3978, 319, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1040, 21.5371, 319, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1041, 2, 319, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1042, 261, 319, 3, 1, 162.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1043, 2, 320, 1, 1, 76.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1044, 499, 320, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1045, 15.2174, 320, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1046, 23.61, 320, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1047, 3, 321, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1048, 2, 322, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1049, 28.1405, 322, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1050, 492, 322, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1051, 13.8635, 322, 0, 1, 8.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1052, 1, 323, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1053, 253, 323, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1054, 24.3971, 323, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1055, 23.6814, 323, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1056, 28.8197, 324, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1057, 21.5571, 325, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1058, 4, 325, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1059, 22.9554, 325, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1060, 365, 325, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1061, 371, 326, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1062, 24.0279, 326, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1063, 4, 326, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1064, 10.429, 326, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1065, 10.0528, 327, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1066, 25.4134, 327, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1067, 461, 327, 3, 1, 155.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1068, 4, 327, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1069, 11.2391, 328, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1070, 344, 328, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1071, 2, 328, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1072, 22.4487, 328, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1073, 12.3304, 329, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1074, 292, 330, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1075, 27.6936, 330, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1076, 2, 330, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1077, 11.021, 330, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1078, 301, 331, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1079, 3, 331, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1080, 16.6257, 331, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1081, 28.2765, 331, 2, 1, 9.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1082, 11.0351, 332, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1083, 5, 332, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1084, 22.8849, 332, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1085, 416, 332, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1086, 18.7915, 333, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1087, 25.2428, 333, 2, 1, 11.85)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1088, 4, 333, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1089, 459, 333, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1090, 5, 334, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1091, 24.9615, 335, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1092, 28.1943, 335, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1093, 398, 335, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1094, 2, 335, 1, 1, 88.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1095, 4, 336, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1096, 442, 336, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1097, 26.2218, 336, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1098, 11.1129, 336, 0, 1, 8.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1099, 14.8727, 337, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1100, 19.6947, 338, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1101, 21.3054, 338, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1102, 5, 338, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1103, 298, 338, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1104, 5, 339, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1105, 22.1158, 339, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1106, 356, 339, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1107, 28.0044, 339, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1108, 2, 340, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1109, 389, 340, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1110, 25.5193, 340, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1111, 12.4383, 340, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1112, 4, 341, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1113, 20.1684, 341, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1114, 13.1587, 341, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1115, 335, 341, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1116, 1, 342, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1117, 357, 342, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1118, 28.7221, 342, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1119, 10.9665, 343, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1120, 5, 343, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1121, 428, 343, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1122, 20.1059, 343, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1123, 385, 344, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1124, 21.725, 344, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1125, 4, 344, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1126, 474, 345, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1127, 18.1124, 345, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1128, 1, 345, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1129, 21.0694, 345, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1130, 5, 346, 1, 1, 65.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1131, 28.1993, 346, 2, 1, 11.85)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1132, 334, 346, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1133, 23.6709, 347, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1134, 13.0535, 347, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1135, 397, 347, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1136, 1, 347, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1137, 12.8275, 348, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1138, 3, 348, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1139, 319, 348, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1140, 22.0381, 348, 2, 1, 10.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1141, 11.7982, 349, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1142, 22.013, 349, 2, 1, 10.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1143, 400, 349, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1144, 4, 349, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1145, 27.8473, 350, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1146, 4, 350, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1147, 5, 351, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1148, 417, 351, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1149, 25.8012, 351, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1150, 20.227, 351, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1151, 485, 352, 3, 1, 175.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1152, 4, 352, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1153, 20.3592, 352, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1154, 18.5569, 352, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1155, 24.7717, 353, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1156, 490, 353, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1157, 422, 354, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1158, 21.6156, 354, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1159, 11.4472, 354, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1160, 5, 354, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1161, 24.8884, 355, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1162, 16.3545, 355, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1163, 325, 355, 3, 1, 180.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1164, 3, 355, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1165, 16.9225, 356, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1166, 2, 356, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1167, 465, 356, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1168, 13.396, 357, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1169, 25.133, 357, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1170, 1, 357, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1171, 378, 357, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1172, 23.7217, 358, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1173, 24.505, 358, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1174, 2, 358, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1175, 369, 358, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1176, 20.4727, 359, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1177, 1, 359, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1178, 10.3807, 359, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1179, 439, 359, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1180, 1, 360, 1, 1, 86.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1181, 21.0813, 360, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1182, 14.3558, 360, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1183, 349, 360, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1184, 5, 361, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1185, 291, 361, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1186, 18.578, 361, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1187, 23.8386, 361, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1188, 17.0144, 362, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1189, 21.6529, 362, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1190, 5, 363, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1191, 343, 363, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1192, 27.3352, 363, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1193, 18.1809, 363, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1194, 21.8148, 364, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1195, 330, 364, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1196, 29.8859, 364, 2, 1, 10.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1197, 1, 364, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1198, 24.0447, 365, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1199, 343, 365, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1200, 2, 365, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1201, 21.0991, 365, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1202, 450, 366, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1203, 4, 366, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1204, 16.5063, 367, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1205, 24.0608, 367, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1206, 402, 367, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1207, 4, 367, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1208, 446, 368, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1209, 413, 369, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1210, 22.595, 370, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1211, 4, 370, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1212, 25.6336, 371, 2, 1, 13.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1213, 22.3775, 372, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1214, 308, 372, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1215, 25.6743, 372, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1216, 3, 372, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1217, 23.7812, 373, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1218, 418, 373, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1219, 1, 373, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1220, 28.1123, 373, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1221, 251, 374, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1222, 21.2187, 374, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1223, 5, 374, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1224, 15.482, 374, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1225, 19.257, 375, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1226, 262, 375, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1227, 1, 375, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1228, 28.5842, 375, 2, 1, 13.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1229, 451, 376, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1230, 21.4346, 376, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1231, 1, 376, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1232, 20.928, 376, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1233, 5, 377, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1234, 20.3993, 377, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1235, 4, 378, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1236, 22.6194, 378, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1237, 254, 378, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1238, 17.3447, 378, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1239, 1, 379, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1240, 427, 379, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1241, 10.6253, 379, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1242, 25.9442, 379, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1243, 5, 380, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1244, 2, 381, 1, 1, 83.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1245, 28.7861, 381, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1246, 361, 381, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1247, 21.3257, 381, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1248, 24.0544, 382, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1249, 287, 382, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1250, 3, 382, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1251, 13.5338, 382, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1252, 22.924, 383, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1253, 1, 383, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1254, 27.1194, 383, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1255, 471, 383, 3, 1, 162.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1256, 20.7994, 384, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1257, 496, 384, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1258, 21.5409, 384, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1259, 1, 384, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1260, 23.1302, 385, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1261, 336, 385, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1262, 5, 385, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1263, 24.8266, 385, 0, 1, 6.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1264, 3, 386, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1265, 20.319, 386, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1266, 358, 386, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1267, 24.3662, 386, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1268, 355, 387, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1269, 22.5092, 387, 0, 1, 8.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1270, 3, 387, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1271, 27.9701, 387, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1272, 23.1675, 388, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1273, 3, 388, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1274, 28.0702, 388, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1275, 408, 388, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1276, 432, 389, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1277, 14.3564, 389, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1278, 20.8771, 389, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1279, 1, 389, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1280, 2, 390, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1281, 309, 390, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1282, 27.12, 390, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1283, 14.633, 390, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1284, 23.4364, 391, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1285, 13.6516, 391, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1286, 1, 391, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1287, 319, 391, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1288, 3, 392, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1289, 283, 392, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1290, 27.7845, 392, 2, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1291, 18.4014, 392, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1292, 24.6668, 393, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1293, 377, 393, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1294, 2, 393, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1295, 13.7923, 393, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1296, 10.9298, 394, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1297, 440, 394, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1298, 1, 394, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1299, 20.8484, 394, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1300, 2, 395, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1301, 356, 395, 3, 1, 225.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1302, 11.9829, 395, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1303, 24.8186, 395, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1304, 22.7382, 396, 0, 1, 8.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1305, 24.0674, 396, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1306, 4, 396, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1307, 403, 396, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1308, 18.5097, 397, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1309, 23.941, 397, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1310, 314, 397, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1311, 4, 397, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1312, 28.904, 398, 2, 1, 12.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1313, 12.2666, 398, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1314, 475, 398, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1315, 11.953, 399, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1316, 28.4297, 399, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1317, 416, 399, 3, 1, 180.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1318, 1, 399, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1319, 3, 400, 1, 1, 86.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1320, 463, 400, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1321, 13.9713, 400, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1322, 28.348, 400, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1323, 5, 401, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1324, 16.394, 401, 0, 1, 8.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1325, 328, 401, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1326, 29.3002, 401, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1327, 2, 402, 1, 1, 83.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1328, 23.229, 402, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1329, 407, 402, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1330, 2, 403, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1331, 23.6045, 403, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1332, 365, 403, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1333, 21.4992, 403, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1334, 21.5419, 404, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1335, 13.957, 404, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1336, 438, 404, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1337, 3, 404, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1338, 384, 405, 3, 1, 192.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1339, 1, 405, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1340, 29.6321, 405, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1341, 2, 406, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1342, 4, 407, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1343, 13.1661, 407, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1344, 266, 407, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1345, 22.4899, 407, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1346, 24.5665, 408, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1347, 384, 408, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1348, 3, 408, 1, 1, 79.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1349, 22.6944, 408, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1350, 10.6993, 409, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1351, 326, 409, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1352, 3, 409, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1353, 485, 410, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1354, 2, 410, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1355, 26.2271, 410, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1356, 13.5129, 410, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1357, 29.3937, 411, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1358, 1, 411, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1359, 12.6988, 411, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1360, 438, 411, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1361, 29.8308, 412, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1362, 396, 413, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1363, 469, 414, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1364, 23.293, 414, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1365, 13.6923, 414, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1366, 5, 414, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1367, 24.9004, 415, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1368, 28.2002, 416, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1369, 21.5262, 417, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1370, 19.2118, 417, 0, 1, 7.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1371, 327, 417, 3, 1, 200.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1372, 3, 417, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1373, 12.8659, 418, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1374, 419, 418, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1375, 2, 419, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1376, 18.3535, 419, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1377, 24.1159, 419, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1378, 277, 419, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1379, 493, 420, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1380, 348, 421, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1381, 2, 421, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1382, 21.4976, 421, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1383, 18.361, 421, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1384, 490, 422, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1385, 20.7583, 422, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1386, 26.9658, 422, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1387, 2, 422, 1, 1, 72.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1388, 24.2274, 423, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1389, 261, 423, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1390, 5, 423, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1391, 29.2617, 423, 2, 1, 10.65)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1392, 22.7362, 424, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1393, 4, 424, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1394, 470, 424, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1395, 14.7954, 424, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1396, 11.2881, 425, 0, 1, 7.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1397, 421, 425, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1398, 5, 425, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1399, 21.892, 425, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1400, 3, 426, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1401, 420, 426, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1402, 23.1612, 426, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1403, 17.4291, 426, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1404, 259, 427, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1405, 5, 427, 1, 1, 87.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1406, 29.8543, 428, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1407, 22.0895, 428, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1408, 285, 428, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1409, 17.7489, 429, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1410, 27.082, 429, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1411, 3, 429, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1412, 485, 429, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1413, 14.6154, 430, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1414, 342, 430, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1415, 24.0696, 430, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1416, 4, 430, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1417, 427, 431, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1418, 20.5128, 431, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1419, 10.5121, 431, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1420, 16.6953, 432, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1421, 308, 432, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1422, 21.5048, 433, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1423, 2, 433, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1424, 29.0901, 433, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1425, 274, 433, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1426, 4, 434, 1, 1, 83.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1427, 28.9754, 434, 2, 1, 13.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1428, 266, 434, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1429, 26.5975, 435, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1430, 12.3197, 435, 0, 1, 6.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1431, 1, 436, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1432, 19.4461, 437, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1433, 300, 437, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1434, 21.0724, 437, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1435, 5, 437, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1436, 28.1641, 438, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1437, 275, 439, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1438, 3, 439, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1439, 13.0733, 439, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1440, 24.5631, 439, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1441, 4, 440, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1442, 14.7052, 440, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1443, 340, 440, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1444, 24.2979, 440, 2, 1, 9.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1445, 22.873, 441, 0, 1, 7.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1446, 26.1237, 441, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1447, 2, 441, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1448, 333, 441, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1449, 428, 442, 3, 1, 200.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1450, 24.2663, 442, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1451, 21.8004, 442, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1452, 4, 442, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1453, 21.2258, 443, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1454, 2, 443, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1455, 290, 443, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1456, 28.8753, 443, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1457, 3, 444, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1458, 19.4846, 444, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1459, 20.1574, 444, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1460, 278, 444, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1461, 14.9432, 445, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1462, 450, 445, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1463, 2, 445, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1464, 20.2977, 445, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1465, 13.3304, 446, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1466, 23.1744, 446, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1467, 5, 446, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1468, 331, 446, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1469, 4, 447, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1470, 2, 448, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1471, 10.5667, 448, 0, 1, 7.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1472, 25.6032, 448, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1473, 467, 448, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1474, 488, 449, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1475, 2, 449, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1476, 23.3269, 449, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1477, 19.903, 449, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1478, 1, 450, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1479, 322, 450, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1480, 21.8328, 450, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1481, 14.2543, 450, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1482, 29.8566, 451, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1483, 315, 451, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1484, 12.0603, 451, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1485, 5, 451, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1486, 27.2634, 452, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1487, 20.9559, 453, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1488, 253, 453, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1489, 1, 453, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1490, 29.3065, 453, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1491, 10.4324, 454, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1492, 449, 454, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1493, 22.5561, 454, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1494, 3, 454, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1495, 3, 455, 1, 1, 87.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1496, 269, 455, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1497, 14.9458, 455, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1498, 26.9536, 455, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1499, 22.1686, 456, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1500, 292, 456, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1501, 5, 457, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1502, 15.2236, 457, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1503, 29.0191, 457, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1504, 443, 457, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1505, 17.4796, 458, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1506, 23.5817, 458, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1507, 367, 458, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1508, 2, 458, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1509, 17.8555, 459, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1510, 396, 459, 3, 1, 200.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1511, 20.9189, 459, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1512, 4, 459, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1513, 333, 460, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1514, 25.8242, 460, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1515, 23.5923, 460, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1516, 4, 460, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1517, 27.0562, 461, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1518, 13.7134, 461, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1519, 4, 461, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1520, 423, 461, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1521, 28.6269, 462, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1522, 5, 462, 1, 1, 79.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1523, 14.3523, 462, 0, 1, 8.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1524, 325, 462, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1525, 5, 463, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1526, 429, 464, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1527, 3, 464, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1528, 15.9777, 464, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1529, 22.0013, 464, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1530, 26.7311, 465, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1531, 10.6089, 465, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1532, 500, 465, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1533, 1, 465, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1534, 23.9847, 466, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1535, 5, 467, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1536, 14.5878, 467, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1537, 270, 467, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1538, 28.3126, 467, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1539, 18.3738, 468, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1540, 2, 468, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1541, 23.0563, 469, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1542, 320, 469, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1543, 2, 469, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1544, 18.6238, 469, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1545, 348, 470, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1546, 4, 470, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1547, 26.5306, 470, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1548, 17.1034, 470, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1549, 5, 471, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1550, 28.0026, 471, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1551, 22.0707, 471, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1552, 335, 471, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1553, 21.1924, 472, 0, 1, 6.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1554, 1, 472, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1555, 336, 473, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1556, 12.274, 473, 0, 1, 7.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1557, 25.6179, 473, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1558, 2, 473, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1559, 22.8457, 474, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1560, 481, 474, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1561, 12.1695, 474, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1562, 1, 474, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1563, 3, 475, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1564, 26.2988, 475, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1565, 301, 475, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1566, 21.2856, 475, 0, 1, 6.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1567, 22.472, 476, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1568, 402, 476, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1569, 19.4747, 476, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1570, 5, 476, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1571, 29.1972, 477, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1572, 11.3783, 477, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1573, 4, 477, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1574, 358, 477, 3, 1, 160.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1575, 12.2616, 478, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1576, 334, 478, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1577, 20.6261, 478, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1578, 2, 478, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1579, 2, 479, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1580, 16.7325, 479, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1581, 466, 480, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1582, 21.4606, 480, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1583, 5, 480, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1584, 19.0042, 480, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1585, 26.3858, 481, 2, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1586, 15.7615, 481, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1587, 260, 482, 3, 1, 210.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1588, 28.3009, 482, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1589, 22.0142, 482, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1590, 5, 482, 1, 1, 71.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1591, 3, 483, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1592, 379, 483, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1593, 5, 484, 1, 1, 68.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1594, 15.117, 484, 0, 1, 7.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1595, 23.2151, 484, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1596, 497, 484, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1597, 13.7345, 485, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1598, 29.1728, 486, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1599, 282, 486, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1600, 5, 486, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1601, 269, 487, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1602, 2, 487, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1603, 11.0899, 487, 0, 1, 8.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1604, 27.5819, 487, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1605, 266, 488, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1606, 3, 488, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1607, 16.5999, 488, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1608, 22.4464, 488, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1609, 22.1316, 489, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1610, 13.1082, 489, 0, 1, 8.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1611, 304, 489, 3, 1, 192.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1612, 5, 489, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1613, 5, 490, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1614, 29.3471, 490, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1615, 11.9133, 490, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1616, 440, 490, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1617, 253, 491, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1618, 17.0649, 491, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1619, 20.4469, 491, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1620, 3, 491, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1621, 319, 492, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1622, 2, 492, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1623, 17.3545, 492, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1624, 20.9529, 492, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1625, 23.66, 493, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1626, 3, 493, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1627, 17.0607, 493, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1628, 357, 493, 3, 1, 175.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1629, 4, 494, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1630, 25.6253, 494, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1631, 322, 494, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1632, 10.3218, 494, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1633, 21.2126, 495, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1634, 4, 495, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1635, 23.2636, 495, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1636, 20.7813, 496, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1637, 12.1938, 496, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1638, 295, 496, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1639, 5, 496, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1640, 23.4641, 497, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1641, 1, 497, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1642, 479, 497, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1643, 24.7377, 497, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1644, 19.5079, 498, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1645, 284, 498, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1646, 24.3129, 498, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1647, 374, 499, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1648, 3, 499, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1649, 14.4232, 499, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1650, 23.2471, 499, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1651, 18.3146, 500, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1652, 26.4389, 500, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1653, 2, 501, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1654, 28.3578, 501, 2, 1, 12.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1655, 427, 501, 3, 1, 182.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1656, 414, 502, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1657, 2, 502, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1658, 27.9177, 502, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1659, 22.5448, 502, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1660, 18.4097, 503, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1661, 22.8684, 503, 2, 1, 9.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1662, 4, 503, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1663, 394, 503, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1664, 3, 504, 1, 1, 88.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1665, 315, 504, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1666, 19.1061, 504, 0, 1, 7.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1667, 28.5225, 504, 2, 1, 11.85)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1668, 457, 505, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1669, 17.8651, 505, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1670, 1, 505, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1671, 23.1824, 505, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1672, 13.1949, 506, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1673, 21.6839, 506, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1674, 3, 506, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1675, 423, 506, 3, 1, 217.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1676, 23.0696, 507, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1677, 300, 507, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1678, 22.5728, 507, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1679, 4, 507, 1, 1, 79.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1680, 16.1821, 508, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1681, 327, 508, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1682, 1, 508, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1683, 22.7716, 508, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1684, 417, 509, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1685, 18.6762, 509, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1686, 21.6608, 510, 2, 1, 9.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1687, 17.6859, 510, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1688, 4, 510, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1689, 438, 510, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1690, 3, 511, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1691, 391, 511, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1692, 24.547, 511, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1693, 12.7491, 511, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1694, 386, 512, 3, 1, 190.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1695, 5, 512, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1696, 23.7452, 512, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1697, 14.436, 512, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1698, 26.1547, 513, 2, 1, 12.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1699, 3, 513, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1700, 22.5066, 513, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1701, 303, 513, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1702, 352, 514, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1703, 16.5183, 514, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1704, 22.3384, 514, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1705, 5, 514, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1706, 26.3615, 515, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1707, 415, 515, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1708, 1, 515, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1709, 16.6794, 515, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1710, 12.7737, 516, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1711, 24.5221, 517, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1712, 3, 517, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1713, 15.6821, 517, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1714, 500, 517, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1715, 21.7575, 518, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1716, 18.0129, 518, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1717, 364, 518, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1718, 4, 518, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1719, 20.2317, 519, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1720, 5, 519, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1721, 26.1131, 520, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1722, 24.9046, 520, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1723, 437, 520, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1724, 2, 520, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1725, 345, 521, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1726, 27.8935, 522, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1727, 2, 522, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1728, 17.4207, 522, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1729, 443, 522, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1730, 19.2613, 523, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1731, 22.1131, 523, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1732, 417, 523, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1733, 2, 523, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1734, 302, 524, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1735, 10.1715, 524, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1736, 21.7921, 524, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1737, 2, 524, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1738, 472, 525, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1739, 4, 525, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1740, 23.9298, 525, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1741, 23.2534, 525, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1742, 429, 526, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1743, 21.309, 526, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1744, 4, 526, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1745, 16.9776, 526, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1746, 4, 527, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1747, 368, 527, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1748, 27.7191, 527, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1749, 24.1073, 527, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1750, 358, 528, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1751, 11.2326, 528, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1752, 29.0545, 528, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1753, 3, 528, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1754, 13.0649, 529, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1755, 304, 529, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1756, 22.0267, 529, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1757, 3, 529, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1758, 359, 530, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1759, 13.2705, 530, 0, 1, 7.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1760, 21.7647, 530, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1761, 5, 530, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1762, 330, 531, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1763, 4, 531, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1764, 16.3586, 531, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1765, 25.6353, 531, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1766, 263, 532, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1767, 21.5917, 532, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1768, 26.3259, 532, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1769, 4, 532, 1, 1, 77.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1770, 3, 533, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1771, 318, 534, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1772, 2, 534, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1773, 20.1094, 534, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1774, 22.1064, 534, 0, 1, 6.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1775, 4, 535, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1776, 1, 536, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1777, 21.5905, 536, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1778, 397, 536, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1779, 2, 537, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1780, 18.3376, 537, 0, 1, 7.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1781, 447, 537, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1782, 24.3046, 537, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1783, 23.0056, 538, 0, 1, 8.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1784, 27.1364, 538, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1785, 279, 538, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1786, 4, 538, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1787, 4, 539, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1788, 495, 539, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1789, 27.2286, 539, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1790, 21.7641, 539, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1791, 2, 540, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1792, 23.9187, 540, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1793, 402, 540, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1794, 20.3094, 540, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1795, 29.584, 541, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1796, 24.4125, 541, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1797, 431, 541, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1798, 2, 541, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1799, 467, 542, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1800, 29.9586, 542, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1801, 277, 543, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1802, 3, 544, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1803, 24.5084, 544, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1804, 12.5956, 544, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1805, 270, 544, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1806, 22.1529, 545, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1807, 19.598, 545, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1808, 365, 545, 3, 1, 157.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1809, 5, 545, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1810, 26.2913, 546, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1811, 21.844, 546, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1812, 2, 546, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1813, 293, 546, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1814, 14.1841, 547, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1815, 20.271, 547, 2, 1, 13.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1816, 431, 547, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1817, 2, 547, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1818, 10.1332, 548, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1819, 3, 548, 1, 1, 65.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1820, 27.2746, 548, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1821, 292, 548, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1822, 459, 549, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1823, 17.3525, 550, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1824, 297, 550, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1825, 29.0853, 550, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1826, 1, 550, 1, 1, 65.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1827, 25.5371, 551, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1828, 16.7827, 551, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1829, 4, 551, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1830, 23.7978, 552, 2, 1, 13.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1831, 14.3392, 552, 0, 1, 8.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1832, 358, 553, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1833, 21.6276, 553, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1834, 12.3309, 553, 0, 1, 8.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1835, 310, 554, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1836, 4, 554, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1837, 18.1458, 555, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1838, 1, 555, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1839, 28.0898, 555, 2, 1, 13.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1840, 284, 555, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1841, 365, 556, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1842, 22.642, 556, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1843, 14.1924, 556, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1844, 1, 556, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1845, 2, 557, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1846, 431, 557, 3, 1, 165.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1847, 26.1739, 557, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1848, 16.8671, 557, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1849, 28.638, 558, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1850, 5, 558, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1851, 338, 558, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1852, 14.3953, 558, 0, 1, 7.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1853, 27.6963, 559, 2, 1, 13.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1854, 324, 559, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1855, 4, 559, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1856, 16.1737, 559, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1857, 285, 560, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1858, 5, 560, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1859, 29.844, 560, 2, 1, 9.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1860, 15.9592, 560, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1861, 478, 561, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1862, 2, 561, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1863, 18.63, 561, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1864, 1, 562, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1865, 402, 562, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1866, 19.683, 562, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1867, 22.3808, 562, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1868, 319, 563, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1869, 29.3723, 564, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1870, 3, 564, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1871, 24.476, 564, 0, 1, 8.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1872, 383, 564, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1873, 22.6405, 565, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1874, 29.1176, 565, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1875, 3, 565, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1876, 453, 566, 3, 1, 157.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1877, 20.4955, 566, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1878, 25.5993, 567, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1879, 3, 567, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1880, 21.907, 567, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1881, 2, 568, 1, 1, 90.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1882, 24.2765, 568, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1883, 28.8947, 568, 2, 1, 12.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1884, 449, 568, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1885, 26.5438, 569, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1886, 23.2552, 569, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1887, 306, 569, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1888, 2, 570, 1, 1, 73.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1889, 29.6468, 570, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1890, 19.9726, 570, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1891, 271, 570, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1892, 27.8411, 571, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1893, 390, 571, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1894, 5, 571, 1, 1, 86.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1895, 10.6277, 571, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1896, 23.554, 572, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1897, 1, 572, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1898, 14.2691, 572, 0, 1, 7.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1899, 280, 572, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1900, 23.8088, 573, 0, 1, 7.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1901, 253, 574, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1902, 27.5144, 574, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1903, 4, 574, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1904, 24.2532, 574, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1905, 26.1679, 575, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1906, 11.2249, 575, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1907, 414, 576, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1908, 22.786, 576, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1909, 22.4429, 576, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1910, 3, 576, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1911, 25.157, 577, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1912, 1, 577, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1913, 456, 577, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1914, 10.1636, 577, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1915, 15.1831, 578, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1916, 312, 578, 3, 1, 225.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1917, 1, 578, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1918, 29.5872, 578, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1919, 447, 579, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1920, 3, 579, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1921, 11.0503, 579, 0, 1, 7.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1922, 14.8733, 580, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1923, 412, 580, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1924, 5, 580, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1925, 23.7824, 580, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1926, 29.5744, 581, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1927, 429, 581, 3, 1, 182.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1928, 15.4746, 581, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1929, 5, 582, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1930, 20.9689, 582, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1931, 435, 583, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1932, 2, 584, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1933, 21.9314, 584, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1934, 20.6287, 584, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1935, 323, 584, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1936, 1, 585, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1937, 369, 585, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1938, 19.0905, 585, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1939, 24.109, 585, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1940, 24.1689, 586, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1941, 2, 587, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1942, 265, 587, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1943, 23.6179, 587, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1944, 25.4918, 587, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1945, 14.5997, 588, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1946, 384, 588, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1947, 27.2632, 588, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1948, 5, 588, 1, 1, 73.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1949, 14.059, 589, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1950, 281, 589, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1951, 3, 589, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1952, 21.3197, 589, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1953, 22.1776, 590, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1954, 1, 590, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1955, 23.2964, 591, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1956, 459, 591, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1957, 16.0958, 591, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1958, 1, 591, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1959, 5, 592, 1, 1, 73.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1960, 475, 592, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1961, 20.5214, 592, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1962, 12.4358, 592, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1963, 29.3071, 593, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1964, 18.1789, 593, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1965, 4, 593, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1966, 349, 593, 3, 1, 187.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1967, 23.0243, 594, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1968, 13.3649, 594, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1969, 313, 594, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1970, 28.7386, 595, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1971, 16.0977, 595, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1972, 439, 596, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1973, 2, 596, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1974, 23.084, 597, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1975, 347, 597, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1976, 15.431, 597, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1977, 1, 597, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1978, 28.1001, 598, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1979, 11.1707, 598, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1980, 2, 598, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1981, 417, 598, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1982, 307, 599, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1983, 1, 599, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1984, 2, 600, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1985, 447, 600, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1986, 19.8875, 600, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1987, 23.6846, 600, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1988, 16.9264, 601, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1989, 5, 601, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1990, 3, 602, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1991, 23.5356, 602, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1992, 28.1396, 602, 2, 1, 10.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1993, 10.9664, 603, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1994, 335, 603, 3, 1, 167.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1995, 28.5787, 603, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1996, 27.4992, 604, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1997, 19.3404, 604, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1998, 4, 604, 1, 1, 73.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1999, 3, 605, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2000, 27.7269, 605, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2001, 20.5176, 606, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2002, 28.3313, 606, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2003, 265, 606, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2004, 3, 606, 1, 1, 60.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2005, 260, 607, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2006, 24.1903, 607, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2007, 5, 607, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2008, 25.5909, 607, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2009, 24.1188, 608, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2010, 21.8886, 608, 0, 1, 6.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2011, 400, 608, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2012, 2, 608, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2013, 459, 609, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2014, 27.8996, 610, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2015, 22.7833, 610, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2016, 271, 610, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2017, 2, 610, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2018, 278, 611, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2019, 3, 611, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2020, 16.1825, 611, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2021, 27.6809, 611, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2022, 300, 612, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2023, 24.196, 612, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2024, 22.79, 612, 2, 1, 13.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2025, 3, 612, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2026, 1, 613, 1, 1, 72.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2027, 29.047, 613, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2028, 13.8029, 613, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2029, 255, 613, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2030, 328, 614, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2031, 26.0576, 614, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2032, 4, 615, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2033, 28.911, 615, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2034, 11.1813, 615, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2035, 286, 615, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2036, 280, 616, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2037, 23.165, 616, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2038, 22.4491, 616, 0, 1, 8.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2039, 1, 616, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2040, 5, 617, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2041, 27.952, 617, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2042, 467, 617, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2043, 23.3243, 617, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2044, 22.1312, 618, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2045, 22.1882, 619, 2, 1, 13.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2046, 21.0472, 619, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2047, 2, 619, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2048, 335, 619, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2049, 24.0848, 620, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2050, 18.8487, 620, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2051, 251, 620, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2052, 2, 620, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2053, 10.8174, 621, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2054, 2, 621, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2055, 302, 621, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2056, 23.0789, 621, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2057, 4, 622, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2058, 21.0502, 622, 2, 1, 13.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2059, 474, 622, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2060, 13.4643, 622, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2061, 5, 623, 1, 1, 83.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2062, 21.8048, 623, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2063, 409, 623, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2064, 13.0452, 623, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2065, 3, 624, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2066, 292, 624, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2067, 25.8567, 624, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2068, 17.6916, 624, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2069, 260, 625, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2070, 2, 625, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2071, 23.9151, 625, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2072, 25.6899, 625, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2073, 5, 626, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2074, 411, 626, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2075, 15.4064, 626, 0, 1, 7.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2076, 24.835, 626, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2077, 261, 627, 3, 1, 165.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2078, 439, 628, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2079, 23.0289, 628, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2080, 23.9257, 628, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2081, 3, 628, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2082, 13.175, 629, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2083, 397, 629, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2084, 4, 629, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2085, 21.3078, 629, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2086, 286, 630, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2087, 20.0175, 630, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2088, 3, 630, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2089, 21.6728, 630, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2090, 29.598, 631, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2091, 2, 631, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2092, 476, 631, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2093, 14.7251, 631, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2094, 5, 632, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2095, 22.5445, 632, 2, 1, 13.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2096, 484, 632, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2097, 15.3165, 632, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2098, 354, 633, 3, 1, 165.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2099, 24.1974, 634, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2100, 21.8345, 634, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2101, 361, 634, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2102, 4, 634, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2103, 16.8407, 635, 0, 1, 7.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2104, 22.2757, 635, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2105, 467, 635, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2106, 1, 635, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2107, 301, 636, 3, 1, 182.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2108, 20.3307, 636, 2, 1, 10.65)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2109, 1, 636, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2110, 13.5731, 636, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2111, 466, 637, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2112, 12.1983, 637, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2113, 23.4837, 637, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2114, 5, 637, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2115, 4, 638, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2116, 18.5235, 638, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2117, 23.1834, 638, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2118, 357, 638, 3, 1, 175.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2119, 3, 639, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2120, 14.1359, 639, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2121, 28.7011, 639, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2122, 393, 639, 3, 1, 152.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2123, 444, 640, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2124, 18.4429, 640, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2125, 4, 640, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2126, 447, 641, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2127, 14.1373, 642, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2128, 29.4456, 642, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2129, 337, 642, 3, 1, 175.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2130, 3, 642, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2131, 329, 643, 3, 1, 155.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2132, 27.5307, 643, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2133, 11.4267, 643, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2134, 5, 643, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2135, 1, 644, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2136, 498, 644, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2137, 21.9163, 644, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2138, 15.5277, 644, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2139, 26.1414, 645, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2140, 4, 645, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2141, 20.403, 645, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2142, 283, 645, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2143, 12.6598, 646, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2144, 404, 646, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2145, 24.7756, 646, 2, 1, 12.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2146, 5, 646, 1, 1, 85.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2147, 24.3919, 647, 2, 1, 10.65)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2148, 14.7707, 647, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2149, 2, 647, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2150, 382, 647, 3, 1, 175.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2151, 17.9794, 648, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2152, 24.4962, 648, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2153, 3, 649, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2154, 16.1616, 649, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2155, 422, 649, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2156, 29.2461, 649, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2157, 20.4206, 650, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2158, 3, 650, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2159, 433, 650, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2160, 15.4579, 650, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2161, 14.6081, 651, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2162, 494, 651, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2163, 416, 652, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2164, 29.6424, 652, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2165, 18.031, 652, 0, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2166, 5, 652, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2167, 26.0344, 653, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2168, 11.3847, 653, 0, 1, 6.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2169, 5, 654, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2170, 24.9662, 654, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2171, 350, 654, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2172, 4, 655, 1, 1, 61.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2173, 452, 655, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2174, 17.232, 655, 0, 1, 6.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2175, 22.6344, 655, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2176, 22.1223, 656, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2177, 23.6449, 656, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2178, 315, 656, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2179, 1, 656, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2180, 26.7519, 657, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2181, 21.3203, 657, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2182, 3, 657, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2183, 383, 657, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2184, 16.0138, 658, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2185, 3, 658, 1, 1, 68.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2186, 322, 658, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2187, 23.4962, 658, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2188, 26.8597, 659, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2189, 11.1689, 659, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2190, 394, 659, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2191, 1, 659, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2192, 308, 660, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2193, 353, 661, 3, 1, 160.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2194, 3, 661, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2195, 10.226, 661, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2196, 25.0294, 661, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2197, 380, 662, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2198, 5, 662, 1, 1, 76.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2199, 18.639, 662, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2200, 23.7685, 662, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2201, 441, 663, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2202, 27.6059, 663, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2203, 5, 663, 1, 1, 67.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2204, 22.692, 663, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2205, 12.4695, 664, 0, 1, 7.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2206, 3, 664, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2207, 285, 664, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2208, 27.8192, 664, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2209, 1, 665, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2210, 22.4224, 665, 0, 1, 7.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2211, 27.9754, 665, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2212, 459, 665, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2213, 2, 666, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2214, 13.8073, 666, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2215, 425, 666, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2216, 20.0071, 666, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2217, 28.5781, 667, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2218, 5, 667, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2219, 19.296, 667, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2220, 266, 667, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2221, 18.1783, 668, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2222, 322, 668, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2223, 24.2866, 668, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2224, 295, 669, 3, 1, 157.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2225, 29.9003, 669, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2226, 15.9963, 669, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2227, 2, 669, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2228, 10.7246, 670, 0, 1, 8.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2229, 20.0936, 670, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2230, 332, 670, 3, 1, 162.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2231, 4, 670, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2232, 488, 671, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2233, 5, 671, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2234, 13.5502, 671, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2235, 22.5755, 672, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2236, 24.9206, 672, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2237, 372, 672, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2238, 4, 672, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2239, 273, 673, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2240, 29.3393, 673, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2241, 19.8219, 673, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2242, 2, 673, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2243, 254, 674, 3, 1, 170.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2244, 22.5447, 674, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2245, 5, 674, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2246, 24.0947, 674, 0, 1, 7.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2247, 278, 675, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2248, 20.4473, 675, 2, 1, 13.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2249, 11.3389, 675, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2250, 2, 675, 1, 1, 71.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2251, 444, 676, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2252, 10.9736, 676, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2253, 25.2982, 676, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2254, 4, 676, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2255, 335, 677, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2256, 21.7023, 677, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2257, 3, 677, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2258, 26.4471, 678, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2259, 22.5686, 678, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2260, 431, 678, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2261, 1, 678, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2262, 22.5473, 679, 0, 1, 7.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2263, 342, 679, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2264, 4, 679, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2265, 24.6076, 679, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2266, 23.3959, 680, 2, 1, 13.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2267, 5, 680, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2268, 22.5451, 680, 0, 1, 8.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2269, 312, 680, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2270, 2, 681, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2271, 21.5489, 682, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2272, 12.2199, 682, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2273, 22.0406, 683, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2274, 21.9473, 684, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2275, 432, 684, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2276, 2, 684, 1, 1, 87.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2277, 20.0312, 684, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2278, 1, 685, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2279, 372, 685, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2280, 23.9702, 685, 2, 1, 10.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2281, 23.7576, 685, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2282, 20.1094, 686, 0, 1, 8.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2283, 484, 686, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2284, 27.6532, 686, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2285, 5, 686, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2286, 15.8489, 687, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2287, 250, 687, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2288, 4, 687, 1, 1, 74.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2289, 26.0598, 687, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2290, 384, 688, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2291, 3, 688, 1, 1, 69.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2292, 18.0131, 688, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2293, 25.5274, 688, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2294, 333, 689, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2295, 28.0539, 689, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2296, 5, 689, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2297, 24.8203, 689, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2298, 29.8557, 690, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2299, 10.2454, 690, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2300, 413, 690, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2301, 2, 690, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2302, 283, 691, 3, 1, 182.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2303, 5, 691, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2304, 16.3528, 692, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2305, 491, 692, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2306, 23.1463, 693, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2307, 24.9029, 694, 2, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2308, 12.9451, 694, 0, 1, 7.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2309, 377, 694, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2310, 2, 694, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2311, 2, 695, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2312, 29.98, 695, 2, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2313, 465, 695, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2314, 23.2905, 695, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2315, 15.1088, 696, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2316, 26.1497, 696, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2317, 1, 696, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2318, 353, 696, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2319, 28.846, 697, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2320, 5, 697, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2321, 428, 697, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2322, 10.0506, 697, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2323, 16.3093, 698, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2324, 299, 698, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2325, 5, 698, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2326, 27.4738, 698, 2, 1, 12.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2327, 24.9753, 699, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2328, 436, 699, 3, 1, 155.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2329, 23.8245, 699, 0, 1, 8.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2330, 2, 699, 1, 1, 65.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2331, 343, 700, 3, 1, 167.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2332, 2, 700, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2333, 21.3281, 700, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2334, 12.8749, 701, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2335, 20.2836, 701, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2336, 3, 702, 1, 1, 76.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2337, 17.868, 702, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2338, 480, 702, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2339, 26.922, 702, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2340, 14.0559, 703, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2341, 25.3244, 703, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2342, 20.6087, 704, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2343, 409, 704, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2344, 29.4917, 704, 2, 1, 11.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2345, 1, 704, 1, 1, 86.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2346, 272, 705, 3, 1, 217.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2347, 26.2182, 705, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2348, 12.2144, 705, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2349, 2, 706, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2350, 431, 706, 3, 1, 222.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2351, 14.8614, 707, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2352, 2, 708, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2353, 29.8522, 708, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2354, 19.1425, 708, 0, 1, 8.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2355, 472, 708, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2356, 354, 709, 3, 1, 160.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2357, 27.6567, 709, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2358, 5, 709, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2359, 14.9138, 709, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2360, 346, 710, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2361, 28.1065, 710, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2362, 1, 710, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2363, 12.3527, 710, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2364, 261, 711, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2365, 2, 711, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2366, 21.8509, 711, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2367, 20.9801, 711, 2, 1, 12.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2368, 432, 712, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2369, 15.0833, 712, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2370, 5, 712, 1, 1, 64.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2371, 20.4869, 712, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2372, 10.8627, 713, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2373, 335, 713, 3, 1, 192.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2374, 5, 713, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2375, 29.8987, 713, 2, 1, 10.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2376, 2, 714, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2377, 24.6232, 714, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2378, 348, 714, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2379, 342, 715, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2380, 29.9696, 715, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2381, 1, 715, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2382, 353, 716, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2383, 23.3458, 716, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2384, 2, 716, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2385, 13.9709, 716, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2386, 16.174, 717, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2387, 2, 717, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2388, 25.5804, 717, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2389, 397, 717, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2390, 372, 718, 3, 1, 160.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2391, 28.1173, 718, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2392, 4, 718, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2393, 11.141, 718, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2394, 29.1361, 719, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2395, 348, 719, 3, 1, 182.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2396, 4, 719, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2397, 12.7024, 719, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2398, 29.1444, 720, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2399, 20.908, 721, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2400, 10.237, 721, 0, 1, 7.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2401, 2, 722, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2402, 414, 722, 3, 1, 175.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2403, 13.9539, 722, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2404, 24.3523, 722, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2405, 24.4295, 723, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2406, 19.7521, 724, 0, 1, 8.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2407, 20.4216, 724, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2408, 24.4243, 725, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2409, 357, 725, 3, 1, 162.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2410, 24.3054, 725, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2411, 3, 725, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2412, 2, 726, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2413, 450, 726, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2414, 21.7841, 726, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2415, 17.0975, 726, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2416, 23.7245, 727, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2417, 2, 727, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2418, 412, 727, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2419, 29.5777, 727, 2, 1, 12.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2420, 24.7546, 728, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2421, 21.2259, 728, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2422, 4, 728, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2423, 455, 728, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2424, 28.6628, 729, 2, 1, 9.45)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2425, 23.2181, 729, 0, 1, 8.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2426, 435, 730, 3, 1, 207.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2427, 28.9132, 730, 2, 1, 12.75)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2428, 3, 730, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2429, 20.4989, 730, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2430, 13.0046, 731, 0, 1, 6.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2431, 29.5124, 731, 2, 1, 12.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2432, 357, 731, 3, 1, 172.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2433, 5, 731, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2434, 5, 732, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2435, 21.1057, 733, 2, 1, 9.90)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2436, 419, 733, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2437, 4, 733, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2438, 17.9575, 733, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2439, 15.9246, 734, 0, 1, 7.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2440, 325, 734, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2441, 25.7837, 734, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2442, 5, 734, 1, 1, 82.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2443, 377, 735, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2444, 10.5975, 735, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2445, 3, 735, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2446, 25.9456, 735, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2447, 2, 736, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2448, 29.3458, 736, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2449, 262, 736, 3, 1, 197.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2450, 10.335, 736, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2451, 2, 737, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2452, 22.8241, 737, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2453, 316, 737, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2454, 21.7577, 737, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2455, 26.779, 738, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2456, 1, 738, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2457, 424, 738, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2458, 16.6151, 738, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2459, 28.6045, 739, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2460, 19.2078, 739, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2461, 1, 740, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2462, 27.1783, 740, 2, 1, 11.85)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2463, 359, 740, 3, 1, 157.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2464, 14.038, 740, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2465, 26.2909, 741, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2466, 485, 741, 3, 1, 190.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2467, 1, 741, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2468, 14.2078, 741, 0, 1, 7.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2469, 13.0923, 742, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2470, 23.8385, 742, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2471, 4, 742, 1, 0, NULL)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (0, 0, NULL, 1, 2, 14)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (1, 1, 4, 0, NULL, 15)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (2, 0, NULL, 1, 1, 41)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (3, 0, NULL, 1, 1, 43)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (4, 0, NULL, 1, 2, 50)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (5, 1, 4, 0, NULL, 53)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (6, 0, NULL, 1, 2, 54)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (7, 0, NULL, 1, 1, 70)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (8, 0, NULL, 1, 1, 79)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (9, 1, 4, 0, NULL, 83)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (10, 1, 1, 0, NULL, 86)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (11, 0, NULL, 1, 2, 97)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (12, 0, NULL, 1, 2, 109)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (13, 0, NULL, 1, 1, 144)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (14, 1, 3, 0, NULL, 147)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (15, 0, NULL, 1, 1, 160)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (16, 0, NULL, 1, 2, 172)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (17, 0, NULL, 1, 1, 183)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (18, 1, 7, 0, NULL, 185)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (19, 0, NULL, 1, 2, 194)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (20, 0, NULL, 1, 2, 195)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (21, 0, NULL, 1, 2, 209)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (22, 0, NULL, 1, 1, 217)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (23, 1, 10, 1, 2, 223)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (24, 1, 1, 0, NULL, 225)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (25, 1, 1, 0, NULL, 229)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (26, 1, 8, 0, NULL, 237)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (27, 0, NULL, 1, 2, 246)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (28, 0, NULL, 1, 2, 248)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (29, 0, NULL, 1, 1, 255)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (30, 1, 9, 0, NULL, 261)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (31, 0, NULL, 1, 1, 271)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (32, 1, 1, 0, NULL, 274)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (33, 0, NULL, 1, 1, 278)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (34, 1, 1, 0, NULL, 311)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (35, 0, NULL, 1, 2, 316)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (36, 1, 4, 0, NULL, 320)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (37, 1, 7, 0, NULL, 345)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (38, 1, 3, 0, NULL, 346)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (39, 0, NULL, 1, 1, 351)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (40, 0, NULL, 1, 2, 354)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (41, 1, 3, 0, NULL, 371)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (42, 0, NULL, 1, 1, 384)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (43, 0, NULL, 1, 2, 405)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (44, 1, 7, 0, NULL, 415)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (45, 0, NULL, 1, 1, 421)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (46, 0, NULL, 1, 2, 424)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (47, 1, 6, 0, NULL, 426)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (48, 1, 2, 0, NULL, 429)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (49, 1, 6, 0, NULL, 433)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (50, 0, NULL, 1, 2, 444)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (51, 1, 4, 0, NULL, 447)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (52, 1, 8, 0, NULL, 488)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (53, 0, NULL, 1, 2, 495)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (54, 0, NULL, 1, 1, 504)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (55, 0, NULL, 1, 2, 515)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (56, 0, NULL, 1, 2, 525)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (57, 0, NULL, 1, 1, 526)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (58, 1, 4, 0, NULL, 548)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (59, 1, 6, 1, 1, 560)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (60, 1, 10, 0, NULL, 570)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (61, 1, 3, 0, NULL, 572)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (62, 0, NULL, 1, 1, 585)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (63, 1, 8, 1, 1, 599)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (64, 1, 2, 0, NULL, 615)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (65, 0, NULL, 1, 2, 621)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (66, 0, NULL, 1, 1, 640)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (67, 1, 4, 1, 1, 641)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (68, 1, 2, 1, 1, 650)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (69, 1, 6, 0, NULL, 662)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (70, 1, 7, 0, NULL, 667)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (71, 0, NULL, 1, 2, 693)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (72, 0, NULL, 1, 2, 701)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (73, 0, NULL, 1, 2, 713)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (74, 0, NULL, 1, 2, 723)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (75, 1, 1, 0, NULL, 760)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (76, 1, 5, 0, NULL, 770)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (77, 0, NULL, 1, 1, 771)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (78, 1, 8, 0, NULL, 796)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (79, 0, NULL, 1, 1, 805)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (80, 0, NULL, 1, 1, 807)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (81, 0, NULL, 1, 2, 813)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (82, 0, NULL, 1, 2, 823)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (83, 0, NULL, 1, 2, 835)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (84, 1, 1, 0, NULL, 841)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (85, 1, 10, 1, 1, 849)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (86, 0, NULL, 1, 1, 850)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (87, 0, NULL, 1, 1, 856)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (88, 1, 2, 0, NULL, 872)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (89, 0, NULL, 1, 2, 894)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (90, 1, 2, 0, NULL, 906)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (91, 0, NULL, 1, 2, 941)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (92, 1, 2, 0, NULL, 974)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (93, 0, NULL, 1, 1, 994)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (94, 0, NULL, 1, 1, 1000)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (95, 0, NULL, 1, 2, 1003)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (96, 1, 2, 0, NULL, 1024)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (97, 1, 2, 0, NULL, 1041)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (98, 0, NULL, 1, 1, 1043)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (99, 0, NULL, 1, 1, 1047)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (100, 1, 3, 0, NULL, 1058)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (101, 1, 10, 0, NULL, 1082)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (102, 0, NULL, 1, 2, 1100)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (103, 0, NULL, 1, 2, 1113)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (104, 1, 1, 0, NULL, 1115)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (105, 1, 10, 0, NULL, 1118)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (106, 1, 9, 0, NULL, 1121)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (107, 0, NULL, 1, 1, 1122)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (108, 1, 4, 0, NULL, 1123)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (109, 1, 2, 0, NULL, 1130)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (110, 1, 8, 0, NULL, 1133)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (111, 1, 8, 0, NULL, 1134)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (112, 1, 7, 0, NULL, 1139)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (113, 0, NULL, 1, 2, 1143)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (114, 1, 5, 0, NULL, 1178)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (115, 0, NULL, 1, 1, 1196)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (116, 1, 6, 0, NULL, 1214)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (117, 1, 1, 0, NULL, 1219)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (118, 0, NULL, 1, 2, 1221)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (119, 1, 7, 0, NULL, 1222)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (120, 0, NULL, 1, 1, 1227)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (121, 0, NULL, 1, 1, 1236)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (122, 0, NULL, 1, 2, 1255)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (123, 1, 8, 0, NULL, 1260)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (124, 0, NULL, 1, 2, 1262)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (125, 1, 4, 0, NULL, 1265)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (126, 0, NULL, 1, 1, 1285)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (127, 0, NULL, 1, 2, 1292)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (128, 1, 4, 0, NULL, 1297)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (129, 0, NULL, 1, 2, 1304)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (130, 0, NULL, 1, 1, 1315)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (131, 1, 10, 0, NULL, 1317)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (132, 1, 3, 0, NULL, 1321)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (133, 1, 3, 0, NULL, 1323)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (134, 1, 9, 0, NULL, 1324)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (135, 0, NULL, 1, 2, 1340)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (136, 0, NULL, 1, 2, 1342)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (137, 0, NULL, 1, 1, 1352)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (138, 0, NULL, 1, 2, 1360)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (139, 0, NULL, 1, 2, 1382)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (140, 1, 2, 0, NULL, 1383)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (141, 1, 7, 0, NULL, 1384)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (142, 1, 5, 0, NULL, 1399)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (143, 1, 5, 0, NULL, 1403)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (144, 0, NULL, 1, 2, 1405)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (145, 1, 9, 0, NULL, 1415)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (146, 1, 3, 0, NULL, 1418)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (147, 1, 8, 0, NULL, 1437)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (148, 0, NULL, 1, 1, 1442)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (149, 1, 10, 1, 1, 1444)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (150, 1, 2, 0, NULL, 1454)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (151, 0, NULL, 1, 2, 1472)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (152, 1, 9, 0, NULL, 1487)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (153, 0, NULL, 1, 1, 1496)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (154, 1, 4, 0, NULL, 1511)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (155, 1, 1, 0, NULL, 1539)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (156, 1, 8, 0, NULL, 1544)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (157, 1, 5, 1, 2, 1551)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (158, 0, NULL, 1, 1, 1562)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (159, 0, NULL, 1, 2, 1574)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (160, 1, 3, 0, NULL, 1591)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (161, 0, NULL, 1, 1, 1630)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (162, 0, NULL, 1, 2, 1634)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (163, 0, NULL, 1, 2, 1636)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (164, 0, NULL, 1, 1, 1637)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (165, 1, 6, 0, NULL, 1645)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (166, 0, NULL, 1, 2, 1655)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (167, 1, 5, 1, 2, 1659)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (168, 1, 7, 1, 2, 1671)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (169, 1, 10, 0, NULL, 1673)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (170, 1, 6, 0, NULL, 1677)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (171, 1, 5, 0, NULL, 1709)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (172, 0, NULL, 1, 2, 1716)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (173, 0, NULL, 1, 2, 1717)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (174, 0, NULL, 1, 1, 1721)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (175, 1, 3, 1, 2, 1726)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (176, 1, 2, 0, NULL, 1733)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (177, 1, 1, 1, 2, 1737)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (178, 0, NULL, 1, 2, 1749)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (179, 0, NULL, 1, 2, 1753)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (180, 1, 4, 0, NULL, 1763)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (181, 0, NULL, 1, 1, 1767)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (182, 0, NULL, 1, 1, 1778)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (183, 1, 1, 0, NULL, 1779)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (184, 0, NULL, 1, 1, 1788)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (185, 1, 10, 0, NULL, 1794)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (186, 0, NULL, 1, 2, 1803)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (187, 0, NULL, 1, 1, 1818)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (188, 0, NULL, 1, 2, 1819)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (189, 1, 6, 0, NULL, 1825)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (190, 0, NULL, 1, 2, 1830)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (191, 0, NULL, 1, 2, 1842)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (192, 1, 4, 0, NULL, 1847)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (193, 1, 9, 0, NULL, 1854)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (194, 1, 1, 0, NULL, 1864)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (195, 1, 1, 0, NULL, 1880)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (196, 1, 1, 0, NULL, 1885)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (197, 0, NULL, 1, 1, 1898)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (198, 1, 10, 0, NULL, 1901)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (199, 1, 4, 0, NULL, 1905)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (200, 0, NULL, 1, 2, 1913)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (201, 1, 1, 0, NULL, 1915)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (202, 1, 7, 0, NULL, 1927)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (203, 1, 5, 0, NULL, 1929)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (204, 1, 10, 0, NULL, 1934)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (205, 0, NULL, 1, 1, 1943)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (206, 1, 6, 0, NULL, 1956)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (207, 1, 7, 0, NULL, 1957)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (208, 0, NULL, 1, 2, 2000)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (209, 1, 2, 0, NULL, 2003)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (210, 0, NULL, 1, 1, 2022)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (211, 0, NULL, 1, 2, 2037)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (212, 1, 3, 0, NULL, 2053)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (213, 1, 6, 0, NULL, 2060)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (214, 0, NULL, 1, 1, 2061)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (215, 1, 3, 1, 1, 2082)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (216, 1, 2, 0, NULL, 2083)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (217, 1, 1, 0, NULL, 2102)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (218, 1, 4, 0, NULL, 2115)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (219, 0, NULL, 1, 2, 2116)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (220, 0, NULL, 1, 1, 2138)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (221, 0, NULL, 1, 1, 2147)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (222, 1, 3, 1, 2, 2182)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (223, 0, NULL, 1, 2, 2195)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (224, 1, 7, 0, NULL, 2204)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (225, 0, NULL, 1, 2, 2211)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (226, 1, 9, 0, NULL, 2235)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (227, 0, NULL, 1, 2, 2255)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (228, 1, 1, 1, 1, 2278)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (229, 1, 5, 0, NULL, 2299)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (230, 0, NULL, 1, 2, 2309)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (231, 0, NULL, 1, 1, 2332)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (232, 0, NULL, 1, 2, 2358)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (233, 0, NULL, 1, 1, 2363)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (234, 1, 3, 0, NULL, 2393)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (235, 1, 3, 0, NULL, 2396)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (236, 0, NULL, 1, 1, 2399)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (237, 1, 8, 1, 2, 2406)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (238, 0, NULL, 1, 1, 2412)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (239, 1, 5, 0, NULL, 2423)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (240, 0, NULL, 1, 2, 2442)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (241, 1, 4, 0, NULL, 2458)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (242, 1, 1, 0, NULL, 2461)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (0, 2, 0, NULL, "2001-00-10", 0, NULL, 0)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1, 24.0707, 1, "2002-01-15", "2001-01-15", 1, NULL, 10)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (2, 2, 0, NULL, "2001-01-15", 0, NULL, 11)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (3, 308, 0, NULL, "2001-01-15", 0, NULL, 12)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (4, 4, 0, NULL, "2001-01-03", 0, NULL, 13)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (5, 22.3115, 0, NULL, "2001-02-06", 0, NULL, 14)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (6, 1, 0, NULL, "2001-02-06", 0, NULL, 15)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (7, 12.6099, 1, "2002-02-06", "2001-02-06", 1, NULL, 16)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (8, 311, 0, NULL, "2001-02-06", 0, NULL, 17)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (9, 326, 0, NULL, "2001-02-10", 0, NULL, 18)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (10, 23.4346, 1, "2002-02-10", "2001-02-10", 1, NULL, 19)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (11, 24.5892, 0, NULL, "2001-02-10", 0, NULL, 20)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (12, 5, 0, NULL, "2001-02-10", 0, NULL, 21)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (13, 26.008, 0, NULL, "2001-03-21", 0, NULL, 22)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (14, 18.4154, 1, "2002-03-21", "2001-03-21", 1, NULL, 23)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (15, 1, 0, NULL, "2001-03-21", 0, NULL, 24)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (16, 474, 0, NULL, "2001-03-21", 0, NULL, 25)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (17, 5, 0, NULL, "2001-03-13", 0, NULL, 26)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (18, 326, 0, NULL, "2001-03-13", 0, NULL, 27)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (19, 17.9227, 1, "2002-04-19", "2001-04-19", 1, NULL, 41)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (20, 377, 0, NULL, "2001-04-19", 0, NULL, 42)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (21, 2, 0, NULL, "2001-04-19", 0, NULL, 43)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (22, 29.7003, 0, NULL, "2001-04-19", 0, NULL, 44)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (23, 485, 0, NULL, "2001-05-27", 0, NULL, 53)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (24, 19.062, 1, "2002-05-27", "2001-05-27", 1, NULL, 54)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (25, 29.2346, 0, NULL, "2001-05-27", 0, NULL, 55)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (26, 3, 0, NULL, "2001-05-27", 0, NULL, 56)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (27, 21.0362, 0, NULL, "2001-05-30", 0, NULL, 57)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (28, 327, 0, NULL, "2001-05-30", 0, NULL, 58)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (29, 3, 0, NULL, "2001-05-30", 0, NULL, 59)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (30, 17.1803, 1, "2002-05-30", "2001-05-30", 1, NULL, 60)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (31, 28.0082, 0, NULL, "2001-05-30", 0, NULL, 64)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (32, 257, 0, NULL, "2001-05-30", 0, NULL, 65)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (33, 4, 0, NULL, "2001-05-30", 0, NULL, 66)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (34, 20.4485, 1, "2002-05-30", "2001-05-30", 1, NULL, 67)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (35, 22.9766, 0, NULL, "2001-06-16", 0, NULL, 70)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (36, 382, 0, NULL, "2001-06-16", 0, NULL, 71)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (37, 19.4929, 1, "2002-06-16", "2001-06-16", 1, NULL, 72)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (38, 5, 0, NULL, "2001-06-16", 0, NULL, 73)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (39, 493, 0, NULL, "2001-07-30", 0, NULL, 82)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (40, 334, 0, NULL, "2001-07-29", 0, NULL, 83)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (41, 22.1172, 0, NULL, "2001-07-29", 0, NULL, 84)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (42, 3, 0, NULL, "2001-07-29", 0, NULL, 85)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (43, 9.828, 1, "2002-07-29", "2001-07-29", 1, NULL, 86)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (44, 364, 0, NULL, "2001-07-20", 0, NULL, 87)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (45, 1, 0, NULL, "2001-08-15", 0, NULL, 88)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (46, 21.0536, 0, NULL, "2001-08-25", 0, NULL, 89)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (47, 12.2812, 1, "2002-08-25", "2001-08-25", 1, NULL, 90)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (48, 5, 0, NULL, "2001-08-25", 0, NULL, 91)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (49, 474, 0, NULL, "2001-08-25", 0, NULL, 92)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (50, 3, 0, NULL, "2001-08-07", 0, NULL, 95)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (51, 21.7796, 0, NULL, "2001-08-07", 0, NULL, 96)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (52, 22.9205, 1, "2002-08-07", "2001-08-07", 1, NULL, 97)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (53, 4, 0, NULL, "2001-09-14", 0, NULL, 98)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (54, 348, 0, NULL, "2001-09-14", 0, NULL, 99)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (55, 21.1455, 1, "2002-09-14", "2001-09-14", 1, NULL, 100)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (56, 28.9219, 0, NULL, "2001-09-14", 0, NULL, 101)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (57, 3, 0, NULL, "2001-09-20", 0, NULL, 106)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (58, 476, 0, NULL, "2001-09-20", 0, NULL, 107)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (59, 28.8978, 0, NULL, "2001-09-20", 0, NULL, 108)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (60, 13.7402, 1, "2002-09-20", "2001-09-20", 1, NULL, 109)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (61, 10.4032, 1, "2002-10-28", "2001-10-28", 1, NULL, 110)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (62, 252, 0, NULL, "2001-10-30", 0, NULL, 111)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (63, 14.429, 1, "2002-10-30", "2001-10-30", 1, NULL, 112)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (64, 29.1439, 0, NULL, "2001-10-30", 0, NULL, 113)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (65, 1, 0, NULL, "2001-10-30", 0, NULL, 114)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (66, 22.031, 1, "2002-10-27", "2001-10-27", 1, NULL, 122)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (67, 5, 0, NULL, "2001-10-27", 0, NULL, 123)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (68, 413, 0, NULL, "2001-10-27", 0, NULL, 124)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (69, 28.1728, 0, NULL, "2001-10-27", 0, NULL, 125)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (70, 14.5071, 1, "2002-10-02", "2001-10-02", 1, NULL, 126)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (71, 1, 0, NULL, "2001-10-02", 0, NULL, 127)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (72, 401, 0, NULL, "2001-10-02", 0, NULL, 128)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (73, 29.1413, 0, NULL, "2001-10-02", 0, NULL, 129)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (74, 1, 0, NULL, "2001-11-19", 0, NULL, 130)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (75, 497, 0, NULL, "2001-11-19", 0, NULL, 131)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (76, 20.9026, 0, NULL, "2001-11-19", 0, NULL, 132)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (77, 19.0612, 1, "2002-11-19", "2001-11-19", 1, NULL, 133)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (78, 18.8888, 1, "2002-11-23", "2001-11-23", 1, NULL, 135)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (79, 27.663, 0, NULL, "2001-11-23", 0, NULL, 136)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (80, 3, 0, NULL, "2001-11-23", 0, NULL, 137)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (81, 271, 0, NULL, "2001-11-23", 0, NULL, 138)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (82, 26.9991, 0, NULL, "2001-11-01", 0, NULL, 139)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (83, 19.422, 1, "2002-11-01", "2001-11-01", 1, NULL, 140)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (84, 290, 0, NULL, "2001-11-01", 0, NULL, 141)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (85, 4, 0, NULL, "2001-11-01", 0, NULL, 142)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (86, 260, 0, NULL, "2001-11-16", 0, NULL, 143)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (87, 26.973, 0, NULL, "2001-11-16", 0, NULL, 144)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (88, 3, 0, NULL, "2001-11-16", 0, NULL, 145)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (89, 408, 0, NULL, "2001-11-12", 0, NULL, 146)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (90, 1, 0, NULL, "2001-11-12", 0, NULL, 147)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (91, 13.51, 1, "2002-11-12", "2001-11-12", 1, NULL, 148)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (92, 27.9942, 0, NULL, "2001-11-12", 0, NULL, 149)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (93, 22.9268, 0, NULL, "2001-11-15", 0, NULL, 150)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (94, 290, 0, NULL, "2001-11-15", 0, NULL, 151)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (95, 19.0059, 1, "2002-11-15", "2001-11-15", 1, NULL, 152)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (96, 5, 0, NULL, "2001-11-15", 0, NULL, 153)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (97, 18.6114, 1, "2003-00-27", "2002-00-27", 1, NULL, 158)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (98, 452, 0, NULL, "2002-00-09", 0, NULL, 159)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (99, 3, 0, NULL, "2002-00-09", 0, NULL, 160)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (100, 23.1937, 0, NULL, "2002-00-09", 0, NULL, 161)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (101, 20.36, 1, "2003-00-09", "2002-00-09", 1, NULL, 162)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (102, 363, 0, NULL, "2002-00-01", 0, NULL, 167)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (103, 28.7446, 0, NULL, "2002-00-01", 0, NULL, 168)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (104, 5, 0, NULL, "2002-00-01", 0, NULL, 169)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (105, 19.7242, 1, "2003-00-01", "2002-00-01", 1, NULL, 170)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (106, 23.5276, 0, NULL, "2002-01-22", 0, NULL, 181)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (107, 4, 0, NULL, "2002-01-22", 0, NULL, 182)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (108, 313, 0, NULL, "2002-01-22", 0, NULL, 183)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (109, 21.8599, 1, "2003-01-22", "2002-01-22", 1, NULL, 184)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (110, 13.0132, 0, NULL, "2002-01-02", 0, NULL, 185)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (111, 23.69, 1, "2003-01-02", "2002-01-02", 1, NULL, 186)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (112, 3, 0, NULL, "2002-02-11", 0, NULL, 191)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (113, 463, 0, NULL, "2002-02-08", 0, NULL, 193)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (114, 12.6104, 1, "2003-02-08", "2002-02-08", 1, NULL, 194)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (115, 282, 0, NULL, "2002-02-28", 0, NULL, 199)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (116, 10.6159, 1, "2003-02-28", "2002-02-28", 1, NULL, 200)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (117, 266, 0, NULL, "2002-03-29", 0, NULL, 201)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (118, 2, 0, NULL, "2002-03-29", 0, NULL, 202)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (119, 16.8071, 1, "2003-03-29", "2002-03-29", 1, NULL, 203)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (120, 317, 0, NULL, "2002-03-14", 0, NULL, 204)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (121, 19.8405, 1, "2003-03-14", "2002-03-14", 1, NULL, 205)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (122, 27.2935, 0, NULL, "2002-03-14", 0, NULL, 206)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (123, 2, 0, NULL, "2002-03-14", 0, NULL, 207)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (124, 271, 0, NULL, "2002-03-20", 0, NULL, 208)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (125, 3, 0, NULL, "2002-03-20", 0, NULL, 209)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (126, 21.1048, 1, "2003-03-20", "2002-03-20", 1, NULL, 210)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (127, 22.2518, 0, NULL, "2002-03-20", 0, NULL, 211)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (128, 22.6252, 0, NULL, "2002-03-26", 0, NULL, 212)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (129, 3, 0, NULL, "2002-03-26", 0, NULL, 213)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (130, 328, 0, NULL, "2002-03-26", 0, NULL, 214)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (131, 12.3786, 1, "2003-03-26", "2002-03-26", 1, NULL, 215)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (132, 301, 0, NULL, "2002-03-19", 0, NULL, 216)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (133, 0, 0, NULL, "2002-03-19", 0, NULL, 217)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (134, 25.35, 0, NULL, "2002-03-19", 0, NULL, 218)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (135, 11.484, 1, "2003-03-19", "2002-03-19", 1, NULL, 219)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (136, 295, 0, NULL, "2002-04-06", 0, NULL, 220)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (137, 5, 0, NULL, "2002-04-06", 0, NULL, 221)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (138, 18.6838, 1, "2003-04-06", "2002-04-06", 1, NULL, 222)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (139, 11.6259, 0, NULL, "2002-04-06", 0, NULL, 223)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (140, 17.3329, 1, "2003-04-09", "2002-04-09", 1, NULL, 224)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (141, 0, 0, NULL, "2002-04-09", 0, NULL, 225)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (142, 22.4121, 0, NULL, "2002-04-09", 0, NULL, 226)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (143, 472, 0, NULL, "2002-04-09", 0, NULL, 227)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (144, 4, 0, NULL, "2002-05-22", 0, NULL, 232)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (145, 494, 0, NULL, "2002-05-22", 0, NULL, 233)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (146, 22.37, 1, "2003-05-22", "2002-05-22", 1, NULL, 234)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (147, 436, 0, NULL, "2002-05-07", 0, NULL, 235)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (148, 21.2894, 1, "2003-05-14", "2002-05-14", 1, NULL, 236)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (149, 17.6165, 0, NULL, "2002-06-22", 0, NULL, 237)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (150, 18.5886, 1, "2003-06-22", "2002-06-22", 1, NULL, 238)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (151, 2, 0, NULL, "2002-06-22", 0, NULL, 239)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (152, 372, 0, NULL, "2002-06-22", 0, NULL, 240)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (153, 4, 0, NULL, "2002-07-01", 0, NULL, 247)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (154, 19.8354, 0, NULL, "2002-07-01", 0, NULL, 248)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (155, 21.0823, 1, "2003-07-01", "2002-07-01", 1, NULL, 249)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (156, 369, 0, NULL, "2002-07-01", 0, NULL, 250)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (157, 22.3471, 0, NULL, "2002-07-17", 0, NULL, 251)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (158, 5, 0, NULL, "2002-07-17", 0, NULL, 252)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (159, 19.6676, 1, "2003-07-17", "2002-07-17", 1, NULL, 253)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (160, 433, 0, NULL, "2002-07-17", 0, NULL, 254)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (161, 9.1645, 1, "2003-07-24", "2002-07-24", 1, NULL, 255)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (162, 254, 0, NULL, "2002-07-24", 0, NULL, 256)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (163, 2, 0, NULL, "2002-07-24", 0, NULL, 257)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (164, 25.2546, 0, NULL, "2002-07-24", 0, NULL, 258)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (165, 27.5724, 0, NULL, "2002-07-31", 0, NULL, 259)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (166, 1, 0, NULL, "2002-07-31", 0, NULL, 260)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (167, 8.2221, 1, "2003-07-31", "2002-07-31", 1, NULL, 261)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (168, 305, 0, NULL, "2002-07-31", 0, NULL, 262)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (169, 21.9191, 0, NULL, "2002-08-24", 0, NULL, 263)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (170, 1, 0, NULL, "2002-08-24", 0, NULL, 264)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (171, 20.5522, 1, "2003-08-24", "2002-08-24", 1, NULL, 265)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (172, 309, 0, NULL, "2002-08-24", 0, NULL, 266)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (173, 3, 0, NULL, "2002-09-14", 0, NULL, 267)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (174, 360, 0, NULL, "2002-09-14", 0, NULL, 268)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (175, 27.9019, 0, NULL, "2002-09-14", 0, NULL, 269)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (176, 14.3727, 1, "2003-09-14", "2002-09-14", 1, NULL, 270)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (177, 18.6449, 1, "2003-09-28", "2002-09-28", 1, NULL, 275)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (178, 2, 0, NULL, "2002-09-28", 0, NULL, 276)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (179, 281, 0, NULL, "2002-09-28", 0, NULL, 277)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (180, 28.908, 0, NULL, "2002-09-28", 0, NULL, 278)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (181, 20.7049, 1, "2003-10-07", "2002-10-07", 1, NULL, 279)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (182, 20.1737, 0, NULL, "2002-10-07", 0, NULL, 280)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (183, 259, 0, NULL, "2002-10-07", 0, NULL, 281)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (184, 5, 0, NULL, "2002-10-07", 0, NULL, 282)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (185, 3, 0, NULL, "2002-10-18", 0, NULL, 287)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (186, 24.9402, 0, NULL, "2002-10-18", 0, NULL, 288)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (187, 15.345, 1, "2003-10-18", "2002-10-18", 1, NULL, 289)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (188, 462, 0, NULL, "2002-10-18", 0, NULL, 290)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (189, 3, 0, NULL, "2002-10-05", 0, NULL, 295)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (190, 497, 0, NULL, "2002-10-05", 0, NULL, 296)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (191, 16.3551, 1, "2003-10-05", "2002-10-05", 1, NULL, 297)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (192, 27.6393, 0, NULL, "2002-10-05", 0, NULL, 298)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (193, 1, 0, NULL, "2002-10-23", 0, NULL, 299)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (194, 384, 0, NULL, "2002-10-23", 0, NULL, 300)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (195, 18.8658, 1, "2003-10-23", "2002-10-23", 1, NULL, 301)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (196, 22.3544, 0, NULL, "2002-10-23", 0, NULL, 302)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (197, 365, 0, NULL, "2002-10-16", 0, NULL, 303)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (198, 26.342, 0, NULL, "2002-10-16", 0, NULL, 304)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (199, 20.0409, 1, "2003-10-16", "2002-10-16", 1, NULL, 305)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (200, 5, 0, NULL, "2002-10-16", 0, NULL, 306)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (201, 24.5077, 0, NULL, "2002-11-19", 0, NULL, 307)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (202, 347, 0, NULL, "2002-11-19", 0, NULL, 308)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (203, 4, 0, NULL, "2002-11-19", 0, NULL, 309)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (204, 20.8463, 1, "2003-11-19", "2002-11-19", 1, NULL, 310)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (205, 337, 0, NULL, "2002-11-12", 0, NULL, 311)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (206, 26.5864, 0, NULL, "2002-11-12", 0, NULL, 312)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (207, 23.404, 1, "2003-11-12", "2002-11-12", 1, NULL, 313)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (208, 4, 0, NULL, "2002-11-12", 0, NULL, 314)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (209, 375, 0, NULL, "2002-11-22", 0, NULL, 315)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (210, 24.0514, 0, NULL, "2002-11-22", 0, NULL, 316)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (211, 13.8649, 1, "2003-11-22", "2002-11-22", 1, NULL, 317)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (212, 2, 0, NULL, "2002-11-22", 0, NULL, 318)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (213, 20.0706, 1, "2003-11-17", "2002-11-17", 1, NULL, 319)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (214, 19.1492, 0, NULL, "2002-11-17", 0, NULL, 320)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (215, 5, 0, NULL, "2002-11-17", 0, NULL, 321)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (216, 318, 0, NULL, "2002-11-17", 0, NULL, 322)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (217, 4, 0, NULL, "2002-11-09", 0, NULL, 327)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (218, 20.6109, 0, NULL, "2002-11-09", 0, NULL, 328)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (219, 15.6467, 1, "2003-11-09", "2002-11-09", 1, NULL, 329)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (220, 349, 0, NULL, "2002-11-09", 0, NULL, 330)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (221, 1, 0, NULL, "2002-11-31", 0, NULL, 331)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (222, 393, 0, NULL, "2002-11-31", 0, NULL, 332)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (223, 23.578, 1, "2003-11-31", "2002-11-31", 1, NULL, 333)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (224, 21.3157, 0, NULL, "2002-11-31", 0, NULL, 334)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (225, 4, 0, NULL, "2003-00-14", 0, NULL, 335)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (226, 21.9558, 0, NULL, "2003-00-14", 0, NULL, 336)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (227, 11.215, 1, "2004-00-14", "2003-00-14", 1, NULL, 337)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (228, 464, 0, NULL, "2003-00-14", 0, NULL, 338)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (229, 18.8897, 1, "2004-00-26", "2003-00-26", 1, NULL, 339)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (230, 412, 0, NULL, "2003-00-26", 0, NULL, 340)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (231, 11.3519, 1, "2004-00-28", "2003-00-28", 1, NULL, 341)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (232, 392, 0, NULL, "2003-00-28", 0, NULL, 342)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (233, 2, 0, NULL, "2003-00-21", 0, NULL, 343)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (234, 12.6687, 1, "2004-00-21", "2003-00-21", 1, NULL, 344)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (235, 5, 0, NULL, "2003-02-23", 0, NULL, 353)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (236, 16.5921, 1, "2004-02-23", "2003-02-23", 1, NULL, 354)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (237, 319, 0, NULL, "2003-02-23", 0, NULL, 355)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (238, 28.5889, 0, NULL, "2003-02-23", 0, NULL, 356)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (239, 23.5578, 1, "2004-02-28", "2003-02-28", 1, NULL, 357)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (240, 27.1559, 0, NULL, "2003-02-28", 0, NULL, 358)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (241, 2, 0, NULL, "2003-02-28", 0, NULL, 359)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (242, 477, 0, NULL, "2003-02-28", 0, NULL, 360)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (243, 4, 0, NULL, "2003-02-05", 0, NULL, 361)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (244, 20.3123, 0, NULL, "2003-02-05", 0, NULL, 362)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (245, 496, 0, NULL, "2003-02-05", 0, NULL, 363)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (246, 14.3635, 1, "2004-02-05", "2003-02-05", 1, NULL, 364)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (247, 14.4964, 1, "2004-03-25", "2003-03-25", 1, NULL, 369)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (248, 26.6227, 0, NULL, "2003-03-31", 0, NULL, 370)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (249, 19.8645, 1, "2004-03-31", "2003-03-31", 1, NULL, 371)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (250, 1, 0, NULL, "2003-03-31", 0, NULL, 372)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (251, 452, 0, NULL, "2003-03-31", 0, NULL, 373)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (252, 12.1259, 1, "2004-03-21", "2003-03-21", 1, NULL, 378)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (253, 499, 0, NULL, "2003-03-21", 0, NULL, 379)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (254, 1, 0, NULL, "2003-03-21", 0, NULL, 380)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (255, 28.6907, 0, NULL, "2003-03-21", 0, NULL, 381)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (256, 3, 0, NULL, "2003-03-29", 0, NULL, 382)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (257, 365, 0, NULL, "2003-03-08", 0, NULL, 383)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (258, 3, 0, NULL, "2003-03-08", 0, NULL, 384)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (259, 29.1135, 0, NULL, "2003-03-08", 0, NULL, 385)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (260, 24.7208, 1, "2004-03-08", "2003-03-08", 1, NULL, 386)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (261, 15.0046, 1, "2004-03-18", "2003-03-18", 1, NULL, 387)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (262, 25.6272, 0, NULL, "2003-03-18", 0, NULL, 388)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (263, 4, 0, NULL, "2003-03-18", 0, NULL, 389)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (264, 488, 0, NULL, "2003-03-18", 0, NULL, 390)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (265, 17.2717, 1, "2004-04-29", "2003-04-29", 1, NULL, 391)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (266, 421, 0, NULL, "2003-04-29", 0, NULL, 392)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (267, 2, 0, NULL, "2003-04-29", 0, NULL, 393)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (268, 26.5537, 0, NULL, "2003-04-29", 0, NULL, 394)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (269, 25.5386, 0, NULL, "2003-04-30", 0, NULL, 395)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (270, 1, 0, NULL, "2003-04-29", 0, NULL, 398)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (271, 18.5717, 1, "2004-04-29", "2003-04-29", 1, NULL, 399)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (272, 1, 0, NULL, "2003-05-25", 0, NULL, 404)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (273, 14.5006, 1, "2004-05-25", "2003-05-25", 1, NULL, 405)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (274, 24.7422, 0, NULL, "2003-05-25", 0, NULL, 406)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (275, 325, 0, NULL, "2003-05-25", 0, NULL, 407)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (276, 4, 0, NULL, "2003-05-03", 0, NULL, 408)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (277, 297, 0, NULL, "2003-05-03", 0, NULL, 409)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (278, 19.6598, 1, "2004-05-03", "2003-05-03", 1, NULL, 410)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (279, 25.7851, 0, NULL, "2003-05-03", 0, NULL, 411)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (280, 19.3787, 0, NULL, "2003-05-16", 0, NULL, 415)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (281, 18.3339, 1, "2004-05-16", "2003-05-16", 1, NULL, 416)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (282, 2, 0, NULL, "2003-05-16", 0, NULL, 417)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (283, 425, 0, NULL, "2003-05-16", 0, NULL, 418)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (284, 264, 0, NULL, "2003-05-18", 0, NULL, 423)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (285, 15.9489, 1, "2004-05-18", "2003-05-18", 1, NULL, 424)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (286, 1, 0, NULL, "2003-05-18", 0, NULL, 425)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (287, 23.505, 0, NULL, "2003-05-18", 0, NULL, 426)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (288, 20.6447, 1, "2004-06-29", "2003-06-29", 1, NULL, 427)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (289, 25.5785, 0, NULL, "2003-06-29", 0, NULL, 428)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (290, 3, 0, NULL, "2003-06-29", 0, NULL, 429)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (291, 491, 0, NULL, "2003-06-29", 0, NULL, 430)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (292, 449, 0, NULL, "2003-06-16", 0, NULL, 431)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (293, 3, 0, NULL, "2003-06-16", 0, NULL, 432)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (294, 15.6354, 0, NULL, "2003-06-16", 0, NULL, 433)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (295, 16.257, 1, "2004-06-16", "2003-06-16", 1, NULL, 434)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (296, 283, 0, NULL, "2003-07-09", 0, NULL, 439)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (297, 25.5111, 0, NULL, "2003-07-09", 0, NULL, 440)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (298, 324, 0, NULL, "2003-07-11", 0, NULL, 441)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (299, 14.4823, 1, "2004-07-11", "2003-07-11", 1, NULL, 442)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (300, 27.2398, 0, NULL, "2003-07-11", 0, NULL, 443)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (301, 1, 0, NULL, "2003-07-11", 0, NULL, 444)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (302, 17.5524, 1, "2004-07-08", "2003-07-08", 1, NULL, 445)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (303, 25.5456, 0, NULL, "2003-07-14", 0, NULL, 446)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (304, 14.9916, 1, "2004-07-14", "2003-07-14", 1, NULL, 447)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (305, 426, 0, NULL, "2003-07-15", 0, NULL, 450)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (306, 5, 0, NULL, "2003-07-15", 0, NULL, 451)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (307, 23.9886, 0, NULL, "2003-07-15", 0, NULL, 452)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (308, 15.0146, 1, "2004-07-15", "2003-07-15", 1, NULL, 453)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (309, 3, 0, NULL, "2003-08-12", 0, NULL, 459)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (310, 320, 0, NULL, "2003-08-12", 0, NULL, 460)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (311, 10.0915, 1, "2004-08-12", "2003-08-12", 1, NULL, 461)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (312, 26.8218, 0, NULL, "2003-08-12", 0, NULL, 462)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (313, 334, 0, NULL, "2003-09-26", 0, NULL, 463)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (314, 12.901, 1, "2004-09-26", "2003-09-26", 1, NULL, 464)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (315, 5, 0, NULL, "2003-09-26", 0, NULL, 465)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (316, 27.3306, 0, NULL, "2003-09-26", 0, NULL, 466)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (317, 4, 0, NULL, "2003-09-23", 0, NULL, 467)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (318, 369, 0, NULL, "2003-09-23", 0, NULL, 468)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (319, 16.6053, 1, "2004-09-23", "2003-09-23", 1, NULL, 469)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (320, 26.135, 0, NULL, "2003-09-23", 0, NULL, 470)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (321, 23.3476, 0, NULL, "2003-10-29", 0, NULL, 471)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (322, 482, 0, NULL, "2003-10-29", 0, NULL, 472)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (323, 10.4895, 1, "2004-10-29", "2003-10-29", 1, NULL, 473)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (324, 5, 0, NULL, "2003-10-29", 0, NULL, 474)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (325, 295, 0, NULL, "2003-10-17", 0, NULL, 475)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (326, 2, 0, NULL, "2003-10-17", 0, NULL, 476)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (327, 19.6998, 1, "2004-10-17", "2003-10-17", 1, NULL, 477)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (328, 24.85, 0, NULL, "2003-10-17", 0, NULL, 478)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (329, 1, 0, NULL, "2003-10-17", 0, NULL, 479)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (330, 26.1072, 0, NULL, "2003-10-17", 0, NULL, 480)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (331, 405, 0, NULL, "2003-10-17", 0, NULL, 481)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (332, 18.1542, 1, "2004-10-17", "2003-10-17", 1, NULL, 482)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (333, 3, 0, NULL, "2003-10-21", 0, NULL, 483)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (334, 12.1533, 1, "2004-10-21", "2003-10-21", 1, NULL, 484)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (335, 20.1755, 0, NULL, "2003-10-21", 0, NULL, 485)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (336, 439, 0, NULL, "2003-10-23", 0, NULL, 486)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (337, 26.3996, 0, NULL, "2003-10-23", 0, NULL, 487)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (338, 15.2275, 1, "2004-10-04", "2003-10-04", 1, NULL, 488)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (339, 13.2079, 1, "2004-10-17", "2003-10-17", 1, NULL, 489)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (340, 471, 0, NULL, "2003-10-17", 0, NULL, 490)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (341, 23.2045, 0, NULL, "2003-10-17", 0, NULL, 491)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (342, 20.1597, 0, NULL, "2003-11-21", 0, NULL, 493)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (343, 489, 0, NULL, "2003-11-21", 0, NULL, 494)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (344, 11.4109, 1, "2004-11-21", "2003-11-21", 1, NULL, 495)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (345, 3, 0, NULL, "2003-11-21", 0, NULL, 496)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (346, 3, 0, NULL, "2003-11-09", 0, NULL, 505)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (347, 296, 0, NULL, "2003-11-09", 0, NULL, 506)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (348, 20.5304, 1, "2004-11-09", "2003-11-09", 1, NULL, 507)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (349, 28.2075, 0, NULL, "2003-11-09", 0, NULL, 508)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (350, 21.8171, 0, NULL, "2003-11-19", 0, NULL, 509)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (351, 4, 0, NULL, "2003-11-19", 0, NULL, 510)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (352, 19.7596, 1, "2004-11-19", "2003-11-19", 1, NULL, 511)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (353, 270, 0, NULL, "2003-11-19", 0, NULL, 512)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (354, 305, 0, NULL, "2004-00-20", 0, NULL, 513)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (355, 25.1833, 0, NULL, "2004-00-20", 0, NULL, 514)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (356, 3, 0, NULL, "2004-00-20", 0, NULL, 515)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (357, 24.6517, 1, "2005-00-20", "2004-00-20", 1, NULL, 516)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (358, 406, 0, NULL, "2004-00-23", 0, NULL, 517)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (359, 14.2151, 1, "2005-00-23", "2004-00-23", 1, NULL, 518)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (360, 20.6365, 0, NULL, "2004-00-23", 0, NULL, 519)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (361, 4, 0, NULL, "2004-00-23", 0, NULL, 520)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (362, 29.8692, 0, NULL, "2004-00-11", 0, NULL, 521)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (363, 293, 0, NULL, "2004-00-11", 0, NULL, 522)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (364, 399, 0, NULL, "2004-00-04", 0, NULL, 523)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (365, 22.8937, 0, NULL, "2004-00-04", 0, NULL, 524)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (366, 19.3983, 1, "2005-00-04", "2004-00-04", 1, NULL, 525)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (367, 3, 0, NULL, "2004-00-04", 0, NULL, 526)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (368, 26.3286, 0, NULL, "2004-00-06", 0, NULL, 527)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (369, 24.9716, 1, "2005-00-06", "2004-00-06", 1, NULL, 528)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (370, 370, 0, NULL, "2004-00-30", 0, NULL, 529)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (371, 20.3639, 0, NULL, "2004-00-30", 0, NULL, 530)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (372, 23.0519, 1, "2005-00-30", "2004-00-30", 1, NULL, 531)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (373, 3, 0, NULL, "2004-00-30", 0, NULL, 532)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (374, 347, 0, NULL, "2004-00-26", 0, NULL, 533)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (375, 20.3733, 0, NULL, "2004-00-26", 0, NULL, 534)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (376, 2, 0, NULL, "2004-00-26", 0, NULL, 535)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (377, 4, 0, NULL, "2004-01-04", 0, NULL, 536)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (378, 22.4893, 0, NULL, "2004-01-04", 0, NULL, 537)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (379, 267, 0, NULL, "2004-01-04", 0, NULL, 538)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (380, 17.8267, 1, "2005-01-04", "2004-01-04", 1, NULL, 539)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (381, 11.537, 1, "2005-01-17", "2004-01-17", 1, NULL, 540)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (382, 331, 0, NULL, "2004-01-17", 0, NULL, 541)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (383, 3, 0, NULL, "2004-01-17", 0, NULL, 542)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (384, 413, 0, NULL, "2004-01-17", 0, NULL, 543)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (385, 11.3466, 1, "2005-01-17", "2004-01-17", 1, NULL, 544)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (386, 26.1874, 0, NULL, "2004-01-17", 0, NULL, 545)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (387, 1, 0, NULL, "2004-01-06", 0, NULL, 546)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (388, 389, 0, NULL, "2004-01-06", 0, NULL, 547)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (389, 12.5633, 1, "2005-01-06", "2004-01-06", 1, NULL, 548)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (390, 28.0315, 0, NULL, "2004-01-06", 0, NULL, 549)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (391, 2, 0, NULL, "2004-02-03", 0, NULL, 554)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (392, 14.6869, 1, "2005-02-03", "2004-02-03", 1, NULL, 555)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (393, 412, 0, NULL, "2004-02-03", 0, NULL, 556)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (394, 23.4466, 0, NULL, "2004-02-03", 0, NULL, 557)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (395, 27.0427, 0, NULL, "2004-03-20", 0, NULL, 558)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (396, 3, 0, NULL, "2004-03-20", 0, NULL, 559)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (397, 3.8569, 1, "2005-03-20", "2004-03-20", 1, NULL, 560)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (398, 465, 0, NULL, "2004-03-20", 0, NULL, 561)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (399, 413, 0, NULL, "2004-03-09", 0, NULL, 562)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (400, 2, 0, NULL, "2004-03-09", 0, NULL, 563)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (401, 20.4381, 0, NULL, "2004-03-09", 0, NULL, 564)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (402, 15.074, 1, "2005-03-09", "2004-03-09", 1, NULL, 565)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (403, 14.7579, 1, "2005-03-17", "2004-03-17", 1, NULL, 566)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (404, 1, 0, NULL, "2004-03-17", 0, NULL, 567)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (405, 10.7557, 1, "2005-04-09", "2004-04-09", 1, NULL, 577)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (406, 336, 0, NULL, "2004-04-09", 0, NULL, 578)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (407, 25.4386, 0, NULL, "2004-04-09", 0, NULL, 579)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (408, 3, 0, NULL, "2004-04-09", 0, NULL, 580)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (409, 4, 0, NULL, "2004-04-03", 0, NULL, 581)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (410, 463, 0, NULL, "2004-04-03", 0, NULL, 582)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (411, 20.0751, 0, NULL, "2004-04-03", 0, NULL, 583)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (412, 21.7928, 1, "2005-04-03", "2004-04-03", 1, NULL, 584)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (413, 25.2882, 0, NULL, "2004-04-27", 0, NULL, 585)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (414, 1, 0, NULL, "2004-04-27", 0, NULL, 586)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (415, 21.1948, 1, "2005-04-27", "2004-04-27", 1, NULL, 587)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (416, 331, 0, NULL, "2004-04-27", 0, NULL, 588)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (417, 3, 0, NULL, "2004-05-16", 0, NULL, 590)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (418, 372, 0, NULL, "2004-05-16", 0, NULL, 591)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (419, 21.3687, 0, NULL, "2004-05-16", 0, NULL, 592)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (420, 10.3592, 1, "2005-05-16", "2004-05-16", 1, NULL, 593)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (421, 20.3059, 1, "2005-05-25", "2004-05-25", 1, NULL, 598)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (422, 7.5679, 1, "2005-05-08", "2004-05-08", 1, NULL, 599)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (423, 294, 0, NULL, "2004-05-08", 0, NULL, 600)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (424, 376, 0, NULL, "2004-05-23", 0, NULL, 601)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (425, 23.7509, 1, "2005-05-23", "2004-05-23", 1, NULL, 602)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (426, 1, 0, NULL, "2004-05-23", 0, NULL, 603)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (427, 410, 0, NULL, "2004-06-08", 0, NULL, 604)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (428, 422, 0, NULL, "2004-06-22", 0, NULL, 605)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (429, 4, 0, NULL, "2004-06-22", 0, NULL, 606)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (430, 27.2531, 0, NULL, "2004-06-22", 0, NULL, 607)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (431, 18.9938, 1, "2005-06-22", "2004-06-22", 1, NULL, 608)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (432, 26.7255, 0, NULL, "2004-06-08", 0, NULL, 609)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (433, 4, 0, NULL, "2004-06-08", 0, NULL, 610)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (434, 493, 0, NULL, "2004-06-08", 0, NULL, 611)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (435, 2, 0, NULL, "2004-06-06", 0, NULL, 612)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (436, 24.7643, 0, NULL, "2004-06-06", 0, NULL, 613)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (437, 482, 0, NULL, "2004-07-04", 0, NULL, 614)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (438, 2, 0, NULL, "2004-07-04", 0, NULL, 615)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (439, 11.2454, 1, "2005-07-29", "2004-07-29", 1, NULL, 616)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (440, 2, 0, NULL, "2004-07-29", 0, NULL, 617)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (441, 20.1833, 0, NULL, "2004-07-29", 0, NULL, 618)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (442, 403, 0, NULL, "2004-07-29", 0, NULL, 619)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (443, 26.4751, 0, NULL, "2004-07-01", 0, NULL, 620)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (444, 20.9783, 1, "2005-07-01", "2004-07-01", 1, NULL, 621)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (445, 353, 0, NULL, "2004-07-01", 0, NULL, 622)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (446, 1, 0, NULL, "2004-07-01", 0, NULL, 623)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (447, 19.7855, 1, "2005-07-22", "2004-07-22", 1, NULL, 624)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (448, 29.206, 0, NULL, "2004-07-22", 0, NULL, 625)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (449, 438, 0, NULL, "2004-07-22", 0, NULL, 626)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (450, 5, 0, NULL, "2004-07-22", 0, NULL, 627)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (451, 4, 0, NULL, "2004-08-10", 0, NULL, 628)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (452, 353, 0, NULL, "2004-08-10", 0, NULL, 629)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (453, 23.8661, 0, NULL, "2004-08-14", 0, NULL, 630)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (454, 2, 0, NULL, "2004-08-14", 0, NULL, 631)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (455, 21.9519, 1, "2005-08-14", "2004-08-14", 1, NULL, 632)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (456, 307, 0, NULL, "2004-08-14", 0, NULL, 633)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (457, 20.8511, 0, NULL, "2004-09-05", 0, NULL, 634)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (458, 439, 0, NULL, "2004-09-05", 0, NULL, 635)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (459, 3, 0, NULL, "2004-09-05", 0, NULL, 636)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (460, 21.5287, 1, "2005-09-05", "2004-09-05", 1, NULL, 637)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (461, 25.0987, 0, NULL, "2004-10-20", 0, NULL, 642)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (462, 494, 0, NULL, "2004-10-20", 0, NULL, 643)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (463, 3, 0, NULL, "2004-10-20", 0, NULL, 644)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (464, 12.0555, 1, "2005-10-20", "2004-10-20", 1, NULL, 645)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (465, 24.4986, 0, NULL, "2004-10-16", 0, NULL, 646)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (466, 4, 0, NULL, "2004-10-16", 0, NULL, 647)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (467, 447, 0, NULL, "2004-10-16", 0, NULL, 648)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (468, 15.2811, 1, "2005-10-12", "2004-10-12", 1, NULL, 649)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (469, 494, 0, NULL, "2004-10-10", 0, NULL, 650)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (470, 25.6632, 0, NULL, "2004-10-10", 0, NULL, 651)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (471, 15.9612, 1, "2005-10-10", "2004-10-10", 1, NULL, 652)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (472, 3, 0, NULL, "2004-10-10", 0, NULL, 653)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (473, 4, 0, NULL, "2004-10-27", 0, NULL, 654)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (474, 409, 0, NULL, "2004-10-27", 0, NULL, 655)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (475, 16.3285, 1, "2005-10-27", "2004-10-27", 1, NULL, 656)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (476, 26.7714, 0, NULL, "2004-10-27", 0, NULL, 657)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (477, 28.2682, 0, NULL, "2004-10-23", 0, NULL, 658)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (478, 1, 0, NULL, "2004-10-23", 0, NULL, 659)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (479, 19.6349, 1, "2005-10-23", "2004-10-23", 1, NULL, 660)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (480, 404, 0, NULL, "2004-10-23", 0, NULL, 661)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (481, 482, 0, NULL, "2004-11-29", 0, NULL, 662)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (482, 20.4563, 0, NULL, "2004-11-29", 0, NULL, 663)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (483, 3, 0, NULL, "2004-11-29", 0, NULL, 664)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (484, 5, 0, NULL, "2004-11-16", 0, NULL, 665)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (485, 15.5897, 1, "2005-11-16", "2004-11-16", 1, NULL, 666)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (486, 14.094, 0, NULL, "2004-11-16", 0, NULL, 667)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (487, 455, 0, NULL, "2004-11-16", 0, NULL, 668)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (488, 1, 0, NULL, "2005-00-23", 0, NULL, 669)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (489, 454, 0, NULL, "2005-00-23", 0, NULL, 670)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (490, 20.972, 0, NULL, "2005-00-23", 0, NULL, 671)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (491, 19.1469, 1, "2006-00-23", "2005-00-23", 1, NULL, 672)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (492, 5, 0, NULL, "2005-00-08", 0, NULL, 676)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (493, 21.565, 0, NULL, "2005-02-06", 0, NULL, 686)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (494, 2, 0, NULL, "2005-02-06", 0, NULL, 687)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (495, 23.2904, 1, "2006-02-06", "2005-02-06", 1, NULL, 688)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (496, 475, 0, NULL, "2005-02-06", 0, NULL, 689)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (497, 25.2571, 0, NULL, "2005-03-11", 0, NULL, 690)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (498, 5, 0, NULL, "2005-03-11", 0, NULL, 691)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (499, 18.2416, 1, "2006-03-27", "2005-03-27", 1, NULL, 692)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (500, 21.0207, 0, NULL, "2005-03-27", 0, NULL, 693)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (501, 24.6716, 0, NULL, "2005-03-30", 0, NULL, 694)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (502, 3, 0, NULL, "2005-03-30", 0, NULL, 695)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (503, 14.1689, 1, "2006-03-30", "2005-03-30", 1, NULL, 696)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (504, 355, 0, NULL, "2005-03-30", 0, NULL, 697)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (505, 25.7833, 0, NULL, "2005-04-20", 0, NULL, 702)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (506, 24.6627, 1, "2006-04-20", "2005-04-20", 1, NULL, 703)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (507, 5, 0, NULL, "2005-04-20", 0, NULL, 704)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (508, 269, 0, NULL, "2005-04-20", 0, NULL, 705)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (509, 2, 0, NULL, "2005-04-23", 0, NULL, 706)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (510, 27.3641, 0, NULL, "2005-04-23", 0, NULL, 707)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (511, 382, 0, NULL, "2005-04-23", 0, NULL, 708)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (512, 24.1432, 1, "2006-04-23", "2005-04-23", 1, NULL, 709)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (513, 381, 0, NULL, "2005-04-01", 0, NULL, 710)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (514, 14.1399, 1, "2006-04-01", "2005-04-01", 1, NULL, 711)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (515, 280, 0, NULL, "2005-04-13", 0, NULL, 712)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (516, 12.506, 1, "2006-04-13", "2005-04-13", 1, NULL, 713)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (517, 2, 0, NULL, "2005-04-13", 0, NULL, 714)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (518, 26.8026, 0, NULL, "2005-04-13", 0, NULL, 715)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (519, 5, 0, NULL, "2005-04-10", 0, NULL, 716)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (520, 282, 0, NULL, "2005-05-02", 0, NULL, 717)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (521, 19.4836, 0, NULL, "2005-05-11", 0, NULL, 723)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (522, 360, 0, NULL, "2005-05-11", 0, NULL, 724)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (523, 16.3394, 1, "2006-05-11", "2005-05-11", 1, NULL, 725)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (524, 5, 0, NULL, "2005-05-11", 0, NULL, 726)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (525, 276, 0, NULL, "2005-05-13", 0, NULL, 727)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (526, 14.2474, 1, "2006-05-13", "2005-05-13", 1, NULL, 728)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (527, 21.958, 0, NULL, "2005-05-13", 0, NULL, 729)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (528, 2, 0, NULL, "2005-05-13", 0, NULL, 730)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (529, 21.1618, 0, NULL, "2005-05-29", 0, NULL, 731)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (530, 1, 0, NULL, "2005-05-29", 0, NULL, 732)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (531, 424, 0, NULL, "2005-05-29", 0, NULL, 733)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (532, 1, 0, NULL, "2005-05-31", 0, NULL, 734)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (533, 324, 0, NULL, "2005-05-31", 0, NULL, 735)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (534, 19.9762, 1, "2006-05-31", "2005-05-31", 1, NULL, 736)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (535, 25.1724, 0, NULL, "2005-05-31", 0, NULL, 737)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (536, 4, 0, NULL, "2005-06-28", 0, NULL, 742)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (537, 408, 0, NULL, "2005-06-01", 0, NULL, 743)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (538, 4, 0, NULL, "2005-06-01", 0, NULL, 744)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (539, 21.1347, 0, NULL, "2005-06-01", 0, NULL, 745)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (540, 17.9478, 1, "2006-06-01", "2005-06-01", 1, NULL, 746)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (541, 460, 0, NULL, "2005-06-21", 0, NULL, 747)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (542, 466, 0, NULL, "2005-06-10", 0, NULL, 752)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (543, 29.9589, 0, NULL, "2005-07-28", 0, NULL, 757)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (544, 12.2269, 1, "2006-07-28", "2005-07-28", 1, NULL, 758)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (545, 252, 0, NULL, "2005-07-28", 0, NULL, 759)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (546, 1, 0, NULL, "2005-07-28", 0, NULL, 760)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (547, 4, 0, NULL, "2005-07-09", 0, NULL, 769)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (548, 345, 0, NULL, "2005-07-09", 0, NULL, 770)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (549, 23.878, 0, NULL, "2005-07-09", 0, NULL, 771)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (550, 12.3781, 1, "2006-07-09", "2005-07-09", 1, NULL, 772)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (551, 433, 0, NULL, "2005-08-22", 0, NULL, 773)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (552, 24.8108, 0, NULL, "2005-08-22", 0, NULL, 774)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (553, 3, 0, NULL, "2005-08-22", 0, NULL, 775)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (554, 21.535, 1, "2006-08-22", "2005-08-22", 1, NULL, 776)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (555, 20.545, 0, NULL, "2005-08-15", 0, NULL, 786)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (556, 2, 0, NULL, "2005-08-15", 0, NULL, 787)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (557, 494, 0, NULL, "2005-08-15", 0, NULL, 788)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (558, 5, 0, NULL, "2005-08-02", 0, NULL, 789)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (559, 262, 0, NULL, "2005-08-02", 0, NULL, 790)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (560, 10.9546, 1, "2006-08-02", "2005-08-02", 1, NULL, 791)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (561, 496, 0, NULL, "2005-09-31", 0, NULL, 792)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (562, 25.6873, 0, NULL, "2005-09-31", 0, NULL, 793)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (563, 10.9067, 1, "2006-09-31", "2005-09-31", 1, NULL, 794)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (564, 4, 0, NULL, "2005-09-31", 0, NULL, 795)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (565, 19.493, 0, NULL, "2005-09-06", 0, NULL, 796)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (566, 368, 0, NULL, "2005-09-06", 0, NULL, 797)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (567, 20.5912, 1, "2006-09-06", "2005-09-06", 1, NULL, 798)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (568, 5, 0, NULL, "2005-09-06", 0, NULL, 799)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (569, 16.2295, 1, "2006-09-25", "2005-09-25", 1, NULL, 800)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (570, 3, 0, NULL, "2005-09-25", 0, NULL, 801)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (571, 370, 0, NULL, "2005-09-25", 0, NULL, 802)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (572, 20.493, 0, NULL, "2005-09-25", 0, NULL, 803)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (573, 1, 0, NULL, "2005-09-02", 0, NULL, 804)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (574, 19.2826, 1, "2006-09-02", "2005-09-02", 1, NULL, 805)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (575, 269, 0, NULL, "2005-10-01", 0, NULL, 806)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (576, 15.5526, 1, "2006-10-01", "2005-10-01", 1, NULL, 807)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (577, 4, 0, NULL, "2005-10-01", 0, NULL, 808)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (578, 22.9572, 0, NULL, "2005-10-01", 0, NULL, 809)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (579, 27.2748, 0, NULL, "2005-11-19", 0, NULL, 814)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (580, 330, 0, NULL, "2005-11-19", 0, NULL, 815)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (581, 4, 0, NULL, "2005-11-19", 0, NULL, 816)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (582, 19.8516, 1, "2006-11-19", "2005-11-19", 1, NULL, 817)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (583, 399, 0, NULL, "2005-11-31", 0, NULL, 818)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (584, 1, 0, NULL, "2005-11-31", 0, NULL, 819)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (585, 17.8003, 1, "2006-11-31", "2005-11-31", 1, NULL, 820)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (586, 22.4699, 0, NULL, "2005-11-31", 0, NULL, 821)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (587, 14.223, 1, "2006-11-31", "2005-11-31", 1, NULL, 822)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (588, 27.2484, 0, NULL, "2005-11-31", 0, NULL, 823)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (589, 310, 0, NULL, "2005-11-31", 0, NULL, 824)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (590, 4, 0, NULL, "2005-11-31", 0, NULL, 825)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (591, 11.9325, 1, "2006-11-03", "2005-11-03", 1, NULL, 826)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (592, 24.3923, 0, NULL, "2005-11-03", 0, NULL, 827)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (593, 384, 0, NULL, "2005-11-16", 0, NULL, 836)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (594, 18.8915, 1, "2006-11-16", "2005-11-16", 1, NULL, 837)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (595, 3, 0, NULL, "2005-11-16", 0, NULL, 838)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (596, 2, 0, NULL, "2006-00-08", 0, NULL, 839)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (597, 10.8505, 1, "2007-00-08", "2006-00-08", 1, NULL, 840)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (598, 22.2652, 0, NULL, "2006-00-08", 0, NULL, 841)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (599, 28.5218, 0, NULL, "2006-00-26", 0, NULL, 842)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (600, 2, 0, NULL, "2006-00-26", 0, NULL, 843)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (601, 1, 0, NULL, "2006-00-12", 0, NULL, 844)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (602, 26.8408, 0, NULL, "2006-00-12", 0, NULL, 845)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (603, 370, 0, NULL, "2006-00-12", 0, NULL, 846)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (604, 24.0961, 1, "2007-00-02", "2006-00-02", 1, NULL, 847)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (605, 29.8795, 0, NULL, "2006-00-30", 0, NULL, 848)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (606, 437, 0, NULL, "2006-00-30", 0, NULL, 849)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (607, 17.6642, 1, "2007-00-30", "2006-00-30", 1, NULL, 850)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (608, 5, 0, NULL, "2006-00-30", 0, NULL, 851)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (609, 15.5827, 1, "2007-00-02", "2006-00-02", 1, NULL, 852)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (610, 2, 0, NULL, "2006-01-14", 0, NULL, 853)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (611, 22.2004, 0, NULL, "2006-01-14", 0, NULL, 854)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (612, 20.7001, 1, "2007-01-14", "2006-01-14", 1, NULL, 855)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (613, 315, 0, NULL, "2006-01-14", 0, NULL, 856)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (614, 13.978, 1, "2007-01-23", "2006-01-23", 1, NULL, 857)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (615, 301, 0, NULL, "2006-01-23", 0, NULL, 858)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (616, 2, 0, NULL, "2006-01-23", 0, NULL, 859)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (617, 25.5301, 0, NULL, "2006-01-23", 0, NULL, 860)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (618, 21.6254, 1, "2007-01-16", "2006-01-16", 1, NULL, 861)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (619, 497, 0, NULL, "2006-01-16", 0, NULL, 862)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (620, 1, 0, NULL, "2006-01-16", 0, NULL, 863)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (621, 21.9773, 0, NULL, "2006-01-16", 0, NULL, 864)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (622, 29.5614, 0, NULL, "2006-01-11", 0, NULL, 869)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (623, 4, 0, NULL, "2006-01-11", 0, NULL, 870)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (624, 23.1447, 0, NULL, "2006-02-26", 0, NULL, 871)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (625, 0, 0, NULL, "2006-02-26", 0, NULL, 872)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (626, 312, 0, NULL, "2006-02-26", 0, NULL, 873)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (627, 20.3764, 1, "2007-02-26", "2006-02-26", 1, NULL, 874)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (628, 28.7885, 0, NULL, "2006-02-29", 0, NULL, 875)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (629, 1, 0, NULL, "2006-02-29", 0, NULL, 876)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (630, 417, 0, NULL, "2006-02-29", 0, NULL, 877)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (631, 15.8122, 1, "2007-02-29", "2006-02-29", 1, NULL, 878)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (632, 16.7396, 1, "2007-02-11", "2006-02-11", 1, NULL, 879)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (633, 20.5578, 0, NULL, "2006-02-11", 0, NULL, 880)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (634, 2, 0, NULL, "2006-02-11", 0, NULL, 881)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (635, 11.0986, 1, "2007-02-05", "2006-02-05", 1, NULL, 882)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (636, 28.9245, 0, NULL, "2006-02-05", 0, NULL, 883)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (637, 4, 0, NULL, "2006-02-05", 0, NULL, 884)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (638, 329, 0, NULL, "2006-02-05", 0, NULL, 885)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (639, 19.8887, 1, "2007-04-12", "2006-04-12", 1, NULL, 901)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (640, 1, 0, NULL, "2006-04-12", 0, NULL, 902)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (641, 261, 0, NULL, "2006-04-12", 0, NULL, 903)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (642, 28.6977, 0, NULL, "2006-04-12", 0, NULL, 904)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (643, 2, 0, NULL, "2006-04-26", 0, NULL, 905)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (644, 19.7368, 1, "2007-04-26", "2006-04-26", 1, NULL, 906)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (645, 331, 0, NULL, "2006-04-26", 0, NULL, 907)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (646, 21.9044, 0, NULL, "2006-04-26", 0, NULL, 908)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (647, 451, 0, NULL, "2006-04-10", 0, NULL, 917)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (648, 5, 0, NULL, "2006-04-10", 0, NULL, 918)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (649, 425, 0, NULL, "2006-05-11", 0, NULL, 921)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (650, 5, 0, NULL, "2006-05-11", 0, NULL, 922)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (651, 14.9437, 1, "2007-05-11", "2006-05-11", 1, NULL, 923)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (652, 27.9338, 0, NULL, "2006-05-11", 0, NULL, 924)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (653, 22.3439, 0, NULL, "2006-05-22", 0, NULL, 925)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (654, 11.0124, 1, "2007-05-22", "2006-05-22", 1, NULL, 926)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (655, 1, 0, NULL, "2006-05-22", 0, NULL, 927)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (656, 403, 0, NULL, "2006-05-22", 0, NULL, 928)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (657, 3, 0, NULL, "2006-05-26", 0, NULL, 929)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (658, 22.51, 0, NULL, "2006-05-26", 0, NULL, 930)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (659, 18.342, 1, "2007-05-26", "2006-05-26", 1, NULL, 931)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (660, 251, 0, NULL, "2006-05-26", 0, NULL, 932)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (661, 5, 0, NULL, "2006-06-22", 0, NULL, 937)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (662, 282, 0, NULL, "2006-06-22", 0, NULL, 938)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (663, 11.5812, 1, "2007-06-22", "2006-06-22", 1, NULL, 939)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (664, 27.2192, 0, NULL, "2006-07-01", 0, NULL, 940)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (665, 13.5743, 1, "2007-07-01", "2006-07-01", 1, NULL, 941)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (666, 2, 0, NULL, "2006-07-01", 0, NULL, 942)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (667, 453, 0, NULL, "2006-07-01", 0, NULL, 943)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (668, 22.7723, 1, "2007-07-30", "2006-07-30", 1, NULL, 944)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (669, 449, 0, NULL, "2006-07-30", 0, NULL, 945)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (670, 27.166, 0, NULL, "2006-07-30", 0, NULL, 946)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (671, 1, 0, NULL, "2006-07-30", 0, NULL, 947)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (672, 24.1507, 1, "2007-07-25", "2006-07-25", 1, NULL, 952)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (673, 27.4184, 0, NULL, "2006-07-25", 0, NULL, 953)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (674, 4, 0, NULL, "2006-07-25", 0, NULL, 954)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (675, 479, 0, NULL, "2006-07-25", 0, NULL, 955)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (676, 382, 0, NULL, "2006-08-11", 0, NULL, 956)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (677, 2, 0, NULL, "2006-08-11", 0, NULL, 957)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (678, 25.2571, 0, NULL, "2006-08-11", 0, NULL, 958)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (679, 15.6001, 1, "2007-08-11", "2006-08-11", 1, NULL, 959)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (680, 321, 0, NULL, "2006-08-01", 0, NULL, 960)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (681, 28.0922, 0, NULL, "2006-08-01", 0, NULL, 961)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (682, 3, 0, NULL, "2006-08-01", 0, NULL, 962)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (683, 11.1292, 1, "2007-08-01", "2006-08-01", 1, NULL, 963)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (684, 389, 0, NULL, "2006-09-08", 0, NULL, 966)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (685, 16.83, 1, "2007-10-16", "2006-10-16", 1, NULL, 970)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (686, 311, 0, NULL, "2006-10-16", 0, NULL, 971)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (687, 1, 0, NULL, "2006-10-16", 0, NULL, 972)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (688, 26.1027, 0, NULL, "2006-10-16", 0, NULL, 973)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (689, 3, 0, NULL, "2006-11-12", 0, NULL, 974)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (690, 319, 0, NULL, "2006-11-12", 0, NULL, 975)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (691, 11.9661, 1, "2007-11-12", "2006-11-12", 1, NULL, 976)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (692, 23.167, 1, "2007-11-04", "2006-11-04", 1, NULL, 983)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (693, 2, 0, NULL, "2006-11-04", 0, NULL, 984)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (694, 4, 0, NULL, "2007-00-28", 0, NULL, 989)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (695, 25.3601, 0, NULL, "2007-00-28", 0, NULL, 990)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (696, 17.5244, 1, "2008-01-28", "2007-01-28", 1, NULL, 991)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (697, 3, 0, NULL, "2007-01-28", 0, NULL, 992)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (698, 407, 0, NULL, "2007-01-28", 0, NULL, 993)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (699, 21.6124, 0, NULL, "2007-01-28", 0, NULL, 994)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (700, 25.1838, 0, NULL, "2007-01-05", 0, NULL, 995)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (701, 377, 0, NULL, "2007-01-05", 0, NULL, 996)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (702, 24.4974, 1, "2008-01-05", "2007-01-05", 1, NULL, 997)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (703, 3, 0, NULL, "2007-01-05", 0, NULL, 998)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (704, 25.1185, 0, NULL, "2007-01-05", 0, NULL, 1005)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (705, 268, 0, NULL, "2007-01-05", 0, NULL, 1006)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (706, 12.489, 1, "2008-01-05", "2007-01-05", 1, NULL, 1007)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (707, 308, 0, NULL, "2007-02-29", 0, NULL, 1008)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (708, 22.9625, 0, NULL, "2007-02-29", 0, NULL, 1009)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (709, 3, 0, NULL, "2007-02-29", 0, NULL, 1010)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (710, 23.7956, 1, "2008-02-29", "2007-02-29", 1, NULL, 1011)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (711, 404, 0, NULL, "2007-02-26", 0, NULL, 1016)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (712, 23.1591, 1, "2008-02-26", "2007-02-26", 1, NULL, 1017)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (713, 22.0141, 0, NULL, "2007-02-26", 0, NULL, 1018)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (714, 3, 0, NULL, "2007-02-26", 0, NULL, 1019)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (715, 258, 0, NULL, "2007-03-27", 0, NULL, 1030)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (716, 28.048, 0, NULL, "2007-03-11", 0, NULL, 1031)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (717, 494, 0, NULL, "2007-03-11", 0, NULL, 1032)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (718, 20.421, 1, "2008-03-11", "2007-03-11", 1, NULL, 1033)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (719, 3, 0, NULL, "2007-03-11", 0, NULL, 1034)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (720, 14.3978, 1, "2008-03-12", "2007-03-12", 1, NULL, 1039)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (721, 21.5371, 0, NULL, "2007-03-12", 0, NULL, 1040)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (722, 0, 0, NULL, "2007-03-12", 0, NULL, 1041)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (723, 261, 0, NULL, "2007-03-12", 0, NULL, 1042)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (724, 1, 0, NULL, "2007-03-13", 0, NULL, 1043)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (725, 499, 0, NULL, "2007-03-13", 0, NULL, 1044)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (726, 15.2174, 1, "2008-03-13", "2007-03-13", 1, NULL, 1045)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (727, 23.61, 0, NULL, "2007-03-13", 0, NULL, 1046)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (728, 2, 0, NULL, "2007-04-30", 0, NULL, 1047)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (729, 2, 0, NULL, "2007-04-07", 0, NULL, 1048)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (730, 28.1405, 0, NULL, "2007-04-07", 0, NULL, 1049)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (731, 492, 0, NULL, "2007-04-07", 0, NULL, 1050)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (732, 13.8635, 1, "2008-04-07", "2007-04-07", 1, NULL, 1051)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (733, 1, 0, NULL, "2007-04-10", 0, NULL, 1052)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (734, 253, 0, NULL, "2007-04-10", 0, NULL, 1053)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (735, 24.3971, 1, "2008-04-10", "2007-04-10", 1, NULL, 1054)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (736, 23.6814, 0, NULL, "2007-04-10", 0, NULL, 1055)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (737, 28.8197, 0, NULL, "2007-05-27", 0, NULL, 1056)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (738, 21.5571, 0, NULL, "2007-05-16", 0, NULL, 1057)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (739, 1, 0, NULL, "2007-05-16", 0, NULL, 1058)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (740, 22.9554, 1, "2008-05-16", "2007-05-16", 1, NULL, 1059)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (741, 365, 0, NULL, "2007-05-16", 0, NULL, 1060)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (742, 10.0528, 1, "2008-05-13", "2007-05-13", 1, NULL, 1065)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (743, 25.4134, 0, NULL, "2007-05-13", 0, NULL, 1066)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (744, 461, 0, NULL, "2007-05-13", 0, NULL, 1067)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (745, 4, 0, NULL, "2007-05-13", 0, NULL, 1068)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (746, 11.2391, 1, "2008-05-28", "2007-05-28", 1, NULL, 1069)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (747, 344, 0, NULL, "2007-05-28", 0, NULL, 1070)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (748, 2, 0, NULL, "2007-05-28", 0, NULL, 1071)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (749, 22.4487, 0, NULL, "2007-05-28", 0, NULL, 1072)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (750, 12.3304, 1, "2008-05-01", "2007-05-01", 1, NULL, 1073)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (751, 292, 0, NULL, "2007-05-09", 0, NULL, 1074)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (752, 27.6936, 0, NULL, "2007-05-09", 0, NULL, 1075)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (753, 2, 0, NULL, "2007-05-09", 0, NULL, 1076)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (754, 11.021, 1, "2008-05-09", "2007-05-09", 1, NULL, 1077)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (755, 1.0351, 1, "2008-06-23", "2007-06-23", 1, NULL, 1082)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (756, 5, 0, NULL, "2007-06-23", 0, NULL, 1083)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (757, 22.8849, 0, NULL, "2007-06-23", 0, NULL, 1084)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (758, 416, 0, NULL, "2007-06-23", 0, NULL, 1085)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (759, 5, 0, NULL, "2007-06-20", 0, NULL, 1090)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (760, 14.8727, 1, "2008-07-19", "2007-07-19", 1, NULL, 1099)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (761, 5, 0, NULL, "2007-08-06", 0, NULL, 1104)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (762, 22.1158, 1, "2008-08-06", "2007-08-06", 1, NULL, 1105)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (763, 356, 0, NULL, "2007-08-06", 0, NULL, 1106)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (764, 28.0044, 0, NULL, "2007-08-06", 0, NULL, 1107)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (765, 2, 0, NULL, "2007-08-28", 0, NULL, 1108)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (766, 389, 0, NULL, "2007-08-28", 0, NULL, 1109)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (767, 25.5193, 0, NULL, "2007-08-28", 0, NULL, 1110)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (768, 12.4383, 1, "2008-08-28", "2007-08-28", 1, NULL, 1111)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (769, 4, 0, NULL, "2007-08-09", 0, NULL, 1112)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (770, 18.1684, 0, NULL, "2007-08-09", 0, NULL, 1113)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (771, 13.1587, 1, "2008-08-09", "2007-08-09", 1, NULL, 1114)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (772, 334, 0, NULL, "2007-08-09", 0, NULL, 1115)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (773, 10.9665, 1, "2008-09-13", "2007-09-13", 1, NULL, 1119)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (774, 5, 0, NULL, "2007-09-13", 0, NULL, 1120)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (775, 419, 0, NULL, "2007-09-13", 0, NULL, 1121)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (776, 19.1059, 0, NULL, "2007-09-13", 0, NULL, 1122)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (777, 381, 0, NULL, "2007-09-14", 0, NULL, 1123)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (778, 21.725, 0, NULL, "2007-09-14", 0, NULL, 1124)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (779, 4, 0, NULL, "2007-09-14", 0, NULL, 1125)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (780, 15.6709, 0, NULL, "2007-09-13", 0, NULL, 1133)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (781, 5.0535, 1, "2008-09-13", "2007-09-13", 1, NULL, 1134)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (782, 397, 0, NULL, "2007-09-13", 0, NULL, 1135)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (783, 1, 0, NULL, "2007-09-13", 0, NULL, 1136)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (784, 11.7982, 1, "2008-09-30", "2007-09-30", 1, NULL, 1141)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (785, 22.013, 0, NULL, "2007-09-30", 0, NULL, 1142)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (786, 398, 0, NULL, "2007-09-30", 0, NULL, 1143)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (787, 4, 0, NULL, "2007-09-30", 0, NULL, 1144)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (788, 27.8473, 0, NULL, "2007-10-25", 0, NULL, 1145)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (789, 4, 0, NULL, "2007-10-25", 0, NULL, 1146)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (790, 24.7717, 0, NULL, "2008-00-20", 0, NULL, 1155)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (791, 490, 0, NULL, "2008-00-20", 0, NULL, 1156)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (792, 422, 0, NULL, "2008-01-24", 0, NULL, 1157)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (793, 21.6156, 0, NULL, "2008-01-24", 0, NULL, 1158)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (794, 11.4472, 1, "2009-01-24", "2008-01-24", 1, NULL, 1159)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (795, 5, 0, NULL, "2008-01-24", 0, NULL, 1160)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (796, 24.8884, 0, NULL, "2008-01-12", 0, NULL, 1161)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (797, 16.3545, 1, "2009-01-12", "2008-01-12", 1, NULL, 1162)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (798, 325, 0, NULL, "2008-01-12", 0, NULL, 1163)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (799, 3, 0, NULL, "2008-01-12", 0, NULL, 1164)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (800, 16.9225, 1, "2009-01-02", "2008-01-02", 1, NULL, 1165)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (801, 2, 0, NULL, "2008-01-02", 0, NULL, 1166)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (802, 465, 0, NULL, "2008-01-02", 0, NULL, 1167)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (803, 13.396, 1, "2009-01-31", "2008-01-31", 1, NULL, 1168)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (804, 25.133, 0, NULL, "2008-01-31", 0, NULL, 1169)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (805, 1, 0, NULL, "2008-01-31", 0, NULL, 1170)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (806, 378, 0, NULL, "2008-01-31", 0, NULL, 1171)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (807, 23.7217, 1, "2009-01-01", "2008-01-01", 1, NULL, 1172)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (808, 24.505, 0, NULL, "2008-01-01", 0, NULL, 1173)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (809, 2, 0, NULL, "2008-01-01", 0, NULL, 1174)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (810, 369, 0, NULL, "2008-01-01", 0, NULL, 1175)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (811, 20.4727, 0, NULL, "2008-02-28", 0, NULL, 1176)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (812, 1, 0, NULL, "2008-02-28", 0, NULL, 1177)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (813, 5.3807, 1, "2009-02-28", "2008-02-28", 1, NULL, 1178)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (814, 439, 0, NULL, "2008-02-28", 0, NULL, 1179)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (815, 5, 0, NULL, "2008-02-06", 0, NULL, 1184)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (816, 291, 0, NULL, "2008-02-06", 0, NULL, 1185)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (817, 18.578, 1, "2009-02-06", "2008-02-06", 1, NULL, 1186)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (818, 23.8386, 0, NULL, "2008-02-06", 0, NULL, 1187)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (819, 17.0144, 1, "2009-02-19", "2008-02-19", 1, NULL, 1188)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (820, 21.6529, 0, NULL, "2008-02-19", 0, NULL, 1189)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (821, 5, 0, NULL, "2008-03-22", 0, NULL, 1190)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (822, 343, 0, NULL, "2008-03-22", 0, NULL, 1191)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (823, 27.3352, 0, NULL, "2008-03-22", 0, NULL, 1192)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (824, 18.1809, 1, "2009-03-22", "2008-03-22", 1, NULL, 1193)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (825, 21.8148, 1, "2009-03-29", "2008-03-29", 1, NULL, 1194)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (826, 330, 0, NULL, "2008-03-29", 0, NULL, 1195)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (827, 28.8859, 0, NULL, "2008-03-29", 0, NULL, 1196)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (828, 1, 0, NULL, "2008-03-29", 0, NULL, 1197)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (829, 24.0447, 0, NULL, "2008-03-20", 0, NULL, 1198)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (830, 343, 0, NULL, "2008-03-20", 0, NULL, 1199)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (831, 2, 0, NULL, "2008-03-20", 0, NULL, 1200)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (832, 21.0991, 1, "2009-03-20", "2008-03-20", 1, NULL, 1201)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (833, 450, 0, NULL, "2008-03-05", 0, NULL, 1202)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (834, 4, 0, NULL, "2008-03-05", 0, NULL, 1203)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (835, 446, 0, NULL, "2008-03-04", 0, NULL, 1208)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (836, 22.595, 0, NULL, "2008-04-28", 0, NULL, 1210)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (837, 4, 0, NULL, "2008-04-28", 0, NULL, 1211)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (838, 25.6336, 0, NULL, "2008-05-02", 0, NULL, 1212)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (839, 23.7812, 1, "2009-06-18", "2008-06-18", 1, NULL, 1217)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (840, 418, 0, NULL, "2008-06-18", 0, NULL, 1218)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (841, 0, 0, NULL, "2008-06-18", 0, NULL, 1219)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (842, 28.1123, 0, NULL, "2008-06-18", 0, NULL, 1220)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (843, 249, 0, NULL, "2008-06-23", 0, NULL, 1221)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (844, 14.2187, 0, NULL, "2008-06-23", 0, NULL, 1222)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (845, 5, 0, NULL, "2008-06-23", 0, NULL, 1223)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (846, 15.482, 1, "2009-06-23", "2008-06-23", 1, NULL, 1224)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (847, 451, 0, NULL, "2008-07-30", 0, NULL, 1229)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (848, 21.4346, 1, "2009-07-30", "2008-07-30", 1, NULL, 1230)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (849, 1, 0, NULL, "2008-07-30", 0, NULL, 1231)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (850, 20.928, 0, NULL, "2008-07-30", 0, NULL, 1232)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (851, 5, 0, NULL, "2008-07-08", 0, NULL, 1233)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (852, 20.3993, 0, NULL, "2008-07-08", 0, NULL, 1234)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (853, 5, 0, NULL, "2008-07-01", 0, NULL, 1243)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (854, 24.0544, 0, NULL, "2008-08-17", 0, NULL, 1248)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (855, 287, 0, NULL, "2008-08-17", 0, NULL, 1249)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (856, 3, 0, NULL, "2008-08-17", 0, NULL, 1250)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (857, 13.5338, 1, "2009-08-17", "2008-08-17", 1, NULL, 1251)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (858, 22.924, 1, "2009-08-25", "2008-08-25", 1, NULL, 1252)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (859, 1, 0, NULL, "2008-08-25", 0, NULL, 1253)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (860, 27.1194, 0, NULL, "2008-08-25", 0, NULL, 1254)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (861, 469, 0, NULL, "2008-08-25", 0, NULL, 1255)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (862, 355, 0, NULL, "2008-09-06", 0, NULL, 1268)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (863, 22.5092, 1, "2009-09-06", "2008-09-06", 1, NULL, 1269)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (864, 3, 0, NULL, "2008-09-06", 0, NULL, 1270)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (865, 27.9701, 0, NULL, "2008-09-06", 0, NULL, 1271)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (866, 432, 0, NULL, "2008-10-01", 0, NULL, 1276)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (867, 14.3564, 1, "2009-10-01", "2008-10-01", 1, NULL, 1277)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (868, 20.8771, 0, NULL, "2008-10-01", 0, NULL, 1278)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (869, 1, 0, NULL, "2008-10-01", 0, NULL, 1279)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (870, 23.4364, 0, NULL, "2008-10-22", 0, NULL, 1284)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (871, 12.6516, 1, "2009-10-22", "2008-10-22", 1, NULL, 1285)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (872, 1, 0, NULL, "2008-10-22", 0, NULL, 1286)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (873, 319, 0, NULL, "2008-10-22", 0, NULL, 1287)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (874, 3, 0, NULL, "2008-10-23", 0, NULL, 1288)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (875, 283, 0, NULL, "2008-10-23", 0, NULL, 1289)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (876, 27.7845, 0, NULL, "2008-10-23", 0, NULL, 1290)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (877, 18.4014, 1, "2009-10-23", "2008-10-23", 1, NULL, 1291)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (878, 10.9298, 1, "20010-00-26", "2009-00-26", 1, NULL, 1296)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (879, 436, 0, NULL, "2009-00-26", 0, NULL, 1297)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (880, 1, 0, NULL, "2009-00-26", 0, NULL, 1298)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (881, 20.8484, 0, NULL, "2009-00-26", 0, NULL, 1299)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (882, 2, 0, NULL, "2009-00-24", 0, NULL, 1300)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (883, 356, 0, NULL, "2009-00-24", 0, NULL, 1301)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (884, 11.9829, 1, "20010-00-24", "2009-00-24", 1, NULL, 1302)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (885, 24.8186, 0, NULL, "2009-00-24", 0, NULL, 1303)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (886, 20.7382, 1, "20010-00-21", "2009-00-21", 1, NULL, 1304)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (887, 24.0674, 0, NULL, "2009-00-21", 0, NULL, 1305)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (888, 4, 0, NULL, "2009-00-21", 0, NULL, 1306)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (889, 403, 0, NULL, "2009-00-21", 0, NULL, 1307)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (890, 18.5097, 1, "20010-01-27", "2009-01-27", 1, NULL, 1308)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (891, 23.941, 0, NULL, "2009-01-27", 0, NULL, 1309)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (892, 314, 0, NULL, "2009-01-27", 0, NULL, 1310)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (893, 4, 0, NULL, "2009-01-27", 0, NULL, 1311)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (894, 21.5419, 0, NULL, "2009-02-17", 0, NULL, 1334)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (895, 13.957, 1, "20010-02-17", "2009-02-17", 1, NULL, 1335)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (896, 438, 0, NULL, "2009-02-17", 0, NULL, 1336)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (897, 3, 0, NULL, "2009-02-17", 0, NULL, 1337)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (898, 384, 0, NULL, "2009-02-13", 0, NULL, 1338)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (899, 1, 0, NULL, "2009-02-13", 0, NULL, 1339)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (900, 27.6321, 0, NULL, "2009-02-13", 0, NULL, 1340)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (901, 2, 0, NULL, "2009-02-15", 0, NULL, 1341)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (902, 24.5665, 0, NULL, "2009-02-24", 0, NULL, 1346)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (903, 384, 0, NULL, "2009-02-24", 0, NULL, 1347)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (904, 3, 0, NULL, "2009-02-24", 0, NULL, 1348)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (905, 22.6944, 1, "20010-02-24", "2009-02-24", 1, NULL, 1349)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (906, 485, 0, NULL, "2009-03-02", 0, NULL, 1353)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (907, 2, 0, NULL, "2009-03-02", 0, NULL, 1354)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (908, 26.2271, 0, NULL, "2009-03-02", 0, NULL, 1355)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (909, 13.5129, 1, "20010-03-02", "2009-03-02", 1, NULL, 1356)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (910, 29.8308, 0, NULL, "2009-04-27", 0, NULL, 1361)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (911, 469, 0, NULL, "2009-04-20", 0, NULL, 1363)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (912, 23.293, 0, NULL, "2009-04-20", 0, NULL, 1364)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (913, 13.6923, 1, "20010-04-20", "2009-04-20", 1, NULL, 1365)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (914, 5, 0, NULL, "2009-04-20", 0, NULL, 1366)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (915, 24.9004, 0, NULL, "2009-04-19", 0, NULL, 1367)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (916, 21.5262, 0, NULL, "2009-05-02", 0, NULL, 1369)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (917, 19.2118, 1, "20010-05-02", "2009-05-02", 1, NULL, 1370)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (918, 327, 0, NULL, "2009-05-02", 0, NULL, 1371)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (919, 3, 0, NULL, "2009-05-02", 0, NULL, 1372)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (920, 493, 0, NULL, "2009-06-07", 0, NULL, 1379)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (921, 348, 0, NULL, "2009-07-04", 0, NULL, 1380)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (922, 2, 0, NULL, "2009-07-04", 0, NULL, 1381)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (923, 19.4976, 0, NULL, "2009-07-04", 0, NULL, 1382)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (924, 16.361, 1, "20010-07-04", "2009-07-04", 1, NULL, 1383)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (925, 483, 0, NULL, "2009-07-18", 0, NULL, 1384)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (926, 20.7583, 1, "20010-07-18", "2009-07-18", 1, NULL, 1385)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (927, 26.9658, 0, NULL, "2009-07-18", 0, NULL, 1386)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (928, 2, 0, NULL, "2009-07-18", 0, NULL, 1387)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (929, 24.2274, 1, "20010-07-14", "2009-07-14", 1, NULL, 1388)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (930, 261, 0, NULL, "2009-07-14", 0, NULL, 1389)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (931, 5, 0, NULL, "2009-07-14", 0, NULL, 1390)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (932, 29.2617, 0, NULL, "2009-07-14", 0, NULL, 1391)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (933, 22.7362, 0, NULL, "2009-07-07", 0, NULL, 1392)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (934, 4, 0, NULL, "2009-07-07", 0, NULL, 1393)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (935, 470, 0, NULL, "2009-07-07", 0, NULL, 1394)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (936, 14.7954, 1, "20010-07-07", "2009-07-07", 1, NULL, 1395)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (937, 11.2881, 1, "20010-07-03", "2009-07-03", 1, NULL, 1396)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (938, 421, 0, NULL, "2009-07-03", 0, NULL, 1397)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (939, 5, 0, NULL, "2009-07-03", 0, NULL, 1398)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (940, 16.892, 0, NULL, "2009-07-03", 0, NULL, 1399)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (941, 3, 0, NULL, "2009-07-02", 0, NULL, 1400)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (942, 420, 0, NULL, "2009-07-02", 0, NULL, 1401)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (943, 23.1612, 0, NULL, "2009-07-02", 0, NULL, 1402)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (944, 12.4291, 1, "20010-07-02", "2009-07-02", 1, NULL, 1403)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (945, 29.8543, 0, NULL, "2009-08-21", 0, NULL, 1406)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (946, 22.0895, 1, "20010-08-21", "2009-08-21", 1, NULL, 1407)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (947, 285, 0, NULL, "2009-08-21", 0, NULL, 1408)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (948, 427, 0, NULL, "2009-08-17", 0, NULL, 1417)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (949, 17.5128, 0, NULL, "2009-08-17", 0, NULL, 1418)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (950, 10.5121, 1, "20010-08-17", "2009-08-17", 1, NULL, 1419)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (951, 21.5048, 1, "20010-09-31", "2009-09-31", 1, NULL, 1422)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (952, 2, 0, NULL, "2009-09-31", 0, NULL, 1423)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (953, 29.0901, 0, NULL, "2009-09-31", 0, NULL, 1424)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (954, 274, 0, NULL, "2009-09-31", 0, NULL, 1425)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (955, 4, 0, NULL, "2009-09-09", 0, NULL, 1426)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (956, 28.9754, 0, NULL, "2009-09-09", 0, NULL, 1427)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (957, 266, 0, NULL, "2009-09-09", 0, NULL, 1428)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (958, 1, 0, NULL, "2009-09-31", 0, NULL, 1431)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (959, 19.4461, 1, "20010-09-14", "2009-09-14", 1, NULL, 1432)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (960, 300, 0, NULL, "2009-09-14", 0, NULL, 1433)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (961, 21.0724, 0, NULL, "2009-09-14", 0, NULL, 1434)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (962, 5, 0, NULL, "2009-09-14", 0, NULL, 1435)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (963, 28.1641, 0, NULL, "2009-09-12", 0, NULL, 1436)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (964, 267, 0, NULL, "2009-09-26", 0, NULL, 1437)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (965, 3, 0, NULL, "2009-09-26", 0, NULL, 1438)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (966, 13.0733, 1, "20010-09-26", "2009-09-26", 1, NULL, 1439)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (967, 24.5631, 0, NULL, "2009-09-26", 0, NULL, 1440)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (968, 428, 0, NULL, "2009-11-18", 0, NULL, 1449)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (969, 24.2663, 0, NULL, "2009-11-18", 0, NULL, 1450)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (970, 21.8004, 1, "20010-11-18", "2009-11-18", 1, NULL, 1451)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (971, 4, 0, NULL, "2009-11-18", 0, NULL, 1452)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (972, 3, 0, NULL, "2009-11-02", 0, NULL, 1457)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (973, 19.4846, 1, "20010-11-02", "2009-11-02", 1, NULL, 1458)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (974, 20.1574, 0, NULL, "2009-11-02", 0, NULL, 1459)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (975, 278, 0, NULL, "2009-11-02", 0, NULL, 1460)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (976, 14.9432, 1, "20010-11-14", "2009-11-14", 1, NULL, 1461)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (977, 450, 0, NULL, "2009-11-14", 0, NULL, 1462)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (978, 2, 0, NULL, "2009-11-14", 0, NULL, 1463)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (979, 20.2977, 0, NULL, "2009-11-14", 0, NULL, 1464)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (980, 13.3304, 1, "20010-11-16", "2009-11-16", 1, NULL, 1465)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (981, 23.1744, 0, NULL, "2009-11-16", 0, NULL, 1466)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (982, 5, 0, NULL, "2009-11-16", 0, NULL, 1467)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (983, 331, 0, NULL, "2009-11-16", 0, NULL, 1468)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (984, 2, 0, NULL, "2009-11-27", 0, NULL, 1470)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (985, 10.5667, 1, "20010-11-27", "2009-11-27", 1, NULL, 1471)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (986, 23.6032, 0, NULL, "2009-11-27", 0, NULL, 1472)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (987, 467, 0, NULL, "2009-11-27", 0, NULL, 1473)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (988, 488, 0, NULL, "2010-00-09", 0, NULL, 1474)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (989, 2, 0, NULL, "2010-00-09", 0, NULL, 1475)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (990, 23.3269, 0, NULL, "2010-00-09", 0, NULL, 1476)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (991, 19.903, 1, "2011-00-09", "2010-00-09", 1, NULL, 1477)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (992, 1, 0, NULL, "2010-00-25", 0, NULL, 1478)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (993, 322, 0, NULL, "2010-00-25", 0, NULL, 1479)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (994, 21.8328, 0, NULL, "2010-00-25", 0, NULL, 1480)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (995, 14.2543, 1, "2011-00-25", "2010-00-25", 1, NULL, 1481)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (996, 29.8566, 0, NULL, "2010-01-04", 0, NULL, 1482)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (997, 315, 0, NULL, "2010-01-04", 0, NULL, 1483)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (998, 12.0603, 1, "2011-01-04", "2010-01-04", 1, NULL, 1484)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (999, 5, 0, NULL, "2010-01-04", 0, NULL, 1485)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1000, 11.9559, 1, "2011-01-08", "2010-01-08", 1, NULL, 1487)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1001, 253, 0, NULL, "2010-01-08", 0, NULL, 1488)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1002, 1, 0, NULL, "2010-01-08", 0, NULL, 1489)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1003, 29.3065, 0, NULL, "2010-01-08", 0, NULL, 1490)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1004, 10.4324, 1, "2011-01-03", "2010-01-03", 1, NULL, 1491)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1005, 449, 0, NULL, "2010-01-03", 0, NULL, 1492)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1006, 22.5561, 0, NULL, "2010-01-03", 0, NULL, 1493)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1007, 3, 0, NULL, "2010-01-03", 0, NULL, 1494)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1008, 3, 0, NULL, "2010-01-22", 0, NULL, 1495)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1009, 268, 0, NULL, "2010-01-22", 0, NULL, 1496)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1010, 14.9458, 1, "2011-01-22", "2010-01-22", 1, NULL, 1497)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1011, 26.9536, 0, NULL, "2010-01-22", 0, NULL, 1498)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1012, 22.1686, 0, NULL, "2010-02-19", 0, NULL, 1499)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1013, 292, 0, NULL, "2010-02-19", 0, NULL, 1500)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1014, 17.4796, 1, "2011-02-10", "2010-02-10", 1, NULL, 1505)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1015, 23.5817, 0, NULL, "2010-02-10", 0, NULL, 1506)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1016, 367, 0, NULL, "2010-02-10", 0, NULL, 1507)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1017, 2, 0, NULL, "2010-02-10", 0, NULL, 1508)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1018, 333, 0, NULL, "2010-03-01", 0, NULL, 1513)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1019, 25.8242, 0, NULL, "2010-03-01", 0, NULL, 1514)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1020, 23.5923, 1, "2011-03-01", "2010-03-01", 1, NULL, 1515)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1021, 4, 0, NULL, "2010-03-01", 0, NULL, 1516)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1022, 27.0562, 0, NULL, "2010-04-09", 0, NULL, 1517)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1023, 13.7134, 1, "2011-04-09", "2010-04-09", 1, NULL, 1518)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1024, 4, 0, NULL, "2010-04-09", 0, NULL, 1519)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1025, 423, 0, NULL, "2010-04-09", 0, NULL, 1520)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1026, 28.6269, 0, NULL, "2010-05-30", 0, NULL, 1521)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1027, 5, 0, NULL, "2010-05-30", 0, NULL, 1522)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1028, 14.3523, 1, "2011-05-30", "2010-05-30", 1, NULL, 1523)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1029, 325, 0, NULL, "2010-05-30", 0, NULL, 1524)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1030, 429, 0, NULL, "2010-06-29", 0, NULL, 1526)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1031, 3, 0, NULL, "2010-06-29", 0, NULL, 1527)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1032, 15.9777, 1, "2011-06-29", "2010-06-29", 1, NULL, 1528)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1033, 22.0013, 0, NULL, "2010-06-29", 0, NULL, 1529)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1034, 23.9847, 0, NULL, "2010-06-24", 0, NULL, 1534)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1035, 5, 0, NULL, "2010-07-17", 0, NULL, 1535)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1036, 14.5878, 1, "2011-07-17", "2010-07-17", 1, NULL, 1536)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1037, 270, 0, NULL, "2010-07-17", 0, NULL, 1537)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1038, 28.3126, 0, NULL, "2010-07-17", 0, NULL, 1538)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1039, 23.0563, 0, NULL, "2010-07-28", 0, NULL, 1541)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1040, 320, 0, NULL, "2010-07-28", 0, NULL, 1542)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1041, 2, 0, NULL, "2010-07-28", 0, NULL, 1543)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1042, 10.6238, 1, "2011-07-28", "2010-07-28", 1, NULL, 1544)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1043, 348, 0, NULL, "2010-07-19", 0, NULL, 1545)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1044, 4, 0, NULL, "2010-07-19", 0, NULL, 1546)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1045, 26.5306, 0, NULL, "2010-07-19", 0, NULL, 1547)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1046, 17.1034, 1, "2011-07-19", "2010-07-19", 1, NULL, 1548)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1047, 21.1924, 1, "2011-07-05", "2010-07-05", 1, NULL, 1553)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1048, 1, 0, NULL, "2010-07-05", 0, NULL, 1554)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1049, 3, 0, NULL, "2010-08-30", 0, NULL, 1563)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1050, 26.2988, 0, NULL, "2010-08-30", 0, NULL, 1564)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1051, 301, 0, NULL, "2010-08-30", 0, NULL, 1565)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1052, 21.2856, 1, "2011-08-30", "2010-08-30", 1, NULL, 1566)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1053, 29.1972, 0, NULL, "2010-08-15", 0, NULL, 1571)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1054, 11.3783, 1, "2011-08-15", "2010-08-15", 1, NULL, 1572)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1055, 4, 0, NULL, "2010-08-15", 0, NULL, 1573)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1056, 356, 0, NULL, "2010-08-15", 0, NULL, 1574)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1057, 12.2616, 1, "2011-08-12", "2010-08-12", 1, NULL, 1575)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1058, 334, 0, NULL, "2010-08-12", 0, NULL, 1576)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1059, 20.6261, 0, NULL, "2010-08-12", 0, NULL, 1577)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1060, 2, 0, NULL, "2010-08-12", 0, NULL, 1578)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1061, 2, 0, NULL, "2010-08-24", 0, NULL, 1579)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1062, 16.7325, 1, "2011-08-24", "2010-08-24", 1, NULL, 1580)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1063, 466, 0, NULL, "2010-09-28", 0, NULL, 1581)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1064, 21.4606, 0, NULL, "2010-09-28", 0, NULL, 1582)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1065, 5, 0, NULL, "2010-09-28", 0, NULL, 1583)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1066, 19.0042, 1, "2011-09-28", "2010-09-28", 1, NULL, 1584)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1067, 26.3858, 0, NULL, "2010-10-04", 0, NULL, 1585)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1068, 15.7615, 1, "2011-10-04", "2010-10-04", 1, NULL, 1586)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1069, 260, 0, NULL, "2010-10-28", 0, NULL, 1587)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1070, 28.3009, 0, NULL, "2010-10-28", 0, NULL, 1588)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1071, 22.0142, 1, "2011-10-28", "2010-10-28", 1, NULL, 1589)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1072, 5, 0, NULL, "2010-10-28", 0, NULL, 1590)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1073, 0, 0, NULL, "2010-10-11", 0, NULL, 1591)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1074, 379, 0, NULL, "2010-10-11", 0, NULL, 1592)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1075, 13.7345, 1, "2011-11-03", "2010-11-03", 1, NULL, 1597)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1076, 29.1728, 0, NULL, "2010-11-05", 0, NULL, 1598)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1077, 282, 0, NULL, "2010-11-05", 0, NULL, 1599)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1078, 5, 0, NULL, "2010-11-05", 0, NULL, 1600)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1079, 266, 0, NULL, "2010-11-15", 0, NULL, 1605)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1080, 3, 0, NULL, "2010-11-15", 0, NULL, 1606)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1081, 16.5999, 1, "2011-11-15", "2010-11-15", 1, NULL, 1607)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1082, 22.4464, 0, NULL, "2010-11-15", 0, NULL, 1608)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1083, 22.1316, 0, NULL, "2010-11-22", 0, NULL, 1609)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1084, 13.1082, 1, "2011-11-22", "2010-11-22", 1, NULL, 1610)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1085, 304, 0, NULL, "2010-11-22", 0, NULL, 1611)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1086, 5, 0, NULL, "2010-11-22", 0, NULL, 1612)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1087, 319, 0, NULL, "2011-00-13", 0, NULL, 1621)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1088, 2, 0, NULL, "2011-00-13", 0, NULL, 1622)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1089, 17.3545, 1, "2012-00-13", "2011-00-13", 1, NULL, 1623)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1090, 20.9529, 0, NULL, "2011-00-13", 0, NULL, 1624)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1091, 23.66, 0, NULL, "2011-00-25", 0, NULL, 1625)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1092, 3, 0, NULL, "2011-00-25", 0, NULL, 1626)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1093, 17.0607, 1, "2012-00-25", "2011-00-25", 1, NULL, 1627)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1094, 357, 0, NULL, "2011-00-25", 0, NULL, 1628)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1095, 4, 0, NULL, "2011-00-27", 0, NULL, 1629)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1096, 24.6253, 0, NULL, "2011-00-27", 0, NULL, 1630)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1097, 322, 0, NULL, "2011-00-27", 0, NULL, 1631)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1098, 10.3218, 1, "2012-00-27", "2011-00-27", 1, NULL, 1632)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1099, 21.2126, 0, NULL, "2011-00-09", 0, NULL, 1633)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1100, 2, 0, NULL, "2011-00-09", 0, NULL, 1634)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1101, 23.2636, 1, "2012-00-09", "2011-00-09", 1, NULL, 1635)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1102, 23.4641, 0, NULL, "2011-01-08", 0, NULL, 1640)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1103, 1, 0, NULL, "2011-01-08", 0, NULL, 1641)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1104, 479, 0, NULL, "2011-01-08", 0, NULL, 1642)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1105, 24.7377, 1, "2012-01-08", "2011-01-08", 1, NULL, 1643)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1106, 374, 0, NULL, "2011-02-15", 0, NULL, 1647)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1107, 3, 0, NULL, "2011-02-15", 0, NULL, 1648)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1108, 14.4232, 1, "2012-02-15", "2011-02-15", 1, NULL, 1649)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1109, 23.2471, 0, NULL, "2011-02-15", 0, NULL, 1650)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1110, 18.3146, 1, "2012-02-18", "2011-02-18", 1, NULL, 1651)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1111, 26.4389, 0, NULL, "2011-02-18", 0, NULL, 1652)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1112, 2, 0, NULL, "2011-02-07", 0, NULL, 1653)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1113, 28.3578, 0, NULL, "2011-02-07", 0, NULL, 1654)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1114, 425, 0, NULL, "2011-02-07", 0, NULL, 1655)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1115, 414, 0, NULL, "2011-02-19", 0, NULL, 1656)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1116, 2, 0, NULL, "2011-02-19", 0, NULL, 1657)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1117, 27.9177, 0, NULL, "2011-02-19", 0, NULL, 1658)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1118, 15.5448, 1, "2012-02-19", "2011-02-19", 1, NULL, 1659)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1119, 3, 0, NULL, "2011-03-23", 0, NULL, 1664)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1120, 315, 0, NULL, "2011-03-23", 0, NULL, 1665)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1121, 19.1061, 1, "2012-03-23", "2011-03-23", 1, NULL, 1666)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1122, 28.5225, 0, NULL, "2011-03-23", 0, NULL, 1667)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1123, 457, 0, NULL, "2011-03-15", 0, NULL, 1668)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1124, 17.8651, 1, "2012-03-15", "2011-03-15", 1, NULL, 1669)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1125, 1, 0, NULL, "2011-03-15", 0, NULL, 1670)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1126, 14.1824, 0, NULL, "2011-03-15", 0, NULL, 1671)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1127, 13.1949, 1, "2012-03-17", "2011-03-17", 1, NULL, 1672)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1128, 11.6839, 0, NULL, "2011-03-17", 0, NULL, 1673)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1129, 3, 0, NULL, "2011-03-17", 0, NULL, 1674)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1130, 423, 0, NULL, "2011-03-17", 0, NULL, 1675)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1131, 16.1821, 1, "2012-04-16", "2011-04-16", 1, NULL, 1680)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1132, 327, 0, NULL, "2011-04-16", 0, NULL, 1681)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1133, 1, 0, NULL, "2011-04-16", 0, NULL, 1682)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1134, 22.7716, 0, NULL, "2011-04-16", 0, NULL, 1683)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1135, 21.6608, 0, NULL, "2011-05-20", 0, NULL, 1686)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1136, 17.6859, 1, "2012-05-20", "2011-05-20", 1, NULL, 1687)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1137, 4, 0, NULL, "2011-05-20", 0, NULL, 1688)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1138, 438, 0, NULL, "2011-05-20", 0, NULL, 1689)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1139, 3, 0, NULL, "2011-05-26", 0, NULL, 1690)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1140, 391, 0, NULL, "2011-05-26", 0, NULL, 1691)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1141, 24.547, 0, NULL, "2011-05-26", 0, NULL, 1692)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1142, 12.7491, 1, "2012-05-26", "2011-05-26", 1, NULL, 1693)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1143, 386, 0, NULL, "2011-05-24", 0, NULL, 1694)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1144, 5, 0, NULL, "2011-05-24", 0, NULL, 1695)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1145, 23.7452, 0, NULL, "2011-05-24", 0, NULL, 1696)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1146, 14.436, 1, "2012-05-24", "2011-05-24", 1, NULL, 1697)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1147, 26.1547, 0, NULL, "2011-05-27", 0, NULL, 1698)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1148, 3, 0, NULL, "2011-05-27", 0, NULL, 1699)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1149, 22.5066, 1, "2012-05-27", "2011-05-27", 1, NULL, 1700)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1150, 303, 0, NULL, "2011-05-27", 0, NULL, 1701)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1151, 352, 0, NULL, "2011-06-28", 0, NULL, 1702)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1152, 16.5183, 1, "2012-06-28", "2011-06-28", 1, NULL, 1703)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1153, 22.3384, 0, NULL, "2011-06-28", 0, NULL, 1704)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1154, 5, 0, NULL, "2011-06-28", 0, NULL, 1705)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1155, 26.3615, 0, NULL, "2011-06-13", 0, NULL, 1706)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1156, 415, 0, NULL, "2011-06-13", 0, NULL, 1707)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1157, 1, 0, NULL, "2011-06-13", 0, NULL, 1708)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1158, 11.6794, 1, "2012-06-13", "2011-06-13", 1, NULL, 1709)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1159, 24.5221, 0, NULL, "2011-06-01", 0, NULL, 1711)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1160, 3, 0, NULL, "2011-06-01", 0, NULL, 1712)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1161, 15.6821, 1, "2012-06-01", "2011-06-01", 1, NULL, 1713)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1162, 500, 0, NULL, "2011-06-01", 0, NULL, 1714)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1163, 21.7575, 0, NULL, "2011-07-12", 0, NULL, 1715)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1164, 16.0129, 1, "2012-07-12", "2011-07-12", 1, NULL, 1716)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1165, 362, 0, NULL, "2011-07-12", 0, NULL, 1717)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1166, 4, 0, NULL, "2011-07-12", 0, NULL, 1718)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1167, 345, 0, NULL, "2011-08-15", 0, NULL, 1725)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1168, 22.8935, 0, NULL, "2011-08-28", 0, NULL, 1726)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1169, 2, 0, NULL, "2011-08-28", 0, NULL, 1727)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1170, 17.4207, 1, "2012-08-28", "2011-08-28", 1, NULL, 1728)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1171, 443, 0, NULL, "2011-08-28", 0, NULL, 1729)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1172, 19.2613, 1, "2012-08-18", "2011-08-18", 1, NULL, 1730)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1173, 22.1131, 0, NULL, "2011-08-18", 0, NULL, 1731)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1174, 417, 0, NULL, "2011-08-18", 0, NULL, 1732)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1175, 0, 0, NULL, "2011-08-18", 0, NULL, 1733)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1176, 429, 0, NULL, "2011-09-29", 0, NULL, 1742)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1177, 21.309, 0, NULL, "2011-09-29", 0, NULL, 1743)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1178, 4, 0, NULL, "2011-09-29", 0, NULL, 1744)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1179, 16.9776, 1, "2012-09-29", "2011-09-29", 1, NULL, 1745)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1180, 4, 0, NULL, "2011-09-19", 0, NULL, 1746)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1181, 368, 0, NULL, "2011-09-19", 0, NULL, 1747)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1182, 27.7191, 0, NULL, "2011-09-19", 0, NULL, 1748)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1183, 22.1073, 1, "2012-09-19", "2011-09-19", 1, NULL, 1749)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1184, 358, 0, NULL, "2011-10-16", 0, NULL, 1750)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1185, 11.2326, 1, "2012-10-16", "2011-10-16", 1, NULL, 1751)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1186, 29.0545, 0, NULL, "2011-10-16", 0, NULL, 1752)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1187, 1, 0, NULL, "2011-10-16", 0, NULL, 1753)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1188, 13.0649, 1, "2012-10-15", "2011-10-15", 1, NULL, 1754)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1189, 304, 0, NULL, "2011-10-15", 0, NULL, 1755)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1190, 22.0267, 0, NULL, "2011-10-15", 0, NULL, 1756)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1191, 3, 0, NULL, "2011-10-15", 0, NULL, 1757)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1192, 263, 0, NULL, "2011-10-12", 0, NULL, 1766)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1193, 20.5917, 1, "2012-10-12", "2011-10-12", 1, NULL, 1767)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1194, 26.3259, 0, NULL, "2011-10-12", 0, NULL, 1768)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1195, 4, 0, NULL, "2011-10-12", 0, NULL, 1769)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1196, 3, 0, NULL, "2011-10-10", 0, NULL, 1770)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1197, 318, 0, NULL, "2011-11-16", 0, NULL, 1771)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1198, 2, 0, NULL, "2011-11-16", 0, NULL, 1772)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1199, 20.1094, 0, NULL, "2011-11-16", 0, NULL, 1773)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1200, 22.1064, 1, "2012-11-16", "2011-11-16", 1, NULL, 1774)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1201, 1, 0, NULL, "2011-11-06", 0, NULL, 1776)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1202, 21.5905, 1, "2012-11-06", "2011-11-06", 1, NULL, 1777)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1203, 396, 0, NULL, "2011-11-06", 0, NULL, 1778)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1204, 1, 0, NULL, "2011-11-21", 0, NULL, 1779)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1205, 18.3376, 1, "2012-11-21", "2011-11-21", 1, NULL, 1780)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1206, 447, 0, NULL, "2011-11-21", 0, NULL, 1781)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1207, 24.3046, 0, NULL, "2011-11-21", 0, NULL, 1782)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1208, 4, 0, NULL, "2012-00-08", 0, NULL, 1787)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1209, 494, 0, NULL, "2012-00-08", 0, NULL, 1788)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1210, 27.2286, 0, NULL, "2012-00-08", 0, NULL, 1789)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1211, 21.7641, 1, "2013-00-08", "2012-00-08", 1, NULL, 1790)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1212, 2, 0, NULL, "2012-00-13", 0, NULL, 1791)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1213, 23.9187, 0, NULL, "2012-00-13", 0, NULL, 1792)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1214, 402, 0, NULL, "2012-00-13", 0, NULL, 1793)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1215, 10.3094, 1, "2013-00-13", "2012-00-13", 1, NULL, 1794)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1216, 29.584, 0, NULL, "2012-00-16", 0, NULL, 1795)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1217, 24.4125, 1, "2013-00-16", "2012-00-16", 1, NULL, 1796)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1218, 431, 0, NULL, "2012-00-16", 0, NULL, 1797)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1219, 2, 0, NULL, "2012-00-16", 0, NULL, 1798)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1220, 467, 0, NULL, "2012-00-18", 0, NULL, 1799)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1221, 29.9586, 0, NULL, "2012-00-18", 0, NULL, 1800)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1222, 277, 0, NULL, "2012-00-17", 0, NULL, 1801)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1223, 3, 0, NULL, "2012-01-07", 0, NULL, 1802)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1224, 22.5084, 0, NULL, "2012-01-07", 0, NULL, 1803)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1225, 12.5956, 1, "2013-01-07", "2012-01-07", 1, NULL, 1804)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1226, 270, 0, NULL, "2012-01-07", 0, NULL, 1805)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1227, 22.1529, 0, NULL, "2012-01-26", 0, NULL, 1806)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1228, 19.598, 1, "2013-01-26", "2012-01-26", 1, NULL, 1807)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1229, 365, 0, NULL, "2012-01-26", 0, NULL, 1808)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1230, 5, 0, NULL, "2012-01-26", 0, NULL, 1809)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1231, 26.2913, 0, NULL, "2012-01-01", 0, NULL, 1810)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1232, 21.844, 1, "2013-01-01", "2012-01-01", 1, NULL, 1811)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1233, 2, 0, NULL, "2012-01-01", 0, NULL, 1812)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1234, 293, 0, NULL, "2012-01-01", 0, NULL, 1813)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1235, 17.3525, 1, "2013-02-23", "2012-02-23", 1, NULL, 1823)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1236, 297, 0, NULL, "2012-02-23", 0, NULL, 1824)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1237, 23.0853, 0, NULL, "2012-02-23", 0, NULL, 1825)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1238, 1, 0, NULL, "2012-02-23", 0, NULL, 1826)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1239, 21.7978, 0, NULL, "2012-03-13", 0, NULL, 1830)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1240, 14.3392, 1, "2013-03-13", "2012-03-13", 1, NULL, 1831)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1241, 310, 0, NULL, "2012-03-11", 0, NULL, 1835)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1242, 4, 0, NULL, "2012-03-11", 0, NULL, 1836)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1243, 18.1458, 1, "2013-03-06", "2012-03-06", 1, NULL, 1837)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1244, 1, 0, NULL, "2012-03-06", 0, NULL, 1838)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1245, 28.0898, 0, NULL, "2012-03-06", 0, NULL, 1839)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1246, 284, 0, NULL, "2012-03-06", 0, NULL, 1840)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1247, 28.638, 0, NULL, "2012-04-07", 0, NULL, 1849)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1248, 5, 0, NULL, "2012-04-07", 0, NULL, 1850)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1249, 338, 0, NULL, "2012-04-07", 0, NULL, 1851)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1250, 14.3953, 1, "2013-04-07", "2012-04-07", 1, NULL, 1852)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1251, 27.6963, 0, NULL, "2012-04-11", 0, NULL, 1853)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1252, 315, 0, NULL, "2012-04-11", 0, NULL, 1854)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1253, 4, 0, NULL, "2012-04-11", 0, NULL, 1855)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1254, 16.1737, 1, "2013-04-11", "2012-04-11", 1, NULL, 1856)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1255, 285, 0, NULL, "2012-04-28", 0, NULL, 1857)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1256, 5, 0, NULL, "2012-04-28", 0, NULL, 1858)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1257, 29.844, 0, NULL, "2012-04-28", 0, NULL, 1859)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1258, 15.9592, 1, "2013-04-28", "2012-04-28", 1, NULL, 1860)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1259, 478, 0, NULL, "2012-04-03", 0, NULL, 1861)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1260, 2, 0, NULL, "2012-04-03", 0, NULL, 1862)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1261, 18.63, 1, "2013-04-03", "2012-04-03", 1, NULL, 1863)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1262, 29.3723, 0, NULL, "2012-05-10", 0, NULL, 1869)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1263, 3, 0, NULL, "2012-05-10", 0, NULL, 1870)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1264, 24.476, 1, "2013-05-10", "2012-05-10", 1, NULL, 1871)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1265, 383, 0, NULL, "2012-05-10", 0, NULL, 1872)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1266, 25.5993, 0, NULL, "2012-05-22", 0, NULL, 1878)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1267, 3, 0, NULL, "2012-05-22", 0, NULL, 1879)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1268, 20.907, 1, "2013-05-22", "2012-05-22", 1, NULL, 1880)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1269, 2, 0, NULL, "2012-06-19", 0, NULL, 1881)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1270, 24.2765, 1, "2013-06-19", "2012-06-19", 1, NULL, 1882)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1271, 28.8947, 0, NULL, "2012-06-19", 0, NULL, 1883)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1272, 449, 0, NULL, "2012-06-19", 0, NULL, 1884)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1273, 25.5438, 0, NULL, "2012-06-27", 0, NULL, 1885)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1274, 23.2552, 1, "2013-06-27", "2012-06-27", 1, NULL, 1886)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1275, 306, 0, NULL, "2012-06-27", 0, NULL, 1887)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1276, 2, 0, NULL, "2012-06-04", 0, NULL, 1888)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1277, 29.6468, 0, NULL, "2012-06-04", 0, NULL, 1889)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1278, 19.9726, 1, "2013-06-04", "2012-06-04", 1, NULL, 1890)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1279, 271, 0, NULL, "2012-06-04", 0, NULL, 1891)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1280, 27.8411, 0, NULL, "2012-06-17", 0, NULL, 1892)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1281, 390, 0, NULL, "2012-06-17", 0, NULL, 1893)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1282, 5, 0, NULL, "2012-06-17", 0, NULL, 1894)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1283, 10.6277, 1, "2013-06-17", "2012-06-17", 1, NULL, 1895)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1284, 23.554, 0, NULL, "2012-06-21", 0, NULL, 1896)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1285, 1, 0, NULL, "2012-06-21", 0, NULL, 1897)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1286, 13.2691, 1, "2013-06-21", "2012-06-21", 1, NULL, 1898)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1287, 280, 0, NULL, "2012-06-21", 0, NULL, 1899)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1288, 243, 0, NULL, "2012-07-18", 0, NULL, 1901)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1289, 27.5144, 0, NULL, "2012-07-18", 0, NULL, 1902)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1290, 4, 0, NULL, "2012-07-18", 0, NULL, 1903)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1291, 24.2532, 1, "2013-07-18", "2012-07-18", 1, NULL, 1904)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1292, 22.1679, 0, NULL, "2012-07-31", 0, NULL, 1905)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1293, 11.2249, 1, "2013-07-31", "2012-07-31", 1, NULL, 1906)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1294, 414, 0, NULL, "2012-07-11", 0, NULL, 1907)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1295, 22.786, 0, NULL, "2012-07-11", 0, NULL, 1908)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1296, 22.4429, 1, "2013-07-11", "2012-07-11", 1, NULL, 1909)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1297, 3, 0, NULL, "2012-07-11", 0, NULL, 1910)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1298, 25.157, 0, NULL, "2012-07-02", 0, NULL, 1911)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1299, 1, 0, NULL, "2012-07-02", 0, NULL, 1912)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1300, 454, 0, NULL, "2012-07-02", 0, NULL, 1913)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1301, 10.1636, 1, "2013-07-02", "2012-07-02", 1, NULL, 1914)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1302, 14.1831, 1, "2013-07-06", "2012-07-06", 1, NULL, 1915)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1303, 312, 0, NULL, "2012-07-06", 0, NULL, 1916)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1304, 1, 0, NULL, "2012-07-06", 0, NULL, 1917)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1305, 29.5872, 0, NULL, "2012-07-06", 0, NULL, 1918)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1306, 447, 0, NULL, "2012-07-10", 0, NULL, 1919)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1307, 3, 0, NULL, "2012-07-10", 0, NULL, 1920)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1308, 11.0503, 1, "2013-07-10", "2012-07-10", 1, NULL, 1921)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1309, 14.8733, 1, "2013-08-19", "2012-08-19", 1, NULL, 1922)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1310, 412, 0, NULL, "2012-08-19", 0, NULL, 1923)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1311, 5, 0, NULL, "2012-08-19", 0, NULL, 1924)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1312, 23.7824, 0, NULL, "2012-08-19", 0, NULL, 1925)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1313, 29.5744, 0, NULL, "2012-08-08", 0, NULL, 1926)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1314, 422, 0, NULL, "2012-08-08", 0, NULL, 1927)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1315, 15.4746, 1, "2013-08-08", "2012-08-08", 1, NULL, 1928)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1316, 0, 0, NULL, "2012-08-29", 0, NULL, 1929)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1317, 20.9689, 0, NULL, "2012-08-29", 0, NULL, 1930)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1318, 435, 0, NULL, "2012-08-11", 0, NULL, 1931)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1319, 2, 0, NULL, "2012-08-04", 0, NULL, 1932)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1320, 21.9314, 1, "2013-08-04", "2012-08-04", 1, NULL, 1933)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1321, 10.6287, 0, NULL, "2012-08-04", 0, NULL, 1934)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1322, 323, 0, NULL, "2012-08-04", 0, NULL, 1935)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1323, 1, 0, NULL, "2012-08-22", 0, NULL, 1936)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1324, 369, 0, NULL, "2012-08-22", 0, NULL, 1937)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1325, 19.0905, 1, "2013-08-22", "2012-08-22", 1, NULL, 1938)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1326, 24.109, 0, NULL, "2012-08-22", 0, NULL, 1939)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1327, 2, 0, NULL, "2012-09-22", 0, NULL, 1941)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1328, 265, 0, NULL, "2012-09-22", 0, NULL, 1942)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1329, 22.6179, 1, "2013-09-22", "2012-09-22", 1, NULL, 1943)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1330, 25.4918, 0, NULL, "2012-09-22", 0, NULL, 1944)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1331, 14.5997, 1, "2013-09-18", "2012-09-18", 1, NULL, 1945)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1332, 384, 0, NULL, "2012-09-18", 0, NULL, 1946)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1333, 27.2632, 0, NULL, "2012-09-18", 0, NULL, 1947)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1334, 5, 0, NULL, "2012-09-18", 0, NULL, 1948)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1335, 23.2964, 0, NULL, "2012-09-30", 0, NULL, 1955)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1336, 453, 0, NULL, "2012-09-30", 0, NULL, 1956)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1337, 9.0958, 1, "2013-09-30", "2012-09-30", 1, NULL, 1957)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1338, 1, 0, NULL, "2012-09-30", 0, NULL, 1958)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1339, 29.3071, 0, NULL, "2012-10-13", 0, NULL, 1963)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1340, 18.1789, 1, "2013-10-13", "2012-10-13", 1, NULL, 1964)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1341, 4, 0, NULL, "2012-10-13", 0, NULL, 1965)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1342, 349, 0, NULL, "2012-10-13", 0, NULL, 1966)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1343, 439, 0, NULL, "2012-11-06", 0, NULL, 1972)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1344, 2, 0, NULL, "2012-11-06", 0, NULL, 1973)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1345, 23.084, 0, NULL, "2012-11-19", 0, NULL, 1974)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1346, 347, 0, NULL, "2012-11-19", 0, NULL, 1975)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1347, 15.431, 1, "2013-11-19", "2012-11-19", 1, NULL, 1976)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1348, 1, 0, NULL, "2012-11-19", 0, NULL, 1977)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1349, 28.1001, 0, NULL, "2012-11-31", 0, NULL, 1978)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1350, 11.1707, 1, "2013-11-31", "2012-11-31", 1, NULL, 1979)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1351, 2, 0, NULL, "2012-11-31", 0, NULL, 1980)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1352, 417, 0, NULL, "2012-11-31", 0, NULL, 1981)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1353, 2, 0, NULL, "2013-00-24", 0, NULL, 1984)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1354, 447, 0, NULL, "2013-00-24", 0, NULL, 1985)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1355, 19.8875, 1, "2014-00-24", "2013-00-24", 1, NULL, 1986)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1356, 23.6846, 0, NULL, "2013-00-24", 0, NULL, 1987)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1357, 16.9264, 1, "2014-00-30", "2013-00-30", 1, NULL, 1988)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1358, 5, 0, NULL, "2013-00-30", 0, NULL, 1989)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1359, 3, 0, NULL, "2013-00-30", 0, NULL, 1990)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1360, 23.5356, 1, "2014-00-30", "2013-00-30", 1, NULL, 1991)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1361, 28.1396, 0, NULL, "2013-00-30", 0, NULL, 1992)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1362, 3, 0, NULL, "2013-00-24", 0, NULL, 1999)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1363, 25.7269, 0, NULL, "2013-00-24", 0, NULL, 2000)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1364, 20.5176, 1, "2014-01-17", "2013-01-17", 1, NULL, 2001)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1365, 28.3313, 0, NULL, "2013-01-17", 0, NULL, 2002)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1366, 263, 0, NULL, "2013-01-17", 0, NULL, 2003)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1367, 3, 0, NULL, "2013-01-17", 0, NULL, 2004)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1368, 24.1188, 0, NULL, "2013-02-16", 0, NULL, 2009)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1369, 21.8886, 1, "2014-02-16", "2013-02-16", 1, NULL, 2010)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1370, 400, 0, NULL, "2013-02-16", 0, NULL, 2011)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1371, 2, 0, NULL, "2013-02-16", 0, NULL, 2012)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1372, 459, 0, NULL, "2013-02-22", 0, NULL, 2013)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1373, 27.8996, 0, NULL, "2013-03-12", 0, NULL, 2014)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1374, 22.7833, 1, "2014-03-12", "2013-03-12", 1, NULL, 2015)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1375, 271, 0, NULL, "2013-03-12", 0, NULL, 2016)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1376, 2, 0, NULL, "2013-03-12", 0, NULL, 2017)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1377, 1, 0, NULL, "2013-04-19", 0, NULL, 2026)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1378, 29.047, 0, NULL, "2013-04-19", 0, NULL, 2027)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1379, 13.8029, 1, "2014-04-19", "2013-04-19", 1, NULL, 2028)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1380, 255, 0, NULL, "2013-04-19", 0, NULL, 2029)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1381, 328, 0, NULL, "2013-04-16", 0, NULL, 2030)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1382, 26.0576, 0, NULL, "2013-04-16", 0, NULL, 2031)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1383, 4, 0, NULL, "2013-04-13", 0, NULL, 2032)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1384, 28.911, 0, NULL, "2013-04-13", 0, NULL, 2033)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1385, 11.1813, 1, "2014-04-13", "2013-04-13", 1, NULL, 2034)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1386, 286, 0, NULL, "2013-04-13", 0, NULL, 2035)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1387, 280, 0, NULL, "2013-04-26", 0, NULL, 2036)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1388, 21.165, 0, NULL, "2013-04-26", 0, NULL, 2037)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1389, 22.4491, 1, "2014-04-26", "2013-04-26", 1, NULL, 2038)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1390, 1, 0, NULL, "2013-04-26", 0, NULL, 2039)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1391, 5, 0, NULL, "2013-04-05", 0, NULL, 2040)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1392, 27.952, 0, NULL, "2013-04-05", 0, NULL, 2041)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1393, 467, 0, NULL, "2013-04-05", 0, NULL, 2042)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1394, 23.3243, 1, "2014-04-05", "2013-04-05", 1, NULL, 2043)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1395, 22.1312, 1, "2014-05-09", "2013-05-09", 1, NULL, 2044)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1396, 22.1882, 0, NULL, "2013-05-29", 0, NULL, 2045)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1397, 21.0472, 1, "2014-05-29", "2013-05-29", 1, NULL, 2046)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1398, 2, 0, NULL, "2013-05-29", 0, NULL, 2047)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1399, 335, 0, NULL, "2013-05-29", 0, NULL, 2048)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1400, 24.0848, 0, NULL, "2013-05-20", 0, NULL, 2049)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1401, 18.8487, 1, "2014-05-20", "2013-05-20", 1, NULL, 2050)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1402, 251, 0, NULL, "2013-05-20", 0, NULL, 2051)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1403, 2, 0, NULL, "2013-05-20", 0, NULL, 2052)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1404, 7.8174, 1, "2014-05-23", "2013-05-23", 1, NULL, 2053)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1405, 2, 0, NULL, "2013-05-23", 0, NULL, 2054)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1406, 302, 0, NULL, "2013-05-23", 0, NULL, 2055)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1407, 23.0789, 0, NULL, "2013-05-23", 0, NULL, 2056)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1408, 4, 0, NULL, "2013-05-01", 0, NULL, 2057)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1409, 21.0502, 0, NULL, "2013-05-01", 0, NULL, 2058)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1410, 474, 0, NULL, "2013-05-01", 0, NULL, 2059)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1411, 7.4643, 1, "2014-05-01", "2013-05-01", 1, NULL, 2060)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1412, 4, 0, NULL, "2013-05-04", 0, NULL, 2061)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1413, 21.8048, 0, NULL, "2013-05-04", 0, NULL, 2062)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1414, 409, 0, NULL, "2013-05-04", 0, NULL, 2063)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1415, 13.0452, 1, "2014-05-04", "2013-05-04", 1, NULL, 2064)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1416, 3, 0, NULL, "2013-06-25", 0, NULL, 2065)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1417, 292, 0, NULL, "2013-06-25", 0, NULL, 2066)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1418, 25.8567, 0, NULL, "2013-06-25", 0, NULL, 2067)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1419, 17.6916, 1, "2014-06-25", "2013-06-25", 1, NULL, 2068)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1420, 260, 0, NULL, "2013-06-17", 0, NULL, 2069)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1421, 2, 0, NULL, "2013-06-17", 0, NULL, 2070)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1422, 23.9151, 1, "2014-06-17", "2013-06-17", 1, NULL, 2071)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1423, 25.6899, 0, NULL, "2013-06-17", 0, NULL, 2072)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1424, 5, 0, NULL, "2013-06-06", 0, NULL, 2073)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1425, 411, 0, NULL, "2013-06-06", 0, NULL, 2074)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1426, 15.4064, 1, "2014-06-06", "2013-06-06", 1, NULL, 2075)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1427, 24.835, 0, NULL, "2013-06-06", 0, NULL, 2076)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1428, 261, 0, NULL, "2013-06-04", 0, NULL, 2077)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1429, 439, 0, NULL, "2013-06-29", 0, NULL, 2078)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1430, 23.0289, 0, NULL, "2013-06-29", 0, NULL, 2079)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1431, 23.9257, 1, "2014-06-29", "2013-06-29", 1, NULL, 2080)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1432, 3, 0, NULL, "2013-06-29", 0, NULL, 2081)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1433, 9.175, 1, "2014-07-28", "2013-07-28", 1, NULL, 2082)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1434, 395, 0, NULL, "2013-07-28", 0, NULL, 2083)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1435, 4, 0, NULL, "2013-07-28", 0, NULL, 2084)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1436, 21.3078, 0, NULL, "2013-07-28", 0, NULL, 2085)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1437, 286, 0, NULL, "2013-07-08", 0, NULL, 2086)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1438, 20.0175, 1, "2014-07-08", "2013-07-08", 1, NULL, 2087)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1439, 3, 0, NULL, "2013-07-08", 0, NULL, 2088)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1440, 21.6728, 0, NULL, "2013-07-08", 0, NULL, 2089)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1441, 5, 0, NULL, "2013-07-19", 0, NULL, 2094)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1442, 22.5445, 0, NULL, "2013-07-19", 0, NULL, 2095)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1443, 484, 0, NULL, "2013-07-19", 0, NULL, 2096)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1444, 15.3165, 1, "2014-07-19", "2013-07-19", 1, NULL, 2097)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1445, 16.8407, 1, "2014-07-07", "2013-07-07", 1, NULL, 2103)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1446, 22.2757, 0, NULL, "2013-07-07", 0, NULL, 2104)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1447, 467, 0, NULL, "2013-07-07", 0, NULL, 2105)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1448, 1, 0, NULL, "2013-07-07", 0, NULL, 2106)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1449, 301, 0, NULL, "2013-08-29", 0, NULL, 2107)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1450, 20.3307, 0, NULL, "2013-08-29", 0, NULL, 2108)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1451, 1, 0, NULL, "2013-08-29", 0, NULL, 2109)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1452, 13.5731, 1, "2014-08-29", "2013-08-29", 1, NULL, 2110)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1453, 466, 0, NULL, "2013-08-13", 0, NULL, 2111)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1454, 12.1983, 1, "2014-08-13", "2013-08-13", 1, NULL, 2112)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1455, 23.4837, 0, NULL, "2013-08-13", 0, NULL, 2113)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1456, 5, 0, NULL, "2013-08-13", 0, NULL, 2114)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1457, 0, 0, NULL, "2013-09-01", 0, NULL, 2115)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1458, 16.5235, 1, "2014-09-01", "2013-09-01", 1, NULL, 2116)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1459, 23.1834, 0, NULL, "2013-09-01", 0, NULL, 2117)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1460, 357, 0, NULL, "2013-09-01", 0, NULL, 2118)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1461, 447, 0, NULL, "2013-09-20", 0, NULL, 2126)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1462, 14.1373, 1, "2014-09-25", "2013-09-25", 1, NULL, 2127)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1463, 29.4456, 0, NULL, "2013-09-25", 0, NULL, 2128)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1464, 337, 0, NULL, "2013-09-25", 0, NULL, 2129)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1465, 3, 0, NULL, "2013-09-25", 0, NULL, 2130)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1466, 26.1414, 0, NULL, "2013-10-24", 0, NULL, 2139)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1467, 4, 0, NULL, "2013-10-24", 0, NULL, 2140)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1468, 20.403, 1, "2014-10-24", "2013-10-24", 1, NULL, 2141)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1469, 283, 0, NULL, "2013-10-24", 0, NULL, 2142)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1470, 12.6598, 1, "2014-10-10", "2013-10-10", 1, NULL, 2143)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1471, 404, 0, NULL, "2013-10-10", 0, NULL, 2144)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1472, 24.7756, 0, NULL, "2013-10-10", 0, NULL, 2145)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1473, 5, 0, NULL, "2013-10-10", 0, NULL, 2146)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1474, 3, 0, NULL, "2013-11-28", 0, NULL, 2153)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1475, 16.1616, 1, "2014-11-28", "2013-11-28", 1, NULL, 2154)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1476, 422, 0, NULL, "2013-11-28", 0, NULL, 2155)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1477, 29.2461, 0, NULL, "2013-11-28", 0, NULL, 2156)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1478, 14.6081, 1, "2014-11-27", "2013-11-27", 1, NULL, 2161)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1479, 494, 0, NULL, "2013-11-27", 0, NULL, 2162)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1480, 416, 0, NULL, "2013-11-19", 0, NULL, 2163)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1481, 29.6424, 0, NULL, "2013-11-19", 0, NULL, 2164)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1482, 18.031, 1, "2014-11-19", "2013-11-19", 1, NULL, 2165)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1483, 5, 0, NULL, "2013-11-19", 0, NULL, 2166)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1484, 5, 0, NULL, "2014-00-25", 0, NULL, 2169)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1485, 24.9662, 0, NULL, "2014-00-25", 0, NULL, 2170)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1486, 350, 0, NULL, "2014-00-25", 0, NULL, 2171)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1487, 4, 0, NULL, "2014-01-15", 0, NULL, 2172)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1488, 452, 0, NULL, "2014-01-15", 0, NULL, 2173)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1489, 17.232, 1, "2015-01-15", "2014-01-15", 1, NULL, 2174)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1490, 22.6344, 0, NULL, "2014-01-15", 0, NULL, 2175)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1491, 26.7519, 0, NULL, "2014-02-01", 0, NULL, 2180)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1492, 21.3203, 1, "2015-02-01", "2014-02-01", 1, NULL, 2181)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1493, -2, 0, NULL, "2014-02-01", 0, NULL, 2182)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1494, 383, 0, NULL, "2014-02-01", 0, NULL, 2183)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1495, 16.0138, 1, "2015-02-02", "2014-02-02", 1, NULL, 2184)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1496, 3, 0, NULL, "2014-02-02", 0, NULL, 2185)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1497, 322, 0, NULL, "2014-02-02", 0, NULL, 2186)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1498, 23.4962, 0, NULL, "2014-02-02", 0, NULL, 2187)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1499, 26.8597, 0, NULL, "2014-02-28", 0, NULL, 2188)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1500, 11.1689, 1, "2015-02-28", "2014-02-28", 1, NULL, 2189)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1501, 394, 0, NULL, "2014-02-28", 0, NULL, 2190)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1502, 1, 0, NULL, "2014-02-28", 0, NULL, 2191)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1503, 308, 0, NULL, "2014-02-13", 0, NULL, 2192)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1504, 353, 0, NULL, "2014-03-10", 0, NULL, 2193)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1505, 3, 0, NULL, "2014-03-10", 0, NULL, 2194)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1506, 8.226, 1, "2015-03-10", "2014-03-10", 1, NULL, 2195)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1507, 25.0294, 0, NULL, "2014-03-10", 0, NULL, 2196)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1508, 380, 0, NULL, "2014-04-12", 0, NULL, 2197)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1509, 5, 0, NULL, "2014-04-12", 0, NULL, 2198)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1510, 18.639, 1, "2015-04-12", "2014-04-12", 1, NULL, 2199)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1511, 23.7685, 0, NULL, "2014-04-12", 0, NULL, 2200)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1512, 12.4695, 1, "2015-04-12", "2014-04-12", 1, NULL, 2205)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1513, 3, 0, NULL, "2014-04-12", 0, NULL, 2206)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1514, 285, 0, NULL, "2014-04-12", 0, NULL, 2207)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1515, 27.8192, 0, NULL, "2014-04-12", 0, NULL, 2208)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1516, 1, 0, NULL, "2014-04-20", 0, NULL, 2209)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1517, 22.4224, 1, "2015-04-20", "2014-04-20", 1, NULL, 2210)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1518, 25.9754, 0, NULL, "2014-04-20", 0, NULL, 2211)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1519, 459, 0, NULL, "2014-04-20", 0, NULL, 2212)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1520, 2, 0, NULL, "2014-04-03", 0, NULL, 2213)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1521, 13.8073, 1, "2015-04-03", "2014-04-03", 1, NULL, 2214)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1522, 425, 0, NULL, "2014-04-03", 0, NULL, 2215)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1523, 20.0071, 0, NULL, "2014-04-03", 0, NULL, 2216)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1524, 18.1783, 1, "2015-05-30", "2014-05-30", 1, NULL, 2221)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1525, 322, 0, NULL, "2014-05-30", 0, NULL, 2222)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1526, 24.2866, 0, NULL, "2014-05-30", 0, NULL, 2223)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1527, 295, 0, NULL, "2014-05-30", 0, NULL, 2224)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1528, 29.9003, 0, NULL, "2014-05-30", 0, NULL, 2225)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1529, 15.9963, 1, "2015-05-30", "2014-05-30", 1, NULL, 2226)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1530, 2, 0, NULL, "2014-05-30", 0, NULL, 2227)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1531, 10.7246, 1, "2015-05-17", "2014-05-17", 1, NULL, 2228)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1532, 20.0936, 0, NULL, "2014-05-17", 0, NULL, 2229)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1533, 332, 0, NULL, "2014-05-17", 0, NULL, 2230)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1534, 4, 0, NULL, "2014-05-17", 0, NULL, 2231)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1535, 488, 0, NULL, "2014-06-05", 0, NULL, 2232)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1536, 5, 0, NULL, "2014-06-05", 0, NULL, 2233)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1537, 13.5502, 1, "2015-06-05", "2014-06-05", 1, NULL, 2234)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1538, 273, 0, NULL, "2014-07-25", 0, NULL, 2239)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1539, 29.3393, 0, NULL, "2014-07-25", 0, NULL, 2240)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1540, 19.8219, 1, "2015-07-25", "2014-07-25", 1, NULL, 2241)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1541, 2, 0, NULL, "2014-07-25", 0, NULL, 2242)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1542, 254, 0, NULL, "2014-07-02", 0, NULL, 2243)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1543, 22.5447, 0, NULL, "2014-07-02", 0, NULL, 2244)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1544, 5, 0, NULL, "2014-07-02", 0, NULL, 2245)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1545, 24.0947, 1, "2015-07-02", "2014-07-02", 1, NULL, 2246)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1546, 278, 0, NULL, "2014-07-17", 0, NULL, 2247)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1547, 20.4473, 0, NULL, "2014-07-17", 0, NULL, 2248)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1548, 11.3389, 1, "2015-07-17", "2014-07-17", 1, NULL, 2249)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1549, 2, 0, NULL, "2014-07-17", 0, NULL, 2250)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1550, 444, 0, NULL, "2014-07-06", 0, NULL, 2251)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1551, 10.9736, 1, "2015-07-06", "2014-07-06", 1, NULL, 2252)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1552, 25.2982, 0, NULL, "2014-07-06", 0, NULL, 2253)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1553, 4, 0, NULL, "2014-07-06", 0, NULL, 2254)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1554, 26.4471, 0, NULL, "2014-08-03", 0, NULL, 2258)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1555, 22.5686, 1, "2015-08-03", "2014-08-03", 1, NULL, 2259)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1556, 431, 0, NULL, "2014-08-03", 0, NULL, 2260)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1557, 1, 0, NULL, "2014-08-03", 0, NULL, 2261)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1558, 22.5473, 1, "2015-08-14", "2014-08-14", 1, NULL, 2262)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1559, 342, 0, NULL, "2014-08-14", 0, NULL, 2263)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1560, 4, 0, NULL, "2014-08-14", 0, NULL, 2264)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1561, 24.6076, 0, NULL, "2014-08-14", 0, NULL, 2265)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1562, 2, 0, NULL, "2014-08-05", 0, NULL, 2270)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1563, 21.5489, 0, NULL, "2014-08-20", 0, NULL, 2271)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1564, 12.2199, 1, "2015-08-20", "2014-08-20", 1, NULL, 2272)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1565, 22.0406, 0, NULL, "2014-08-08", 0, NULL, 2273)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1566, 21.9473, 1, "2015-09-19", "2014-09-19", 1, NULL, 2274)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1567, 432, 0, NULL, "2014-09-19", 0, NULL, 2275)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1568, 2, 0, NULL, "2014-09-19", 0, NULL, 2276)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1569, 20.0312, 0, NULL, "2014-09-19", 0, NULL, 2277)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1570, -1, 0, NULL, "2014-09-26", 0, NULL, 2278)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1571, 372, 0, NULL, "2014-09-26", 0, NULL, 2279)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1572, 23.9702, 0, NULL, "2014-09-26", 0, NULL, 2280)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1573, 23.7576, 1, "2015-09-26", "2014-09-26", 1, NULL, 2281)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1574, 15.8489, 1, "2015-09-07", "2014-09-07", 1, NULL, 2286)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1575, 250, 0, NULL, "2014-09-07", 0, NULL, 2287)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1576, 4, 0, NULL, "2014-09-07", 0, NULL, 2288)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1577, 26.0598, 0, NULL, "2014-09-07", 0, NULL, 2289)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1578, 29.8557, 0, NULL, "2014-10-04", 0, NULL, 2298)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1579, 5.2454, 1, "2015-10-04", "2014-10-04", 1, NULL, 2299)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1580, 413, 0, NULL, "2014-10-04", 0, NULL, 2300)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1581, 2, 0, NULL, "2014-10-04", 0, NULL, 2301)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1582, 16.3528, 1, "2015-11-11", "2014-11-11", 1, NULL, 2304)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1583, 491, 0, NULL, "2014-11-11", 0, NULL, 2305)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1584, 23.1463, 0, NULL, "2014-11-16", 0, NULL, 2306)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1585, 24.9029, 0, NULL, "2015-00-09", 0, NULL, 2307)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1586, 12.9451, 1, "2016-00-09", "2015-00-09", 1, NULL, 2308)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1587, 375, 0, NULL, "2015-00-09", 0, NULL, 2309)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1588, 2, 0, NULL, "2015-00-09", 0, NULL, 2310)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1589, 2, 0, NULL, "2015-00-12", 0, NULL, 2311)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1590, 29.98, 0, NULL, "2015-00-12", 0, NULL, 2312)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1591, 465, 0, NULL, "2015-00-12", 0, NULL, 2313)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1592, 23.2905, 1, "2016-00-12", "2015-00-12", 1, NULL, 2314)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1593, 15.1088, 1, "2016-01-11", "2015-01-11", 1, NULL, 2315)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1594, 26.1497, 0, NULL, "2015-01-11", 0, NULL, 2316)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1595, 1, 0, NULL, "2015-01-11", 0, NULL, 2317)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1596, 353, 0, NULL, "2015-01-11", 0, NULL, 2318)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1597, 28.846, 0, NULL, "2015-01-19", 0, NULL, 2319)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1598, 5, 0, NULL, "2015-01-19", 0, NULL, 2320)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1599, 428, 0, NULL, "2015-01-19", 0, NULL, 2321)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1600, 10.0506, 1, "2016-01-19", "2015-01-19", 1, NULL, 2322)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1601, 16.3093, 1, "2016-01-11", "2015-01-11", 1, NULL, 2323)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1602, 299, 0, NULL, "2015-01-11", 0, NULL, 2324)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1603, 5, 0, NULL, "2015-01-11", 0, NULL, 2325)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1604, 27.4738, 0, NULL, "2015-01-11", 0, NULL, 2326)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1605, 343, 0, NULL, "2015-01-05", 0, NULL, 2331)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1606, 1, 0, NULL, "2015-01-05", 0, NULL, 2332)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1607, 21.3281, 1, "2016-01-05", "2015-01-05", 1, NULL, 2333)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1608, 12.8749, 1, "2016-01-14", "2015-01-14", 1, NULL, 2334)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1609, 20.2836, 0, NULL, "2015-01-14", 0, NULL, 2335)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1610, 3, 0, NULL, "2015-01-30", 0, NULL, 2336)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1611, 17.868, 1, "2016-01-30", "2015-01-30", 1, NULL, 2337)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1612, 480, 0, NULL, "2015-01-30", 0, NULL, 2338)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1613, 26.922, 0, NULL, "2015-01-30", 0, NULL, 2339)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1614, 272, 0, NULL, "2015-02-26", 0, NULL, 2346)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1615, 26.2182, 0, NULL, "2015-02-26", 0, NULL, 2347)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1616, 12.2144, 1, "2016-02-26", "2015-02-26", 1, NULL, 2348)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1617, 2, 0, NULL, "2015-03-27", 0, NULL, 2352)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1618, 29.8522, 0, NULL, "2015-03-27", 0, NULL, 2353)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1619, 19.1425, 1, "2016-03-27", "2015-03-27", 1, NULL, 2354)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1620, 472, 0, NULL, "2015-03-27", 0, NULL, 2355)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1621, 346, 0, NULL, "2015-03-09", 0, NULL, 2360)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1622, 28.1065, 0, NULL, "2015-03-09", 0, NULL, 2361)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1623, 1, 0, NULL, "2015-03-09", 0, NULL, 2362)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1624, 11.3527, 1, "2016-03-09", "2015-03-09", 1, NULL, 2363)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1625, 10.8627, 1, "2016-03-28", "2015-03-28", 1, NULL, 2372)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1626, 335, 0, NULL, "2015-03-28", 0, NULL, 2373)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1627, 5, 0, NULL, "2015-03-28", 0, NULL, 2374)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1628, 29.8987, 0, NULL, "2015-03-28", 0, NULL, 2375)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1629, 2, 0, NULL, "2015-04-03", 0, NULL, 2376)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1630, 24.6232, 0, NULL, "2015-04-03", 0, NULL, 2377)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1631, 348, 0, NULL, "2015-04-03", 0, NULL, 2378)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1632, 342, 0, NULL, "2015-04-21", 0, NULL, 2379)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1633, 29.9696, 0, NULL, "2015-04-21", 0, NULL, 2380)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1634, 1, 0, NULL, "2015-04-21", 0, NULL, 2381)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1635, 353, 0, NULL, "2015-04-10", 0, NULL, 2382)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1636, 23.3458, 0, NULL, "2015-04-10", 0, NULL, 2383)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1637, 2, 0, NULL, "2015-04-10", 0, NULL, 2384)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1638, 13.9709, 1, "2016-04-10", "2015-04-10", 1, NULL, 2385)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1639, 16.174, 1, "2016-04-20", "2015-04-20", 1, NULL, 2386)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1640, 2, 0, NULL, "2015-04-20", 0, NULL, 2387)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1641, 25.5804, 0, NULL, "2015-04-20", 0, NULL, 2388)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1642, 397, 0, NULL, "2015-04-20", 0, NULL, 2389)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1643, 372, 0, NULL, "2015-04-19", 0, NULL, 2390)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1644, 28.1173, 0, NULL, "2015-04-19", 0, NULL, 2391)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1645, 4, 0, NULL, "2015-04-19", 0, NULL, 2392)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1646, 8.141, 1, "2016-04-19", "2015-04-19", 1, NULL, 2393)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1647, 29.1361, 0, NULL, "2015-04-27", 0, NULL, 2394)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1648, 348, 0, NULL, "2015-04-27", 0, NULL, 2395)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1649, 1, 0, NULL, "2015-04-27", 0, NULL, 2396)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1650, 12.7024, 1, "2016-04-27", "2015-04-27", 1, NULL, 2397)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1651, 29.1444, 0, NULL, "2015-05-01", 0, NULL, 2398)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1652, 19.908, 0, NULL, "2015-05-16", 0, NULL, 2399)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1653, 10.237, 1, "2016-05-16", "2015-05-16", 1, NULL, 2400)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1654, 24.4295, 1, "2016-06-22", "2015-06-22", 1, NULL, 2405)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1655, 24.4243, 0, NULL, "2015-06-25", 0, NULL, 2408)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1656, 357, 0, NULL, "2015-06-25", 0, NULL, 2409)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1657, 24.3054, 1, "2016-06-25", "2015-06-25", 1, NULL, 2410)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1658, 3, 0, NULL, "2015-06-25", 0, NULL, 2411)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1659, 24.7546, 1, "2016-07-06", "2015-07-06", 1, NULL, 2420)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1660, 21.2259, 0, NULL, "2015-07-06", 0, NULL, 2421)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1661, 4, 0, NULL, "2015-07-06", 0, NULL, 2422)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1662, 450, 0, NULL, "2015-07-06", 0, NULL, 2423)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1663, 435, 0, NULL, "2015-07-20", 0, NULL, 2426)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1664, 28.9132, 0, NULL, "2015-07-20", 0, NULL, 2427)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1665, 3, 0, NULL, "2015-07-20", 0, NULL, 2428)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1666, 20.4989, 1, "2016-07-20", "2015-07-20", 1, NULL, 2429)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1667, 5, 0, NULL, "2015-07-31", 0, NULL, 2434)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1668, 15.9246, 1, "2016-07-23", "2015-07-23", 1, NULL, 2439)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1669, 325, 0, NULL, "2015-07-23", 0, NULL, 2440)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1670, 25.7837, 0, NULL, "2015-07-23", 0, NULL, 2441)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1671, 3, 0, NULL, "2015-07-23", 0, NULL, 2442)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1672, 2, 0, NULL, "2015-09-21", 0, NULL, 2447)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1673, 29.3458, 0, NULL, "2015-09-21", 0, NULL, 2448)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1674, 262, 0, NULL, "2015-09-21", 0, NULL, 2449)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1675, 10.335, 1, "2016-09-21", "2015-09-21", 1, NULL, 2450)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1676, 26.779, 0, NULL, "2015-09-05", 0, NULL, 2455)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1677, 1, 0, NULL, "2015-09-05", 0, NULL, 2456)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1678, 424, 0, NULL, "2015-09-05", 0, NULL, 2457)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1679, 12.6151, 1, "2016-09-05", "2015-09-05", 1, NULL, 2458)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1680, 0, 0, NULL, "2015-10-06", 0, NULL, 2461)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1681, 27.1783, 0, NULL, "2015-10-06", 0, NULL, 2462)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1682, 359, 0, NULL, "2015-10-06", 0, NULL, 2463)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1683, 14.038, 1, "2016-10-06", "2015-10-06", 1, NULL, 2464)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1684, 26.2909, 0, NULL, "2015-10-25", 0, NULL, 2465)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1685, 485, 0, NULL, "2015-10-25", 0, NULL, 2466)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1686, 1, 0, NULL, "2015-10-25", 0, NULL, 2467)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1687, 14.2078, 1, "2016-10-25", "2015-10-25", 1, NULL, 2468)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1688, 13.0923, 1, "2016-10-15", "2015-10-15", 1, NULL, 2469)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1689, 23.8385, 0, NULL, "2015-10-15", 0, NULL, 2470)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1690, 4, 0, NULL, "2015-10-15", 0, NULL, 2471)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (0, "2016-00-05", 1, 1, 0, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (1, "2016-00-30", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (2, "2016-00-23", 0, 0, 0, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (3, "2016-01-13", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (4, "2016-01-12", 1, 1, 1, 13)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (5, "2016-01-11", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (6, "2016-01-21", 1, 1, 1, 13)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (7, "2016-01-26", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (8, "2016-01-24", 1, 1, 1, 13)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (9, "2016-02-13", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (10, "2016-03-27", 0, 0, 0, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (11, "2016-03-23", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (12, "2016-03-30", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (13, "2016-04-18", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (14, "2016-04-28", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (15, "2016-04-16", 0, 0, 0, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (16, "2016-04-11", 1, 0, 0, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (17, "2016-04-13", 1, 1, 1, 16)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (18, "2016-04-19", 1, 1, 1, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (19, "2016-04-20", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (20, "2016-05-07", 1, 1, 1, 14)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (21, "2016-05-03", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (22, "2016-05-21", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (23, "2016-06-03", 1, 1, 1, 13)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (24, "2016-06-25", 1, 1, 1, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (25, "2016-06-18", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (26, "2016-06-17", 1, 1, 0, 14)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (27, "2016-06-20", 1, 1, 0, 14)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (28, "2016-06-06", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (29, "2016-06-13", 0, 0, 0, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (30, "2016-07-04", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (31, "2016-07-05", 1, 1, 1, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (32, "2016-07-14", 0, 0, 0, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (33, "2016-07-26", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (34, "2016-07-08", 1, 1, 1, 18)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (35, "2016-07-18", 0, 0, 0, 15)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (36, "2016-08-30", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (37, "2016-08-16", 0, 0, 0, 14)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (38, "2016-08-27", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (39, "2016-09-15", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (40, "2016-09-06", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (41, "2016-09-17", 1, 1, 1, 7)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (42, "2016-09-31", 1, 1, 1, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (43, "2016-10-08", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (44, "2016-10-07", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (45, "2016-10-13", 0, 0, 0, 13)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (46, "2016-11-30", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (47, "2016-11-11", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (48, "2017-00-15", 1, 1, 1, 13)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (49, "2017-00-07", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (50, "2017-00-01", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (51, "2017-00-01", 0, 0, 0, 18)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (52, "2017-01-25", 1, 1, 1, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (53, "2017-02-20", 1, 1, 1, 16)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (54, "2017-02-25", 1, 1, 1, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (55, "2017-02-08", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (56, "2017-03-03", 1, 1, 1, 0)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (57, "2017-03-01", 0, 0, 0, 9)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (58, "2017-03-30", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (59, "2017-03-05", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (60, "2017-04-20", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (61, "2017-04-20", 1, 1, 1, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (62, "2017-04-23", 1, 1, 1, 1)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (63, "2017-04-19", 1, 1, 1, 16)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (64, "2017-04-28", 1, 1, 1, 18)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (65, "2017-05-26", 1, 1, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (66, "2017-06-30", 0, 0, 0, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (67, "2017-06-30", 1, 1, 1, 2)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (68, "2017-06-04", 1, 1, 1, 13)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (69, "2017-06-16", 0, 0, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (70, "2017-06-15", 1, 1, 1, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (71, "2017-06-16", 1, 1, 1, 18)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (72, "2017-06-02", 1, 1, 0, 10)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (73, "2017-07-30", 1, 1, 1, 6)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (74, "2017-07-06", 1, 1, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (75, "2017-07-27", 1, 1, 1, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (76, "2017-07-04", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (77, "2017-08-09", 1, 0, 0, 14)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (78, "2017-08-19", 0, 0, 0, 17)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (79, "2017-08-18", 1, 1, 1, 16)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (80, "2017-08-18", 1, 1, 1, 18)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (81, "2017-09-16", 1, 0, 0, 4)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (82, "2017-10-12", 1, 1, 1, 12)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (83, "2017-11-22", 1, 1, 1, 3)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (84, "2017-11-13", 0, 0, 0, 11)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (85, "2017-11-29", 1, 1, 1, 14)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (86, "2017-11-25", 0, 0, 0, 5)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (87, "2017-11-29", 0, 0, 0, 16)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (88, "2017-11-25", 0, 0, 0, 16)

INSERT INTO Order ("ID", "Date", "IsOrdered", "IsDelivered", "IsFinished", "WorkerID")
VALUES (89, "2017-11-08", 1, 1, 1, 2)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (0, 21.355, 0, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (1, 23.8057, 0, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (2, 4, 0, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (3, 16.9701, 1, 0, 1, 7.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (4, 1, 1, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (5, 27.6185, 1, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (6, 408, 1, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (7, 365, 2, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (8, 21.8599, 2, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (9, 27.8494, 2, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (10, 3, 2, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (11, 2, 3, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (12, 21.8044, 3, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (13, 350, 3, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (14, 13.2226, 3, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (15, 310, 4, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (16, 27.5971, 4, 2, 1, 10.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (17, 20.0852, 4, 0, 1, 6.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (18, 5, 4, 1, 1, 78.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (19, 458, 5, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (20, 3, 5, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (21, 14.0552, 5, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (22, 21.9707, 5, 2, 1, 10.35)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (23, 1, 6, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (24, 429, 6, 3, 1, 177.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (25, 21.4996, 6, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (26, 10.9814, 6, 0, 1, 8.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (27, 26.7539, 7, 2, 1, 12.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (28, 3, 8, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (29, 22.6894, 8, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (30, 294, 8, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (31, 23.6585, 8, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (32, 366, 9, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (33, 4, 9, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (34, 21.226, 9, 0, 1, 6.40)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (35, 29.2662, 9, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (36, 27.7841, 10, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (37, 338, 10, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (38, 11.8782, 10, 0, 1, 7.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (39, 3, 10, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (40, 405, 11, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (41, 21.005, 11, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (42, 26.947, 12, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (43, 377, 12, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (44, 19.0747, 12, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (45, 5, 13, 1, 1, 81.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (46, 368, 13, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (47, 13.3163, 13, 0, 1, 7.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (48, 29.4504, 13, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (49, 1, 14, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (50, 23.4095, 14, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (51, 278, 14, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (52, 12.1426, 14, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (53, 4, 15, 1, 1, 62.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (54, 27.3529, 15, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (55, 20.814, 15, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (56, 460, 15, 3, 1, 165.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (57, 10.7908, 16, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (58, 424, 16, 3, 1, 155.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (59, 5, 16, 1, 1, 80.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (60, 29.702, 16, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (61, 13.8174, 17, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (62, 379, 17, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (63, 20.8698, 17, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (64, 4, 17, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (65, 5, 18, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (66, 281, 18, 3, 1, 155.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (67, 18.1924, 18, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (68, 25.3604, 18, 2, 1, 13.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (69, 15.4498, 19, 0, 1, 6.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (70, 23.3843, 19, 2, 1, 13.5)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (71, 462, 19, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (72, 2, 19, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (73, 20.2678, 20, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (74, 317, 20, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (75, 16.7431, 20, 0, 1, 8.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (76, 1, 20, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (77, 1, 21, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (78, 15.5517, 21, 0, 1, 7.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (79, 10.6128, 22, 0, 1, 6.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (80, 304, 22, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (81, 1, 22, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (82, 21.6138, 22, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (83, 477, 23, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (84, 20.2512, 23, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (85, 2, 23, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (86, 287, 24, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (87, 21.4688, 25, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (88, 5, 25, 1, 1, 84.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (89, 473, 25, 3, 1, 202.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (90, 18.0175, 25, 0, 1, 6.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (91, 19.3127, 26, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (92, 25.4716, 26, 2, 1, 10.05)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (93, 2, 27, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (94, 22.6509, 27, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (95, 23.7943, 27, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (96, 319, 27, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (97, 2, 28, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (98, 25.8759, 28, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (99, 11.4895, 29, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (100, 5, 29, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (101, 346, 29, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (102, 29.9763, 29, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (103, 271, 30, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (104, 11.3842, 30, 0, 1, 7.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (105, 23.2705, 30, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (106, 4, 30, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (107, 431, 31, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (108, 21.292, 31, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (109, 1, 31, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (110, 29.7274, 31, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (111, 23.7283, 32, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (112, 16.9918, 32, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (113, 316, 32, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (114, 4, 32, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (115, 330, 33, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (116, 22.1225, 33, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (117, 20.4221, 33, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (118, 1, 33, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (119, 270, 34, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (120, 4, 35, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (121, 21.6267, 35, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (122, 500, 35, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (123, 22.2755, 35, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (124, 5, 36, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (125, 407, 36, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (126, 18.3008, 36, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (127, 28.2812, 36, 2, 1, 12.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (128, 428, 37, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (129, 14.1809, 37, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (130, 19.3545, 38, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (131, 22.7161, 38, 2, 1, 12.30)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (132, 471, 38, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (133, 4, 38, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (134, 13.5253, 39, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (135, 366, 39, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (136, 21.0414, 39, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (137, 5, 39, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (138, 292, 40, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (139, 2, 40, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (140, 21.0298, 40, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (141, 23.8006, 40, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (142, 26.0712, 41, 2, 1, 9.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (143, 368, 41, 3, 1, 180.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (144, 13.9867, 41, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (145, 15.6451, 42, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (146, 5, 42, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (147, 433, 42, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (148, 25.666, 42, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (149, 11.2183, 43, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (150, 297, 43, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (151, 25.1293, 43, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (152, 3, 43, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (153, 331, 44, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (154, 366, 45, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (155, 5, 45, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (156, 22.8273, 45, 0, 1, 7.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (157, 23.713, 45, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (158, 4, 46, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (159, 489, 46, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (160, 17.5003, 47, 0, 1, 6.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (161, 2, 48, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (162, 11.5397, 48, 0, 1, 8.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (163, 24.8441, 48, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (164, 389, 48, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (165, 25.5305, 49, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (166, 3, 49, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (167, 343, 49, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (168, 20.1733, 49, 0, 1, 6.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (169, 23.4246, 50, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (170, 3, 50, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (171, 25.4967, 50, 2, 1, 11.55)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (172, 381, 50, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (173, 23.6909, 51, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (174, 438, 51, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (175, 1, 51, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (176, 320, 52, 3, 1, 167.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (177, 22.6587, 52, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (178, 4, 53, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (179, 20.3894, 53, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (180, 23.5471, 53, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (181, 1, 54, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (182, 290, 54, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (183, 22.1722, 54, 2, 1, 11.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (184, 20.9547, 54, 0, 1, 6.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (185, 276, 55, 3, 1, 185.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (186, 20.8188, 55, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (187, 339, 56, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (188, 22.786, 56, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (189, 23.9587, 57, 2, 1, 10.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (190, 11.53, 57, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (191, 314, 58, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (192, 22.0187, 58, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (193, 21.3806, 58, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (194, 4, 58, 1, 1, 70.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (195, 5, 59, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (196, 324, 59, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (197, 29.1758, 59, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (198, 19.7962, 59, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (199, 16.0304, 60, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (200, 1, 60, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (201, 20.4563, 60, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (202, 456, 60, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (203, 266, 61, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (204, 20.7637, 61, 2, 1, 10.80)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (205, 4, 61, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (206, 490, 62, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (207, 14.1337, 62, 0, 1, 6.60)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (208, 27.5572, 62, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (209, 5, 62, 1, 1, 79.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (210, 18.3415, 63, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (211, 287, 63, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (212, 354, 64, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (213, 27.4869, 64, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (214, 2, 65, 1, 1, 71.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (215, 26.9976, 65, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (216, 299, 66, 3, 1, 182.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (217, 27.2622, 66, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (218, 367, 67, 3, 1, 220.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (219, 23.1623, 67, 0, 1, 8.10)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (220, 289, 68, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (221, 5, 68, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (222, 21.1334, 68, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (223, 17.3214, 68, 0, 1, 8.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (224, 25.1738, 69, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (225, 22.3781, 69, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (226, 396, 69, 3, 1, 195.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (227, 1, 69, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (228, 256, 70, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (229, 1, 70, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (230, 17.3786, 70, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (231, 25.9479, 70, 2, 1, 10.95)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (232, 13.4668, 71, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (233, 5, 71, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (234, 416, 71, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (235, 25.4858, 71, 2, 1, 9.15)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (236, 20.7976, 72, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (237, 2, 72, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (238, 337, 72, 3, 1, 160.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (239, 20.5183, 72, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (240, 3, 73, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (241, 433, 73, 3, 1, 215.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (242, 16.4635, 73, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (243, 1, 74, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (244, 20.2468, 74, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (245, 485, 74, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (246, 28.568, 74, 2, 1, 11.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (247, 426, 75, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (248, 2, 75, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (249, 21.0686, 75, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (250, 19.5631, 75, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (251, 492, 76, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (252, 17.2991, 76, 0, 1, 8.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (253, 25.3219, 76, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (254, 4, 76, 1, 1, 66.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (255, 25.4143, 77, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (256, 4, 77, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (257, 22.7366, 77, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (258, 390, 77, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (259, 4, 78, 1, 1, 65.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (260, 25.6473, 78, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (261, 14.7616, 78, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (262, 22.6413, 79, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (263, 253, 79, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (264, 20.4295, 79, 0, 1, 8.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (265, 3, 79, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (266, 14.3841, 80, 0, 1, 7.20)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (267, 1, 80, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (268, 414, 80, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (269, 24.8045, 80, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (270, 23.3188, 81, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (271, 1, 81, 1, 1, 89.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (272, 20.2858, 81, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (273, 342, 82, 3, 1, 205.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (274, 29.8044, 82, 2, 1, 11.25)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (275, 22.1576, 82, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (276, 1, 82, 1, 1, 88.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (277, 11.8951, 83, 0, 1, 7.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (278, 2, 84, 1, 1, 63.00)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (279, 421, 84, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (280, 23.6702, 84, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (281, 20.0136, 84, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (282, 20.2844, 85, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (283, 11.9574, 85, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (284, 328, 85, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (285, 477, 86, 3, 1, 150.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (286, 4, 86, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (287, 16.1505, 86, 0, 1, 8.70)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (288, 25.3487, 86, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (289, 2, 87, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (290, 23.7981, 87, 2, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (291, 22.3817, 87, 0, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (292, 402, 87, 3, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (293, 418, 88, 3, 1, 212.50)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (294, 4, 88, 1, 0, NULL)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (295, 24.2486, 88, 2, 1, 9.0)

INSERT INTO OrderItem ("ID", "Quantity", "OrderID", "ProductID", "IsDiscounted", "DiscountedUnitPrice")
VALUES (296, 10.8108, 88, 0, 1, 7.40)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (0, 0, NULL, 1, 1, 4)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (1, 1, 4, 0, NULL, 12)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (2, 0, NULL, 1, 2, 14)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (3, 0, NULL, 1, 2, 18)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (4, 1, 2, 0, NULL, 22)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (5, 1, 1, 0, NULL, 23)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (6, 1, 5, 0, NULL, 27)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (7, 1, 2, 0, NULL, 35)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (8, 1, 10, 0, NULL, 42)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (9, 0, NULL, 1, 1, 53)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (10, 0, NULL, 1, 1, 66)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (11, 0, NULL, 1, 2, 75)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (12, 0, NULL, 1, 1, 77)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (13, 1, 3, 0, NULL, 108)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (14, 1, 4, 0, NULL, 114)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (15, 1, 6, 0, NULL, 134)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (16, 1, 5, 0, NULL, 145)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (17, 1, 3, 0, NULL, 158)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (18, 1, 6, 1, 1, 183)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (19, 0, NULL, 1, 2, 185)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (20, 1, 1, 0, NULL, 186)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (21, 0, NULL, 1, 2, 188)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (22, 0, NULL, 1, 1, 223)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (23, 1, 2, 0, NULL, 234)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (24, 0, NULL, 1, 2, 246)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (25, 1, 10, 0, NULL, 249)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (26, 1, 4, 0, NULL, 256)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (27, 0, NULL, 1, 2, 260)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (28, 1, 2, 0, NULL, 262)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (29, 1, 3, 0, NULL, 269)

INSERT INTO OrderItemException ("ID", "IsWrongQuantity", "QuantityError", "IsProductBroken", "QuantityOfBrokenProducts", "OrderItemID")
VALUES (30, 1, 1, 1, 1, 289)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (0, 16.9701, 1, "2017-00-30", "2016-00-30", 1, NULL, 3)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (1, 0, 0, NULL, "2016-00-30", 0, NULL, 4)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (2, 27.6185, 0, NULL, "2016-00-30", 0, NULL, 5)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (3, 408, 0, NULL, "2016-00-30", 0, NULL, 6)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (4, 2, 0, NULL, "2016-01-13", 0, NULL, 11)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (5, 17.8044, 0, NULL, "2016-01-13", 0, NULL, 12)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (6, 350, 0, NULL, "2016-01-13", 0, NULL, 13)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (7, 11.2226, 1, "2017-01-13", "2016-01-13", 1, NULL, 14)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (8, 310, 0, NULL, "2016-01-12", 0, NULL, 15)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (9, 27.5971, 0, NULL, "2016-01-12", 0, NULL, 16)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (10, 20.0852, 1, "2017-01-12", "2016-01-12", 1, NULL, 17)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (11, 3, 0, NULL, "2016-01-12", 0, NULL, 18)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (12, 0, 0, NULL, "2016-01-21", 0, NULL, 23)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (13, 429, 0, NULL, "2016-01-21", 0, NULL, 24)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (14, 21.4996, 0, NULL, "2016-01-21", 0, NULL, 25)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (15, 10.9814, 1, "2017-01-21", "2016-01-21", 1, NULL, 26)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (16, 21.7539, 0, NULL, "2016-01-26", 0, NULL, 27)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (17, 3, 0, NULL, "2016-01-24", 0, NULL, 28)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (18, 22.6894, 1, "2017-01-24", "2016-01-24", 1, NULL, 29)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (19, 294, 0, NULL, "2016-01-24", 0, NULL, 30)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (20, 23.6585, 0, NULL, "2016-01-24", 0, NULL, 31)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (21, 366, 0, NULL, "2016-02-13", 0, NULL, 32)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (22, 4, 0, NULL, "2016-02-13", 0, NULL, 33)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (23, 21.226, 1, "2017-02-13", "2016-02-13", 1, NULL, 34)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (24, 27.2662, 0, NULL, "2016-02-13", 0, NULL, 35)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (25, 405, 0, NULL, "2016-03-23", 0, NULL, 40)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (26, 21.005, 0, NULL, "2016-03-23", 0, NULL, 41)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (27, 16.947, 0, NULL, "2016-03-30", 0, NULL, 42)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (28, 377, 0, NULL, "2016-03-30", 0, NULL, 43)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (29, 19.0747, 1, "2017-03-30", "2016-03-30", 1, NULL, 44)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (30, 5, 0, NULL, "2016-04-18", 0, NULL, 45)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (31, 368, 0, NULL, "2016-04-18", 0, NULL, 46)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (32, 13.3163, 1, "2017-04-18", "2016-04-18", 1, NULL, 47)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (33, 29.4504, 0, NULL, "2016-04-18", 0, NULL, 48)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (34, 1, 0, NULL, "2016-04-28", 0, NULL, 49)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (35, 23.4095, 0, NULL, "2016-04-28", 0, NULL, 50)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (36, 278, 0, NULL, "2016-04-28", 0, NULL, 51)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (37, 12.1426, 1, "2017-04-28", "2016-04-28", 1, NULL, 52)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (38, 13.8174, 1, "2017-04-13", "2016-04-13", 1, NULL, 61)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (39, 379, 0, NULL, "2016-04-13", 0, NULL, 62)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (40, 20.8698, 0, NULL, "2016-04-13", 0, NULL, 63)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (41, 4, 0, NULL, "2016-04-13", 0, NULL, 64)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (42, 5, 0, NULL, "2016-04-19", 0, NULL, 65)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (43, 280, 0, NULL, "2016-04-19", 0, NULL, 66)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (44, 18.1924, 1, "2017-04-19", "2016-04-19", 1, NULL, 67)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (45, 25.3604, 0, NULL, "2016-04-19", 0, NULL, 68)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (46, 15.4498, 1, "2017-04-20", "2016-04-20", 1, NULL, 69)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (47, 23.3843, 0, NULL, "2016-04-20", 0, NULL, 70)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (48, 462, 0, NULL, "2016-04-20", 0, NULL, 71)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (49, 2, 0, NULL, "2016-04-20", 0, NULL, 72)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (50, 20.2678, 0, NULL, "2016-05-07", 0, NULL, 73)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (51, 317, 0, NULL, "2016-05-07", 0, NULL, 74)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (52, 14.7431, 1, "2017-05-07", "2016-05-07", 1, NULL, 75)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (53, 1, 0, NULL, "2016-05-07", 0, NULL, 76)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (54, 10.6128, 1, "2017-05-21", "2016-05-21", 1, NULL, 79)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (55, 304, 0, NULL, "2016-05-21", 0, NULL, 80)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (56, 1, 0, NULL, "2016-05-21", 0, NULL, 81)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (57, 21.6138, 0, NULL, "2016-05-21", 0, NULL, 82)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (58, 477, 0, NULL, "2016-06-03", 0, NULL, 83)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (59, 20.2512, 0, NULL, "2016-06-03", 0, NULL, 84)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (60, 2, 0, NULL, "2016-06-03", 0, NULL, 85)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (61, 287, 0, NULL, "2016-06-25", 0, NULL, 86)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (62, 21.4688, 0, NULL, "2016-06-18", 0, NULL, 87)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (63, 5, 0, NULL, "2016-06-18", 0, NULL, 88)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (64, 473, 0, NULL, "2016-06-18", 0, NULL, 89)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (65, 18.0175, 1, "2017-06-18", "2016-06-18", 1, NULL, 90)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (66, 2, 0, NULL, "2016-06-06", 0, NULL, 97)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (67, 25.8759, 0, NULL, "2016-06-06", 0, NULL, 98)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (68, 271, 0, NULL, "2016-07-04", 0, NULL, 103)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (69, 11.3842, 1, "2017-07-04", "2016-07-04", 1, NULL, 104)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (70, 23.2705, 0, NULL, "2016-07-04", 0, NULL, 105)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (71, 4, 0, NULL, "2016-07-04", 0, NULL, 106)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (72, 431, 0, NULL, "2016-07-05", 0, NULL, 107)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (73, 18.292, 1, "2017-07-05", "2016-07-05", 1, NULL, 108)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (74, 1, 0, NULL, "2016-07-05", 0, NULL, 109)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (75, 29.7274, 0, NULL, "2016-07-05", 0, NULL, 110)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (76, 330, 0, NULL, "2016-07-26", 0, NULL, 115)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (77, 22.1225, 1, "2017-07-26", "2016-07-26", 1, NULL, 116)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (78, 20.4221, 0, NULL, "2016-07-26", 0, NULL, 117)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (79, 1, 0, NULL, "2016-07-26", 0, NULL, 118)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (80, 270, 0, NULL, "2016-07-08", 0, NULL, 119)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (81, 5, 0, NULL, "2016-08-30", 0, NULL, 124)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (82, 407, 0, NULL, "2016-08-30", 0, NULL, 125)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (83, 18.3008, 1, "2017-08-30", "2016-08-30", 1, NULL, 126)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (84, 28.2812, 0, NULL, "2016-08-30", 0, NULL, 127)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (85, 19.3545, 1, "2017-08-27", "2016-08-27", 1, NULL, 130)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (86, 22.7161, 0, NULL, "2016-08-27", 0, NULL, 131)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (87, 471, 0, NULL, "2016-08-27", 0, NULL, 132)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (88, 4, 0, NULL, "2016-08-27", 0, NULL, 133)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (89, 7.5253, 1, "2017-09-15", "2016-09-15", 1, NULL, 134)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (90, 366, 0, NULL, "2016-09-15", 0, NULL, 135)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (91, 21.0414, 0, NULL, "2016-09-15", 0, NULL, 136)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (92, 5, 0, NULL, "2016-09-15", 0, NULL, 137)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (93, 292, 0, NULL, "2016-09-06", 0, NULL, 138)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (94, 2, 0, NULL, "2016-09-06", 0, NULL, 139)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (95, 21.0298, 1, "2017-09-06", "2016-09-06", 1, NULL, 140)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (96, 23.8006, 0, NULL, "2016-09-06", 0, NULL, 141)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (97, 26.0712, 0, NULL, "2016-09-17", 0, NULL, 142)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (98, 368, 0, NULL, "2016-09-17", 0, NULL, 143)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (99, 13.9867, 1, "2017-09-17", "2016-09-17", 1, NULL, 144)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (100, 10.6451, 1, "2017-09-31", "2016-09-31", 1, NULL, 145)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (101, 5, 0, NULL, "2016-09-31", 0, NULL, 146)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (102, 433, 0, NULL, "2016-09-31", 0, NULL, 147)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (103, 25.666, 0, NULL, "2016-09-31", 0, NULL, 148)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (104, 11.2183, 1, "2017-10-08", "2016-10-08", 1, NULL, 149)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (105, 297, 0, NULL, "2016-10-08", 0, NULL, 150)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (106, 25.1293, 0, NULL, "2016-10-08", 0, NULL, 151)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (107, 3, 0, NULL, "2016-10-08", 0, NULL, 152)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (108, 331, 0, NULL, "2016-10-07", 0, NULL, 153)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (109, 1, 0, NULL, "2016-11-30", 0, NULL, 158)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (110, 489, 0, NULL, "2016-11-30", 0, NULL, 159)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (111, 17.5003, 1, "2017-11-11", "2016-11-11", 1, NULL, 160)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (112, 2, 0, NULL, "2017-00-15", 0, NULL, 161)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (113, 11.5397, 1, "2018-00-15", "2017-00-15", 1, NULL, 162)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (114, 24.8441, 0, NULL, "2017-00-15", 0, NULL, 163)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (115, 389, 0, NULL, "2017-00-15", 0, NULL, 164)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (116, 25.5305, 0, NULL, "2017-00-07", 0, NULL, 165)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (117, 3, 0, NULL, "2017-00-07", 0, NULL, 166)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (118, 343, 0, NULL, "2017-00-07", 0, NULL, 167)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (119, 20.1733, 1, "2018-00-07", "2017-00-07", 1, NULL, 168)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (120, 23.4246, 1, "2018-00-01", "2017-00-01", 1, NULL, 169)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (121, 3, 0, NULL, "2017-00-01", 0, NULL, 170)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (122, 25.4967, 0, NULL, "2017-00-01", 0, NULL, 171)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (123, 381, 0, NULL, "2017-00-01", 0, NULL, 172)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (124, 320, 0, NULL, "2017-01-25", 0, NULL, 176)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (125, 22.6587, 0, NULL, "2017-01-25", 0, NULL, 177)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (126, 4, 0, NULL, "2017-02-20", 0, NULL, 178)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (127, 20.3894, 0, NULL, "2017-02-20", 0, NULL, 179)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (128, 23.5471, 1, "2018-02-20", "2017-02-20", 1, NULL, 180)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (129, 1, 0, NULL, "2017-02-25", 0, NULL, 181)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (130, 290, 0, NULL, "2017-02-25", 0, NULL, 182)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (131, 15.1722, 0, NULL, "2017-02-25", 0, NULL, 183)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (132, 20.9547, 1, "2018-02-25", "2017-02-25", 1, NULL, 184)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (133, 274, 0, NULL, "2017-02-08", 0, NULL, 185)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (134, 19.8188, 1, "2018-02-08", "2017-02-08", 1, NULL, 186)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (135, 339, 0, NULL, "2017-03-03", 0, NULL, 187)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (136, 20.786, 1, "2018-03-03", "2017-03-03", 1, NULL, 188)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (137, 314, 0, NULL, "2017-03-30", 0, NULL, 191)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (138, 22.0187, 1, "2018-03-30", "2017-03-30", 1, NULL, 192)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (139, 21.3806, 0, NULL, "2017-03-30", 0, NULL, 193)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (140, 4, 0, NULL, "2017-03-30", 0, NULL, 194)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (141, 5, 0, NULL, "2017-03-05", 0, NULL, 195)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (142, 324, 0, NULL, "2017-03-05", 0, NULL, 196)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (143, 29.1758, 0, NULL, "2017-03-05", 0, NULL, 197)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (144, 19.7962, 1, "2018-03-05", "2017-03-05", 1, NULL, 198)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (145, 16.0304, 1, "2018-04-20", "2017-04-20", 1, NULL, 199)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (146, 1, 0, NULL, "2017-04-20", 0, NULL, 200)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (147, 20.4563, 0, NULL, "2017-04-20", 0, NULL, 201)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (148, 456, 0, NULL, "2017-04-20", 0, NULL, 202)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (149, 266, 0, NULL, "2017-04-20", 0, NULL, 203)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (150, 20.7637, 0, NULL, "2017-04-20", 0, NULL, 204)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (151, 4, 0, NULL, "2017-04-20", 0, NULL, 205)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (152, 490, 0, NULL, "2017-04-23", 0, NULL, 206)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (153, 14.1337, 1, "2018-04-23", "2017-04-23", 1, NULL, 207)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (154, 27.5572, 0, NULL, "2017-04-23", 0, NULL, 208)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (155, 5, 0, NULL, "2017-04-23", 0, NULL, 209)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (156, 18.3415, 1, "2018-04-19", "2017-04-19", 1, NULL, 210)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (157, 287, 0, NULL, "2017-04-19", 0, NULL, 211)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (158, 354, 0, NULL, "2017-04-28", 0, NULL, 212)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (159, 27.4869, 0, NULL, "2017-04-28", 0, NULL, 213)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (160, 367, 0, NULL, "2017-06-30", 0, NULL, 218)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (161, 23.1623, 1, "2018-06-30", "2017-06-30", 1, NULL, 219)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (162, 289, 0, NULL, "2017-06-04", 0, NULL, 220)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (163, 5, 0, NULL, "2017-06-04", 0, NULL, 221)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (164, 21.1334, 0, NULL, "2017-06-04", 0, NULL, 222)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (165, 16.3214, 1, "2018-06-04", "2017-06-04", 1, NULL, 223)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (166, 256, 0, NULL, "2017-06-15", 0, NULL, 228)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (167, 1, 0, NULL, "2017-06-15", 0, NULL, 229)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (168, 17.3786, 1, "2018-06-15", "2017-06-15", 1, NULL, 230)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (169, 25.9479, 0, NULL, "2017-06-15", 0, NULL, 231)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (170, 13.4668, 1, "2018-06-16", "2017-06-16", 1, NULL, 232)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (171, 5, 0, NULL, "2017-06-16", 0, NULL, 233)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (172, 414, 0, NULL, "2017-06-16", 0, NULL, 234)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (173, 25.4858, 0, NULL, "2017-06-16", 0, NULL, 235)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (174, 3, 0, NULL, "2017-07-30", 0, NULL, 240)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (175, 433, 0, NULL, "2017-07-30", 0, NULL, 241)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (176, 16.4635, 1, "2018-07-30", "2017-07-30", 1, NULL, 242)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (177, 426, 0, NULL, "2017-07-27", 0, NULL, 247)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (178, 2, 0, NULL, "2017-07-27", 0, NULL, 248)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (179, 11.0686, 0, NULL, "2017-07-27", 0, NULL, 249)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (180, 19.5631, 1, "2018-07-27", "2017-07-27", 1, NULL, 250)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (181, 20.6413, 0, NULL, "2017-08-18", 0, NULL, 262)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (182, 253, 0, NULL, "2017-08-18", 0, NULL, 263)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (183, 20.4295, 1, "2018-08-18", "2017-08-18", 1, NULL, 264)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (184, 3, 0, NULL, "2017-08-18", 0, NULL, 265)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (185, 14.3841, 1, "2018-08-18", "2017-08-18", 1, NULL, 266)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (186, 1, 0, NULL, "2017-08-18", 0, NULL, 267)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (187, 414, 0, NULL, "2017-08-18", 0, NULL, 268)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (188, 21.8045, 0, NULL, "2017-08-18", 0, NULL, 269)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (189, 342, 0, NULL, "2017-10-12", 0, NULL, 273)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (190, 29.8044, 0, NULL, "2017-10-12", 0, NULL, 274)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (191, 22.1576, 1, "2018-10-12", "2017-10-12", 1, NULL, 275)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (192, 1, 0, NULL, "2017-10-12", 0, NULL, 276)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (193, 11.8951, 1, "2018-11-22", "2017-11-22", 1, NULL, 277)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (194, 20.2844, 0, NULL, "2017-11-29", 0, NULL, 282)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (195, 11.9574, 1, "2018-11-29", "2017-11-29", 1, NULL, 283)

INSERT INTO ProductInStorage ("ID", "Quantity", "HasExpireDate", "ExpireDate", "InStorageDate", "IsExpired", "ExpiredQuantity", "OrderItemID")
VALUES (196, 328, 0, NULL, "2017-11-29", 0, NULL, 284)

