CREATE TABLE Company {
"ID" int PRIMARY KEY,
"PhoneNumber" int,
"Address" varChar(100),
"Name" int,
"AccountNumber" int,
}

CREATE TABLE Product {
"ID" int PRIMARY KEY,
"UnitPrice" decimal(12,2),
"CompanyID" int,
"ForAnimal" bit,
"AnimalCategory" int,
"Name" varChar(50),
}

CREATE TABLE Order {
"ID" int PRIMARY KEY,
"Date" Date,
"IsOrdered" bit,
"IsDelivered" bit,
"IsFinished" bit,
"WorkerID" int,
}

CREATE TABLE OrderItem {
"ID" int PRIMARY KEY,
"Quantity" decimal(12,5),
"OrderID" FOREIGN KEY REFERENCES Order(ID),
"ProductID" FOREIGN KEY REFERENCES Product(ID),
"IsDiscounted" bit,
"DiscountedUnitPrice" decimal(12,2),
}

CREATE TABLE OrderItemException {
"ID" int PRIMARY KEY,
"IsWrongQuantity" bit,
"QuantityError" decimal(12,5),
"IsProductBroken" bit,
"QuantityOfBrokenProducts" decimal(12,5),
"OrderItemID" FOREIGN KEY REFERENCES OrderItem(ID),
}

CREATE TABLE ProductInStorage {
"ID" int PRIMARY KEY,
"Quantity" decimal(12,5),
"HasExpireDate" bit,
"ExpireDate" Date,
"InStorageDate" Date,
"IsExpired" bit,
"ExpiredQuantity" decimal(12,5),
"OrderItemID" FOREIGN KEY REFERENCES OrderItem(ID),
}

